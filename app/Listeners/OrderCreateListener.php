<?php

namespace App\Listeners;

use App\Models\Checkout;
use App\Models\User;
use App\Traits\GraphQLTrait;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Psy\VersionUpdater\Checker;

class OrderCreateListener
{

    use GraphQLTrait;
    public $user;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        try {
            logger('------------------ Listening Order Create ---------------');

            $data = $event->data;
            $this->user = $event->user; // Globally update user

            $isOrderInCheckout = false;
            $line_items = $data->line_items;
            if (count($line_items) > 0) {
                $exist = false;

                $calculatedOrder = $this->beginEdit($data->id);

                if ($calculatedOrder) {

                    foreach ($line_items as $key => $item) {
                        $checkoutData = Checkout::where('main_variant_id', $item->variant_id)->first();

                        if ($checkoutData != null && $checkoutData->use_your_products) {
                            logger('+++++++++++ Found Similar Order ++++++++++');

                            $checkoutLineItems = json_decode($checkoutData->line_items);
                            $discountableItems = [];

                            // Lopping through line items of checkouot
                            foreach ($checkoutLineItems as $index => $line) {

                                $addedItem = $this->addItem($calculatedOrder, $line->id, 1);

                                if ($addedItem['errors'] == false) {
                                    $discountableItems[] =  $addedItem['body']['container']['data']['orderEditAddVariant']['calculatedLineItem']['id'];
                                    $exist = true;
                                } else {
                                    logger('Added Item has Error :: ' . json_encode($addedItem['body']));
                                }
                            }

                            if ($checkoutData->Keep_the_builder) {
                                foreach ($discountableItems as $itemId) {
                                    $discountItem = $this->addDiscount($calculatedOrder, $itemId, 100);
                                }
                            } else {
                                $removedItem = $this->removeLineItem($calculatedOrder, $item->id);
                            }
                        }
                    }
                    if ($exist) {
                        $comitEdit = $this->comitEdit($calculatedOrder);
                    }
                }
            }
        } catch (\Exception $e) {
            logger('============ ERROR:: handle ===========');
            logger($e);
        }
    }

    public function beginEdit($order_id)
    {
        try {
            $query = '
                mutation beginEdit{
                    orderEditBegin(id: "gid://shopify/Order/' . $order_id . '"){
                        calculatedOrder{
                            id
                        }
                    }
                }
            ';

            $result = $this->graphQLRequest($query, []);
            if ($result['errors'] == false) {
                $result = $result['body']->data->orderEditBegin->calculatedOrder->id;
            } else {
                $result = false;
            }

            return $result;
        } catch (\Exception $e) {
            logger('============ ERROR:: graphQLRequest ===========');
            logger(json_encode($e));
        }
    }

    public function addItem($calculatedOrder, $variant_id, $quanity)
    {
        try {
            $query = '
                mutation addVariantToOrder {
                    orderEditAddVariant(id: "' . $calculatedOrder . '", variantId: "gid://shopify/ProductVariant/' . $variant_id . '", quantity: 1) {
                        calculatedLineItem {
                            id
                            title
                            variantTitle
                        }
                        userErrors {
                            field
                            message
                        }
                    }
                }
            ';

            $result = $this->graphQLRequest($query, []);

            return $result;
        } catch (\Exception $e) {
            logger('============ ERROR:: graphQLRequest ===========');
            logger($e);
        }
    }

    public function addDiscount($calculatedOrder, $lineItemId, $percent)
    {
        try {
            $query = '
                mutation addDiscount {
                    orderEditAddLineItemDiscount(id: "' . $calculatedOrder . '", lineItemId: "' . $lineItemId . '", discount: {percentValue: ' . $percent . ', description: "Box Builder"}) {
                        calculatedLineItem {
                            id
                            title
                            variantTitle
                        }
                        userErrors {
                            field
                            message
                        }
                    }
                }
            ';

            $result = $this->graphQLRequest($query, []);

            return $result;
        } catch (\Exception $e) {
            logger('============ ERROR:: graphQLRequest ===========');
            logger($e);
        }
    }

    public function removeLineItem($calculatedOrder, $lineItemId)
    {
        try {
            $query = '
                mutation increaseLineItemQuantity {
                    orderEditSetQuantity(id: "' . $calculatedOrder . '", lineItemId: "gid://shopify/CalculatedLineItem/' . $lineItemId . '", quantity: 0) {
                        calculatedLineItem {
                            id
                            title
                            variantTitle
                        }
                        userErrors {
                            field
                            message
                        }
                    }
                }
            ';

            $result = $this->graphQLRequest($query, []);

            return $result;
        } catch (\Exception $e) {
            logger('============ ERROR:: graphQLRequest ===========');
            logger($e);
        }
    }

    public function comitEdit($calculatedOrder)
    {
        try {
            $query = '
                mutation commitEdit {
                    orderEditCommit(id: "' . $calculatedOrder . '", notifyCustomer: false, staffNote: "' . env('APP_NAME') . ' has edited the order to add each variant separately.") {
                        order {
                            id
                        }
                        userErrors {
                            field
                            message
                        }
                    }
                }
            ';

            $result = $this->graphQLRequest($query, []);

            return $result;
        } catch (\Exception $e) {
            logger('============ ERROR:: graphQLRequest ===========');
            logger($e);
        }
    }

    public function graphQLRequest($query, $parameters = [])
    {
        try {
            logger("====================== QUERY ======================");
            logger($query);
            $user = $this->user;
            return $user->api()->graph($query, $parameters);
        } catch (\Exception $e) {
            logger('============ ERROR:: graphQLRequest ===========');
            logger($e);
        }
    }

    // public function addItem($calculatedOrder, $variant_id, $quanity)
    // {
    //     try {
    //         $query = '
    //             mutation addVariantToOrder {
    //                 orderEditAddVariant(id: "' . $calculatedOrder . '", variantId: "gid://shopify/ProductVariant/' . $variant_id . '", quantity: 1) {
    //                     calculatedLineItem {
    //                         id
    //                         title
    //                         variantTitle
    //                     }
    //                     calculatedOrder {
    //                             id
    //                             addedLineItems(first: ' . $quanity . ') {
    //                             edges {
    //                                 node {
    //                                     id
    //                                     quantity
    //                                 }
    //                             }
    //                         }
    //                     }
    //                     userErrors {
    //                         field
    //                         message
    //                     }
    //                 }
    //             }
    //         ';

    //         $result = $this->graphQLRequest($query, []);

    //         return $result;
    //     } catch (\Exception $e) {
    //         logger('============ ERROR:: graphQLRequest ===========');
    //         logger($e);
    //     }
    // }
}
