<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;

class BundleRequest extends FormRequest
{
    public static $rules = [];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Self::$rules;
        $data = $this::all();

        dd($data);
        switch (Route::currentRouteName()) {
            case 'bundle.store':
            {
                foreach( $data as $key=>$val ){
                    $rules[$key .'.name'] = 'required';
                }
                break;
            }
            default:
                break;
        }
        return $rules;
    }
}
