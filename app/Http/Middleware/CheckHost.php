<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class CheckHost
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $params = $request->all();
        $host = session('shopifyHost');  //Just in case the host was already set

        if(isset($params["host"])){
            session(['shopifyHost' => $params['host']]);  //Setting the host if it is present in the params
        }else{
//            if($host == null){
//                $url = "https://".$shop->name.'/admin/apps/'.env("SHOPIFY_API_KEY");
//                logger("redirect because there was no host..."); // force a redirect for getting the new host param on start
//                return redirect()->away($url);
//            }
        }
        if(@$request->host){
            \Session::put('host', $request->host);
        }
        return $next($request);
    }
}
