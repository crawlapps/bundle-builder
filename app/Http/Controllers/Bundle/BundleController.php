<?php

namespace App\Http\Controllers\Bundle;

use App\Exports\OrdersExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\BundleRequest;
use App\Http\Resources\BundleResource;
use App\Models\Builder;
use App\Models\BuilderDiscount;
use App\Models\BuilderSetting;
use App\Models\BuilderSteps;
use App\Models\Discount;
use App\Models\User;
use App\Traits\PreLoaderTrait;
use App\Traits\ShopifyTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class BundleController extends Controller
{
    use ShopifyTrait;
    use PreLoaderTrait;


    public $user;

    public function index()
    {
        try {
            $user = Auth::user();
            $builders = [];
            if ($user) {
                $builders = Builder::select('id', 'title', 'type', 'is_active', 'shopify_product_id')
                    ->withCount(['BuilderSteps'])
                    ->where('user_id', $user->id)
                    ->paginate(10);
            }
            if (count($builders) > 0) {

                $paginationData = [
                    'total' => $builders->total(),
                    'currentPage' => $builders->currentPage(),
                    'hasPages' => $builders->hasPages(),
                    'perPage' => $builders->perPage(),
                    'previousPageUrl' => $builders->previousPageUrl(),
                    'nextPageUrl' => $builders->nextPageUrl(),
                ];
                $data['pagination'] = $paginationData;
            } else {
                $paginationData = [
                    'total' => 0,
                    'currentPage' => 1,
                    'hasPages' => false,
                    'perPage' => 0,
                    'previousPageUrl' => null,
                    'nextPageUrl' => null,
                ];
                $data['pagination'] = $paginationData;
            }
            $data['builders'] = BundleResource::collection($builders);
            return response()->json(['data' => $data, 'isSuccess' => true], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage(), 'isSuccess' => false], 422);
        }
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {

            // dd($request->data);
            $user = Auth::user();
            $this->user = $user;

            // $user = Auth::user();
            // $data = $request->json()->all();
            $data = json_decode($request->data);
            $builder = false;
            if ($data->settings->customize_urlpath_settings->url != "" || $data->settings->customize_urlpath_settings->url != null) {
                $builder = ($data->id) ? Builder::find($data->id) : new Builder;

                $urlExistSettings = BuilderSetting::whereJsonContains('settings->customize_urlpath_settings->url', $data->settings->customize_urlpath_settings->url)
                    ->where('user_id', $user->id)
                    ->get(['id', 'builder_id']);
                if (count($urlExistSettings) > 0) {
                    // dd($urlExistSettings);
                    foreach ($urlExistSettings as $urlExistSetting) {
                        if ($urlExistSetting->builder_id !== $data->id) {
                            return response()->json([
                                'data' => [
                                    'url' => 'Custom Url alreasy exist.'
                                ],
                                'isSuccess' => true
                            ], 422);
                        }
                    }
                }
            }

            DB::beginTransaction();
            if (count($data->deletedSteps) > 0) {
                BuilderSteps::destroy($data->deletedSteps);
            }
            if (count($data->deletedDiscounts) > 0) {
                BuilderDiscount::destroy($data->deletedDiscounts);
            }
            if (!$builder) {
                $builder = ($data->id) ? Builder::find($data->id) : new Builder;
            }
            $builder->user_id = $user->id;
            $builder->title = $data->title;
            $builder->type = $data->type;
            $builder->is_active = $data->is_active;
            $builder->save();

            //            save settings
            $settings = BuilderSetting::where('builder_id', $builder->id)->first();
            $settings = ($settings) ?: new BuilderSetting();
            $settings->user_id = $user->id;
            $settings->builder_id = $builder->id;
            $settings->is_charge_tax = $data->is_charge_tax;
            $settings->on_complete_action = $data->on_complete_action;
            $settings->is_use_fixed_price = $data->is_use_fixed_price;
            $settings->start_price = (int)$data->start_price;
            $settings->settings = json_encode($data->settings);
            $settings->save();

            //            save Steps
            $steps = $data->steps;
            foreach ($steps as $step) {
                $db_step = ($step->id) ? BuilderSteps::find($step->id) : new BuilderSteps;
                $db_step->user_id = $user->id;
                $db_step->builder_id = $builder->id;
                $db_step->type = $step->type;
                $db_step->title = $step->title;
                $db_step->description = $step->description;
                $db_step->short_title = $step->short_title;
                $db_step->button_text = $step->button_text;
                $db_step->display_order = (int)$step->display_order;
                $db_step->minimum_selection = (int)$step->minimum_selection;
                $db_step->maximum_selection = (int)$step->maximum_selection;
                $db_step->is_allow_more_than_one = $step->is_allow_more_than_one;
                $db_step->is_required_step = $step->is_required_step;
                $db_step->is_show_box_content = $step->is_show_box_content;
                $db_step->is_hide_from_step_progress = $step->is_hide_from_step_progress;
                $db_step->is_show_sortby_dropdown = $step->is_show_sortby_dropdown;
                $db_step->is_show_title_filter = $step->is_show_title_filter;
                $db_step->is_show_collection_filter = $step->is_show_collection_filter;
                $db_step->is_show_tags_filter = $step->is_show_tags_filter;
                $db_step->collections = json_encode($step->collections);

                $db_step->selected_products = json_encode($step->selected_products);

                $db_step->dropdown_tags = $step->dropdown_tags;
                $db_step->collections_filter = json_encode($step->collections_filter);
                // if($step->is_show_collection_filter && count($step->collections_filter) > 0){
                //     $this->preLoadCollectionFilter($step->collections_filter); 
                // }

                $spareArr = [];

                if (count($step->field) > 0) {
                    foreach ($step->field as $field) {
                        if ($field->field_type == "image") {
                            foreach ($request->file() as $key => $value) {
                                foreach ($field->images as $image) {
                                    if ($key == $image->uniqueId && $image->src == '') {
                                        $name = $value->hashName();
                                        $uploadFile = $value->storeAs('/public/uploads', $name);
                                        if ($uploadFile) {
                                            $imageName[] = $name;
                                        }
                                        $image->src = asset('storage/uploads/' . $name);
                                        $image->uniqueId = $name;
                                        $image->file = 'null';
                                    }
                                }
                            }
                        }
                        $spareArr[] = $field;
                    }
                }
                $db_step->field = json_encode($spareArr);

                // $db_step->field = json_encode($step->field);
                $db_step->selected_tab = json_encode($step->selectedTab);

                // if (count($step->collections) > 0 && $step->selectedTab != 1) {
                //     $this->loadCollections($step->collections);
                // }
                // if (count($step->selected_products) > 0 && $step->selectedTab == 1) {
                //     $this->loadProducts($step->selected_products);
                // }

                $db_step->products_per_row     = (int)$step->products_per_row;
                $db_step->show_variant_as     = (int)$step->show_variant_as;
                $db_step->content_data     = $step->content_data;
                $db_step->save();
            }

            //            save discounts
            $discounts = $data->discounts;
            foreach ($discounts as $key => $discount) {
                $db_discount = ($discount->id) ? BuilderDiscount::find($discount->id) : new BuilderDiscount;
                $db_discount->user_id = $user->id;
                $db_discount->builder_id = $builder->id;
                $db_discount->title = $discount->title;
                $db_discount->type = $discount->type;
                $db_discount->amount = $discount->amount;
                $db_discount->apply_discount = $discount->apply_discount;
                // dd($discount);

                isset($discount->price_required) ? $db_discount->price_required = (int)$discount->price_required : '';
                isset($discount->no_of_items_required) ? $db_discount->no_of_items_required = (int)$discount->no_of_items_required : '';
                isset($discount->encourage_discount_price) ? $db_discount->encourage_discount_price = (int)$discount->encourage_discount_price : '';
                $db_discount->is_encourage_discount = (int)$discount->is_encourage_discount;
                $db_discount->apply_to = $discount->apply_to;
                $db_discount->is_remove_other_discount = $discount->is_remove_other_discount;
                $db_discount->order = $discount->order;
                $db_discount->save();
            }

            $this->createProductInShopify($builder, $settings, $user);

            DB::commit();
            return response()->json(['data' => [], 'isSuccess' => true], 200);
        } catch (\Exception $e) {
            throw $e;
            DB::rollBack();
            return response()->json(['data' => $e->getMessage(), 'isSuccess' => false], 422);
        }
    }

    public function edit($id)
    {
        try {
            $builder = Builder::with('BuilderSettings', 'BuilderSteps', 'BuilderDiscounts', 'BuilderSteps.BuilderResources')->where('id', $id)->first();

            $setting = $builder->BuilderSettings[0];
            $steps = $builder->BuilderSteps;
            $discounts = $builder->BuilderDiscounts;

            $b = [
                'id' => $builder->id,
                'shopify_product_id' => $builder->shopify_product_id,
                'title' => $builder->title,
                'type' => $builder->type,
                'is_active' => $builder->is_active,
                'is_charge_tax' => $setting->is_charge_tax,
                'on_complete_action' => $setting->on_complete_action,
                'is_use_fixed_price' => $setting->is_use_fixed_price,
                'start_price' => $setting->start_price,
                'settings' => json_decode($setting->settings, true),
                'steps' => [],
                'discounts' => [],
            ];

            foreach ($steps as $key => $step) {
                $b['steps'][$key] = [
                    'id' => $step['id'],
                    'title' => $step['title'],
                    'description' => $step['description'],
                    'short_title' => $step['short_title'],
                    'button_text' => $step['button_text'],
                    'display_order' => $step['display_order'],
                    'minimum_selection' => $step['minimum_selection'],
                    'maximum_selection' => $step['maximum_selection'],
                    'is_allow_more_than_one' => $step['is_allow_more_than_one'],
                    'is_required_step' => $step['is_required_step'],
                    'is_show_box_content' => $step['is_show_box_content'],
                    'is_hide_from_step_progress' => $step['is_hide_from_step_progress'],
                    'is_show_sortby_dropdown' => $step['is_show_sortby_dropdown'],
                    'is_show_title_filter' => $step['is_show_title_filter'],
                    'is_show_collection_filter' => $step['is_show_collection_filter'],
                    'is_show_tags_filter' => $step['is_show_tags_filter'],
                    'is_show_price' => $step['is_show_price'],
                    'products_per_row' => $step['products_per_row'],
                    'show_variant_as' => $step['show_variant_as'],
                    'content_data' => $step['content_data'],
                    'dropdown_tags' => '',
                    'type' => $step['type'],
                    'collections' => json_decode($step['collections']),
                    'selected_products' => json_decode($step['selected_products']),
                    'collections_filter' => json_decode($step['collections_filter']),
                    'dropdown_tags' => ($step['dropdown_tags']),
                    'field' => json_decode($step['field']),
                    'selectedTab' => ($step['selected_tab']),
                ];
            }

            foreach ($discounts as $key => $discount) {
                $b['discounts'][$key] = [
                    'id' => $discount['id'],
                    'title' => $discount['title'],
                    'type' => $discount['type'],
                    'amount' => $discount['amount'],
                    'apply_discount' => $discount['apply_discount'],
                    'no_of_items_required' => $discount['no_of_items_required'],
                    'apply_to' => $discount['apply_to'],
                    'price_required' => $discount['price_required'],
                    'is_encourage_discount' => $discount['is_encourage_discount'],
                    'encourage_discount_price' => $discount['encourage_discount_price'],
                    'is_remove_other_discount' => $discount['is_remove_other_discount'],
                    'order' => $discount['order']
                ];
            }

            $b = replaceNullWithEmptyString($b);

            $data['builder'] = $b;
            return response()->json(['data' => $data, 'isSuccess' => true], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e, 'isSuccess' => false], 422);
        }
    }

    public function search($keyword)
    {
        try {
            $user = Auth::user();

            $builders = [];
            if ($user) {
                $builders = Builder::select('id', 'title', 'type', 'is_active', 'shopify_product_id')
                    ->withCount(['BuilderSteps'])
                    ->where('user_id', $user->id)
                    ->where('title', 'like', "%$keyword%")
                    ->get();
            }

            $data['builders'] = BundleResource::collection($builders);
            return response()->json(['data' => $data, 'isSuccess' => true], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage(), 'isSuccess' => false], 422);
        }
    }

    public function delete($id)
    {
        try {

            $builder = Builder::where('id', $id)->first();
            if ($builder) {
                $builder->delete();
                return response()->json(['data' => ['message' => 'Builder deleted successfully.'], 'isSuccess' => true], 200);
            }
            return response()->json(['data' => [], 'isSuccess' => false], 422);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage(), 'isSuccess' => false], 422);
        }
    }

    public function copyBuilder($id)
    {
        try {
            $builder = Builder::where('id', $id)->first();
            if ($builder) {

                $data = $this->getFullData($id);
                // return($data);
                if ($data) {
                    unset($data['builder']['id']);
                    $data['builder']['title'] = $data['builder']['title'] . ' - Copy';
                    $data['builder']['settings']['customize_urlpath_settings']['url'] = '';
                    $data['builder']['settings']['seo_settings']['title'] = '';
                    $data['builder']['settings']['seo_settings']['keywords'] = '';
                    $data['builder']['settings']['seo_settings']['description'] = '';

                    return $copyBuilderResult = $this->copyBundle($data);
                    // dd($data);

                    // return response()->json(['data' => ['message' => 'Builder copied successfully.'], 'isSuccess' => true], 200);
                }
            }
            return response()->json(['data' => [], 'isSuccess' => false], 422);
        } catch (\Exception $e) {
            throw $e;

            return response()->json(['data' => $e->getMessage(), 'isSuccess' => false], 422);
        }
    }

    public function exportOrders($id)
    {
        try {
            $user = Auth::user();

            $builder = Builder::where('shopify_product_id', $id)->first();
            if ($builder) {
                $headings = [
                    "FULFILLMENT",
                    "CUSTOMER",
                    "ORDER",
                    "DATE",
                    "BUILDER TYPE"
                ];

                $data = [];
                $orders = $user->api()->rest('GET', '/admin/api/orders.json');
                if ($orders['errors'] == false) {

                    foreach (($orders['body']->container['orders']) as $order) {
                        foreach ($order['line_items'] as $item) {
                            if ($item['product_id'] == $id) {
                                // Adding basic static data
                                $arr = [];
                                $arr['FULFILLMENT'] = $order['fulfillment_status'];
                                $arr['CUSTOMER'] = $order['customer']['first_name'] . ' ' . $order['customer']['last_name'];
                                $arr['ORDER'] = (string)($order['order_number']);
                                $arr['DATE'] = $order['processed_at'];
                                $arr['BUILDER TYPE'] = $builder->type;

                                foreach ($item['properties'] as $property) { // These Properties are actual products in csv
                                    $arr[$property['name']] = $property['value'];
                                    if (!in_array($property['name'], $headings)) { // If property does not exist in heading then it will include it
                                        $headings[] = $property['name'];
                                    }
                                }
                                $data[] = $arr;
                                break;
                            }
                        }
                    }
                    if (count($data) > 0) {

                        // Dynamic Headings adding non existing values
                        $dataPro = [];
                        foreach ($data as $item) { // Iterating through row data
                            $newArr = [];
                            foreach ($headings as $title) { // Iterating through headings to filter titles in row data
                                foreach ($item as $key => $value) {

                                    if (array_key_exists($title, $item)) {
                                        $newArr[$key] = $value;
                                    } else {
                                        $newArr[$title] = "";
                                    }
                                }
                            }
                            $dataPro[] = $newArr;
                        }
                        // Dynamic Headings adding non existing values

                        // Making Column arrays for refactoration.
                        $headingArray = [];
                        foreach ($headings as $heading) {
                            $headingArray[$heading] = array_column($dataPro, $heading);
                        }
                        // Making Column arrays for refactoration.

                        // Sorting the associative array according to headings order.
                        $dataUltraPromax = [];
                        for ($i = 0; $i < count($dataPro); $i++) {
                            $dataLite = [];
                            foreach ($headingArray as $key => $value) {
                                $dataLite[$key] = $value[$i];
                            }
                            $dataUltraPromax[] = $dataLite;
                        }
                        // Sorting the associative array according to headings order.

                        return Excel::download(new OrdersExport(collect($dataUltraPromax), $headings), "$builder->title Recent-orders.csv");
                    }
                }
            }

            return redirect('/');
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage(), 'isSuccess' => false], 422);
        }
    }
}
