<?php

namespace App\Http\Controllers\Customization;

use App\Http\Controllers\Controller;
use App\Models\Customization;
use App\Models\User;
use Illuminate\Http\Request;

class CustomizationController extends Controller
{
    public function index()
    {
        try {
            $user = User::find(1);
            if ($user) {
                $customizationSettings = Customization::where('user_id', $user->id)->first();

                return response()->json([
                    'data' => json_decode($customizationSettings->customization_settings),
                    'isSuccess' => true,
                ], 200);
            } else {
                return response()->json([
                    'data' => [],
                    'isSuccess' => false,
                ], 422);
            }
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage(), 'isSuccess' => false], 422);
        }
    }

    public function edit(Request $request)
    {
        try {
            $user = User::find(1);
            if ($user) {
                $customizationSettings = Customization::where('user_id', $user->id)->first();
                if ($customizationSettings) {

                    $customizationSettings->customization_settings = json_encode($request->data);
                    $customizationSettings->save();

                    return response()->json([
                        'data' => 'Saved',
                        'isSuccess' => true,
                    ], 200);
                }
            }
            return response()->json([
                'data' => [],
                'isSuccess' => false,
            ], 422);
        } catch (\Exception $e) {



            return response()->json(['data' => $e->getMessage(), 'isSuccess' => false], 422);
        }
    }
}
