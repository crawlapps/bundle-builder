<?php

namespace App\Http\Controllers\Proxy;

use App\Http\Controllers\Controller;
use App\Models\Builder;
use App\Models\BuilderSetting;
use App\Models\PreCollection;
use App\Models\PreProduct;
use App\Models\PreVariant;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BuilderController extends Controller
{
    public function index(Request $request, $slug = null)
    {
        try {
            
            $shop = User::find(1);
            $bSettings = null;
            $builder = Builder::with('BuilderSettings', 'BuilderSteps', 'BuilderDiscounts', 'BuilderSteps.BuilderResources')
                ->where('shopify_product_id', $request->b)
                ->first();
            // Fetching builder if slug is given
            if (!$builder) {
                $bSettings = BuilderSetting::whereJsonContains('settings->customize_urlpath_settings->url', $slug)->first();

                if ($bSettings) {
                    $builder = Builder::with('BuilderSettings', 'BuilderSteps', 'BuilderDiscounts', 'BuilderSteps.BuilderResources')
                        ->where('id', $bSettings->builder_id)
                        ->where('user_id', $shop->id)
                        ->first();
                }
            }
            if ($builder) {

                $builder->url = url('/');
                $data = [];
                if (!$bSettings) {
                    $bSettings = BuilderSetting::where('builder_id', $builder->id)->first();
                }
                $bSettings->settings = json_decode($bSettings->settings);
                
                $seo = [
                    'description' => $bSettings->settings->seo_settings->description,
                    'keywords' => $bSettings->settings->seo_settings->keywords,
                    'title' => $bSettings->settings->seo_settings->title,
                ];
                
                $data['shop'] = $request->shop;
                $data['title'] = $builder->title;
                $data['user_id'] = $builder->user_id;
                $data['shopify_product_id'] = $builder->shopify_product_id;
                $data['is_active'] = $builder->is_active;
                $data['builderSettings'] = $bSettings;
                $data['builderSteps'] = $builder->BuilderSteps;
                $data['builderDiscounts'] = $builder->BuilderDiscounts;
                $data['url'] = env('APP_URL');

                if (env('APP_URL') == 'https://bundle-builder.loca.lt' || env('APP_URL') == 'https://bundle-builder.test') {
                    return view('proxy.index', ['data' => $data, 'slug' => $slug, 'seo' => $seo]);
                }

                return response()->view('proxy.index', compact('data', 'slug', 'seo'), 200)->withHeaders([
                    'Content-Type' => 'application/liquid',
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Bundle not found'
                ], 200);
            }
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage() . ' ' . $e->getLine()
            ], 200);
            logger($e);
        }
    }

    public function fetchData(Request $request, $id)
    {
        try {
            // dd($id);
            $builder = Builder::with('BuilderSettings', 'BuilderSteps', 'BuilderDiscounts', 'BuilderSteps.BuilderResources')
                ->where('id', $id)
                ->first();
            if (!$builder) {
                $bSettings = BuilderSetting::whereJsonContains('settings->customize_urlpath_settings->url', $id)->first();
                if ($bSettings) {
                    $builder = Builder::with('BuilderSettings', 'BuilderSteps', 'BuilderDiscounts', 'BuilderSteps.BuilderResources')
                        ->where('id', $bSettings->builder_id)
                        ->first();
                }
            }

            // dd($builder);
            if ($builder) {
                $shop = User::find($builder->user_id);

                $builderSettings = $builder['BuilderSettings'];

                $set = json_decode($builderSettings[0]->settings);
                $builderSettings->settings = $set;
                $builderSettings = $builderSettings[0];
                $builderSettings->settings = json_decode($builderSettings->settings);

                $builderSteps = [];

                foreach ($builder['BuilderSteps'] as $step) {
                    $step->collections = json_decode($step->collections);
                    $step->selected_products = json_decode($step->selected_products);
                    $step->collections_filter = json_decode($step->collections_filter);

                    if ($step->type == "product" || $step->type == 'pre_selected') {
                        if ($step->selected_tab == 1) {
                            $spareProd = [];
                            foreach ($step->selected_products as $product) {

                                // Fetching tags of product
                                $res = $shop->api()->graph('query MyQuery {
                                    product(id: "gid://shopify/Product/' . $product['id'] . '") {
                                      tags
                                    }
                                  }
                                ');
                                if ($res['errors'] == false) {
                                    $product['tags'] = $res['body']->data->product->tags;
                                }

                                // Fetching variants data
                                $spareVar = $product['variants'];
                                $innerproduct = [];
                                foreach ($spareVar as $variant) {
                                    $res = $shop->api()->graph('query MyQuery {
                                        productVariant(id: "gid://shopify/ProductVariant/' . $variant['id'] . '") {
                                          price
                                          inventoryQuantity
                                          inventoryManagement
                                        }
                                      }
                                    ');
                                    $variant['price'] = $res['body']->data->productVariant->price;
                                    $variant['inventoryQuantity'] = $res['body']->data->productVariant->inventoryManagement == "NOT_MANAGED" ? 1000000 : $res['body']->data->productVariant->inventoryQuantity;

                                    $innerproduct[] = $variant;
                                }
                                $product['variants'] = $innerproduct; // Filling spare array with updated products data
                                $spareProd[] = $product;
                            }
                            $step->selected_products = $spareProd; // Setting spare array as new products array
                        } else if ($step->selected_tab == 0) {

                            // fetch data from the collection 
                            $spareCollections = [];
                            foreach ($step->collections as $collecation) {
                                $innerCollection = [
                                    'id' => $collecation['id'],
                                    'handle' => $collecation['handle'],
                                    'title' => $collecation['title'],
                                    'image' => $collecation['image'],
                                ];
                                $spareProd = [];

                                $res = $shop->api()->graph('query MyQuery {
                                    collection(id: "gid://shopify/Collection/' . $collecation['id'] . '") {
                                      id
                                      products(first: 20) { 
                                        edges {
                                          node {
                                            id
                                            title
                                            tags
                                            variants(first: 15) {
                                              edges {
                                                node {
                                                  id
                                                  image {
                                                    src
                                                    originalSrc
                                                  }
                                                  price
                                                  title
                                                  displayName
                                                  inventoryQuantity
                                                  inventoryManagement
                                                }
                                              }
                                            }
                                          }
                                        }
                                      }
                                    }
                                  }
                                ');
                                $res = $res['body']->data->collection;

                                foreach ($res['products']['edges'] as $prod) {

                                    $innerProd = [
                                        'id' => (int)str_replace("gid://shopify/Product/", "", $prod['node']['id']),
                                        'title' => $prod['node']['title'],
                                        'tags' => $prod['node']['tags'],
                                    ];
                                    $spareVar = [];
                                    foreach ($prod['node']['variants']['edges'] as $variant) {

                                        $innerVar = [
                                            'id' => (int)str_replace("gid://shopify/ProductVariant/", "", $variant['node']['id']),
                                            'image' => $variant['node']['image'],
                                            'price' => $variant['node']['price'],
                                            'title' => $variant['node']['title'],
                                            'displayName' => $variant['node']['displayName'],
                                            'inventoryQuantity' => $variant['node']['inventoryManagement'] == "NOT_MANAGED" ? 100000 : $variant['node']['inventoryQuantity'],
                                        ];
                                        $spareVar[] = $innerVar;
                                    }
                                    $innerProd['variants'] = $spareVar;
                                    $spareProd[] = $innerProd;
                                }
                                $innerCollection['products'] = $spareProd;
                                $spareCollections[] = $innerCollection;
                            }
                            $step->collections = $spareCollections;
                        }

                        // Fetch prodcut and variants id from collection if collection filter is enabled in step
                        if ($step->is_show_collection_filter) {
                            $spareCollectionsFilter = [];
                            // dd($step->collections_filter);
                            foreach ($step->collections_filter as $collecation) {
                                $innerCollection = [
                                    'id' => $collecation->id,
                                    'handle' => $collecation->handle,
                                    'title' => $collecation->title,
                                ];
                                $spareProd = [];

                                $res = $shop->api()->graph('query MyQuery {
                                    collection(id: "gid://shopify/Collection/' . $collecation->id . '") {
                                      id
                                      products(first: 20) { 
                                        edges {
                                          node {
                                            id
                                            variants(first: 15) {
                                              edges {
                                                node {
                                                  id
                                                }
                                              }
                                            }
                                          }
                                        }
                                      }
                                    }
                                  }
                                ');
                                $res = $res['body']->data->collection;

                                foreach ($res['products']['edges'] as $prod) {
                                    $spareProd[] = (int)str_replace("gid://shopify/Product/", "", $prod['node']['id']);
                                }
                                $innerCollection['products'] = $spareProd;
                                $spareCollectionsFilter[] = $innerCollection;
                            }
                            $step->collections_filter = $spareCollectionsFilter;
                        }
                    }

                    if ($step->type == 'form') {
                        $step->field = json_decode($step->field);
                    }

                    $builderSteps[] = $step;
                }

                $builderDiscounts = $builder['BuilderDiscounts'];

                $data['shop'] = $request->shop;
                $data['title'] = $builder->title;
                $data['user_id'] = $builder->user_id;
                $data['shopify_product_id'] = $builder->shopify_product_id;
                $data['is_active'] = $builder->is_active;
                $data['builderSettings'] = $builderSettings;
                $data['builderSteps'] = $builderSteps;
                $data['builderDiscounts'] = $builderDiscounts;
                $data['url'] = env('APP_URL');

                return $data;
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Bundle not found'
                ], 200);
            }
        } catch (\Exception $e) {
            throw $e;
            logger($e);
        }
    }

    public function fetchPreLoadedData(Request $request, $id)
    {
        try {
            // dd($id);
            $builder = Builder::with('BuilderSettings', 'BuilderSteps', 'BuilderDiscounts', 'BuilderSteps.BuilderResources')
                ->where('id', $id)
                ->first();
            if (!$builder) {
                $bSettings = BuilderSetting::whereJsonContains('settings->customize_urlpath_settings->url', $id)->first();
                if ($bSettings) {
                    $builder = Builder::with('BuilderSettings', 'BuilderSteps', 'BuilderDiscounts', 'BuilderSteps.BuilderResources')
                        ->where('id', $bSettings->builder_id)
                        ->first();
                }
            }

            // dd($builder);
            if ($builder) {
                $shop = User::find($builder->user_id);

                $builderSettings = $builder['BuilderSettings'];

                $set = json_decode($builderSettings[0]->settings);
                $builderSettings->settings = $set;
                $builderSettings = $builderSettings[0];
                $builderSettings->settings = json_decode($builderSettings->settings);

                $builderSteps = [];

                foreach ($builder['BuilderSteps'] as $step) {
                    $step->collections = json_decode($step->collections);
                    $step->selected_products = json_decode($step->selected_products);
                    $step->collections_filter = json_decode($step->collections_filter);

                    if ($step->type == "product" || $step->type == 'pre_selected') {
                        if ($step->selected_tab == 1) {
                            $spareProd = [];
                            foreach ($step->selected_products as $product) {

                                $preProduct = DB::table('pre_products')->Where('shopify_product_id', $product)->first();
                                // $preProduct = PreProduct::Where('shopify_product_id', $product)->first();

                                if ($preProduct) {
                                    $product['tags'] = json_decode($preProduct->tags);
                                    $product['handle'] = ($preProduct->handle);
                                    $product['title'] = ($preProduct->title);
                                    $product['image'] = json_decode($preProduct->images)[0];

                                    // Variants :
                                    $spareVariants = [];
                                    foreach (json_decode($preProduct->selected_variants) as $var) {
                                        $spareVar = [];
                                        $preVariant = DB::table('pre_variants')->where('shopify_variant_id', $var)->first();
                                        // $preVariant = PreVariant::where('shopify_variant_id', $var)->first();
                                        if ($preVariant) {
                                            $spareVar['id'] = $preVariant->id;
                                            $spareVar['sku'] = $preVariant->sku;
                                            $spareVar['title'] = $preVariant->title;
                                            $spareVar['displayName'] = $preVariant->displayName;
                                            $spareVar['price'] = $preVariant->price;

                                            $res = $shop->api()->graph('
                                                query MyQuery {
                                                    productVariant(id: "gid://shopify/ProductVariant/' . $var . '") {
                                                        inventoryQuantity
                                                        inventoryManagement
                                                    }
                                                }
                                            ');
                                            $spareVar['inventoryQuantity'] = $res['body']->data->productVariant->inventoryManagement == "NOT_MANAGED" ? 1000000 : $res['body']->data->productVariant->inventoryQuantity;

                                            $spareVariants[] = $spareVar;
                                        }
                                    }
                                    $product['variants'] = $spareVariants;

                                    $spareProd[] = $product;
                                }
                            }
                            $step->selected_products = $spareProd; // Setting spare array as new products array
                        } else if ($step->selected_tab == 0) {

                            // fetch data from the collection 
                            $spareCollections = [];
                            foreach ($step->collections as $collecation) {

                                $preCollection = DB::table('pre_collections')->where('shopify_collection_id', $collecation['id'])->first();
                                // $preCollection = PreCollection::where('shopify_collection_id', $collecation['id'])->first();

                                $innerCollection = [
                                    'id' => $preCollection->id,
                                    'handle' => $preCollection->handle,
                                    'title' => $preCollection->title,
                                    'image' => json_decode($preCollection->image),
                                ];
                                $spareProd = [];

                                foreach (json_decode($preCollection->product_ids) as $prod) {
                                    $innerProd = [];

                                    $preProduct = DB::table('pre_products')->Where('shopify_product_id', $prod)->first();
                                    // $preProduct = PreProduct::Where('shopify_product_id', $prod)->first();
                                    if ($preProduct) {
                                        $innerProd['id'] = json_decode($preProduct->shopify_product_id);
                                        $innerProd['tags'] = json_decode($preProduct->tags);
                                        $innerProd['handle'] = ($preProduct->handle);
                                        $innerProd['title'] = ($preProduct->title);
                                        $innerProd['image'] = json_decode($preProduct->images);

                                        $innerProd['variants'] = [];

                                        $preVariants = DB::table('pre_variants')->where('shopify_product_id', $preProduct->shopify_product_id)->get();
                                        // $preVariants = PreVariant::where('shopify_product_id', $preProduct->shopify_product_id)->get();
                                        $spareVariants = [];
                                        if ($preVariants) {
                                            foreach ($preVariants as $preVariant) {
                                                $spareVar = [];

                                                $spareVar['id'] = $preVariant->shopify_variant_id;
                                                $spareVar['sku'] = $preVariant->sku;
                                                $spareVar['title'] = $preVariant->title;
                                                $spareVar['displayName'] = $preVariant->displayName;
                                                $spareVar['price'] = $preVariant->price;

                                                $res = $shop->api()->graph('
                                                    query MyQuery {
                                                        productVariant(id: "gid://shopify/ProductVariant/' . $preVariant->shopify_variant_id . '") {
                                                            inventoryQuantity
                                                            inventoryManagement
                                                        }
                                                    }
                                                ');
                                                $spareVar['inventoryQuantity'] = $res['body']->data->productVariant->inventoryManagement == "NOT_MANAGED" ? 1000000 : $res['body']->data->productVariant->inventoryQuantity;

                                                $spareVariants[] = $spareVar;
                                            }
                                            $innerProd['variants'] = $spareVariants;
                                        }

                                        $spareProd[] = $innerProd;
                                    }
                                }

                                $innerCollection['products'] = $spareProd;
                                $spareCollections[] = $innerCollection;
                            }
                            $step->collections = $spareCollections;
                        }

                        // Fetch prodcut and variants id from collection if collection filter is enabled in step
                        if ($step->is_show_collection_filter) {
                            $spareCollectionsFilter = [];
                            // dd($step->collections_filter);
                            foreach ($step->collections_filter as $collecation) {


                                $preCollection = DB::table('pre_collections')->where('shopify_collection_id', $collecation->id)->first();
                                if ($preCollection) {
                                    $innerCollection = [
                                        'id' => $preCollection->id,
                                        'handle' => $preCollection->handle,
                                        'title' => $preCollection->title,
                                        'image' => json_decode($preCollection->image),
                                        'products' => json_decode($preCollection->product_ids)
                                    ];

                                    $innerCollection['products'] = $spareProd;
                                    $spareCollectionsFilter[] = $innerCollection;
                                }
                            }
                            $step->collections_filter = $spareCollectionsFilter;
                        }
                    }

                    if ($step->type == 'form') {
                        $step->field = json_decode($step->field);
                    }

                    $builderSteps[] = $step;
                }

                $builderDiscounts = $builder['BuilderDiscounts'];

                $data['shop'] = $request->shop;
                $data['title'] = $builder->title;
                $data['user_id'] = $builder->user_id;
                $data['shopify_product_id'] = $builder->shopify_product_id;
                $data['is_active'] = $builder->is_active;
                $data['builderSettings'] = $builderSettings;
                $data['builderSteps'] = $builderSteps;
                $data['builderDiscounts'] = $builderDiscounts;
                $data['url'] = env('APP_URL');

                return $data;
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Bundle not found'
                ], 200);
            }
        } catch (\Exception $e) {
            throw $e;
            logger($e);
        }
    }
}
