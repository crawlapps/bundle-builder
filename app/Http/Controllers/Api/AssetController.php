<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Builder;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class AssetController extends Controller
{

    public function fetchAssets()
    {
        try {
            $user = User::find(1);
            $themeId = $this->getThemeId($user);
            if ($themeId != null) {
                $assets = $this->getAssets($user, $themeId);
                return response()->json([
                    'data' => $assets,
                    'isSuccess' => true
                ], 200);
            } else {
                return response()->json(['data' => 'Somethig went wrong.', 'isSuccess' => false], 422);
            }
        } catch (\Exception $e) {
            throw $e;
            return response()->json(['data' => $e, 'isSuccess' => false], 422);
        }
    }

    // Get active theme id
    public function getThemeId(User $user)
    {
        $themes = $user->api()->rest('GET', '/admin/api/themes.json');
        $themeId = null;
        foreach ($themes['body']->container['themes'] as $theme) {
            if ($theme['role'] == 'main') {
                $themeId = $theme['id'];
            }
        }
        return ($themeId);
    }

    // Fetch assets from themeId
    public function getAssets(User $user, $themeId)
    {
        $assetsRes = $user->api()->rest('GET', '/admin/api/themes/' . $themeId . '/assets.json');
        $assets = [];
        foreach ($assetsRes['body']->container['assets'] as $asset) {
            if ($asset['content_type'] == 'image/gif') {
                $assets[] = $asset;
            }
        }
        return ($assets);
    }

}
