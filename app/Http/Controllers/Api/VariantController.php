<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\MakeVariantRequest;
use App\Models\Builder;
use App\Models\BuilderSetting;
use App\Models\Checkout;
use App\Models\User;
use Illuminate\Http\Request;

class VariantController extends Controller
{
    public function makeVariant(MakeVariantRequest $request)
    {
        try {

            $product_id = (int)$request->product_id;
            $variant_title = $request->variant_title;
            $price = (int)$request->price;

            $lineItems = $request->lineItems;
            // dd($lineItems);

            $builder = Builder::where('shopify_product_id', $product_id)->first();
            if ($builder) {
                $user = User::find($builder->user_id);
                $settings = BuilderSetting::where('builder_id', $builder->id)->first();
                $chargeTax = $settings->is_charge_tax;
                $settings = json_decode($settings->settings);

                if ($user) {
                    for ($i = 0; $i < 1; $i++) {

                        $data = [
                            "variant" => [
                                "option1" => $variant_title . ': ' . uniqid(),
                                "price" => $price,
                                "sku" => uniqid(),
                                "inventory_policy" => "deny",
                                "inventory_management" => null,
                                'requires_shipping' => $settings->shipping_fulfillment_settings->is_shipping_required,
                                'taxable' => $chargeTax,
                            ]
                        ];
                        $variant = $user->api()->rest('POST', '/admin/api/products/' . $product_id . '/variants.json', $data);

                        // dd($variant);
                    }

                    if ($variant['errors'] == false) {

                        $checkout = new Checkout();
                        $checkout->title = $variant_title;
                        $checkout->product_id = $product_id;
                        $checkout->main_variant_id = $variant['body']->container['variant']['id'];
                        $checkout->line_items = json_encode($lineItems);
                        $checkout->use_your_products = $settings->product_settings->is_use_on_new_orders;
                        $checkout->Keep_the_builder = $settings->product_settings->is_keep_builder_on_order;
                        $checkout->save();

                        return response()->json([
                            'isSuccess' => true,
                            'data' => [
                                "variant_id" => $variant['body']->container['variant']['id']
                            ]
                        ], 201);
                    } else {
                        return response()->json([
                            'isSuccess' => false,
                            'data' => 'Something went wrong.'
                        ], 200);
                    }
                } else {
                    return response()->json([
                        'isSuccess' => false,
                        'data' => 'Something went wrong.'
                    ], 200);
                }
            } else {
                return response()->json([
                    'isSuccess' => false,
                    'data' => 'Something went wrong.'
                ], 200);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function test(){
        $user = User::where('id',1)->first();

        $res = $user->api()->rest('GET', '/admin/webhooks.json');

        dd($res);
    }

}
