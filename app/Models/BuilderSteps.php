<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BuilderSteps extends Model
{
    use HasFactory, SoftDeletes;

    protected $casts = [
        'is_allow_more_than_one' => 'boolean',
        'is_required_step' => 'boolean',
        'is_show_box_content' => 'boolean',
        'is_hide_from_step_progress' => 'boolean',
        'is_show_sortby_dropdown' => 'boolean',
        'is_show_title_filter' => 'boolean',
        'is_show_collection_filter' => 'boolean',
        'is_show_tags_filter' => 'boolean',
        'show_variant_as' => 'integer',
        'collections' => 'array',
        'selected_products' => 'array',
        'display_order' => 'integer',
    ];

    public function BuilderResources()
    {
        return $this->hasMany(BuilderResource::class, 'builder_step_id', 'id');
    }
}
