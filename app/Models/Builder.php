<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Builder extends Model
{
    use HasFactory, SoftDeletes;

    public function BuilderSettings()
    {
        return $this->hasMany(BuilderSetting::class, 'builder_id', 'id');
    }

    public function BuilderSteps()
    {
        return $this->hasMany(BuilderSteps::class, 'builder_id', 'id');
    }

    public function BuilderDiscounts()
    {
        return $this->hasMany(BuilderDiscount::class, 'builder_id', 'id');
    }
}
