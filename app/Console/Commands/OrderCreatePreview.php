<?php

namespace App\Console\Commands;

use App\Events\OrderCreated;
use App\Models\User;
use App\Models\Webhook;
use Illuminate\Console\Command;

class OrderCreatePreview extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'preview:orderCreate {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $id = $this->argument('id');
        $user = User::find(1);
        $webhook = Webhook::where('id', $id)->first();

        if ($webhook) {
            OrderCreated::dispatch($user, json_decode($webhook->body));
        }
    }
}
