<?php

namespace App\Jobs;

use App\Models\Customization;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AfterAuthenticationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $shopDomain;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($shopDomain)
    {
        $this->shopDomain = $shopDomain;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            logger('============== START:: AfterAuthenticationJob ===========');
            $user = $this->shopDomain;

            $customization = Customization::where('user_id', $user->id)->first();
            if (!$customization) {
                logger('++++++++++ New Customizations Saved +++++++++++');
                $customization = new Customization();
                $customization->user_id = $user->id;
                $customization->customization_settings = json_encode([
                    "theme" => [
                        "builder" => "full",
                        "is_show_image_in_footer" => true
                    ],
                    "style_settings" => [
                        "primary_button_color" => '#463B36',
                        "secondary_button_color" => '#E06C6C',
                        "product_select_color" => '#81A252',
                        "product_border_color" => '#D9E1E6',
                        "lightbox_background_color" => '#000000',
                        "lightbox_text_color" => '#FFFFFF',
                        "progress_bar_text_color" => '#3A3A3A',
                        "progress_bar_color" => '#D9E1E6',
                        "total_box_background_color" => '#FFFFFF',
                        "total_text_color" => '#CCCCCC',
                        "total_price_text_color" => '#000000',
                        "order_box_details_product_text_color" => '#555555',
                        "adding_to_cart_popup_background_color" => '#FFFFFF',
                        "adding_to_cart_popup_title_color" => '#000000',
                        "adding_to_cart_popup_text_color" => '#000000',
                    ],
                    "display_settings" => [
                        "gif" => "1.gif",
                        "add_button_text" => "Buy",
                        "is_remember_selection" => 1,
                        "is_show_step_progress" => true,
                        "is_show_image_carousels" => 1,
                        "is_show_price_in_button" => 1,
                        "is_enable_image_lightbox" => 1,
                        "is_hide_soldout_products" => false,
                        "is_show_msg_on_addtocart" => true,
                        "is_show_formatted_description" => true
                    ]
                ]);
                $customization->save();
            }
        } catch (\Exception $e) {
            logger('============== ERROR:: AfterAuthenticationJob ===========');
            logger($e);
        }
    }
}
