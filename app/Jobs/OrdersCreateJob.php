<?php

namespace App\Jobs;

use App\Events\OrderCreated;
use App\Models\User;
use App\Models\Webhook;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Osiset\ShopifyApp\Objects\Values\ShopDomain;
use stdClass;

class OrdersCreateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Shop's myshopify domain
     *
     * @var ShopDomain|string
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;

    /**
     * Create a new job instance.
     *
     * @param string   $shopDomain The shop's myshopify domain.
     * @param stdClass $data       The webhook data (JSON decoded).
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Convert domain
        $this->shopDomain = ShopDomain::fromNative($this->shopDomain);
        logger("Order Create .......");
        // logger(json_encode($this->data));

        $user = User::where('name', $this->shopDomain->toNative())->first();
        if ($user) {

            $webhook = new Webhook();
            $webhook->user_id = $user->id;
            $webhook->topic = 'orders/created';
            $webhook->body = json_encode($this->data);
            $webhook->status = 0;
            $conf = $webhook->save();

            if($conf){
                OrderCreated::dispatch($user,$this->data);
            }

        }
        return response()->json(
            [
                "success" => true
            ],
            200
        );
        // Do what you wish with the data
        // Access domain name as $this->shopDomain->toNative()
    }
}
