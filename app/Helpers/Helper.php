<?php

if (!function_exists('replaceNullWithEmptyString')) {
    /**
     * @return mixed
     *
     */
    function replaceNullWithEmptyString($array)
    {
        foreach ($array as $key => $value)
        {
            if(is_array($value)){
                $array[$key] = replaceNullWithEmptyString($value);
            }else{
                if (is_null($value)){
                    $array[$key] = (string) $value;
                }
            }
        }
        return $array;
    }
}
?>
