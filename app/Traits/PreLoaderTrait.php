<?php

namespace App\Traits;

use App\Models\PreCollection;
use App\Models\PreProduct;
use App\Models\PreVariant;
use Illuminate\Support\Facades\Auth;

trait PreLoaderTrait
{

    public function loadCollections($collections)
    {
        foreach ($collections as $collection) {

            $product_ids = [];

            // Getting all products of collection
            $getProducts = $this->user->api()->rest('GET', '/admin/api/collections/' . $collection->id . '/products.json');

            if ($getProducts['errors'] == false) {
                $getProducts = $getProducts['body']->container['products'];

                foreach ($getProducts as $product) {
                    $spareVarIds = [];

                    // Getting all variants of product
                    $getVariants = $this->user->api()->rest('GET', '/admin/api/products/' . $product['id'] . '/variants.json');
                    if ($getVariants['errors'] == false) {
                        $getVariants = $getVariants['body']->container['variants'];
                        foreach ($getVariants as $variant) {
                            $spareVarIds[] = $variant['id'];
                            $this->preLoadVariant(
                                $variant['id'],
                                $product['title'] . ' - ' . $variant['title'],
                                $variant['price'],
                                $variant['sku'],
                                $variant['title'],
                                $product['id']
                            );
                        }
                    }

                    $spareImages = [];
                    foreach ($product['images'] as $img) {
                        $spareImages[] = $img['src'];
                    }

                    $this->preLoadProduct(
                        $product['id'],
                        $spareImages,
                        $product['handle'],
                        $product['title'],
                        $spareVarIds,
                        $product['tags'],
                        $product['body_html']
                    );
                    $product_ids[] = $product['id'];
                }
            }

            $this->preLoadCollection(
                $collection->id,
                $collection->handle,
                $collection->title,
                $collection->image,
                $product_ids
            );
        }
    }

    public function loadProducts($products)
    {
        foreach ($products as $product) {
            $spareVars = [];
            foreach ($product->variants as $var) {
                $spareVars[] = $var->id;
            }
            $this->preLoadProduct($product->id, [$product->image], $product->handle, $product->title, $spareVars, null, null);
        }
    }

    public function loadVariants($variants, $product_title, $product_id)
    {
        foreach ($variants as $variant) {
            $this->preLoadVariant(
                $variant['id'],
                $product_title . ' - ' . $variant['title'],
                $variant['price'],
                $variant['sku'],
                $variant['title'],
                $product_id
            );
        }
    }


    // ------------------------- Preloading Resources --------------------------
    public function preLoadCollection(
        $collection_id,
        $handle,
        $title,
        $image,
        $product_ids
    ) {

        // Making instance for PreCollection
        $preLoadedCollection = PreCollection::where('shopify_collection_id', $collection_id)->first();
        $preLoadedCollection = $preLoadedCollection ? $preLoadedCollection : new PreCollection();

        $preLoadedCollection->shopify_collection_id = $collection_id;
        $preLoadedCollection->handle = $handle;
        $preLoadedCollection->title = $title;
        $preLoadedCollection->image = json_encode($image);
        $preLoadedCollection->product_ids = json_encode($product_ids);
        $preLoadedCollection->user_id = Auth::user()->id;

        $preLoadedCollection->save();
    }

    public function preLoadProduct(
        $product_id,
        $product_images,
        $product_handle,
        $product_title,
        $product_variants = [],
        $tags = null,
        $description = null
    ) {

        $preLoadedProduct = PreProduct::where('shopify_product_id', $product_id)->first();
        $preLoadedProduct = $preLoadedProduct ? $preLoadedProduct : new PreProduct();

        $preLoadedProduct->shopify_product_id = $product_id;
        $preLoadedProduct->images = json_encode($product_images);
        $preLoadedProduct->handle = $product_handle;
        $preLoadedProduct->title = $product_title;
        $preLoadedProduct->selected_variants = json_encode($product_variants);

        if ($tags == null || $description == null) {
            // Fetching tags of product
            $res = $this->user->api()->graph('
                query MyQuery {
                    product(id: "gid://shopify/Product/' . $product_id . '") {
                        tags
                        description
                    }
                }
            ');
            if ($res['errors'] == false) {
                $preLoadedProduct->tags = json_encode($res['body']->data->product->tags);
                $preLoadedProduct->description = ($res['body']->data->product->description);
            }
        } else {
            $preLoadedProduct->tags = json_encode($tags);
            $preLoadedProduct->description = $description;
        }

        $preLoadedProduct->user_id = Auth::user()->id;


        $conf = $preLoadedProduct->save();
        if ($conf) {

            $getVariants = $this->user->api()->rest('GET', '/admin/api/products/' . $product_id . '/variants.json');
            if ($getVariants['errors'] == false) {
                $getVariants = $getVariants['body']->container['variants'];
                $this->loadVariants($getVariants, $product_title, $product_id);
            }
        }
    }

    public function preLoadVariant(
        $variant_id,
        $displayName,
        $price,
        $sku,
        $title,
        $product_id
    ) {

        // Making instance for PreVariant
        $preLoadedVariant = PreVariant::where('shopify_variant_id', $variant_id)->first();
        $preLoadedVariant = $preLoadedVariant ? $preLoadedVariant : new PreVariant();

        $preLoadedVariant->shopify_variant_id = $variant_id;
        $preLoadedVariant->shopify_product_id = $product_id;
        $preLoadedVariant->displayName = $displayName;
        $preLoadedVariant->price = $price;
        $preLoadedVariant->sku = $sku;
        $preLoadedVariant->title = $title;
        $preLoadedVariant->user_id = Auth::user()->id;

        $preLoadedVariant->save();
    }

    public function preLoadCollectionFilter($collections)
    {
        foreach ($collections as $collection) {

            $product_ids = [];

            $count = $this->user->api()->graph('
                query MyQuery {
                    collection(id: "gid://shopify/Collection/' . $collection->id . '") {
                        productsCount
                    }
                }
            ');

            if ($count['errors'] == false) {
                $count = $count['body']->data->collection->productsCount;

                $productIds = $this->user->api()->graph('
                    query MyQuery {
                        collection(id: "gid://shopify/Collection/' . $collection->id . '") {
                            products (first : ' . $count . '){
                                edges {
                                    node {
                                      id
                                    }
                                }
                            }
                        }
                    }
                ');

                if ($productIds['errors'] == false) {
                    $productIds = $productIds['body']->data->collection;
                    foreach ($productIds['products']['edges'] as $prod) {
                        $product_ids[] = (int)str_replace("gid://shopify/Product/", "", $prod['node']['id']);
                    }
                }
            }

            $this->preLoadCollection(
                $collection->id,
                $collection->handle,
                $collection->title,
                $collection->image,
                $product_ids
            );
        }
    }
}
