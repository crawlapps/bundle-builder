<?php

namespace App\Traits;

use App\Models\Builder;
use App\Models\BuilderDiscount;
use App\Models\BuilderSetting;
use App\Models\BuilderSteps;
use App\Models\User;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

trait ShopifyTrait
{
    use GraphQLTrait;
    /**
     *
     */
    public function request($method, $endPoint, $parameter, $userID)
    {
        try {
            $shop = User::where('id', $userID)->first();
            return $shop->api()->rest($method, $endPoint, $parameter);
        } catch (\Exception $e) {
            return false;
        }
    }

    public function graphQLRequest($user_id, $query, $parameters = [])
    {
        try {
            logger("====================== QUERY ======================");
            logger($query);
            $user = User::find($user_id);
            return $this->graph($user, $query, $parameters);
        } catch (\Exception $e) {
            logger('============ ERROR:: graphQLRequest ===========');
            logger(json_encode($e));
        }
    }

    public function getReturnMessage($result, $action)
    {
        logger('=============== ' . $action . ' ==============');
        logger(json_encode($result));
        if (!$result['errors']) {
            $data = $result['body']->container['data'][$action];
            if (empty($data['userErrors'])) {
                return config('const.SHOPIFY_GQL_RETURN_MESSAGE');
            } else {
                return $data['userErrors'][0]['message'];
            }
        } else {
            return (@$result['errors'][0]['message']) ? $result['errors'][0]['message'] : false;
        }
        return false;
    }

    public function createProductInShopify($builder, $settings, $user)
    {
        try {
            logger('============ START:: createProductInShopify ===========');

            $query = $this->shopifyProductQuery($builder);
            $endPoint = ($builder->shopify_product_id) ? '/admin/api/' . env('SHOPIFY_API_VERSION') . '/products/' . $builder->shopify_product_id . '.json' : '/admin/api/' . env('SHOPIFY_API_VERSION') . '/products.json';
            $method = ($builder->shopify_product_id) ? 'PUT' : 'POST';
            $result = $user->api()->rest($method, $endPoint, $query);
            // dd($result);
            // $result = $this->graphQLRequest($user->id, $query);

            // log($result);
            if (!$result['errors']) {
                $product = $result['body']->container['product'];
                $builder->shopify_product_id = $product['id'];
                $builder->save();
                // dd('asd');
                $this->updateSeo($result['body']->container['product']['admin_graphql_api_id'], $settings, $user);
            }
            // $msg = $this->getReturnMessage($result, 'productCreate');
            // if($msg == config('const.SHOPIFY_GQL_RETURN_MESSAGE')){

            // }


            logger('============ END:: createProductInShopify ===========');
        } catch (\Exception $e) {
            logger('============ ERROR:: createProductInShopify ===========');
            logger(json_encode($e));
        }
    }

    public function shopifyProductQuery($builder)
    {
        try {
            logger('============ START:: shopifyProductQuery ===========');

            $parameter = [
                'product' => [
                    'title' => $builder->title,
                    'vendor' => config('const.PRODUCT_VENDOR'),
                    'status' => "active",
                    'productType' => config('const.PRODUCT_CUSTOM_TYPE'),
                    'template_suffix' => config('const.PRODUCT_THEME_TEMPLATE'),
                    'published_scope' => 'web'
                ]
            ];

            // dd($parameter);

            return $parameter;
            // $query = '
            //     mutation {
            //         productCreate(input: {
            //             title: "'. $builder->title .'",
            //             vendor: "'. config('const.PRODUCT_VENDOR') .'",
            //             status: ACTIVE,
            //             productType: "'. config('const.PRODUCT_CUSTOM_TYPE') .'",
            //             templateSuffix: "'. config('const.PRODUCT_THEME_TEMPLATE') .'"
            //         })
            //         {
            //             userErrors {
            //               field
            //               message
            //             }
            //         }
            //     }
            // ';

            // return $query;
        } catch (\Exception $e) {
            logger('============ ERROR:: shopifyProductQuery ===========');
            logger(json_encode($e));
        }
    }

    public function updateSeo($product_id, BuilderSetting $settings, User $user)
    {
        try {
            logger('============ START:: Update Seo ===========');

            $seo_settings = json_decode($settings->settings)->seo_settings;

            $title = $seo_settings->title;
            $description = $seo_settings->description;

            $res = $user->api()->graph('
                mutation MyMutation {
                    productUpdate(input: {id: "' . $product_id . '", seo: {title: "' . $title . '", description: "' . $description . '"}}) {
                        product {
                            id
                        }
                    }
                }
            ');
        } catch (\Exception $e) {
            logger('============ ERROR:: shopifyProductQuery ===========');
            logger(json_encode($e));
        }
    }

    public function getFullData($id)
    {
        try {
            $builder = Builder::with('BuilderSettings', 'BuilderSteps', 'BuilderDiscounts', 'BuilderSteps.BuilderResources')->where('id', $id)->first();

            $setting = $builder->BuilderSettings[0];
            $steps = $builder->BuilderSteps;
            $discounts = $builder->BuilderDiscounts;

            $b = [
                'id' => $builder->id,
                'title' => $builder->title,
                'type' => $builder->type,
                'is_active' => $builder->is_active,
                'is_charge_tax' => $setting->is_charge_tax,
                'on_complete_action' => $setting->on_complete_action,
                'is_use_fixed_price' => $setting->is_use_fixed_price,
                'start_price' => $setting->start_price,
                'settings' => json_decode($setting->settings, true),
                'steps' => [],
                'discounts' => [],
            ];

            foreach ($steps as $key => $step) {
                $b['steps'][$key] = [
                    'id' => $step['id'],
                    'title' => $step['title'],
                    'description' => $step['description'],
                    'short_title' => $step['short_title'],
                    'button_text' => $step['button_text'],
                    'display_order' => $step['display_order'],
                    'minimum_selection' => $step['minimum_selection'],
                    'maximum_selection' => $step['maximum_selection'],
                    'is_allow_more_than_one' => $step['is_allow_more_than_one'],
                    'is_required_step' => $step['is_required_step'],
                    'is_show_box_content' => $step['is_show_box_content'],
                    'is_hide_from_step_progress' => $step['is_hide_from_step_progress'],
                    'is_show_sortby_dropdown' => $step['is_show_sortby_dropdown'],
                    'is_show_title_filter' => $step['is_show_title_filter'],
                    'is_show_collection_filter' => $step['is_show_collection_filter'],
                    'is_show_tags_filter' => $step['is_show_tags_filter'],
                    'is_show_price' => $step['is_show_price'],
                    'products_per_row' => $step['products_per_row'],
                    'show_variant_as' => $step['show_variant_as'],
                    'content_data' => $step['content_data'],
                    'dropdown_tags' => '',
                    'type' => $step['type'],
                    'collections' => json_decode($step['collections']),
                    'selected_products' => json_decode($step['selected_products']),
                    'collections_filter' => json_decode($step['collections_filter']),
                    'dropdown_tags' => ($step['dropdown_tags']),
                    'field' => json_decode($step['field']),
                    'selectedTab' => ($step['selected_tab']),
                ];
            }

            foreach ($discounts as $key => $discount) {
                $b['discounts'][$key] = [
                    'id' => $discount['id'],
                    'title' => $discount['title'],
                    'type' => $discount['type'],
                    'amount' => $discount['amount'],
                    'apply_discount' => $discount['apply_discount'],
                    'no_of_items_required' => $discount['no_of_items_required'],
                    'apply_to' => $discount['apply_to'],
                    'price_required' => $discount['price_required'],
                    'is_encourage_discount' => $discount['is_encourage_discount'],
                    'encourage_discount_price' => $discount['encourage_discount_price'],
                    'is_remove_other_discount' => $discount['is_remove_other_discount'],
                    'order' => $discount['order']
                ];
            }

            $b = replaceNullWithEmptyString($b);

            $data['builder'] = $b;
            return $data;
        } catch (\Exception $e) {
            return response()->json(['data' => $e, 'isSuccess' => false], 422);
        }
    }

    public function copyBundle($data)
    {
        try {

            $user = User::find(1);

            // $user = Auth::user();
            // $data = $request->json()->all();

            $data = $data['builder'];
            // dd($data);
            DB::beginTransaction();

            $builder = new Builder();

            $builder->user_id = $user->id;
            $builder->title = $data['title'];
            $builder->type = $data['type'];
            $builder->is_active = $data['is_active'];
            $builder->save();

            //            save settings

            $settings = new BuilderSetting();
            $settings->user_id = $user->id;
            $settings->builder_id = $builder->id;
            $settings->is_charge_tax = $data['is_charge_tax'];
            $settings->on_complete_action = $data['on_complete_action'];
            $settings->is_use_fixed_price = $data['is_use_fixed_price'];
            $settings->start_price = (int)$data['start_price'];
            $settings->settings = json_encode($data['settings']);
            $settings->save();

            //            save Steps
            $steps = $data['steps'];
            foreach ($steps as $step) {
                $db_step = new BuilderSteps;
                $db_step->user_id = $user->id;
                $db_step->builder_id = $builder->id;
                $db_step->type = $step['type'];
                $db_step->title = $step['title'];
                $db_step->description = $step['description'];
                $db_step->short_title = $step['short_title'];
                $db_step->button_text = $step['button_text'];
                $db_step->display_order = (int)$step['display_order'];
                $db_step->minimum_selection = (int)$step['minimum_selection'];
                $db_step->maximum_selection = (int)$step['maximum_selection'];
                $db_step->is_allow_more_than_one = $step['is_allow_more_than_one'];
                $db_step->is_required_step = $step['is_required_step'];
                $db_step->is_show_box_content = $step['is_show_box_content'];
                $db_step->is_hide_from_step_progress = $step['is_hide_from_step_progress'];
                $db_step->is_show_sortby_dropdown = $step['is_show_sortby_dropdown'];
                $db_step->is_show_title_filter = $step['is_show_title_filter'];
                $db_step->is_show_collection_filter = $step['is_show_collection_filter'];
                $db_step->is_show_tags_filter = $step['is_show_tags_filter'];
                $db_step->collections = json_encode($step['collections']);
                $db_step->selected_products = json_encode($step['selected_products']);
                $db_step->dropdown_tags = $step['dropdown_tags'];
                $db_step->collections_filter = json_encode($step['collections_filter']);

                $spareArr = [];

                if (count($step['field']) > 0) {
                    foreach ($step['field'] as $field) {

                        if ($field->field_type == "image") {

                            foreach ($field->images as $image) {

                                $name = uniqid() . '.' . File::extension('storage/uploads/' . $image->uniqueId);
                                if (File::exists('storage/uploads/' . $image->uniqueId)) {
                                    $copied = File::copy('storage/uploads/' . $image->uniqueId, 'storage/uploads/' . $name);
                                    if ($copied) {
                                        $image->src = asset('storage/uploads/' . $name);
                                        $image->uniqueId = $name;
                                    }
                                }
                            }
                        }

                        $spareArr[] = $field;
                    }
                }
                $db_step->field = json_encode($spareArr);

                // $db_step->field = json_encode($step['field']);
                $db_step->selected_tab = json_encode($step['selectedTab']);

                $db_step->products_per_row = (int)$step['products_per_row'];
                $db_step->show_variant_as = (int)$step['show_variant_as'];
                $db_step->content_data = $step['content_data'];
                $db_step->save();
            }

            //            save discounts
            $discounts = $data['discounts'];
            foreach ($discounts as $key => $discount) {
                $db_discount = new BuilderDiscount;
                $db_discount->user_id = $user->id;
                $db_discount->builder_id = $builder->id;
                $db_discount->title = $discount['title'];
                $db_discount->type = $discount['type'];
                $db_discount->amount = $discount['amount'];
                $db_discount->apply_discount = $discount['apply_discount'];
                $db_discount->price_required = (int)$discount['price_required'];
                $db_discount->no_of_items_required = $discount['no_of_items_required'];
                $db_discount->apply_to = $discount['apply_to'];
                $db_discount->is_encourage_discount = $discount['is_encourage_discount'];
                $db_discount->encourage_discount_price = (int)$discount['encourage_discount_price'];
                $db_discount->is_remove_other_discount = $discount['is_remove_other_discount'];
                $db_discount->order = $discount['order'];
                $db_discount->save();
            }

            $this->createProductInShopify($builder, $settings, $user);

            DB::commit();
            return response()->json(['data' => [], 'isSuccess' => true], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
            return response()->json(['data' => $e->getMessage(), 'isSuccess' => false], 422);
        }
    }
}
