<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBuilderDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('builder_discounts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('builder_id');

            $table->string('title')->nullable();
            $table->string('type')->nullable();
            $table->string('amount')->nullable();
            $table->string('apply_discount')->nullable();
            $table->integer('no_of_items_required')->nullable();
            $table->boolean('apply_to')->nullable();
            $table->boolean('is_encourage_discount')->nullable();
            $table->boolean('is_remove_other_discount')->nullable();
            $table->integer('order')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('builder_discounts');
    }
}
