<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('builder_discounts', function (Blueprint $table) {
            $table->integer('price_required')->after('apply_discount')->nullable();
            $table->integer('encourage_discount_price')->after('is_encourage_discount')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('builder_discounts', function (Blueprint $table) {
            $table->dropColumn('price_required');
            $table->dropColumn('encourage_discount_price');
        });
    }
}
