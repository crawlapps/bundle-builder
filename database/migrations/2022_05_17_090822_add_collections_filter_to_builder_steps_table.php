<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCollectionsFilterToBuilderStepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('builder_steps', function (Blueprint $table) {
            $table->json('collections_filter')->nullable()->after('selected_products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('builder_steps', function (Blueprint $table) {
            $table->dropColumn('collections_filter');
        });
    }
}
