<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTagsToBuilderStepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('builder_steps', function (Blueprint $table) {
            $table->text('dropdown_tags')->nullable()->after('collections_filter');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('builder_steps', function (Blueprint $table) {
            $table->dropColumn('dropdown_tags');
        });
    }
}
