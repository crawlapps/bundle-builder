<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_variants', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('shopify_variant_id')->nullable()->index();
            $table->bigInteger('shopify_product_id')->nullable()->index();
            $table->unsignedBigInteger('user_id')->nullable();
            
            $table->string('displayName')->nullable();
            $table->string('price')->nullable();
            $table->string('sku')->nullable();
            $table->string('title')->nullable();

            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_variants');
    }
}
