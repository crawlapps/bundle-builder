<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreCollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_collections', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('shopify_collection_id')->nullable()->index();
            $table->string('handle')->nullable();
            $table->string('title')->nullable();
            $table->json('image')->nullable();
            $table->json('product_ids')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();

            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_collections');
    }
}
