<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSelectedTabToBuilderStepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('builder_steps', function (Blueprint $table) {
            $table->integer('selected_tab')->nullable()->after('selected_products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('builder_steps', function (Blueprint $table) {
            $table->dropColumn('selected_tab');
        });
    }
}
