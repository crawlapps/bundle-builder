<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBuilderStepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('builder_steps', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('builder_id');

            $table->string('type')->nullable('Type of the step');
            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->string('short_title')->nullable();
            $table->string('button_text')->nullable();

            $table->integer('display_order')->nullable();
            $table->integer('minimum_selection')->nullable();
            $table->integer('maximum_selection')->nullable();

            $table->boolean('is_allow_more_than_one')->default(0);
            $table->boolean('is_required_step')->default(0);
            $table->boolean('is_show_box_content')->default(0);
            $table->boolean('is_hide_from_step_progress')->default(0);
            $table->boolean('is_show_sortby_dropdown')->default(0);
            $table->boolean('is_show_title_filter')->default(0);
            $table->boolean('is_show_collection_filter')->default(0);
            $table->boolean('is_show_tags_filter')->default(0);

            $table->json('collections')->nullable();
            $table->json('selected_products')->nullable();

            $table->integer('products_per_row')->nullable();
            $table->integer('show_variant_as')->default(0)->comment('Show each variant separately or Show a variant select box beneath product');

            $table->longText('content_data')->nullable()->comment('This field is for Add a Content/HTML Step ');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->on('users')->references('id')->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->foreign('builder_id')->on('builders')->references('id')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('builder_steps');
    }
}
