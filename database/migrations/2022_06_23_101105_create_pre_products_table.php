<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_products', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('shopify_product_id')->nullable()->index();
            $table->string('handle')->nullable();
            $table->string('title')->nullable();
            $table->json('images')->nullable();
            $table->longText('description')->nullable();
            $table->json('tags')->nullable();
            $table->json('selected_variants')->nullable();

            $table->unsignedBigInteger('user_id')->nullable();

            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_products');
    }
}
