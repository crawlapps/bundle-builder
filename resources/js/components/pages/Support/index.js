import { Card, Heading, Page, Subheading, TextContainer } from '@shopify/polaris'
import React from 'react'

function Support() {
    return (
        <div>
            <Page>
                <Card>
                    <div className="Polaris-Card__Header custom-card-heading">
                        <h2 className="Polaris-Heading">Frequently Asked Questions</h2>
                    </div>

                    <div className='supportContainer'>

                        <div className='supportItem'>
                            <TextContainer>
                                <Heading>One or more products are not showing up on my builder</Heading>
                                <p>
                                    Products with no collections will not show as our app has to locate them using collections. If you have products like this, simply create a collection and attach all products. The collection you create can be named anything you like.
                                </p>
                            </TextContainer>
                        </div>

                        <div className='supportItem'>
                            <TextContainer>
                                <Heading>How can I contact you?</Heading>
                                <p>
                                    Our live chat will be available at the bottom-right of the screen within office hours. Please send us an email to support@boxup.io if that is not available
                                </p>
                            </TextContainer>
                        </div>

                        <div className='supportItem'>
                            <TextContainer>
                                <Heading>Can I see a demo?</Heading>
                                <p>
                                    Of course! You can see both styles by visiting our demo store and using the links in the main menu.
                                </p>
                            </TextContainer>
                        </div>

                        <div className='supportItem'>
                            <TextContainer>
                                <Heading>How do I set the price the customer pays for each build?</Heading>
                                <p>
                                    The price comes from all the products/variants that the customer ends up selecting throughout each step. As they add more products, the price will increase based on the prices you set for each variant.
                                </p>
                            </TextContainer>
                        </div>

                        <div className='supportItem'>
                            <TextContainer>
                                <Heading>How do I get customers to see my builder?</Heading>
                                <p>
                                    Once you have set up your builder. You will be given a link (URL) to preview it. You can use this link wherever you would like. For example, you can add a menu item in the Online Store {'>'} Navigation page.
                                </p>
                            </TextContainer>
                        </div>
                    </div>





                </Card>

            </Page>
        </div>
    )
}

export default Support