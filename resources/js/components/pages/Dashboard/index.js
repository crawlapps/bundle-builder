import React, { useCallback, useState, useEffect } from "react";
import { Page, Card, Pagination, DataTable, Link, Button, TopBar, TextField, Tag, Popover, ActionList, Icon } from '@shopify/polaris';
import { useNavigate } from "react-router-dom";

import { ArchiveMajor, DeleteMinor, DuplicateMinor, ExportMinor, SearchMinor } from "@shopify/polaris-icons";

import card1 from '../../snippets/card1.png';
import card2 from '../../snippets/card2.png';
import instance from "../../shopify/instance";
import { toastIsError, toastMessage, toastShow } from "../../AppbridgeActions/ToastActions";
import { showModal } from "../../AppbridgeActions/ModalActions";


function Dashboard() {
    const navigate = useNavigate();
    const [rows, setRows] = useState([]);
    const [isLoading, setIsLoading] = useState([]);
    const [pagination, setPagnation] = useState({
        total: 0,
        currentPage: 1,
        hasPages: false,
        perPage: 0,
        previousPageUrl: null,
        nextPageUrl: null,
    });
    const [searchText, setSearchText] = useState('');

    useEffect(() => {
        getData('/bundle');
    }, []);

    function getData(url) {
        setIsLoading(true)
        instance({
            url: url,
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        })
            .then(function (res) {
                if (res.data.isSuccess) {
                    setRows(res.data.data.builders);
                    setPagnation(res.data.data.pagination);
                }
            })
            .catch(function (err) {
                console.log(err);
                toastMessage('Something went wrong.')
                toastIsError(true)
                toastShow();
            })
            .finally(function () {
                setIsLoading(false)
            })
    }

    function searchHandle() {
        setIsLoading(true)
        if (searchText.length < 1 || searchText == '') { // If search box is emty then symply call get api
            getData('/bundle');
        } else { // It will perform search api
            instance({
                method: 'GET',
                url: `bundle/search/${searchText}`
            })
                .then((res) => {
                    console.log(res);
                    if (res.data.isSuccess) {
                        setRows(res.data.data.builders);
                        setPagnation({
                            total: 0,
                            currentPage: 1,
                            hasPages: false,
                            perPage: 0,
                            previousPageUrl: null,
                            nextPageUrl: null,
                        });
                    }
                })
                .catch((err) => {
                    console.log(err);
                    toastMessage('Something went wrong.')
                    toastIsError(true)
                    toastShow();
                })
                .finally(() => {
                    setIsLoading(false)
                })
        }
    }

    function MoreActionsComponent({ dataId, index }) {
        const [active, setActive] = useState(false);

        const toggleActive = useCallback(() => setActive((active) => !active), []);
        const activator = (
            <Button onClick={toggleActive} disclosure>
                More actions
            </Button>
        );

        const handleCopyBuilder = useCallback(
            () => {
                setIsLoading(true)
                instance({
                    url: `bundle/copy/${dataId}`,
                    method: 'get'
                })
                    .then((res) => {
                        console.log(res);
                        toastMessage('Bundle copied successfully.')
                        toastIsError(false)
                        toastShow();
                        getData('/bundle');
                    })
                    .catch((err) => {
                        console.log(err);
                    })
                    .finally(() => {
                        setIsLoading(false)
                    })
            },
            [],
        );
        const handleViewProduct = useCallback(
            () => {
                window.open(`http://${window.shopOrigin}/admin/products/${rows[index].shopify_product_id}`, '_blank');
            },
            [],
        );
        const handleExportRecent = useCallback(
            () => {
                console.log('handleExportRecent :: ', dataId)
                window.location.href = (`/bundle/export/orders/${rows[index].shopify_product_id}`)
            },
            [],
        );
        const handleDeleteBuilder = useCallback(
            () => {

                showModal(
                    'Warning',
                    'Are you sure you want to delete this builder ? You won\'t be able to retrive it again!',
                    'Delete',
                    deleteBuilder,
                    true
                );

            },
            [],
        );

        const deleteBuilder = () => {
            setIsLoading(true)

            instance({
                url: `bundle/delete/${dataId}`,
                method: 'delete'
            })
                .then((res) => {
                    console.log(res);
                    if (res.data.isSuccess) {
                        let spareRows = JSON.parse(JSON.stringify(rows))
                        spareRows.splice(index, 1);
                        setRows(spareRows);
                        toastMessage('Builder deleted succesfully.')
                        toastShow();
                    }
                })
                .catch((err) => {
                    console.log(err);
                })
                .finally(() => {
                    setIsLoading(false)
                    toastMessage('Something went wrong.')
                    toastIsError(true)
                    toastShow();
                })
        }

        return (
            <div style={{ width: '15rem' }} className="m-auto">
                <Popover
                    active={active}
                    activator={activator}
                    autofocusTarget="first-node"
                    onClose={toggleActive}

                >
                    <ActionList
                        actionRole="menuitem"
                        items={[
                            {
                                content: <div className="d-flex justify-content"> <Icon source={DuplicateMinor} color="base" /> Copy Builder </div>,
                                onAction: handleCopyBuilder,
                            },
                            {
                                content: <div className="d-flex justify-content"> <Icon source={ArchiveMajor} color="base" /> View Product </div>,
                                onAction: handleViewProduct,
                            },
                            {
                                content: <div className="d-flex justify-content"> <Icon source={ExportMinor} color="base" /> Export Recent </div>,
                                onAction: handleExportRecent,
                            },
                            {
                                content: <div className="d-flex justify-content deleteAction" > <Icon source={DeleteMinor} color="base" /> Delete builder </div>,
                                onAction: handleDeleteBuilder,
                            },
                        ]}
                    />
                </Popover>
            </div>
        );
    }

    return (
        <Page>
            <div className="pTitle">
                <h2>Design Your Builder</h2>
                <p>Complete each tab below to create a product builder. You'll be given instructions at the end.</p>
            </div>
            <div className="mainCard">
                <div className="builderCard">
                    <div className="cardImg">
                        <img src={card1} />
                    </div>
                    <div className="cardDetail">
                        <h4>Simple Builder</h4>
                        <p className="subText">Popular setting for creating same builder</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Adipiscing et diam cursus sed mi quam sagittis.</p>
                        <Button type="button" className='builder_primary_btn' onClick={() => navigate(`bundle/0/simple`)}>Get Started</Button>
                    </div>


                </div>
                <div className="builderCard">
                    <div className="cardImg">
                        <img src={card2} />
                    </div>
                    <div className="cardDetail">
                        <div className="d-flex">
                            <h4>Advance Builder</h4> <Tag >Popular</Tag>
                        </div>
                        <p className="subText">Advance setting features for your builder</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Adipiscing et diam cursus sed mi quam sagittis.</p>
                        <Button type="button" className='builder_primary_btn' onClick={() => navigate(`bundle/0/advance`)}>Get Started</Button>
                    </div>


                </div>
            </div>

            <div className="custom-border-card">
                {/* Header */}
                <div className="Polaris-Card__Header custom-card-heading">
                    <h2 className="Polaris-Heading">My builders</h2>
                </div>
                <Card sectioned>

                    <div className='Polaris-DataTable'>
                        {/* Search Field */}
                        <div>
                            <div className="search_box">
                                <div className="serchIcon">
                                    <SearchMinor />
                                    <div className="inputSerch w-98">
                                        <TextField
                                            className=""
                                            value={searchText}
                                            onChange={(val) => { setSearchText(val) }}
                                            autoComplete="off"
                                            placeholder="Filter"
                                        />
                                    </div>
                                </div>

                                <Button type="button" className='builder_primary_btn' onClick={() => { searchHandle() }}>Search</Button>
                            </div>
                        </div>


                        {/* Table */}
                        <div className='Polaris-DataTable__ScrollContainer'>
                            <table className='Polaris-DataTable__Table '>

                                <thead>
                                    <tr>
                                        <th className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn Polaris-DataTable__Cell--header '>Builder</th>
                                        <th className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn Polaris-DataTable__Cell--header text-center'>No. of steps</th>
                                        <th className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn Polaris-DataTable__Cell--header text-center'>Status</th>
                                        <th className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn Polaris-DataTable__Cell--header text-center'>Type</th>
                                        <th className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn Polaris-DataTable__Cell--header text-center'>Action</th>
                                        <th className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn Polaris-DataTable__Cell--header text-center'>More Action</th>
                                    </tr>
                                </thead>

                                {(rows.length > 0 && !isLoading) ?
                                    <tbody>
                                        {rows.map((item, index) => {
                                            return (
                                                <tr key={index}>
                                                    <td className='Polaris-DataTable__Cell'>{item.title}</td>
                                                    <td className='Polaris-DataTable__Cell text-center'>{item.total_steps}</td>
                                                    <td className='Polaris-DataTable__Cell text-center'>
                                                        {(item.is_active) ?
                                                            <span className="builder-status active">Active</span>
                                                            :
                                                            <span className="builder-status inactive">Inactive</span>
                                                        }
                                                    </td>
                                                    <td className='Polaris-DataTable__Cell firstCapital text-center'>{item.type}</td>
                                                    <td className='Polaris-DataTable__Cell text-center'>
                                                        <Button type="button" className='builder_primary_btn' onClick={() => navigate(`bundle/${item.id}/${item.type}`)}>Edit</Button>
                                                    </td>
                                                    <td className='Polaris-DataTable__Cell '>
                                                        <MoreActionsComponent dataId={item.id} index={index} />
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                    </tbody>
                                    :
                                    <tbody>
                                        <tr>
                                            <td className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop text-center'
                                                colSpan='3'> {(isLoading) ? 'Loading...' : 'No data found...'} </td>
                                        </tr>
                                    </tbody>
                                }
                            </table>
                        </div>

                        {
                            pagination.hasPages
                                ?
                                <div className="d-flex justify-center">
                                    <Pagination
                                        hasPrevious={pagination.previousPageUrl != null ? true : false}
                                        onPrevious={() => {
                                            getData(pagination.previousPageUrl);
                                        }}
                                        hasNext={pagination.nextPageUrl != null ? true : false}
                                        onNext={() => {
                                            getData(pagination.nextPageUrl);
                                        }}
                                    />
                                </div>
                                :
                                ''
                        }

                    </div>
                </Card>
            </div>

        </Page>
    );
}

export default Dashboard;
