import React, {useCallback, useState, useEffect} from "react";
import {Page, Card, Pagination, DataTable, Link, Button} from '@shopify/polaris';

function Index(){
    const navigate = useNavigate();
    const [rows, setRows] = useState([]);
    const [isLoading, setIsLoading] = useState([]);

    // useEffect(() => {
    //     getData('/bundle');
    // }, []);

    async function getData(url){
        const response = fetch(url, {
            method: 'GET', headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        }).then(response => response.json())
            .then(res =>{
                setRows(res.data.builders);
                setIsLoading(false);
            })
    }

    function editBuilder(id){
        navigate('bundle', {'id': id});
    }
    
    return (
        <Page>
            <Card sectioned title="My Builders">
                <div className='Polaris-DataTable'>
                   Portal js
                </div>
            </Card>
        </Page>
    );
}
export default Index;
