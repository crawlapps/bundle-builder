import {
    TextField, FormLayout, Select, Checkbox
} from '@shopify/polaris';
import React from "react";

function AddDiscount(props) {
    const rows = [

    ];
    const discountTypeOptions = [
        { label: 'Percentage', value: 'percent' },
        { label: 'Fixed Amount', value: 'fixed' },
    ];
    const applyDiscountWhenOptions = [
        { label: 'Number of items added', value: 'count' },
        { label: 'Total price reaches an amount', value: 'price' },
    ];
    const applyDiscountOptions = [
        { label: 'Whole Builder', value: 'whole' },
    ];
    console.log(props);

    return (
        <FormLayout>
            <TextField
                label="Discount Title"
                autoComplete="off"
                placeholder="e.g. 10% off 8 items or more"
                helpText="Customers will see this in the cart/checkout pages"
                value={props.discountForm.form.discounts[props.selectedDiscountIndex].title}
                onChange={value => props.discountForm.updateForm('discounts', value, 'discounts', 'title', props.selectedDiscountIndex, -1)}
                error={(typeof props.discountForm.errors.discounts != 'undefined') ? props.discountForm.errors.discounts[props.selectedDiscountIndex].title : ''}
            />
            <Select
                label="Discount Type"
                options={discountTypeOptions}
                value={props.discountForm.form.discounts[props.selectedDiscountIndex].type}
                onChange={value => props.discountForm.updateForm('discounts', value, 'discounts', 'type', props.selectedDiscountIndex, -1)}
            />
            {
                props.discountForm.form.discounts[props.selectedDiscountIndex].type == 'percent'
                    ?
                    <TextField
                        label="Discount Amount %"
                        type="number"
                        autoComplete="off"
                        helpText="A percentage between 1-99"
                        value={String(props.discountForm.form.discounts[props.selectedDiscountIndex].amount)}
                        onChange={value => props.discountForm.updateForm('discounts', value, 'discounts', 'amount', props.selectedDiscountIndex, -1)}
                        error={(typeof props.discountForm.errors.discounts != 'undefined') ? props.discountForm.errors.discounts[props.selectedDiscountIndex].amount : ''}
                    />
                    :
                    <TextField
                        label="Discount Amount"
                        type="number"
                        autoComplete="off"
                        value={String(props.discountForm.form.discounts[props.selectedDiscountIndex].amount)}
                        onChange={value => props.discountForm.updateForm('discounts', value, 'discounts', 'amount', props.selectedDiscountIndex, -1)}
                        error={(typeof props.discountForm.errors.discounts != 'undefined') ? props.discountForm.errors.discounts[props.selectedDiscountIndex].amount : ''}
                    />
            }
            <FormLayout.Group>
                <Select
                    label="Apply Discount When"
                    options={applyDiscountWhenOptions}
                    value={props.discountForm.form.discounts[props.selectedDiscountIndex].apply_discount}
                    onChange={value => props.discountForm.updateForm('discounts', value, 'discounts', 'apply_discount', props.selectedDiscountIndex, -1)}
                />
                {
                    props.discountForm.form.discounts[props.selectedDiscountIndex].apply_discount == 'count'
                        ?
                        <TextField
                            label="Number of items required"
                            type="number"
                            autoComplete="off"
                            helpText="A percentage between 1-99"
                            value={String(props.discountForm.form.discounts[props.selectedDiscountIndex].no_of_items_required)}
                            onChange={value => props.discountForm.updateForm('discounts', value, 'discounts', 'no_of_items_required', props.selectedDiscountIndex, -1)}
                            error={(typeof props.discountForm.errors.discounts != 'undefined') ? props.discountForm.errors.discounts[props.selectedDiscountIndex].no_of_items_required : ''}
                        />
                        :
                        <TextField
                            label="Price Required"
                            type="number"
                            autoComplete="off"
                            value={String(props.discountForm.form.discounts[props.selectedDiscountIndex].price_required)}
                            onChange={value => props.discountForm.updateForm('discounts', value, 'discounts', 'price_required', props.selectedDiscountIndex, -1)}
                            error={(typeof props.discountForm.errors.discounts != 'undefined') ? props.discountForm.errors.discounts[props.selectedDiscountIndex].price_required : ''}
                        />
                }

            </FormLayout.Group>
            <Select
                label="Apply to a step or the whole builder"
                options={applyDiscountOptions}
                helpText="The discount will only apply when the above requirements are met on the step you choose, or the builder as a whole."
                value={props.discountForm.form.discounts[props.selectedDiscountIndex].apply_to}
                onChange={value => props.discountForm.updateForm('discounts', value, 'discounts', 'apply_to', props.selectedDiscountIndex, -1)}
            />
            <FormLayout.Group>
                <Checkbox
                    label="Encourage discount"
                    helpText="A small alert will appear to tell the customer how close they are to receiving a discount."
                    checked={props.discountForm.form.discounts[props.selectedDiscountIndex].is_encourage_discount}
                    onChange={value => props.discountForm.updateForm('discounts', value, 'discounts', 'is_encourage_discount', props.selectedDiscountIndex, -1)}
                />
                {
                    props.discountForm.form.discounts[props.selectedDiscountIndex].is_encourage_discount
                        ?

                        props.discountForm.form.discounts[props.selectedDiscountIndex].apply_discount == 'count'
                            ?
                            <TextField
                                label="Encourage when items selected reach"
                                type="number"
                                autoComplete="off"
                                helpText="We will show a small popup when the item count or current price equals the amount you enter here."
                                value={String(props.discountForm.form.discounts[props.selectedDiscountIndex].encourage_discount_price)}
                                onChange={value => props.discountForm.updateForm('discounts', value, 'discounts', 'encourage_discount_price', props.selectedDiscountIndex, -1)}
                                error={(typeof props.discountForm.errors.discounts != 'undefined') ? props.discountForm.errors.discounts[props.selectedDiscountIndex].encourage_discount_price : ''}
                            />
                            :
                            <TextField
                                label="Encourage when price reaches"
                                type="number"
                                autoComplete="off"
                                helpText="We will show a small popup when the item count or current price equals the amount you enter here."
                                value={String(props.discountForm.form.discounts[props.selectedDiscountIndex].encourage_discount_price)}
                                onChange={value => props.discountForm.updateForm('discounts', value, 'discounts', 'encourage_discount_price', props.selectedDiscountIndex, -1)}
                                error={(typeof props.discountForm.errors.discounts != 'undefined') ? props.discountForm.errors.discounts[props.selectedDiscountIndex].encourage_discount_price : ''}
                            />
                        :
                        ''
                }


            </FormLayout.Group>
            <FormLayout.Group>
                <Checkbox
                    label="Remove any discounts applied before this"
                    helpText="If unchecked, this discount will be added to any other discounts that are applied earlier in the order."
                    checked={props.discountForm.form.discounts[props.selectedDiscountIndex].is_remove_other_discount}
                    onChange={value => props.discountForm.updateForm('discounts', value, 'discounts', 'is_remove_other_discount', props.selectedDiscountIndex, -1)}
                />
                <TextField
                    label="Order"
                    type="number"
                    autoComplete="off"
                    helpText="The order in which the discount is applied in case multiple discounts are added."
                    value={String(props.discountForm.form.discounts[props.selectedDiscountIndex].order)}
                    onChange={value => props.discountForm.updateForm('discounts', value, 'discounts', 'order', props.selectedDiscountIndex, -1)}
                    error={(typeof props.discountForm.errors.discounts != 'undefined') ? props.discountForm.errors.discounts[props.selectedDiscountIndex].order : ''}
                />
            </FormLayout.Group>
        </FormLayout>
    );
}

export default AddDiscount;
