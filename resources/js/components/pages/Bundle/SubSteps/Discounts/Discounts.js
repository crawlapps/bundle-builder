import {
    Card, FormLayout, Banner, DataTable, Button, Modal, ButtonGroup, Icon, Heading, TextField, Navigation, Frame
} from '@shopify/polaris';
import {
    DraftOrdersMajor, DeleteMajor
} from '@shopify/polaris-icons';
import React, { useCallback, useState } from "react";
import AddField from "../Steps/SubSteps/Models/AddField";
import AddDiscount from "./AddDiscount";
import caretArrow from '../../../../snippets/caretArrow.svg'


function Discounts(props) {
    const rows = props.form.discounts;

    const [showDiscountModal, setShowDiscountModal] = useState(false);
    const [selectedDindex, setSelectedDindex] = useState('');
    const [oldDiscount, setOldDiscount] = useState('');
    const [active, setActive] = useState(true);


    function discountModal(dindex = -1) {
        let fl = '';
        if (dindex == -1) {
            let f = {
                id: '',
                title: '',
                type: 'percent',
                amount: '',
                apply_discount: 'count',
                no_of_items_required: '',
                apply_to: 0,
                is_encourage_discount: 0,
                is_remove_other_discount: true,
                order: ''
            };
            props.form.discounts.push(f);
            fl = props.form.discounts.length - 1;
        } else {
            fl = dindex;
            setOldDiscount(JSON.stringify(props.form.discounts[dindex]));
        }

        setSelectedDindex(fl);
        setShowDiscountModal(true);
    }
    function saveDiscount() {
        if (!props.validate('discounts', selectedDindex)) {
            setShowDiscountModal(false);
        }
    }
    function closeDiscount() {
        if (props.form.discounts[selectedDindex].id == '') {
            props.form.discounts.pop();
        } else {
            props.form.discounts[selectedDindex] = JSON.parse(oldDiscount);
        }

        setShowDiscountModal(false);
    }
    const handleSaveAction = useCallback(
        () => props.storeSetting(),
        [],
    );

    function removeStep(id, index) {
        props.form.discounts.splice(index, 1);
        props.updateForm('steps', props.form.discounts)

        let spareObj = Object.assign({}, props.form);
        spareObj.deletedDiscounts.push(id);
        props.updateForm('deletedDiscounts', spareObj.deletedDiscounts)
    }

    return (
        <div className='card-wrapper-radius'>
            <Card>
                <div className='p-5'>
                    {
                        props.form.settings.product_settings.is_use_on_new_orders == true && props.form.settings.product_settings.is_keep_builder_on_order == false
                            ?
                            <Banner
                                title="Unavailable on this builder."
                                action={{
                                    content: 'Save & Next',
                                    onAction: () => handleSaveAction(),
                                    loading: props.isDisable ? true : false
                                }}
                                status="info"
                            >
                                <p>As you have chosen to 'Use Your Products on new Orders' but not to 'Keep the builder on the order' on the Builder Settings tab, automatic discounts aren't available on this builder.</p>
                                <p>You can enable the 'Keep the builder on the order' setting on the Builder Settings tab to create automatic discounts.</p>
                            </Banner>
                            :
                            <>

                                <Modal
                                    title="Add a Discount"
                                    open={showDiscountModal}
                                    primaryAction={{
                                        content: 'Add Field',
                                        onAction: saveDiscount,
                                    }}
                                    secondaryActions={[
                                        {
                                            content: 'Cancel',
                                            onAction: closeDiscount,
                                        },
                                    ]}
                                    onClose={closeDiscount}
                                >
                                    <div className='modal_right'>
                                        <Modal.Section>
                                            <AddDiscount discountForm={props} selectedDiscountIndex={selectedDindex} />
                                        </Modal.Section>
                                    </div>
                                </Modal>

                                <Banner
                                    title="Automatic discounts."
                                    status="info"
                                >
                                    <p>The discounts you add here will be applied automatically, without requiring a discount code to be entered. You can use these to encourage your customers to add more items to their boxes.
                                    </p>
                                </Banner>

                                <div className='builder_settings_accrodian pt-5'>
                                    <ul className='list-unstyled'>
                                        <li className={`builder_settings_accrodian_list ${active ? 'accrodian_active' : ''}`} >
                                            <div className='builder_settings_accrodian_title'>
                                                <Heading>Automatic Builder Discounts</Heading>
                                                <div className='accrodian_caret' onClick={() => { setActive(!active) }}>
                                                    <img src={caretArrow} alt="Caret icon" />
                                                </div>
                                            </div>
                                            <div className='builder_settings_accrodian_content' style={{ padding: '0' }}>
                                                <Card sectioned>
                                                    <FormLayout>
                                                        <FormLayout.Group>
                                                            <table className='Polaris-DataTable__Table'>
                                                                <thead>
                                                                    <tr>
                                                                        <th className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn Polaris-DataTable__Cell--header'>Discounts</th>
                                                                        <th className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn Polaris-DataTable__Cell--header text-center'>
                                                                            <div className='discountActions'>
                                                                                Action
                                                                            </div>
                                                                        </th>
                                                                    </tr>
                                                                </thead>

                                                                {(rows.length > 0) ?
                                                                    <tbody>
                                                                        {rows.map((item, index) => {
                                                                            return (
                                                                                <tr key={index} className={rows.length != index ? "box-shadow-tr" : ''}>
                                                                                    <td className='Polaris-DataTable__Cell'>{item.title}</td>
                                                                                    <td className='Polaris-DataTable__Cell text-center'>
                                                                                        <div className='discountActions'>
                                                                                            <Button outline type="button" onClick={() => discountModal(index)}>
                                                                                                Edit
                                                                                                {/* <Icon source={DraftOrdersMajor} color="base" /> */}
                                                                                            </Button>
                                                                                            <div style={{ color: '#D72C0D', display: 'inline-block' }}>
                                                                                                <Button monochrome outline type="button" onClick={() => removeStep(item.id, index)} >
                                                                                                    Delete
                                                                                                    {/* <Icon source={DeleteMajor} color="base" /> */}
                                                                                                </Button>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            )
                                                                        })}
                                                                        <tr className='last-tr'>
                                                                            <td className='Polaris-DataTable__Cell text-center' colSpan={2}>{rows.length} Discounts Added</td>
                                                                        </tr>
                                                                    </tbody>
                                                                    :
                                                                    <tbody>
                                                                        <tr>
                                                                            <td className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop text-center'
                                                                                colSpan='3'>{rows.length} discount(s) added
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                }
                                                            </table>
                                                        </FormLayout.Group>
                                                        <FormLayout.Group>
                                                            <Button onClick={() => discountModal(-1)}>Add a Discount</Button>
                                                        </FormLayout.Group>
                                                    </FormLayout>

                                                </Card>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                                <div className='mt-5 center-buttons'>
                                    <ButtonGroup>
                                        <Button onClick={() => props.handleTabChange(1)}>Back</Button>
                                        {(props.isDisable) ?
                                            <Button primary loading>Save & Next</Button>
                                            :
                                            <Button primary onClick={() => handleSaveAction()} >Save & Next</Button>
                                        }
                                    </ButtonGroup>
                                </div>

                            </>
                    }
                </div>
            </Card >
        </div>
    );
}

export default Discounts;
