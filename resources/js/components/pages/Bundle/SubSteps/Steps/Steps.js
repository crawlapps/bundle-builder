import {
    Card,
    ButtonGroup,
    Button
} from '@shopify/polaris';

import BuilderSteps from './SubSteps/BuilderSteps';
import ProductSteps from './SubSteps/ProductSteps';
import OtherSteps from './SubSteps/OtherSteps';
import AddSteps from "./SubSteps/AddSteps";
import React, { useState } from "react";


function Steps(props) {
    console.log(props);
    const [isForm, setIsForm] = useState('hide');
    const [selectedForm, setSelectedForm] = useState('');
    const [selectedIndex, setSelectedIndex] = useState('');
    const [oldSaved, setOldSaved] = useState('');

    const showHideForm = (action, type, isSave = 0, isNew = 1, sIndex = 1) => {
        if (action === 'show' && isNew) {

            let d_order = props.form.steps.length > 0 ? (props.form.steps.reduce((prev, current) => ((prev.display_order > current.display_order) ? prev.display_order : current.display_order), 0) + 1) : 1;

            let p = {
                id: '',
                title: '',
                description: '',
                short_title: '',
                button_text: '',
                display_order: d_order,
                minimum_selection: '',
                maximum_selection: '',
                is_allow_more_than_one: 0,
                is_required_step: 0,
                is_show_box_content: 0,
                is_hide_from_step_progress: 0,
                is_show_sortby_dropdown: 0,
                is_show_title_filter: 0,
                is_show_collection_filter: 0,
                is_show_tags_filter: 0,
                is_show_price: 0,
                products_per_row: '',
                show_variant_as: 0,
                content_data: '',
                dropdown_tags: '',
                resource: [],
                collections: [],
                selected_products: [],
                collections_filter: [],
                filter_tags: '',
                selectedTab: 0,
                field: [],
                type: type,
            }
            props.form.steps.push(p);

            let l = props.form.steps.length - 1;
            setSelectedIndex(l);

            setIsForm(action);
            setSelectedForm(type);
        } else if (action === 'show' && !isNew) {
            setOldSaved(JSON.stringify(props.form.steps[sIndex]));

            setSelectedIndex(sIndex);
            setIsForm(action);
            setSelectedForm(type);
        }
        if (action == 'hide' && !isSave) {
            if (props.form.steps[selectedIndex].id == '') {
                props.form.steps.pop();
            } else {
                // props.updateForm(selectedIndex, JSON.parse(oldSaved) , 'steps', '', '', -1, -1);
                props.form.steps[selectedIndex] = JSON.parse(oldSaved);

            }
            // props.form.steps.pop();
            setIsForm(action);
            setSelectedForm(type);
        } else {
            if (!props.validate(selectedForm, selectedIndex)) {
                setIsForm(action);
                setSelectedForm(type);
            }
        }
    }

    return (
        <div className='card-wrapper-radius'>
            <Card sectioned>
                {isForm === 'hide' ?
                    <div>
                        <div className='builder_settings_accrodian'>
                            <ul className='list-unstyled'>

                                {/*BuilderSteps*/}
                                <BuilderSteps stepsData={props} showHideForm={showHideForm} />

                                {/*ProductSteps*/}
                                <ProductSteps showHideForm={showHideForm} />

                                {/*OtherSteps*/}
                                <OtherSteps showHideForm={showHideForm} />

                            </ul>
                        </div>

                        <div className='center-buttons'>
                            <Button onClick={() => props.handleTabChange(0)}>Back </Button>
                            {(props.form.steps.length > 0) ?
                                <Button primary onClick={() => props.handleTabChange(2)}>Save Step</Button>
                                :
                                <Button disabled>Save Steps</Button>
                            }
                            {/* <Button primary onClick={() => props.handleTabChange(2)}>Save & Next</Button> */}
                        </div>
                    </div>
                    :
                    <div>
                        <AddSteps formstep={props} index={selectedIndex} selectedForm={selectedForm} />
                        {/*{selectedForm === 'product' ?*/}
                        {/*    <AddProductSteps formstep={props} index={selectedIndex}/>*/}
                        {/*: selectedForm === 'pre_selected' ?*/}
                        {/*    <AddPreSelectedProductSteps />*/}
                        {/*: selectedForm === 'form' ?*/}
                        {/*    <AddFormSteps />*/}
                        {/*: selectedForm === 'subscription' ?*/}
                        {/*    <AddSubscriptionSteps />*/}
                        {/*: selectedForm === 'content' ?*/}
                        {/*    <AddContentSteps />*/}
                        {/*: "Neither"*/}
                        {/*}*/}
                        <div className='center-buttons'>
                            <Button onClick={() => showHideForm('hide', '', 0)}>Back</Button>
                            <Button primary onClick={() => showHideForm('hide', '', 1)}>Save Step</Button>
                        </div>
                    </div>
                }
            </Card>
        </div>
    );
}

export default Steps;
