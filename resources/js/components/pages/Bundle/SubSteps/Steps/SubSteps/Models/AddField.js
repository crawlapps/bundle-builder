import {
    FormLayout,
    TextField,
    Checkbox, RadioButton, Heading, Select, DatePicker, Button, Modal, Banner, Stack, Thumbnail, Caption, List, DropZone, Label
} from '@shopify/polaris';
import { useCallback, useEffect, useReducer, useState } from "react";
import { ChromePicker } from 'react-color';

function AddField(props) {
    // console.log('AddField');
    // console.log(props);
    const [rerender, setRerender] = useState(false);


    const [dateModal, setDateModal] = useState(false);
    const [{ month, year }, setDateOptions] = useState({ month: new Date().getMonth(), year: new Date().getFullYear() });
    const [selectedDates, setSelectedDates] = useState({
        start: new Date(),
        end: new Date(),
    });
    const [color, setColor] = useState('#000');
    const [files, setFiles] = useState([]);


    const options = [
        { label: 'Text Entry', value: 'text' },
        { label: 'Text Entry (multi-line)', value: 'textarea' },
        { label: 'Dropdown Box', value: 'select' },
        { label: 'Number Entry', value: 'number' },
        { label: 'Date Entry', value: 'date' },
        { label: 'Custom Date Picker', value: 'custom-date' },
        { label: 'Swatch Color Picker', value: 'color' },
        { label: 'Image Selection', value: 'image' },
        { label: 'File/Image Upload', value: 'file' },
        { label: 'Checkbox', value: 'checkbox' },
        { label: 'Color Picker', value: 'color-picker' },
    ];
    const dateFormates = [
        { label: 'dd/mm/yyyy', value: 'dd/mm/yyyy' },
        { label: 'mm/dd/yyyy', value: 'mm/dd/yyyy' },
        { label: 'yyyy/mm/dd', value: 'yyyy/mm/dd' },
    ];
    const languages = [
        { label: 'British/US English', value: 'en' },
        { label: 'Spanish', value: 'es' },
        { label: 'Hindi', value: 'hi' },
    ];
    const days = [
        { label: 'Monday', value: false },
        { label: 'Tuesday', value: false },
        { label: 'Wednesday', value: false },
        { label: 'Thursday', value: false },
        { label: 'Frieday', value: false },
        { label: 'Saturday', value: false },
        { label: 'Sunday', value: false },
    ];

    // ResetColor
    function resetColor() {
        setColor({
            hex: "#000000",
            hsl: { h: 0, s: 0, l: 0, a: 1 },
            hsv: { h: 0, s: 0, v: 0, a: 1 },
            oldHue: 0,
            rgb: { r: 0, g: 0, b: 0, a: 1 },
            source: "hsv"
        });
        // setColor(props.form.formstep.form.steps[props.form.index].field[props.formindex].selected_colors.at(-1).color);
    }
    // Remove extra data of other types.
    const typeChanged = (val) => {
        let currentObj = props.form.formstep.form.steps[props.form.index].field[props.formindex];
        let spareObj = {
            field_type: val,
            field_title: currentObj.field_title,
            display_order: currentObj.display_order,
            is_required_field: currentObj.is_required_field,
            condition: currentObj.condition
        }

        if (['text', 'textarea', 'number'].includes(val)) {
            spareObj.minlength = '';
            spareObj.maxlength = '';
        } else if (['select'].includes(val)) {
            spareObj.dropdown_options = '';
        } else if (['custom-date'].includes(val)) {
            spareObj.date_formate = 'dd/mm/yyyy';
            spareObj.language = 'hi';
            spareObj.lead_time = '';
            spareObj.only_allow_future_dates = false;
            spareObj.days_to_block = [
                { label: 'Monday', value: false },
                { label: 'Tuesday', value: false },
                { label: 'Wednesday', value: false },
                { label: 'Thursday', value: false },
                { label: 'Friday', value: false },
                { label: 'Saturday', value: false },
                { label: 'Sunday', value: false },
            ];
            spareObj.block_specific_dates = [];
        } else if (['color'].includes(val)) {
            spareObj.selected_colors = [];
        } else if (['image'].includes(val)) {
            spareObj.images = [];
        } else if (['file'].includes(val)) {
            spareObj.file_type = 'Any';
        }
        props.form.formstep.form.steps[props.form.index].field[props.formindex] = spareObj;
        console.log(props.form.formstep.form.steps[props.form.index].field);
    }

    const dayChanged = (val, i) => {
        let spareObj = props.form.formstep.form.steps[props.form.index].field[props.formindex];
        spareObj.days_to_block[i].value = val;
        props.form.formstep.form.steps[props.form.index].field[props.formindex] = spareObj;
        props.form.formstep.updateForm(props.form.selectedForm, props.form.formstep.form.steps[props.form.index].field[props.formindex].days_to_block, 'steps', 'days_to_block', props.form.index, props.formindex)
    }

    function BlockDatePicker() {
        const handleMonthChange = useCallback(
            (month, year) => setDateOptions({ month, year }),
            [],
        );

        return (
            <DatePicker
                month={month}
                year={year}
                onChange={setSelectedDates}
                onMonthChange={handleMonthChange}
                selected={selectedDates}
            />
        );
    }
    function saveDate() {
        props.form.formstep.form.steps[props.form.index].field[props.formindex].block_specific_dates.push(selectedDates);
        setDateModal(false)
    }
    // Add color for color selector
    function addColor() {
        props.form.formstep.form.steps[props.form.index].field[props.formindex].selected_colors.push(
            {
                color: color,
                color_name: ''
            }
        );
        resetColor();
        console.log(props.form.formstep.form.steps[props.form.index].field[props.formindex]);
    }

    function DropZoneWithImageFileUpload() {

        const [rejectedFiles, setRejectedFiles] = useState([]);
        const hasError = rejectedFiles.length > 0;

        const handleDrop = useCallback(
            (_droppedFiles, acceptedFiles, rejectedFiles) => {
                setFiles((files) => [...files, ...acceptedFiles]);
                setRejectedFiles(rejectedFiles);
                imagesDropped(acceptedFiles);
            },
            [],
        );

        const fileUpload = !files.length && <DropZone.FileUpload />;
        const uploadedFiles = files.length > 0 && (
            <Stack vertical>
                {files.map((file, index) => (
                    <Stack alignment="center" key={index}>
                        <Thumbnail
                            size="small"
                            alt={file.name}
                            source={window.URL.createObjectURL(file)}
                        />
                        <div>
                            {file.name} <Caption>{file.size} bytes</Caption>
                        </div>
                    </Stack>
                ))}
            </Stack>
        );

        const errorMessage = hasError && (
            <Banner
                title="The following images couldn’t be uploaded:"
                status="critical"
            >
                <List type="bullet">
                    {rejectedFiles.map((file, index) => (
                        <List.Item key={index}>
                            {`"${file.name}" is not supported. File type must be .gif, .jpg, .png or .svg.`}
                        </List.Item>
                    ))}
                </List>
            </Banner>
        );

        return (
            <Stack vertical>
                {errorMessage}
                <DropZone accept="image/*" type="image" onDrop={handleDrop}>
                    {uploadedFiles}
                    {fileUpload}
                </DropZone>
            </Stack>
        );
    }

    function uploadImages() {
        console.log(files);
    }

    function imagesDropped(acceptedFiles) {
        const generateId = () => `${performance.now()}${Math.random().toString().slice(5)}`.replace('.', '')
        let spareObj = props.form.formstep.form.steps[props.form.index].field[props.formindex];
        acceptedFiles.map((file) => {
            spareObj.images.push({
                file: file,
                src: '',
                imageName: '',
                uniqueId: generateId(),
            });
            console.log('pushed');
        })

    }

    function removeImage(i) {
        let spareObj = props.form.formstep.form.steps[props.form.index].field[props.formindex];
        // let value = props.form.formstep.form.steps[props.form.index].field[props.formindex].images.slice(i, 1);
        spareObj.images.splice(i, 1);
        props.form.formstep.updateForm(props.form.selectedForm, spareObj.images, 'steps', 'images', props.form.index, props.formindex)
    }

    function imgNameChange(val, i) {
        let spareObj = props.form.formstep.form.steps[props.form.index].field[props.formindex];
        spareObj.images[i].imageName = val;
        props.form.formstep.updateForm(props.form.selectedForm, spareObj.images, 'steps', 'images', props.form.index, props.formindex)
    }

    return (
        <FormLayout>
            <Select
                label="Field Type"
                options={options}
                value={props.form.formstep.form.steps[props.form.index].field[props.formindex].field_type}
                onChange={(value) => { typeChanged(value), props.form.formstep.updateForm(props.form.selectedForm, value, 'steps', 'field_type', props.form.index, props.formindex) }}
            />

            {/* 'select' => Dropdown Options */}
            {
                ['select'].includes(props.form.formstep.form.steps[props.form.index].field[props.formindex].field_type)
                &&
                < FormLayout.Group >
                    <TextField
                        label="Dropdown Options"
                        type="text"
                        autoComplete="off"
                        helpText="A comma separated list of options for the dropdown box."
                        placeholder="Option 1, Option 2"
                        value={props.form.formstep.form.steps[props.form.index].field[props.formindex].dropdown_options}
                        onChange={value => props.form.formstep.updateForm(props.form.selectedForm, value, 'steps', 'dropdown_options', props.form.index, props.formindex)}
                    />
                </FormLayout.Group>

            }

            <TextField
                label="Field Title"
                autoComplete="off"
                helpText="Text to appear above the field."
                placeholder="e.g. Would you like to engrave this product?"
                value={props.form.formstep.form.steps[props.form.index].field[props.formindex].field_title}
                onChange={value => props.form.formstep.updateForm(props.form.selectedForm, value, 'steps', 'field_title', props.form.index, props.formindex)}
                error={(typeof props.form.formstep.errors.steps != 'undefined') ? props.form.formstep.errors.steps[props.form.index].field[props.formindex].field_title : ''}
            />

            {/* 'text', 'textarea', 'number' => Min and Max-Length */}
            {
                ['text', 'textarea', 'number'].includes(props.form.formstep.form.steps[props.form.index].field[props.formindex].field_type)
                &&
                <FormLayout.Group >
                    <TextField
                        label="Min Length (optional)"
                        type="number"
                        autoComplete="off"
                        helpText="Leave blank if no minimum required."
                        placeholder="e.g. 5"
                        value={props.form.formstep.form.steps[props.form.index].field[props.formindex].minlength}
                        onChange={value => props.form.formstep.updateForm(props.form.selectedForm, value, 'steps', 'minlength', props.form.index, props.formindex)}
                    />
                    <TextField
                        label="Max Length (optional)"
                        type="number"
                        autoComplete="off"
                        helpText="Leave blank if no maximum required."
                        placeholder="e.g. 10"
                        value={props.form.formstep.form.steps[props.form.index].field[props.formindex].maxlength}
                        onChange={value => props.form.formstep.updateForm(props.form.selectedForm, value, 'steps', 'maxlength', props.form.index, props.formindex)}
                    />
                </FormLayout.Group>

            }



            {/* "custom-date" => Custome Date entry */}
            {
                ["custom-date"].includes(props.form.formstep.form.steps[props.form.index].field[props.formindex].field_type)
                &&
                <FormLayout >
                    <br />
                    <FormLayout.Group >
                        <Select
                            label="Date Formate"
                            options={dateFormates}
                            value={props.form.formstep.form.steps[props.form.index].field[props.formindex].date_formate}
                            onChange={(value) => { props.form.formstep.updateForm(props.form.selectedForm, value, 'steps', 'date_formate', props.form.index, props.formindex) }}
                        />
                        <Select
                            label="Language"
                            options={languages}
                            value={props.form.formstep.form.steps[props.form.index].field[props.formindex].language}
                            onChange={(value) => { props.form.formstep.updateForm(props.form.selectedForm, value, 'steps', 'language', props.form.index, props.formindex) }}
                        />
                    </FormLayout.Group>

                    <TextField
                        label="Lead Time"
                        type="number"
                        autoComplete="off"
                        helpText="The number of days to block from the current date onwards."
                        placeholder="e.b. 5"
                        value={props.form.formstep.form.steps[props.form.index].field[props.formindex].lead_time}
                        onChange={value => props.form.formstep.updateForm(props.form.selectedForm, value, 'steps', 'lead_time', props.form.index, props.formindex)}
                    />

                    <Checkbox
                        label="Only allow future dates Field"
                        helpText="If selected, customers cannot select today's date or a date in the past"
                        checked={props.form.formstep.form.steps[props.form.index].field[props.formindex].only_allow_future_dates}
                        onChange={value => props.form.formstep.updateForm(props.form.selectedForm, value, 'steps', 'only_allow_future_dates', props.form.index, props.formindex)}
                    />

                    <FormLayout>
                        <p className="Polaris-OptionList__Title" style={{ margin: '0px', marginTop: '35px' }}>Select Days to Block</p>
                        {
                            props.form.formstep.form.steps[props.form.index].field[props.formindex].days_to_block.map((day, day_i) => {

                                return (
                                    <Checkbox
                                        style={{ margin: '0px 15px' }}
                                        key={day_i}
                                        label={day.label}
                                        checked={day.value == true}
                                        onChange={value => { dayChanged(value, day_i) }}
                                    />
                                );

                            })
                        }
                    </FormLayout>

                    <Button onClick={() => { setDateModal(true) }}>Set Blocked Dates</Button>

                    <Modal
                        title="Selecte Blocked Date"
                        open={dateModal}
                        primaryAction={{
                            content: 'Add Date',
                            onAction: saveDate,
                        }}
                        secondaryActions={[
                            {
                                content: 'Cancel',
                                onAction: () => { setDateModal(false) },
                            },
                        ]}
                        onClose={() => { setDateModal(false) }}
                    >
                        <Modal.Section>
                            <BlockDatePicker />
                        </Modal.Section>
                    </Modal>

                    {
                        props.form.formstep.form.steps[props.form.index].field[props.formindex].block_specific_dates.map((date, i) => {
                            return <div key={i} style={{ border: '1px solid rgb(223, 227, 232)', display: 'inline-block', padding: '5px', borderRadius: '5px' }}>
                                {/* {date.start} */}
                                {date.start.toString()}
                                <button type="button" onClick={() => { props.form.formstep.form.steps[props.form.index].field[props.formindex].block_specific_dates.pop(i) }} className="Polaris-Button Polaris-Button--plain Polaris-Button--monochrome">
                                    <span className="Polaris-Button__Content">
                                        <span className="Polaris-Button__Text">
                                            remove
                                        </span>
                                    </span>
                                </button>
                            </div>
                        })
                    }

                    <br />
                </FormLayout>
            }

            {/* "color" => Color selector */}
            {
                ["color"].includes(props.form.formstep.form.steps[props.form.index].field[props.formindex].field_type)
                &&
                <FormLayout.Group>
                    <ChromePicker
                        color={color.rgb}
                        onChangeComplete={(e) => { setColor(e) }}
                    // disableAlpha={true}
                    />
                    <div>
                        <Button onClick={() => addColor()}>Add Selected Color</Button>
                        <div>

                            {
                                props.form.formstep.form.steps[props.form.index].field[props.formindex].selected_colors.map((col, i) => {
                                    return (
                                        <div className='color_picker_box' key={i + col.color.hex} style={{ display: 'flex', marginTop: '10px', justifyContent: 'space-between' }}>
                                            <div className='color-box mr-10' style={{ background: col.color.hex }}>
                                                <div className="remove-color">
                                                    <a onClick={() => {
                                                        props.form.formstep.form.steps[props.form.index].field[props.formindex].selected_colors.splice(i, 1)
                                                        resetColor();
                                                    }}>X</a>
                                                </div>
                                            </div>
                                            <div>
                                                <TextField label='Color Name' placeholder='e.g. Red' value={col.color_name} onChange={(e) => {
                                                    let currentObj = props.form.formstep.form.steps[props.form.index].field[props.formindex];
                                                    currentObj.selected_colors[i].color_name = e;
                                                    props.form.formstep.form.steps[props.form.index].field[props.formindex] = currentObj;
                                                    resetColor();
                                                }}
                                                />
                                            </div>
                                        </div>
                                    );
                                })
                            }


                        </div>
                    </div>

                </FormLayout.Group>
            }

            {/* "image" => image selector */}
            {
                ["image"].includes(props.form.formstep.form.steps[props.form.index].field[props.formindex].field_type)
                &&
                <FormLayout>
                    <div className='add_img'>
                        <Label>
                            Upload the images the customer can choose from
                        </Label>
                        <DropZoneWithImageFileUpload />
                        {/* <Button onClick={() => { uploadImages() }}>Upload Selected Images</Button> */}
                    </div>


                    <div className='Polaris-ResourceList__ResourceListWrapper'>
                        <ul className='Polaris-ResourceList'>

                            {
                                props.form.formstep.form.steps[props.form.index].field[props.formindex].images.map((img, i) => {

                                    return (
                                        <li key={i} className="Polaris-ResourceList__ItemWrapper">
                                            <div className="Polaris-ResourceItem Polaris-ResourceItem--persistActions"><button className="Polaris-ResourceItem__Button"
                                                aria-label=""></button>
                                                <div className="Polaris-ResourceItem__Container" id="0">
                                                    <div className="Polaris-ResourceItem__Owned">
                                                        <div className="Polaris-ResourceItem__Media"><span
                                                            className="Polaris-Thumbnail Polaris-Thumbnail--sizeMedium">
                                                            <img
                                                                src={(img.src == '' || img.src == null) ? window.URL.createObjectURL(img.file) : img.src}
                                                                // src={''}
                                                                alt=""
                                                                className="Polaris-Thumbnail__Image" /></span></div>
                                                    </div>
                                                    <div className="Polaris-ResourceItem__Content">
                                                        <div className="">
                                                            <div className="Polaris-Connected">
                                                                <div className="Polaris-Connected__Item Polaris-Connected__Item--primary">
                                                                    <div className="Polaris-TextField">
                                                                        <input
                                                                            id="PolarisTextField138" placeholder="Image name"
                                                                            className="Polaris-TextField__Input" type="text"
                                                                            aria-describedby="PolarisTextField138HelpText"
                                                                            aria-labelledby="PolarisTextField138Label" aria-invalid="false"
                                                                            aria-multiline="false"
                                                                            value={img.imageName}
                                                                            onChange={(e) => {
                                                                                imgNameChange(e.target.value, i)
                                                                            }}
                                                                        />
                                                                        <div className="Polaris-TextField__Backdrop"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="Polaris-Labelled__HelpText" id="PolarisTextField138HelpText"><span>The name appears on
                                                                the order details, not the image.</span></div>
                                                        </div>
                                                    </div>
                                                    <div className="Polaris-ResourceItem__Actions">
                                                        <div className="Polaris-ButtonGroup">
                                                            <div className="Polaris-ButtonGroup__Item Polaris-ButtonGroup__Item--plain">
                                                                <button
                                                                    type="button"
                                                                    className="Polaris-Button Polaris-Button--plain"
                                                                    onClick={() => {
                                                                        removeImage(i)
                                                                    }}
                                                                >
                                                                    <span className="Polaris-Button__Content">
                                                                        <span className="Polaris-Button__Text">Remove</span>
                                                                    </span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    );
                                })
                            }

                        </ul>
                    </div>


                </FormLayout>
            }

            {/* "file" => file/image upload */}
            {
                ["file"].includes(props.form.formstep.form.steps[props.form.index].field[props.formindex].field_type)
                &&
                <FormLayout >
                    <div className='bannerClass'>
                        <Banner
                            title="Shopify only allow 1 file upload per field."
                            style={{ marginTop: '2.6rem !important' }}
                            status="info"
                        >
                            <p>Add more file fields for more uploads.</p>
                        </Banner>
                    </div>
                    <Select
                        label="File Type"
                        options={[
                            { label: 'Any', value: 'Any' },
                            { label: 'Only Images', value: 'Only Images' },
                        ]}
                        value={props.form.formstep.form.steps[props.form.index].field[props.formindex].file_type}
                        onChange={(value) => { props.form.formstep.updateForm(props.form.selectedForm, value, 'steps', 'file_type', props.form.index, props.formindex) }}
                    />
                </FormLayout>
            }

            <FormLayout.Group>
                <TextField
                    label="Display Order"
                    type="number"
                    autoComplete="off"
                    helpText="Set the order the field will display within this step."
                    value={props.form.formstep.form.steps[props.form.index].field[props.formindex].display_order}
                    onChange={value => props.form.formstep.updateForm(props.form.selectedForm, value, 'steps', 'display_order', props.form.index, props.formindex)}
                    error={(typeof props.form.formstep.errors.steps != 'undefined') ? props.form.formstep.errors.steps[props.form.index].field[props.formindex].display_order : ''}
                />
                <Checkbox
                    label="Required Field"
                    helpText="Requires an answer/entry from the customer."
                    checked={props.form.formstep.form.steps[props.form.index].field[props.formindex].is_required_field}
                    onChange={value => props.form.formstep.updateForm(props.form.selectedForm, value, 'steps', 'is_required_field', props.form.index, props.formindex)}
                />
            </FormLayout.Group>

            <Heading>Conditions</Heading>

            <RadioButton
                label="Show all the time"
                helpText="Always shown on the form"
                id="disabled"
                name="form_show"
                checked={props.form.formstep.form.steps[props.form.index].field[props.formindex].condition === 'all_time'}
                onChange={value => props.form.formstep.updateForm(props.form.selectedForm, 'all_time', 'steps', 'condition', props.form.index, props.formindex)}
            />
            <RadioButton
                label="Only show when one of these products are selected"
                id="optional"
                name="form_show"
                checked={props.form.formstep.form.steps[props.form.index].field[props.formindex].condition === 'selected_products'}
                onChange={value => props.form.formstep.updateForm(props.form.selectedForm, 'selected_products', 'steps', 'condition', props.form.index, props.formindex)}
            />

        </FormLayout >
    );
}

export default AddField;
