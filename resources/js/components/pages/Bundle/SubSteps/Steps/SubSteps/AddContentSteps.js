import {
    Page,
    Card,
    FormLayout,
    Heading,
    ButtonGroup,
    Button, TextField, Checkbox, RadioButton, Tabs, DataTable, Banner, List
} from '@shopify/polaris';
import React from "react";

function AddContentSteps(){

    const rows = [];
    return (
        <Card.Section>
            <FormLayout>
                <Heading>A few things to note before setting up a Content Step.
                </Heading>
                <Banner
                    title="Setting Up Subscription Plans"
                    status="info"
                >
                    <List type="bullet">
                        <List.Item>Any images that are added to the content, that are chosen from your device, will be uploaded to your theme's assets.</List.Item>
                        <List.Item>The fonts will be based on the fonts provided by your theme, so there are no font options here.
                        </List.Item>
                        <List.Item>Unless you specify font size here, it will take the default size of your theme.</List.Item>
                        <List.Item>You may need to be familiar with CSS to achieve a more advanced design. You can use the 'Code View' in the editor to add CSS and custom classes.</List.Item>
                    </List>
                </Banner>
                <TextField
                    label="Step Title"
                    placeholder="e.g. Some information about you"
                    autoComplete="off"
                />
                <TextField
                    label="Step Description"
                    placeholder="e.g. You can personalize this order by providing your information below. "
                    autoComplete="off"
                    helpText="Additional text to help the customer understand what's required from this step."
                />
                <FormLayout.Group>
                    <TextField
                        label="Display Order"
                        type="number"
                        autoComplete="off"
                        placeholder="e.g. 1"
                        helpText="Set the order the step will display within this builder."
                    />
                    <Checkbox
                        label="Hide from Step Progress"
                        helpText="Hide this step from the list of steps in the progress bar."
                    />
                </FormLayout.Group>
            </FormLayout>
            <Button>Add a field</Button>

            <FormLayout>
                <DataTable
                    columnContentTypes={[
                        'text',
                        'numeric',
                        'numeric',
                        'numeric',
                        'numeric',
                    ]}
                    headings={[
                        'Field',
                        'Type',
                        'Order',
                        'Edit',
                        'Remove',
                    ]}
                    rows={rows}
                    footerContent={`${rows.length} fields added`}
                />
            </FormLayout>
        </Card.Section>
    );
}

export default AddContentSteps;
