import {
    Page,
    Card,
    FormLayout,
    Heading,
    ButtonGroup,
    Button, TextField, Checkbox, RadioButton, Tabs, DataTable, Banner, Modal, Select, InlineError, List, TextContainer, Link, Icon, Image, Spinner
} from '@shopify/polaris';
import React, { useState, useCallback, useEffect } from "react";
import { ResourcePicker } from '@shopify/app-bridge-react';
import AddField from "../SubSteps/Models/AddField";
import { ChevronRightMinor, DeleteMajor, DraftOrdersMajor } from '@shopify/polaris-icons';
import { Editor } from '@tinymce/tinymce-react';
import axios from 'axios';

function AddSteps(props) {
    // console.log('Add steps');
    // console.log(props);
    const [collectionPicker, setCollectionPicker] = useState(false);
    const [productPicker, setProductPicker] = useState(false);
    const [showFormModal, setShowFormModal] = useState(false);
    const [selectedFindex, setSelectedFindex] = useState('');
    const [resourceType, setResourceType] = useState('product');
    const [initialSelectedId, setInitialSelectedId] = useState([]);
    const [collectionSelectMultiple, setCollectionSelectMultiple] = useState(false);
    const [selectedTab, setSelectedTab] = useState(props.formstep.form.steps[props.index].selectedTab);
    const [editForm, setEditForm] = useState(false);
    const [assets, setAssets] = useState([]);
    const [loadAsset, setloadAsset] = useState([]);

    //   const handleTabChange = useCallback(
    //     (selectedTabIndex) => setSelectedTab(selectedTabIndex),
    //     [],

    //   );
    const handleTabChange = useCallback(
        (selectedTabIndex) => {
            setSelectedTab(selectedTabIndex);
            props.formstep.updateForm(props.selectedForm, selectedTabIndex, 'steps', 'selectedTab', props.index)
        },
        [],
    );
    function changeTab() {
        console.log('Tab changed');
        props.formstep.updateForm(props.selectedForm, selectedTab, 'steps', 'selectedTab', props.index)
    }

    const productPerRow = [
        { label: '3', value: '3' },
        { label: '4', value: '4' }
    ];

    function formModal(action) {
        if (action) {
            setEditForm(false);
            let f = {
                field_type: 'text',
                minlength: '',
                maxlength: '',
                field_title: '',
                display_order: '',
                is_required_field: 0,
                condition: 'all_time'
            };
            props.formstep.form.steps[props.index].field.push(f);

            let fl = props.formstep.form.steps[props.index].field.length - 1;
            setSelectedFindex(fl);
        }
        setShowFormModal(action);
    }

    function editFormModal(action, index) {
        if (action) {
            setEditForm(true);
            setSelectedFindex(index);
        }
        setShowFormModal(action);
    }

    function openPicker(resource, action) {
        setResourceType(resource);
        console.log(resource);

        let selectedIds = [];
        let r = (resource == 'selected_products') ? 'Product' : 'Collection';
        let resources = props.formstep.form.steps[props.index][`${resource}`];
        console.log(resources);
        if (resources.length > 0) {
            resources.forEach(function (resource, index) {
                selectedIds.push({ id: 'gid://shopify/' + r + '/' + resource['id'] });

                if (r == 'Product') {
                    let variants = resource['variants'];

                    selectedIds['variants'] = []
                    variants.forEach(function (variant, vi) {
                        selectedIds['variants'].push({ id: 'gid://shopify/ProductVariant/' + variant['id'] });
                    });
                }
            });
        }
        setInitialSelectedId(selectedIds);
        if (resource == 'selected_products') {
            setProductPicker(true);
        } else if (resource == 'collections' || resource == 'collections_filter') {
            setCollectionPicker(true);
            setCollectionSelectMultiple((resource == 'collections_filter'));

        }
    }

    function closeCollectionPicker() {
        setProductPicker(false);
        setCollectionPicker(false);
    }

    function handleSelection({ selection }) {
        let r = [];

        let action = (resourceType === 'selected_products') ? 'Product' : 'Collection';
        selection.forEach(function (resource, index) {
            r[index] = {};
            let resourceId = resource['id'];
            r[index]['id'] = resourceId.replace('gid://shopify/' + action + '/', "");
            r[index]['handle'] = resource['handle'];
            r[index]['title'] = resource['title'];

            if (props.formstep.form.steps[props.index].type == 'pre_selected') {
                r[index]['quantity'] = 1;
            }

            if (action == 'Product') {
                r[index]['image'] = (typeof resource['images'][0]['originalSrc'] == "undefined") ? "" : resource['images'][0]['originalSrc'];
            } else {
                r[index]['image'] = (typeof resource['image'] == "undefined") ? "" : resource['image'];
            }

            if (action == 'Product') {
                r[index]['variants'] = [];
                resource['variants'].forEach(function (variant, vindex) {
                    r[index]['variants'][vindex] = {};
                    r[index]['variants'][vindex]['id'] = variant['id'].replace('gid://shopify/ProductVariant/', "");
                    r[index]['variants'][vindex]['sku'] = variant['sku'];
                    r[index]['variants'][vindex]['title'] = variant['title'];
                    r[index]['variants'][vindex]['displayName'] = variant['displayName'];
                    if (props.formstep.form.steps[props.index].type == 'pre_selected') {
                        r[index]['variants'][vindex]['quantity'] = 1;
                    }
                });
            }
        });
        props.formstep.updateForm(props.selectedForm, r, 'steps', resourceType, props.index);

        closeCollectionPicker();
    }

    function removeToken() {
        console.log('removeToken');
    }

    function closeFormField() {
        if (!editForm) {
            props.formstep.form.steps[props.index].field.pop();
        }
        setShowFormModal(false);
    }

    function saveFormField() {
        if (!props.formstep.validate(props.selectedForm, props.index, selectedFindex)) {
            setShowFormModal(false);
        }
    }
    function removeResource(resource, index) {
        console.log('resourceeeeee');
        props.formstep.updateForm(props.selectedForm, resource, 'steps', resourceType, props.index, -1, index);
        // props.formstep.form.steps[props.index][`${resource}`].splice(index, 1);
    }

    function handleRows() {
        let spareArr = [];
        props.formstep.form.steps[props.index].field.forEach((field, i) => {
            spareArr.push([
                field.field_title,
                field.field_type,
                field.display_order,
                <Button onClick={() => (editFormModal(true, i))}> <Icon source={DraftOrdersMajor} color="base" /></Button>,
                <Button ><Icon source={DeleteMajor} color="base" /></Button>,
            ]);
        })
        return spareArr;
    }

    function editQuantity(type, p_i, v_i, quantity) {
        if (type == 'Product') {
            console.log(props.formstep.form.steps[props.index].selected_products[p_i]);
            const obj = Object.assign({}, props.formstep.form);

            obj.steps[props.index].selected_products[p_i].variants[v_i].quantity = parseInt(quantity);

            console.log(obj);
            props.formstep.setData(obj)
        }
    }

    const onChangeContent = (editorState) => {
        props.formstep.updateForm(props.selectedForm, editorState, 'steps', 'content_data', props.index)
    };

    function showThemeAssets() {
        setloadAsset(2);
        axios({
            url: '/api/fetch-assets',
            method: 'GET'
        })
            .then((res) => {
                if (res.data.isSuccess == true) {
                    setAssets(res.data.data);
                    assets.length > 0 ? setloadAsset(1) : setloadAsset(0)
                }
            })
            .catch((err) => {
                console.log(err);
            })
    }

    const tabs = [
        {
            id: 'use-collection',
            content: 'Use Collection',
            accessibilityLabel: 'Use Collection',
        },
        {
            id: 'use-selected-product',
            content: 'Use Selected products',
            accessibilityLabel: 'Use Selected products',
        }];

    useEffect(() => {
        console.log('reloaded');
    }, []);

    return (
        <div className='form_custom'>
            <Modal
                title="Add a field"
                open={showFormModal}
                primaryAction={{
                    content: !editForm ? 'Add Field' : 'Save',
                    onAction: saveFormField,
                }}
                secondaryActions={[
                    {
                        content: 'Cancel',
                        onAction: closeFormField,
                    },
                ]}
                onClose={closeFormField}
            >
                <Modal.Section><AddField form={props} formindex={selectedFindex} editForm={editForm} /></Modal.Section>
            </Modal>

            {productPicker ?
                <ResourcePicker
                    resourceType="Product"
                    actionVerb="select"
                    showVariants={true}
                    selectMultiple={true}
                    open={productPicker}
                    onSelection={handleSelection}
                    onCancel={closeCollectionPicker}
                    initialSelectionIds={initialSelectedId}
                />
                : ''
            }
            {collectionPicker ?
                <ResourcePicker
                    resourceType="Collection"
                    actionVerb="select"
                    selectMultiple={collectionSelectMultiple}
                    open={collectionPicker}
                    onSelection={handleSelection}
                    onCancel={closeCollectionPicker}
                    initialSelectionIds={initialSelectedId}
                /> : ''
            }
            <Card.Section>

                <ul className="breadcrumb">
                    <li><a href="#">My Builder</a></li>
                    <Icon
                        source={ChevronRightMinor}
                        color="base"
                    />
                    <li><a href="#" className='active'>
                        {props.selectedForm === 'product' ?
                            <span>Add a Product Steps</span>
                            : props.selectedForm === 'pre_selected' ?
                                <span>Add Products to be pre-selected</span>
                                : props.selectedForm === 'form' ?
                                    <span>Adding a Form Step</span>
                                    : props.selectedForm === 'subscription' ?
                                        <span>Adding a Subscription Step</span>
                                        : props.selectedForm === 'content' ?
                                            <span>A few things to note before setting up a Content Step.</span>
                                            : ""
                        }
                    </a></li>
                </ul>

                <div className='builder_settings_accrodian'>
                    <ul className='list-unstyled'>

                        <li className={`builder_settings_accrodian_list accrodian_active mt-30`} >

                            <div className='builder_settings_accrodian_title'>
                                {props.selectedForm === 'product' ?
                                    <Heading>Adding a Product Step</Heading>
                                    : props.selectedForm === 'pre_selected' ?
                                        <Heading>Add Products to be pre-selected</Heading>
                                        : props.selectedForm === 'form' ?
                                            <Heading>Adding a Form Step</Heading>
                                            : props.selectedForm === 'subscription' ?
                                                <Heading>Adding a Subscription Step</Heading>
                                                : props.selectedForm === 'content' ?
                                                    <Heading>A few things to note before setting up a Content Step.</Heading>
                                                    : ""
                                }
                            </div>

                            <div className='builder_settings_accrodian_content progress_checkbox'>

                                <FormLayout>

                                    {props.selectedForm === 'subscription' ?
                                        <Banner
                                            title="Setting Up Subscription Plans"
                                            status="info"
                                        >
                                            <p>Please note that you will need to set up subscription plans using your preferred subscription
                                                app. Once they have been setup, you can link the plan to the product generated for this
                                                builder. All linked subscription plans/options will show within this step.</p>
                                        </Banner>
                                        : ''
                                    }
                                    {props.selectedForm === 'content' ?
                                        <Banner
                                            title="A few things to note before setting up a Content Step."
                                            status="info"
                                        >
                                            <List type="bullet">
                                                <List.Item>Any images that are added to the content, that are chosen from your device, will be uploaded to your theme's assets.</List.Item>
                                                <List.Item>The fonts will be based on the fonts provided by your theme, so there are no font options here.</List.Item>
                                                <List.Item>Unless you specify font size here, it will take the default size of your theme.</List.Item>
                                                <List.Item>You may need to be familiar with CSS to achieve a more advanced design. You can use the 'Code View' in the editor to add CSS and custom classes.</List.Item>
                                            </List>
                                        </Banner>
                                        : ''
                                    }
                                    <TextField
                                        label="Step Title"
                                        placeholder="e.g. Select Your Hard Drive"
                                        autoComplete="off"
                                        value={props.formstep.form.steps[props.index].title}
                                        onChange={value => props.formstep.updateForm(props.selectedForm, value, 'steps', 'title', props.index)}
                                        error={(typeof props.formstep.errors.steps != 'undefined') ? props.formstep.errors.steps[props.index].title : ''}
                                    />
                                    <TextField
                                        label="Step Description"
                                        placeholder="e.g. You can only select one hard drive "
                                        autoComplete="off"
                                        helpText="Additional text to help the customer understand what's required from this step."
                                        value={props.formstep.form.steps[props.index].description}
                                        onChange={value => props.formstep.updateForm(props.selectedForm, value, 'steps', 'description', props.index)}
                                    />
                                    {props.selectedForm !== 'pre_selected' && props.selectedForm !== 'content' ?
                                        <TextField
                                            label="Step Short Title"
                                            placeholder="e.g. Gift Box "
                                            autoComplete="off"
                                            helpText="If you're using the step progress bar, you can add a shorter title for this step to show there."
                                            value={props.formstep.form.steps[props.index].short_title}
                                            onChange={value => props.formstep.updateForm(props.selectedForm, value, 'steps', 'short_title', props.index)}
                                        />
                                        :
                                        ''
                                    }
                                    {props.selectedForm === 'product' ?
                                        <Checkbox
                                            label="Allow more than 1 of the same product to be selected"
                                            helpText="The maximum quantity allowed, per item, will take into account your settings below."
                                            onChange={value => props.formstep.updateForm(props.selectedForm, value, 'steps', 'is_allow_more_than_one', props.index)}
                                            checked={props.formstep.form.steps[props.index].is_allow_more_than_one}
                                        />
                                        : ''
                                    }
                                    <FormLayout.Group>
                                        <TextField
                                            label="Display Order"
                                            type="number"
                                            autoComplete="off"
                                            placeholder="e.g. 1"
                                            helpText="Set the order the step will display within this builder."
                                            value={props.formstep.form.steps[props.index].display_order.toString()}
                                            onChange={value => props.formstep.updateForm(props.selectedForm, value, 'steps', 'display_order', props.index)}
                                            error={(typeof props.formstep.errors.steps != 'undefined') ? props.formstep.errors.steps[props.index].display_order : ''}
                                        />
                                        {props.selectedForm === 'pre_selected' ?
                                            <Select
                                                label="Products per row"
                                                options={productPerRow}
                                                value={props.formstep.form.steps[props.index].products_per_row}
                                                onChange={value => props.formstep.updateForm(props.selectedForm, value, 'steps', 'products_per_row', props.index)}
                                            />
                                            : ''
                                        }
                                        {props.selectedForm === 'pre_selected' ?
                                            <TextField
                                                label="Button Text"
                                                type="text"
                                                autoComplete="off"
                                                placeholder="e.g. Included"
                                                helpText="These buttons are disabled for pre-selected products."
                                                value={props.formstep.form.steps[props.index].button_text}
                                                onChange={value => props.formstep.updateForm(props.selectedForm, value, 'steps', 'button_text', props.index)}
                                                error={(typeof props.formstep.errors.steps != 'undefined') ? props.formstep.errors.steps[props.index].display_order : ''}
                                            />
                                            : ''
                                        }
                                        {props.selectedForm === 'pre_selected' ?
                                            <Checkbox
                                                label="Show Prices"
                                                helpText="Removes the price from the button but still charges for the product."
                                                value={props.formstep.form.steps[props.index].is_show_price}
                                                onChange={value => props.formstep.updateForm(props.selectedForm, value, 'steps', 'is_show_price', props.index)}
                                            />
                                            : ''
                                        }
                                        {props.selectedForm === 'product' ?
                                            <TextField
                                                label="Minimum Selections"
                                                type="number"
                                                autoComplete="off"
                                                placeholder="e.g. 1"
                                                helpText="How many products does the customer have to select on this step?"
                                                value={props.formstep.form.steps[props.index].minimum_selection.toString()}
                                                onChange={value => props.formstep.updateForm(props.selectedForm, value, 'steps', 'minimum_selection', props.index)}
                                                error={(typeof props.formstep.errors.steps != 'undefined') ? props.formstep.errors.steps[props.index].minimum_selection : ''}
                                            />
                                            : ''
                                        }
                                        {props.selectedForm === 'product' ?
                                            <TextField
                                                label="Maximum Selections"
                                                type="number"
                                                autoComplete="off"
                                                placeholder="e.g. 1"
                                                helpText="Can the customer select more than 1 product on this step?"
                                                value={props.formstep.form.steps[props.index].maximum_selection.toString()}
                                                onChange={value => props.formstep.updateForm(props.selectedForm, value, 'steps', 'maximum_selection', props.index)}
                                                error={(typeof props.formstep.errors.steps != 'undefined') ? props.formstep.errors.steps[props.index].maximum_selection : ''}
                                            />
                                            : ''
                                        }
                                    </FormLayout.Group>
                                </FormLayout>
                                <Card.Section>
                                    <FormLayout>
                                        <FormLayout.Group>
                                            {props.selectedForm !== 'pre_selected' && props.selectedForm !== 'content' ?
                                                <Checkbox
                                                    label="A Required Step"
                                                    helpText="Requires at least 1 item selection from the customer"
                                                    onChange={value => props.formstep.updateForm(props.selectedForm, value, 'steps', 'is_required_step', props.index)}
                                                    checked={props.formstep.form.steps[props.index].is_required_step}
                                                />
                                                : ''
                                            }
                                            {props.selectedForm !== 'content' ?
                                                <Checkbox
                                                    label="Show Box Contents/Summary"
                                                    helpText="This will show the currently selected products below the step title."
                                                    onChange={value => props.formstep.updateForm(props.selectedForm, value, 'steps', 'is_show_box_content', props.index)}
                                                    checked={props.formstep.form.steps[props.index].is_show_box_content}
                                                />
                                                : ''
                                            }
                                        </FormLayout.Group>
                                        <Checkbox
                                            label="Hide from Step Progress"
                                            helpText="Hide this step from the list of steps in the progress bar."
                                            onChange={value => props.formstep.updateForm(props.selectedForm, value, 'steps', 'is_hide_from_step_progress', props.index)}
                                            checked={props.formstep.form.steps[props.index].is_hide_from_step_progress}
                                        />
                                        {props.selectedForm === 'product' ?
                                            <FormLayout.Group>
                                                <RadioButton
                                                    label="Show each variant separately"
                                                    helpText="Each variant will have it's own item available for selection."
                                                    id="seperate-variant"
                                                    name="productStepVariant"
                                                    checked={props.formstep.form.steps[props.index].show_variant_as === 0}
                                                    onChange={value => props.formstep.updateForm(props.selectedForm, 0, 'steps', 'show_variant_as', props.index)}
                                                />
                                                <RadioButton
                                                    label="Show a variant select box beneath product"
                                                    helpText="One item per product with a dropdown menu for the variant"
                                                    id="beneath-product"
                                                    name="productStepVariant"
                                                    checked={props.formstep.form.steps[props.index].show_variant_as === 1}
                                                    onChange={value => props.formstep.updateForm(props.selectedForm, 1, 'steps', 'show_variant_as', props.index)}
                                                />
                                            </FormLayout.Group>
                                            : ''
                                        }

                                    </FormLayout>
                                </Card.Section>

                                {props.selectedForm === 'product' || props.selectedForm === 'pre_selected' ?
                                    <div>
                                        <Button onClick={() => openPicker('selected_products', 'add')}>Choose selectable products for this step</Button>
                                        <Banner
                                            title="Link a collection to products you wish to show"
                                            status="info"
                                        >
                                            <p>Products with no collections will not show. If you have products like this, simply create a collection and attach all products before completing this step..</p>
                                        </Banner>
                                        <Tabs tabs={tabs} selected={selectedTab} onSelect={handleTabChange}>
                                            {(selectedTab == 0) ? <Button onClick={() => openPicker('collections', 'add')}>Pick Collection</Button> : ''}

                                            <table className='Polaris-DataTable__Table'>
                                                {(selectedTab == 0) ?
                                                    <tbody>
                                                        {props.formstep.form.steps[props.index]['collections'].map((item, index) => {
                                                            if (props.formstep.form.steps[props.index].type == 'product') {
                                                                return (
                                                                    <tr key={index}>
                                                                        <td className='Polaris-DataTable__Cell resource_img'>
                                                                            {(item.image != '') ?
                                                                                <img src={item.image} alt="" />
                                                                                :
                                                                                <img src="/images/static/placeholder.png" alt="" />
                                                                            }
                                                                        </td>
                                                                        <td className='Polaris-DataTable__Cell resource_title'>
                                                                            <TextContainer spacing="tight">
                                                                                <Heading>{item.title} selected</Heading>
                                                                                <p>
                                                                                    Products will load in the order defined in the collection.
                                                                                </p>
                                                                            </TextContainer>

                                                                        </td>
                                                                        <td className='Polaris-DataTable__Cell'>
                                                                            <Link type="a" className='builder_primary_btn' onClick={() => removeResource('collections', index)}>Remove</Link>
                                                                        </td>
                                                                    </tr>
                                                                )
                                                            }
                                                            if (props.formstep.form.steps[props.index].type == 'pre_selected') {
                                                                return (
                                                                    <tr key={index}>
                                                                        <td className='Polaris-DataTable__Cell resource_img'>
                                                                            {(item.image != '') ?
                                                                                <img src={item.image} alt="" />
                                                                                :
                                                                                <img src="/images/static/placeholder.png" alt="" />
                                                                            }
                                                                        </td>
                                                                        <td className='Polaris-DataTable__Cell resource_title'>
                                                                            <TextContainer spacing="tight">
                                                                                <Heading>{item.title} selected</Heading>
                                                                                <p>
                                                                                    Products will load in the order defined in the collection.
                                                                                </p>
                                                                            </TextContainer>

                                                                            <TextField
                                                                                label="Quantity"
                                                                                placeholder='Add Quantity'
                                                                                value={props.formstep.form.steps[props.index].selected_products[index].quantity}
                                                                                onChange={(e) => { editQuantity('Collection', index, e) }}
                                                                                type='number'
                                                                            />

                                                                        </td>
                                                                        <td className='Polaris-DataTable__Cell'>
                                                                            <Link type="a" className='builder_primary_btn' onClick={() => removeResource('collections', index)}>Remove</Link>
                                                                        </td>
                                                                    </tr>
                                                                )
                                                            }
                                                        })}
                                                    </tbody>
                                                    :

                                                    <tbody>
                                                        {props.formstep.form.steps[props.index]['selected_products'].map((item, index) => {
                                                            if (props.formstep.form.steps[props.index].type == 'product') {
                                                                return (
                                                                    <tr key={index}>
                                                                        <td className='Polaris-DataTable__Cell resource_img'>
                                                                            {(item.image != '') ?
                                                                                <img src={item.image} alt="" />
                                                                                :
                                                                                <img src="/images/static/placeholder.png" alt="" />
                                                                            }
                                                                        </td>
                                                                        <td className='Polaris-DataTable__Cell resource_title'>
                                                                            <TextContainer spacing="tight">
                                                                                <Heading>{item.title}</Heading>
                                                                                <p>
                                                                                    {item.variants.length} variant(s) available for selection.
                                                                                </p>
                                                                            </TextContainer>

                                                                        </td>
                                                                        <td className='Polaris-DataTable__Cell'>
                                                                            <Link type="a" className='builder_primary_btn' onClick={() => removeResource('selected_products', index)}>Remove</Link>
                                                                        </td>
                                                                    </tr>
                                                                )
                                                            }

                                                            if (props.formstep.form.steps[props.index].type == 'pre_selected') {
                                                                return (
                                                                    <React.Fragment key={index}>
                                                                        {props.formstep.form.steps[props.index]['selected_products'][index]['variants'].map((v, vindex) => {
                                                                            return (
                                                                                <tr key={vindex}>
                                                                                    <td className='Polaris-DataTable__Cell resource_img'>
                                                                                        {(item.image != '') ?
                                                                                            <img src={item.image} alt="" />
                                                                                            :
                                                                                            <img src="/images/static/placeholder.png" alt="" />
                                                                                        }
                                                                                    </td>
                                                                                    <td className='Polaris-DataTable__Cell resource_title'>
                                                                                        <TextContainer spacing="tight">
                                                                                            <Heading>{item.title} - {v.displayName}</Heading>
                                                                                            <p>
                                                                                                {item.variants.length} variant(s) available for selection.
                                                                                            </p>
                                                                                        </TextContainer>

                                                                                        {/* <TextField
                                                                        label="Quantity"
                                                                        placeholder='Add Quantity'
                                                                        value={props.formstep.form.steps[props.index].selected_products[index].quantity}
                                                                        onChange={(e) => { editQuantity('Product', index, e) }}
                                                                        type='number'
                                                                    /> */}

                                                                                        <input placeholder='Add Quantity'
                                                                                            value={props.formstep.form.steps[props.index].selected_products[index].variants[vindex].quantity}
                                                                                            onChange={(e) => { editQuantity('Product', index, vindex, e.target.value) }}
                                                                                            type='number' />
                                                                                    </td>
                                                                                    <td className='Polaris-DataTable__Cell'>
                                                                                        <Link type="a" className='builder_primary_btn' onClick={() => removeResource('selected_products', index)}>Remove</Link>
                                                                                    </td>
                                                                                </tr>
                                                                            )
                                                                        })}
                                                                    </React.Fragment>
                                                                )

                                                            }

                                                        })}
                                                    </tbody>
                                                }
                                            </table>
                                        </Tabs>
                                    </div>
                                    : ''
                                }
                                {props.selectedForm === 'form' ?
                                    <div>
                                        {
                                            <FormLayout>
                                                <DataTable
                                                    columnContentTypes={[
                                                        'text',
                                                        'numeric',
                                                        'numeric',
                                                        'numeric',
                                                        'numeric',
                                                    ]}
                                                    headings={[
                                                        'Field',
                                                        'Type',
                                                        'Order',
                                                        'Edit',
                                                        'Remove',
                                                    ]}
                                                    rows={(handleRows())}
                                                    footerContent={`${(handleRows()).length} fields added`}
                                                />
                                            </FormLayout>
                                        }

                                        <Button onClick={() => formModal(true, true)}>Add a field</Button>
                                        <InlineError message={(typeof props.formstep.errors.steps != 'undefined' && !showFormModal) ? props.formstep.errors.steps[props.index].field : ''} />
                                    </div>
                                    : ''
                                }
                                {props.selectedForm === 'product' ?
                                    <FormLayout>
                                        <FormLayout.Group>
                                            <Checkbox
                                                label="Show sort by drop down menu"
                                                helpText="Allows customers to sort by product title, collections and price"
                                                onChange={value => props.formstep.updateForm(props.selectedForm, value, 'steps', 'is_show_sortby_dropdown', props.index)}
                                                checked={props.formstep.form.steps[props.index].is_show_sortby_dropdown}
                                            />
                                            <Checkbox
                                                label="Show title filter"
                                                helpText="Allows customers to filter by product title for this step"
                                                onChange={value => props.formstep.updateForm(props.selectedForm, value, 'steps', 'is_show_title_filter', props.index)}
                                                checked={props.formstep.form.steps[props.index].is_show_title_filter}
                                            />
                                        </FormLayout.Group>
                                        <FormLayout.Group >

                                            <Checkbox
                                                label="Show collection filter"
                                                helpText="Allows customers to filter by collection for this step"
                                                onChange={value => props.formstep.updateForm(props.selectedForm, value, 'steps', 'is_show_collection_filter', props.index)}
                                                checked={props.formstep.form.steps[props.index].is_show_collection_filter}
                                            />

                                            <Checkbox
                                                label="Show tags filter"
                                                helpText="Allows customers to sort by product tags"
                                                onChange={value => props.formstep.updateForm(props.selectedForm, value, 'steps', 'is_show_tags_filter', props.index)}
                                                checked={props.formstep.form.steps[props.index].is_show_tags_filter}
                                            />

                                        </FormLayout.Group>
                                        <FormLayout.Group>
                                            {(props.formstep.form.steps[props.index].is_show_collection_filter) ?
                                                <Button onClick={() => openPicker('collections_filter', 'add')}>Set collections for the collection filter</Button>
                                                : ''}

                                            {(props.formstep.form.steps[props.index].is_show_tags_filter) ?
                                                <TextField
                                                    label="Set the tags to show in the drop down menu"
                                                    placeholder="e.g. Bags, Shoes, Necklaces"
                                                    autoComplete="off"
                                                    helpText="A comma-separated list of tags to show within the drop down menu."
                                                    value={props.formstep.form.steps[props.index].dropdown_tags}
                                                    onChange={value => props.formstep.updateForm(props.selectedForm, value, 'steps', 'dropdown_tags', props.index)}
                                                /> : ''}
                                        </FormLayout.Group>
                                    </FormLayout>
                                    : ''
                                }
                                {props.selectedForm === 'content' ?
                                    <FormLayout>
                                        <Editor
                                            value={props.formstep.form.steps[props.index].content_data}
                                            onEditorChange={onChangeContent}
                                            init={{
                                                height: 500,
                                                menubar: true,
                                                plugins: [
                                                    'advlist', 'autolink', 'lists', 'link', 'image', 'charmap',
                                                    'anchor', 'searchreplace', 'visualblocks', 'code', 'fullscreen',
                                                    'insertdatetime', 'media', 'table', 'preview', 'help', 'wordcount'
                                                ],
                                                toolbar: `undo redo | blocks fontfamily fontsize | 
                                                bold italic underline strikethrough | 
                                                alignleft aligncenter alignright alignjustify | 
                                                outdent indent codesample| 
                                                numlist bullist checklist | 
                                                forecolor backcolor casechange permanentpen formatpainter removeformat |
                                                pagebreak |
                                                charmap emoticons | 
                                                fullscreen  preview save print code |
                                                insertfile image media pageembed template link anchor codesample | 
                                                a11ycheck ltr rtl | 
                                                showcomments addcomment`,
                                                content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'

                                            }}
                                        />

                                        <Button onClick={showThemeAssets}>Show Your Theme's Image Assets with URL's</Button>
                                        <div>
                                            {
                                                loadAsset == 0 ? <></> : <Spinner accessibilityLabel="Spinner example" size="large" />
                                            }
                                            {
                                                assets.map((asset, i) => {
                                                    return (
                                                        <div className='asset__Preview-Box' key={i}>
                                                            <div >
                                                                <span className="Polaris-Thumbnail Polaris-Thumbnail--sizeMedium">
                                                                    <img src={asset.public_url} className="Polaris-Thumbnail__Image" />
                                                                </span>
                                                            </div>
                                                            <div>
                                                                <Button plain>{asset.public_url}</Button>
                                                            </div>
                                                            <div>
                                                                <Button plain onClick={() => { navigator.clipboard.writeText(asset.public_url) }}>Copy Url</Button>
                                                            </div>
                                                        </div>
                                                    );
                                                })
                                            }
                                        </div>

                                    </FormLayout>
                                    : ''
                                }
                            </div>

                        </li >
                    </ul>
                </div>

            </Card.Section>
        </div>
    );
}

export default AddSteps;
