import {
    Page,
    Card, FormLayout, Heading, Button, ButtonGroup
} from '@shopify/polaris';
import { useState } from "react";
import caretArrow from '../../../../../snippets/caretArrow.svg'

function ProductSteps(props) {

    const [active, setActive] = useState(false);

    return (
        <li className={`builder_settings_accrodian_list ${active ? 'accrodian_active' : ''}`} >

            <div className='builder_settings_accrodian_title'>
                <Heading>Add a Product Step</Heading>
                <div className='accrodian_caret' onClick={() => { setActive(!active) }}>
                    <img src={caretArrow} alt="Caret icon" />
                </div>
            </div>

            <div className='builder_settings_accrodian_content' style={{padding : '0px'}}>
                <Card.Section>
                    <FormLayout>
                        <ButtonGroup>
                            <Button onClick={() => props.showHideForm('show', 'product', 0, 1)}>Add a Product Selection Step</Button>
                            <Button onClick={() => props.showHideForm('show', 'pre_selected', 0, 1)}>Add a Pre-Selected Product Step </Button>
                        </ButtonGroup>
                    </FormLayout>
                </Card.Section>
            </div>
            
        </li>
    );
}

export default ProductSteps;
