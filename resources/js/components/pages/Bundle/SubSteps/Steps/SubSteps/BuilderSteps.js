import {
    Page,
    Card, Heading, Subheading, TextContainer, FormLayout, Banner, DataTable, Button, Icon
} from '@shopify/polaris';
import React, { useState } from "react";
import {
    DraftOrdersMajor, DeleteMajor
} from '@shopify/polaris-icons';
import caretArrow from '../../../../../snippets/caretArrow.svg'

function BuilderSteps(props) {
    const rows = props.stepsData.form.steps;
    const [active, setActive] = useState(true);

    function removeStep(id, index) {
        props.stepsData.form.steps.splice(index, 1);
        props.stepsData.updateForm('steps', props.stepsData.form.steps)

        let spareObj = Object.assign({}, props.stepsData.form);
        spareObj.deletedSteps.push(id);
        props.stepsData.updateForm('deletedSteps', spareObj.deletedSteps)
    }
    return (
        <li className={`builder_settings_accrodian_list ${active ? 'accrodian_active' : ''}`} >

            <div className='builder_settings_accrodian_title'>
                <Heading>Builder Steps</Heading>
                <div className='accrodian_caret' onClick={() => { setActive(!active) }}>
                    <img src={caretArrow} alt="Caret icon" />
                </div>
            </div>

            <div className='builder_settings_accrodian_content' style={{ padding: '0' }}>
                <Card.Section>
                    <FormLayout>
                        <TextContainer spacing="tight">
                            <p>Add the steps in the order you wish for them to appear to the customer.</p>
                        </TextContainer>
                        <div className='force-radius-8'>
                            <Banner
                                title="Form Steps"
                                status="info"
                            >
                                <p>A form step will allow you to collect custom information from the customer. This could be an engraving, a personalised note or anything you like. This information you collect will appear on the order.
                                </p>
                            </Banner>
                        </div>
                    </FormLayout>
                    <div className="pt-5">
                        <FormLayout>
                            <FormLayout.Group>
                                <Card>
                                    <table className='Polaris-DataTable__Table'>
                                        <thead>
                                            <tr>
                                                <th className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn Polaris-DataTable__Cell--header'>Step</th>
                                                <th className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn Polaris-DataTable__Cell--header text-right pr-0'>Step #</th>
                                                <th className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn Polaris-DataTable__Cell--header'>
                                                    <div className='stepActionHeading'>
                                                        Action
                                                    </div>
                                                </th>
                                            </tr>
                                        </thead>

                                        {(rows.length > 0) ?
                                            <tbody>
                                                {rows.map((item, index) => {
                                                    return (
                                                        <tr key={index}>
                                                            <td className='Polaris-DataTable__Cell'>{item.title}</td>
                                                            <td className='Polaris-DataTable__Cell text-right'>{item.display_order}</td>
                                                            <td className='Polaris-DataTable__Cell stepsActions'>

                                                                <Button outline type="button" onClick={() => props.showHideForm('show', item.type, 0, 0, index)}>
                                                                    Edit
                                                                </Button>
                                                                <div style={{ color: '#D72C0D', display: 'inline-block' }}>
                                                                    <Button monochrome outline type="button" onClick={() => removeStep(item.id, index)}>
                                                                        Delete
                                                                        {/* <Icon source={DeleteMajor} color="base" /> */}
                                                                    </Button>
                                                                </div>

                                                            </td>
                                                        </tr>
                                                    )
                                                })}
                                            </tbody>
                                            :
                                            <tbody>
                                                <tr><td className='Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop text-center'
                                                    colSpan='3'>{rows.length} steps added</td></tr>
                                            </tbody>
                                        }
                                    </table>
                                </Card>
                            </FormLayout.Group>
                        </FormLayout>
                    </div>
                </Card.Section>
            </div>

        </li>
    );
}

export default BuilderSteps;
