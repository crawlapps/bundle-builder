import {
    Card,
    FormLayout,
    Heading,
    ButtonGroup,
    Button
} from '@shopify/polaris';
import { useState } from 'react';
import caretArrow from '../../../../../snippets/caretArrow.svg'

function OtherSteps(props) {
    const [active, setActive] = useState(false);

    return (
        <li className={`builder_settings_accrodian_list ${active ? 'accrodian_active' : ''}`} >

            <div className='builder_settings_accrodian_title'>
                <Heading>Other Step Options</Heading>
                <div className='accrodian_caret' onClick={() => { setActive(!active) }}>
                    <img src={caretArrow} alt="Caret icon" />
                </div>
            </div>

            <div className='builder_settings_accrodian_content' style={{ padding: '0px' }}>
                <Card.Section>
                    <FormLayout>
                        <FormLayout.Group>
                            <ButtonGroup>
                                <Button onClick={() => props.showHideForm('show', 'form', 0, 1)}>Add a Form Step </Button>
                                {/* <Button onClick={() => props.showHideForm('show', 'subscription', 0, 1)}>Add a Subscription Step </Button> */}
                                <Button onClick={() => props.showHideForm('show', 'content', 0, 1)}>Add a Content/HTML Step </Button>
                            </ButtonGroup>
                        </FormLayout.Group>
                    </FormLayout>
                </Card.Section>
            </div>

        </li>
    );
}

export default OtherSteps;
