import {
    Page,
    Card,
    FormLayout,
    Heading,
    ButtonGroup,
    Button, TextField, Checkbox, RadioButton, Tabs, DataTable, Banner
} from '@shopify/polaris';
import React from "react";

function AddSubscriptionSteps(){

    const rows = [];
    return (
        <Card.Section>
            <FormLayout>
                <Heading>Adding a Subscription Step</Heading>
                <Banner
                    title="Setting Up Subscription Plans"
                    status="info"
                >
                    <p>Please note that you will need to set up subscription plans using your preferred subscription app. Once they have been setup, you can link the plan to the product generated for this builder. All linked subscription plans/options will show within this step.</p>
                </Banner>
                <TextField
                    label="Step Title"
                    placeholder="e.g. Some information about you"
                    autoComplete="off"
                />
                <TextField
                    label="Step Description"
                    placeholder="e.g. You can personalize this order by providing your information below. "
                    autoComplete="off"
                    helpText="Additional text to help the customer understand what's required from this step."
                />
                <TextField
                    label="Step Short Title"
                    placeholder="e.g. Gift Box "
                    autoComplete="off"
                    helpText="If you're using the step progress bar, you can add a shorter title for this step to show there."
                />
                <FormLayout.Group>
                    <TextField
                        label="Display Order"
                        type="number"
                        autoComplete="off"
                        placeholder="e.g. 1"
                        helpText="Set the order the step will display within this builder."
                    />
                    <Checkbox
                        label="A Required Step"
                        helpText="Requires at least 1 item selection from the customer"
                    />
                    <Checkbox
                        label="Show Box Contents/Summary"
                        helpText="This will show the currently selected products below the step title."
                    />
                </FormLayout.Group>
                <FormLayout.Group>
                    <Checkbox
                        label="Hide from Step Progress"
                        helpText="Hide this step from the list of steps in the progress bar."
                    />
                </FormLayout.Group>
            </FormLayout>
            <Button>Add a field</Button>

            <FormLayout>
                <DataTable
                    columnContentTypes={[
                        'text',
                        'numeric',
                        'numeric',
                        'numeric',
                        'numeric',
                    ]}
                    headings={[
                        'Field',
                        'Type',
                        'Order',
                        'Edit',
                        'Remove',
                    ]}
                    rows={rows}
                    footerContent={`${rows.length} fields added`}
                />
            </FormLayout>
        </Card.Section>
    );
}

export default AddSubscriptionSteps;
