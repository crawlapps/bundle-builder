import {
    Page,
    Card,
    FormLayout,
    Heading,
    ButtonGroup,
    Button, TextField, Checkbox, RadioButton, Tabs
} from '@shopify/polaris';
import React from "react";

function AddPreSelectedProductSteps(){
    const tabs = [
        {
            id: 'use-collection',
            content: 'Use Collection',
            accessibilityLabel: 'Use Collection',
        }];
    return (
        <Card.Section>
            <FormLayout>
                <Heading>Add Products to be pre-selected</Heading>
                <p>
                    The products you choose here will show up as pre-selected and cannot be removed by the customer.
                </p>
                <TextField
                    label="Step Title"
                    placeholder="e.g. Select Your Hard Drive"
                    autoComplete="off"
                />
                <TextField
                    label="Step Description"
                    placeholder="e.g. You can only select one hard drive "
                    autoComplete="off"
                    helpText="Additional text to help the customer understand what's required from this step."
                />
                <TextField
                    label="Display Order"
                    placeholder="e.g. 1 "
                    autoComplete="off"
                    helpText="Set the order the step will display within this builder."
                />
                <FormLayout.Group>
                    <TextField
                        label="Button Text"
                        type="text"
                        autoComplete="off"
                        placeholder="e.g. Included"
                        helpText="These buttons are disabled for pre-selected products."
                    />
                    <Checkbox
                        label="Show Prices"
                        helpText="Removes the price from the button but still charges for the product."
                    />
                </FormLayout.Group>
                <FormLayout.Group>
                    <Checkbox
                        label="Show Box Contents/Summary"
                        helpText="This will show the currently selected products below the step title."
                    />
                    <Checkbox
                        label="Hide from Step Progress"
                        helpText="Hide this step from the list of steps in the progress bar."
                    />
                </FormLayout.Group>
            </FormLayout>
            <Button>Pick Collection</Button>
        </Card.Section>
    );
}

export default AddPreSelectedProductSteps;
