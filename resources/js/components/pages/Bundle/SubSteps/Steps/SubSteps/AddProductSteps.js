import {
    Page,
    Card,
    FormLayout,
    Heading,
    ButtonGroup,
    Button, TextField, Checkbox, RadioButton, Tabs
} from '@shopify/polaris';
import React from "react";

function AddProductSteps(props){
    const tabs = [
        {
            id: 'use-collection',
            content: 'Use Collection',
            accessibilityLabel: 'Use Collection',
        }];
    return (
        <Card.Section>
            <FormLayout>
                    <Heading>Adding a Product Step</Heading>
                    <TextField
                        label="Step Title"
                        placeholder="e.g. Select Your Hard Drive"
                        autoComplete="off"
                        value={props.formstep.form.steps.product[props.index].title}
                        onChange={value => props.formstep.updateForm('product', value, 'steps', 'title', props.index)}
                    />
                    <TextField
                        label="Step Description"
                        placeholder="e.g. You can only select one hard drive "
                        autoComplete="off"
                        helpText="Additional text to help the customer understand what's required from this step."
                        value={props.formstep.form.steps.product[props.index].description}
                        onChange={value => props.formstep.updateForm('product', value, 'steps', 'description', props.index)}
                    />
                    <TextField
                        label="Step Short Title"
                        placeholder="e.g. Gift Box "
                        autoComplete="off"
                        helpText="If you're using the step progress bar, you can add a shorter title for this step to show there."
                        value={props.formstep.form.steps.product[props.index].short_title}
                        onChange={value => props.formstep.updateForm('product', value, 'steps', 'short_title', props.index)}
                    />
                    <Checkbox
                        label="Allow more than 1 of the same product to be selected"
                        helpText="The maximum quantity allowed, per item, will take into account your settings below."
                    />
                    <FormLayout.Group>
                        <TextField
                            label="Display Order"
                            type="number"
                            autoComplete="off"
                            placeholder="e.g. 1"
                            helpText="Set the order the step will display within this builder."
                        />
                        <TextField
                            label="Minimum Selections"
                            type="number"
                            autoComplete="off"
                            placeholder="e.g. 1"
                            helpText="How many products does the customer have to select on this step?"
                        />
                        <TextField
                            label="Maximum Selections"
                            type="number"
                            autoComplete="off"
                            placeholder="e.g. 1"
                            helpText="Can the customer select more than 1 product on this step?"
                        />
                    </FormLayout.Group>
                </FormLayout>
            <Card.Section>
                <FormLayout>
                <FormLayout.Group>
                    <Checkbox
                        label="A Required Step"
                        helpText="Requires at least 1 item selection from the customer"
                    />
                    <Checkbox
                        label="Show Box Contents/Summary"
                        helpText="This will show the currently selected products below the step title."
                    />
                </FormLayout.Group>
                <Checkbox
                    label="Hide from Step Progress"
                    helpText="Hide this step from the list of steps in the progress bar."
                />
                <FormLayout.Group>
                    <RadioButton
                        label="Show each variant separately"
                        helpText="Each variant will have it's own item available for selection."
                        id="seperate-variant"
                        name="productStepVariant"
                    />
                    <RadioButton
                        label="Show a variant select box beneath product"
                        helpText="One item per product with a dropdown menu for the variant"
                        id="beneath-product"
                        name="productStepVariant"
                    />
                </FormLayout.Group>
            </FormLayout>
            </Card.Section>

            <Tabs tabs={tabs} selected={0}>
                <Button>Pick Collection</Button>
            </Tabs>
            <FormLayout>
                <FormLayout.Group>
                    <Checkbox
                        label="Show sort by drop down menu"
                        helpText="Allows customers to sort by product title, collections and price"
                    />
                    <Checkbox
                        label="Show title filter"
                        helpText="Allows customers to filter by product title for this step"
                    />
                </FormLayout.Group>
                <FormLayout.Group>
                    <Checkbox
                        label="Show collection filter"
                        helpText="Allows customers to filter by collection for this step"
                    />
                </FormLayout.Group>
                <FormLayout.Group>
                    <Checkbox
                        label="Show tags filter"
                        helpText="Allows customers to sort by product tags"
                    />
                    <TextField
                        label="Set the tags to show in the drop down menu"
                        placeholder="e.g. Bags, Shoes, Necklaces"
                        autoComplete="off"
                        helpText="A comma-separated list of tags to show within the drop down menu."
                    />
                </FormLayout.Group>
            </FormLayout>
        </Card.Section>
    );
}

export default AddProductSteps;
