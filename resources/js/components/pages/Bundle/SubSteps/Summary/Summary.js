import {
    Page,
    Card, FormLayout, Banner, TextContainer, TextStyle, Heading
} from '@shopify/polaris';
import { useState } from 'react';
import { toastMessage, toastShow } from '../../../../AppbridgeActions/ToastActions';
import caretArrow from '../../../../snippets/caretArrow.svg'


function Summary(props) {
    console.log(props);
    const [usingProduct, setUsingProduct] = useState(false);
    const [usingLink, setUsingLink] = useState(true);

    return (
        <div className='card-wrapper-radius'>
            <Card>
                <div style={{ padding: '30px' }}>
                    <div className='builder_settings_accrodian'>
                        <ul className='list-unstyled'>
                            <li className={`builder_settings_accrodian_list ${usingProduct ? 'accrodian_active' : ''}`} >
                                <div className='builder_settings_accrodian_title'>
                                    <Heading>View Using a Product</Heading>
                                    <div className='accrodian_caret' onClick={() => { setUsingProduct(!usingProduct) }}>
                                        <img src={caretArrow} alt="Caret icon" />
                                    </div>
                                </div>
                                <div className='builder_settings_accrodian_content'>
                                    <Banner
                                        title="A product was created for this builder"
                                        action={{ content: 'View Product', onAction: () => { window.open(`http://${window.shopOrigin}/admin/products/${props.form.shopify_product_id}`, '_blank') } }}
                                        status="info"
                                    >
                                        <TextContainer>
                                            <p>We have created a product with the same name as this builder. You can link customers to this product to load the builder.</p>
                                            <p><TextStyle variation="strong">Please note</TextStyle> that editing or deleting this product will stop the builder from working.</p>
                                            <p>You can only edit this product to add a product image and description.</p>
                                        </TextContainer>
                                    </Banner>
                                </div>
                            </li>

                            <li className={`builder_settings_accrodian_list ${usingLink ? 'accrodian_active' : ''}`} >
                                <div className='builder_settings_accrodian_title'>
                                    <Heading>View Using a Link</Heading>
                                    <div className='accrodian_caret' onClick={() => { setUsingLink(!usingLink) }}>
                                        <img src={caretArrow} alt="Caret icon" />
                                    </div>
                                </div>
                                <div className='builder_settings_accrodian_content'>
                                    {(props.form.is_active) ?
                                        <Banner
                                            title="You can now view your builder on your store."
                                            action={{ content: 'View Builder', onAction: () => { window.open(`http://${window.shopOrigin}/apps/builder?b=${props.form.shopify_product_id}`, '_blank') } }}
                                            secondaryAction={{
                                                content: 'Copy Url',
                                                onAction: () => {
                                                    const elem = document.createElement('textarea');
                                                    elem.value = `http://${window.shopOrigin}/apps/builder?b=${props.form.shopify_product_id}`;
                                                    document.body.appendChild(elem);
                                                    elem.select();
                                                    document.execCommand('copy');
                                                    document.body.removeChild(elem);

                                                    toastMessage('Url Copied');
                                                    toastShow();

                                                }
                                            }}
                                            status="success"
                                        >
                                            <TextContainer>
                                                <p>In order to get your customers loading the builder, you will need to direct them to this same link.</p>
                                                <p>You can copy this link by using the button below.</p>
                                                <p>e.g. in Online Store {'>'} Navigation, you can use this link for a menu item.</p>
                                            </TextContainer>
                                        </Banner>
                                        :
                                        <Banner
                                            title="Your builder has been saved."
                                            status="info"
                                        >
                                            <TextContainer>
                                                <p>You have chosen to not go live with this builder yet, so it's not visible on your store..</p>
                                                <p>When you are ready, edit this builder and check the 'Live' box to make it visible to your customers.</p>
                                            </TextContainer>
                                        </Banner>
                                    }
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </Card>
        </div>
    );
}

export default Summary;
