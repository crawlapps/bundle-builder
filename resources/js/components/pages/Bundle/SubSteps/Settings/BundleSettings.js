import {
    Card,
    Heading,
    TextField,
    FormLayout,
    Checkbox,
    Select,
    Popover,
    ActionList,
    Button,
    Link,
    Modal,
    TextContainer
} from '@shopify/polaris';
import React, { useCallback, useState, useEffect } from "react";

import DisplaySettings from "./SubSettings/DisplaySettings";
import ThemeSettings from "./SubSettings/ThemeSettings";
import StyleSettings from "./SubSettings/StyleSettings";
import SEOSettings from "./SubSettings/SEOSettings";
import ShippingFulfillment from "./SubSettings/ShippingFulfillment";
import CustomizeURL from "./SubSettings/CustomizeURL";
import { useNavigate } from "react-router-dom";
import caretArrow from '../../../../snippets/caretArrow.svg'

const BundleSettings = (props) => {
    console.log('From Child :: ', props.form);

    const navigate = useNavigate();
    const whenCompleteOptions = [
        { label: 'Cart', value: '/cart' },
        { label: 'Checkout', value: '/checkout' },
    ];
    const [active, setActive] = useState(true);

    const [activeBuilderSetting, setActiveBuilderSetting] = useState(true);
    const [activeProductSku, setActiveProductSku] = useState(false);
    const [activeDisplay, setActiveDisplay] = useState(false);
    const [activeTheme, setActiveTheme] = useState(false);

    const toggleActive = useCallback(() => setActive((active) => !active), []);

    const handleExitAction = useCallback(
        () => navigate('/'),
        [],
    );

    const handleSaveAction = () => {
        props.storeSetting()
    }


    const activator = (
        <Button onClick={toggleActive} disclosure>
            Exit/Update
        </Button>
    );


    const MoreInfoModal = () => {
        const [activeMoreInfo, setActiveMoreInfo] = useState(false);

        const handleMoreInfo = () => {
            setActiveMoreInfo(!activeMoreInfo);
        }

        const moreInfoActivatore = (
            <Link onClick={() => { handleMoreInfo() }}>More Info</Link>
        )

        return (
            <Modal
                activator={moreInfoActivatore}
                open={activeMoreInfo}
                onClose={handleMoreInfo}
                title="Order Product/SKU Settings"
                secondaryActions={
                    [
                        {
                            content: 'Okay',
                            onAction: handleMoreInfo,
                        },
                    ]}
            >
                <Modal.Section>
                    <Heading>Use Your Products on new Orders</Heading>
                    <TextContainer>
                        <p>
                            By default, each box shows up on the cart, checkout and order as 1 item/product, with the box's contents listed beneath.
                        </p>
                        <p>
                            You can check this box to use the actual variants on the order so, instead of showing 1 product, it will apply all the individual variants to the order.
                        </p>
                        <p>
                            This is useful if you have downstream systems relying on the order details telling it what variants were actually sold.
                        </p>
                        <p>
                            After the order has been paid, we will automatically make these adjustments. To the customer, it will still show as 1 product on the cart and checkout pages.
                        </p>
                        <p>
                            Either method will adjust the stock for each variant chosen by the customer.
                            If you're using a base price, please note that this will show as an amount owed to the customer once the products are added.
                        </p>
                    </TextContainer>
                    <br />
                    <Heading>Keep the builder on the order?</Heading>
                    <TextContainer>
                        <p>
                            When you want to use a Fixed Price, a Start Price on in-app discounts, the total price the customer pays will not match the sum of all the selected products.
                        </p>
                        <p>
                            If you also use the 'Use your own products' setting, to show your own SKUs on the order, Shopify will realise all the SKUs' prices don't match the amount paid and will chase the customer for the outstanding amount.
                        </p>
                        <p>
                            You can use this setting, which keeps the builder's product on the order, still adds all the SKUs but sets the individual products to 0.00, so there's no outstanding amounts.
                        </p>
                    </TextContainer>
                </Modal.Section>
            </Modal >
        )
    }

    const ExtendedMoreInfo = () => {
        return (
            <>
                <span>
                    If the price of the builder won't be determined by the selected products, (e.g. you are using the Fixed Price option), this option is recommended <MoreInfoModal />
                </span>

            </>
        );
    }

    return (
        <div className='card-wrapper-radius'>
            <Card sectioned>

                <div className='builder_settings_accrodian'>
                    <ul className='list-unstyled'>

                        <li className={`builder_settings_accrodian_list ${activeBuilderSetting ? 'accrodian_active' : ''}`} >
                            <div className='builder_settings_accrodian_title'>
                                <Heading>Builder's Settings </Heading>
                                <div className='accrodian_caret' onClick={() => { setActiveBuilderSetting(!activeBuilderSetting) }}>
                                    <img src={caretArrow} alt="Caret icon" />
                                </div>
                            </div>
                            <div className='builder_settings_accrodian_content'>
                                <FormLayout>
                                    <FormLayout.Group>
                                        <TextField
                                            label="Builder Name"
                                            autoComplete="off"
                                            placeholder="e.g. Build Your Custom Laptop"
                                            value={props.form.title}
                                            onChange={value => props.updateForm('title', value)}
                                            error={props.errors.title}
                                        />
                                    </FormLayout.Group>
                                </FormLayout>
                                <FormLayout>
                                    <FormLayout.Group>
                                        <Checkbox
                                            label="Live"
                                            helpText="You can uncheck if you wish to deactivate the builder."
                                            checked={props.form.is_active}
                                            onChange={value => props.updateForm('is_active', value)}
                                        />
                                        <Checkbox
                                            label="Charge Tax"
                                            helpText="Would you like to charge tax when adding the boxes to the cart?"
                                            checked={props.form.is_charge_tax}
                                            onChange={value => props.updateForm('is_charge_tax', value)}
                                        />
                                        <Select
                                            label="When complete, send customers to the:"
                                            options={whenCompleteOptions}
                                            onChange={value => props.updateForm('on_complete_action', value)}
                                            value={props.form.on_complete_action}
                                        />
                                    </FormLayout.Group>
                                    <FormLayout.Group>
                                        <Checkbox
                                            label="Use a Fixed Price"
                                            helpText="If checked, the 'Use Your Products on new Orders' option above is unavailable."
                                            checked={props.form.is_use_fixed_price}
                                            onChange={value => props.updateForm('is_use_fixed_price', value)}
                                        />
                                        <TextField
                                            label="Start Price"
                                            type="number"
                                            autoComplete="off"
                                            helpText="Default is 0.00. This price of the builder will start at this value."
                                            value={props.form.start_price}
                                            onChange={value => props.updateForm('start_price', value)}
                                        />
                                    </FormLayout.Group>
                                </FormLayout>
                            </div>
                        </li>

                        <li className={`builder_settings_accrodian_list ${activeProductSku ? 'accrodian_active' : ''}`} >
                            <div className='builder_settings_accrodian_title'>
                                <Heading>Order Product/SKU Settings</Heading>
                                <div className='accrodian_caret' onClick={() => { setActiveProductSku(!activeProductSku) }}>
                                    <img src={caretArrow} alt="Caret icon" />
                                </div>
                            </div>
                            <div className='builder_settings_accrodian_content'>
                                <FormLayout>
                                    <FormLayout.Group>

                                        <Checkbox
                                            label="Use Your Products on new Orders"
                                            helpText={<MoreInfoModal />}
                                            checked={props.form.settings.product_settings.is_use_on_new_orders}
                                            onChange={value => props.updateForm('product_settings', value, 'settings', 'is_use_on_new_orders')}
                                        />
                                        <Checkbox
                                            label="Keep the builder on the order?"
                                            helpText={<ExtendedMoreInfo />}
                                            checked={props.form.settings.product_settings.is_keep_builder_on_order}
                                            onChange={value => props.updateForm('product_settings', value, 'settings', 'is_keep_builder_on_order')}
                                        />
                                    </FormLayout.Group>
                                </FormLayout>
                            </div>
                        </li>

                        {
                            props.type == 'advance'
                                ?
                                <>
                                    {/*Display Settings*/}
                                    <li className={`builder_settings_accrodian_list ${activeDisplay ? 'accrodian_active' : ''}`} >
                                        <div className='builder_settings_accrodian_title'>
                                            <Heading>Display Settings</Heading>
                                            <div className='accrodian_caret' onClick={() => { setActiveDisplay(!activeDisplay) }}>
                                                <img src={caretArrow} alt="Caret icon" />
                                            </div>
                                        </div>
                                        <div className='builder_settings_accrodian_content'>
                                            <DisplaySettings form={props} />
                                        </div>
                                    </li>

                                    {/* StyleSettings */}
                                    <StyleSettings form={props} caretArrow={caretArrow} />

                                </>
                                :
                                ''
                        }

                        {/*SEOSettings*/}
                        <SEOSettings form={props} caretArrow={caretArrow} />

                        {/*ShippingFulfillment*/}
                        <ShippingFulfillment form={props} caretArrow={caretArrow} />

                        {/*CustomizeURL*/}
                        <CustomizeURL form={props} caretArrow={caretArrow} />


                        {
                            props.type == 'advance'
                                ?
                                <>
                                    {/*Theme Settings*/}
                                    <li className={`builder_settings_accrodian_list ${activeTheme ? 'accrodian_active' : ''}`} >
                                        <div className='builder_settings_accrodian_title'>
                                            <Heading>Select which theme the builder will use</Heading>
                                            <div className='accrodian_caret' onClick={() => { setActiveTheme(!activeTheme) }}>
                                                <img src={caretArrow} alt="Caret icon" />
                                            </div>
                                        </div>
                                        <div className='builder_settings_accrodian_content'>
                                            <ThemeSettings form={props} />
                                        </div>
                                    </li>
                                </>
                                :
                                ''
                        }

                    </ul>
                </div>


                {/*footer buttons*/}
                <div className='center-buttons'>
                    <Popover active={active} activator={activator} onClose={toggleActive} >
                        <ActionList
                            items={[
                                {
                                    content: 'Exit Builder',
                                    onAction: handleExitAction,
                                },
                                {
                                    content: 'Save Settings',
                                    onAction: handleSaveAction,
                                },
                            ]}
                        />
                    </Popover>
                    <Button primary loading={props.isDisable} onClick={() => props.handleTabChange(1)}>Next</Button>
                </div>
            </Card>
        </div>
    );
}

export default BundleSettings;
