import {
    Page,
    Card,
    Heading,
    FormLayout,
    Checkbox,
    TextField,
    Subheading,
    RadioButton
} from '@shopify/polaris';

function DisplaySettings(props) {
    return (
        <Card.Section>

            <FormLayout>
                <FormLayout.Group>
                    <Checkbox
                        label="Enable Image Lightbox"
                        helpText="Allow customers to click product images to enlarge them."
                        checked={props.form.form.settings.display_settings.is_enable_image_lightbox}
                        onChange={value => props.form.updateForm('display_settings', value, 'settings', 'is_enable_image_lightbox')}
                    />
                    <Checkbox
                        label="Show Prices in the Button"
                        helpText="The only toggles the price's visibility within the button."
                        checked={props.form.form.settings.display_settings.is_show_price_in_button}
                        onChange={value => props.form.updateForm('display_settings', value, 'settings', 'is_show_price_in_button')}
                    />
                    <Checkbox
                        label="Show Formatted Descriptions"
                        helpText="This will copy the format of the descriptions you saved for each product and display it on the lightbox."
                        checked={props.form.form.settings.display_settings.is_show_formatted_description}
                        onChange={value => props.form.updateForm('display_settings', value, 'settings', 'is_show_formatted_description')}
                    />
                </FormLayout.Group>
                <FormLayout.Group>
                    <Checkbox
                        label="Hide Sold Out Products"
                        helpText="Sold out products will not show to the customer."
                        checked={props.form.form.settings.display_settings.is_hide_soldout_products}
                        onChange={value => props.form.updateForm('display_settings', value, 'settings', 'is_hide_soldout_products')}
                    />
                    <Checkbox
                        label="Show a message when adding to cart"
                        helpText="A small popup will display when the customer waits for the builder to be added to the cart."
                        checked={props.form.form.settings.display_settings.is_show_msg_on_addtocart}
                        onChange={value => props.form.updateForm('display_settings', value, 'settings', 'is_show_msg_on_addtocart')}
                    />
                    <TextField
                        label="Add Button Text"
                        type="text"
                        helpText="The price will appear next to this text. Note that long text may not be ideal on mobile devices."
                        autoComplete="email"
                        placeholder="e.g. Add:"
                        value={props.form.form.settings.display_settings.add_button_text}
                        onChange={value => props.form.updateForm('display_settings', value, 'settings', 'add_button_text')}
                    />
                </FormLayout.Group>
                <FormLayout.Group>
                    <Checkbox
                        label="Remember Selections"
                        helpText="If the customer leaves the page, the
                         builder can remember what they had selected before they left and auto-select these upon their return."
                        checked={props.form.form.settings.display_settings.is_remember_selection}
                        onChange={value => props.form.updateForm('display_settings', value, 'settings', 'is_remember_selection')}
                    />
                    <Checkbox
                        label="Show Image Carousels"
                        helpText="Show/hide image carousels when there are multiple images for the product."
                        checked={props.form.form.settings.display_settings.is_show_image_carousels}
                        onChange={value => props.form.updateForm('display_settings', value, 'settings', 'is_show_image_carousels')}
                    />
                    <Checkbox
                        label="Show Step Progress"
                        helpText="Show/hide the step numbers above each step."
                        checked={props.form.form.settings.display_settings.is_show_step_progress}
                        onChange={value => props.form.updateForm('display_settings', value, 'settings', 'is_show_step_progress')}
                    />
                </FormLayout.Group>
            </FormLayout>
            {/* =============== Cart GIFS =============== */}
            {
                props.form.form.settings.display_settings.is_show_msg_on_addtocart
                    ?
                    <div className="pt-5">
                        <FormLayout>
                            <Subheading>Select a gif to include on the popup.</Subheading>
                            <FormLayout.Group>
                                <div className="Polaris-Stack row animatedimgGroup">
                                    {
                                        [...Array(18)].map((x, i) => {
                                            return (
                                                <div className="col-md-4" key={i}>
                                                    <label className="Polaris-Choice d-block" htmlFor="PolarisRadioButton1">
                                                        <span className="Polaris-Choice__Control w-100 d-block">
                                                            <span className="Polaris-RadioButton">
                                                                <span className="Polaris-Choice__Label Polaris-Choice__swatch_img">
                                                                    <img
                                                                        className={props.form.form.settings.display_settings.gif == `${(i + 1)}.gif` ? 'activeGif' : ''}
                                                                        src={`/gifs/${i + 1}.gif`}
                                                                        width="150px"
                                                                        height="150px"
                                                                        onClick={() => props.form.updateForm('display_settings', `${(i + 1)}.gif`, 'settings', 'gif')}
                                                                    />
                                                                </span>
                                                            </span>
                                                        </span>
                                                    </label>
                                                </div>
                                            );
                                        })
                                    }
                                </div>
                            </FormLayout.Group>
                        </FormLayout>
                    </div>
                    :
                    ''
            }
        </Card.Section>
    );
}

export default DisplaySettings;
