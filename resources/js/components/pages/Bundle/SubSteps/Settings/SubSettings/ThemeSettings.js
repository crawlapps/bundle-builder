import {
    Page,
    Card,
    RadioButton,
    FormLayout, Heading, Checkbox
} from '@shopify/polaris';

function ThemeSettings(props){
    return (
        <Card.Section>
           
            <FormLayout>
                <FormLayout.Group>
                    <RadioButton
                        label="Full width with fixed bottom pricing bar"
                        helpText={<img src="https://builder.boxup.io/assets/image/full.png" className="theme-set-radiobutton-img"/>}
                        id="full-width-bottom-pricing-bar"
                        name="themeDisplaySetting"
                        checked={props.form.form.settings.theme.builder === 'full'}
                        onChange={value => props.form.updateForm('theme', 'full', 'settings', 'builder')}
                    />
                    <RadioButton
                        label="Fixed right side total with vertical scroll"
                        helpText={<img src="https://builder.boxup.io/assets/image/right.png" className="theme-set-radiobutton-img"/>}
                        id="fixed-right-side-total"
                        name="themeDisplaySetting"
                        checked={props.form.form.settings.theme.builder === 'right'}
                        onChange={value => props.form.updateForm('theme', 'right', 'settings', 'builder')}
                    />
                </FormLayout.Group>
            </FormLayout>
            <Checkbox
                label="Show images of selected items in the footer?"
                helpText="This only applies to the 'full' layout."
                checked={props.form.form.settings.theme.is_show_image_in_footer}
                onChange={value => props.form.updateForm('theme', value, 'settings', 'is_show_image_in_footer')}
            />
        </Card.Section>
    );
}

export default ThemeSettings;
