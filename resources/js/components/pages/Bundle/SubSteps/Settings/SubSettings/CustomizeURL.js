import {
    Page,
    Card, Stack, Link, Collapsible, FormLayout, TextField, Banner, Heading
} from '@shopify/polaris';
import React, { useCallback, useState } from "react";

function CustomizeURL(props) {
    const [open, setOpen] = useState(false);
    const [urlError, setUrlError] = useState('');

    const handleToggle = useCallback(() => setOpen((open) => !open), []);
    const handleUrl = (value) => {

        let val = value.toLowerCase().replace(/ /g, '-')
            .replace(/[^\w-]+/g, '');

        val = val.toLowerCase()
        props.form.updateForm('customize_urlpath_settings', val, 'settings', 'url')
    }

    return (
        <li className={`builder_settings_accrodian_list ${open ? 'accrodian_active' : ''}`} >
            <div className='builder_settings_accrodian_title'>
                <Heading>
                    Customize URL Path
                </Heading>
                <img className={open == true ? '' : "btnRotate"} src={props.caretArrow} alt="Caret icon" onClick={handleToggle} />
            </div>
            <div className='builder_settings_accrodian_content'>
                <Collapsible
                    open={open}
                    id="basic-collapsible"
                    transition={{ duration: '500ms', timingFunction: 'ease-in-out' }}
                    expandOnPrint
                >
                    <FormLayout>
                        <FormLayout.Group>
                            <Banner
                                title="Generate a unique URL for this builder."
                                status="info"
                            >
                                <p>The text you enter below will be used in the URL, so no spaces or non-alphabetic characters are allowed except for hyphens (dashes). Some examples might be 'build-a-box' or 'gift-box'.</p>
                            </Banner>
                        </FormLayout.Group>
                        <FormLayout.Group>
                            <TextField
                                label={`Custom URL - https://example.myshopify.com/apps/builder/${props.form.form.settings.customize_urlpath_settings.url}`}
                                type="text"
                                helpText="Start typing to view the URL."
                                onChange={value => { handleUrl(value) }}
                                value={props.form.form.settings.customize_urlpath_settings.url}
                            // error={props.errors.url}
                            />
                        </FormLayout.Group>
                    </FormLayout>
                </Collapsible>
            </div>
        </li>
    );
}

export default CustomizeURL;
