import React, { useCallback, useState, useEffect } from "react";
import { ChromePicker } from 'react-color';

import {
    Page,
    Card, Heading, FormLayout, RadioButton, Stack, Button, Collapsible, TextContainer, Link, Subheading
} from '@shopify/polaris';

function StyleSettings(props) {
    const [open, setOpen] = useState(false);
    const handleToggle = useCallback(() => setOpen((open) => !open), []);

    const colors = [
        { label: 'Primary Button Color', key: 'primary_button_color' },
        { label: 'Secondary Button Color', key: 'secondary_button_color' },
        { label: 'Product Select Color', key: 'product_select_color' },
        { label: 'Product Border Color', key: 'product_border_color' },
        { label: 'Lightbox Background Color', key: 'lightbox_background_color' },
        { label: 'Lightbox Text Color', key: 'lightbox_text_color' },
        { label: 'Progress Bar Text Color', key: 'progress_bar_text_color' },
        { label: 'Progress Bar Color', key: 'progress_bar_color' },
        { label: 'Total Box Background Color', key: 'total_box_background_color' },
        { label: '\'Total\' Text Color', key: 'total_text_color' },
        { label: 'Total Price Text Color', key: 'total_price_text_color' },
        { label: 'Order/Box Details Product Text Color', key: 'order_box_details_product_text_color' },
        { label: 'Adding to cart popup background color', key: 'adding_to_cart_popup_background_color' },
        { label: 'Adding to cart popup title Color', key: 'adding_to_cart_popup_title_color' },
        { label: 'Adding to cart popup text color', key: 'adding_to_cart_popup_text_color' }
    ];
    return (
        // <Card.Section>
        <li className={`builder_settings_accrodian_list ${open ? 'accrodian_active' : ''}`} >
            <div className="d-flex justify-content" style={{ padding: '20px' }}>
                <Heading>
                    Show style settings
                </Heading>
                <img className={open == true ? '' : "btnRotate"} src={props.caretArrow} alt="Caret icon" onClick={handleToggle} />
            </div>
            <div className='builder_settings_accrodian_content'>
                <Collapsible
                    open={open}
                    id="basic-collapsible"
                    transition={{ duration: '500ms', timingFunction: 'ease-in-out' }}
                    expandOnPrint
                >
                    <FormLayout>
                        <FormLayout.Group>
                            {colors.map((color, index) => {
                                return (
                                    <div key={index}>
                                        <Subheading>{color.label}</Subheading>
                                        <ChromePicker
                                            value={props.form.form.settings.style_settings[color.key]}
                                            color={props.form.form.settings.style_settings[color.key]}
                                            onChange={value => props.form.updateForm('style_settings', value.hex, 'settings', color.key)}
                                        />
                                    </div>
                                );
                            })}
                        </FormLayout.Group>
                    </FormLayout>
                </Collapsible>
            </div>
        </li>
        // <Stack vertical>
        // </Stack>
        // </Card.Section>
    );
}

export default StyleSettings;
