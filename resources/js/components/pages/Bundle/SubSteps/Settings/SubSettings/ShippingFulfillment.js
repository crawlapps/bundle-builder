import {
    Page,
    Card, Stack, Link, Collapsible, FormLayout, Select, Checkbox, Heading
} from '@shopify/polaris';
import React, { useCallback, useState } from "react";

function ShippingFulfillment(props) {
    const [open, setOpen] = useState(false);

    const handleToggle = useCallback(() => setOpen((open) => !open), []);

    const options = [
        { label: 'Default', value: 'manual' },
    ];

    return (
        <li className={`builder_settings_accrodian_list ${open ? 'accrodian_active' : ''}`}>
            <div className='builder_settings_accrodian_title'>
                <Heading>
                    Shipping & Fulfilment
                </Heading>
                <img className={open == true ? '' : "btnRotate"} src={props.caretArrow} alt="Caret icon" onClick={handleToggle} />
            </div>
            <div className='builder_settings_accrodian_content'>
                <Collapsible
                    open={open}
                    id="basic-collapsible"
                    transition={{ duration: '500ms', timingFunction: 'ease-in-out' }}
                    expandOnPrint
                >
                    <FormLayout>
                        <FormLayout.Group>
                            <Select
                                label="Fulfilment Service"
                                options={options}
                                helpText="Optionally change the fulfilment service. Keep as Default to use the standard Shopify fulfilment method. If your fulfilment options aren't showing, you may need to enable permission for our app to access them."
                                onChange={value => props.form.updateForm('shipping_fulfillment_settings', value, 'settings', 'fulfillment_service')}
                                value={props.form.form.settings.shipping_fulfillment_settings.fulfillment_service}

                            />
                            <Checkbox
                                label="Requires Shipping"
                                helpText="Does this box/builder require a shipping address at checkout?"
                                onChange={value => props.form.updateForm('shipping_fulfillment_settings', value, 'settings', 'is_shipping_required')}
                                checked={props.form.form.settings.shipping_fulfillment_settings.is_shipping_required}
                            />
                        </FormLayout.Group>
                    </FormLayout>
                </Collapsible>
            </div>
        </li>

    );
}

export default ShippingFulfillment;
