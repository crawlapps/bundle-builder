import {
    Page,
    Card, Stack, Link, Collapsible, FormLayout, Subheading, TextField, Heading
} from '@shopify/polaris';
import React, { useCallback, useState } from "react";
import { ChromePicker } from "react-color";

function SEOSettings(props) {
    const [open, setOpen] = useState(false);

    const handleToggle = useCallback(() => setOpen((open) => !open), []);

    return (
        <li className={`builder_settings_accrodian_list ${open ? 'accrodian_active' : ''}`}>
            <div className='builder_settings_accrodian_title'>
                <Heading>
                    Show SEO Settings
                </Heading>
                <img className={open == true ? '' : "btnRotate"} src={props.caretArrow} alt="Caret icon" onClick={handleToggle} />
            </div>
            <div className='builder_settings_accrodian_content'>
                <Collapsible
                    open={open}
                    id="basic-collapsible"
                    transition={{ duration: '500ms', timingFunction: 'ease-in-out' }}
                    expandOnPrint
                >
                    <FormLayout>
                        <FormLayout.Group>
                            <TextField
                                label="SEO Title"
                                placeholder="e.g. Custom Gift Box by My Company"
                                autoComplete="off"
                                helpText="Override the default SEO title."
                                value={props.form.form.settings.seo_settings.title}
                                onChange={value => props.form.updateForm('seo_settings', value, 'settings', 'title')}
                            />
                            <TextField
                                label="SEO Keywords"
                                autoComplete="off"
                                helpText="Comma-separated list of keywords."
                                value={props.form.form.settings.seo_settings.keywords}
                                onChange={value => props.form.updateForm('seo_settings', value, 'settings', 'keywords')}
                            />
                        </FormLayout.Group>
                        <FormLayout.Group>
                            <TextField
                                label="SEO Description"
                                autoComplete="off"
                                value={props.form.form.settings.seo_settings.description}
                                onChange={value => props.form.updateForm('seo_settings', value, 'settings', 'description')}
                            />
                        </FormLayout.Group>
                    </FormLayout>
                </Collapsible>
            </div>
        </li>
    );
}

export default SEOSettings;
