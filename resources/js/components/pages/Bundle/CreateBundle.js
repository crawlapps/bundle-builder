import {
    Page,
    Tabs,
    Banner
} from '@shopify/polaris';
import React, { useCallback, useState, useEffect } from "react";
import {
    useParams,
} from "react-router-dom";

import BundleSettings from "./SubSteps/Settings/BundleSettings";
import Steps from "./SubSteps/Steps/Steps";
import Discounts from "./SubSteps/Discounts/Discounts";
import Summary from "./SubSteps/Summary/Summary";

import axios from 'axios';
import instance from '../../shopify/instance';
import { toastIsError, toastMessage, toastShow } from '../../AppbridgeActions/ToastActions';


function CreateBundle() {
    let params = useParams();

    let id = params.id;
    let type = params.type;

    const tabs = [
        {
            id: 'all-customers-1',
            content: '1. Builder Settings',
            accessibilityLabel: '1. Builder Settings',
        },
        {
            id: 'accepts-marketing-1',
            content: '2. Steps',
        },
        {
            id: 'repeat-customers-1',
            content: '3. Discounts',
        },
        {
            id: 'prospects-1',
            content: '4. Summary',
        },
    ];
    const [selected, setSelected] = useState(0);

    const [data, setData] = useState({
        id: '',
        title: '',
        type: 'advance',
        is_active: 1,
        is_charge_tax: 1,
        on_complete_action: '/cart',
        is_use_fixed_price: 0,
        start_price: '',
        settings: {
            product_settings: {
                is_use_on_new_orders: 0,
                is_keep_builder_on_order: 0,
            },
            display_settings: {
                is_enable_image_lightbox: 1,
                is_show_price_in_button: 1,
                is_show_formatted_description: 0,
                is_hide_soldout_products: 0,
                is_show_msg_on_addtocart: 0,
                add_button_text: '',
                is_remember_selection: 1,
                is_show_image_carousels: 1,
                is_show_step_progress: 0,
                gif: '1.gif'
            },
            style_settings: {
                primary_button_color: '#463B36',
                secondary_button_color: '#E06C6C',
                product_select_color: '#81A252',
                product_border_color: '#D9E1E6',
                lightbox_background_color: '#000000',
                lightbox_text_color: '#FFFFFF',
                progress_bar_text_color: '#3A3A3A',
                progress_bar_color: '#D9E1E6',
                total_box_background_color: '#FFFFFF',
                total_text_color: '#CCCCCC',
                total_price_text_color: '#000000',
                order_box_details_product_text_color: '#555555',
                adding_to_cart_popup_background_color: '#FFFFFF',
                adding_to_cart_popup_title_color: '#000000',
                adding_to_cart_popup_text_color: '#000000',

            },
            seo_settings: {
                title: '',
                keywords: '',
                description: ''
            },
            shipping_fulfillment_settings: {
                fulfillment_service: '',
                is_shipping_required: 0
            },
            customize_urlpath_settings: {
                url: ''
            },
            theme: {
                builder: 'full',
                is_show_image_in_footer: 0
            },
        },
        steps: [],
        discounts: [],
        deletedSteps: [],
        deletedDiscounts: []
    });

    const [errors, setErrors] = useState({});

    const [isDisable, setIsDisable] = useState(false);

    const [globalError, setGlobalError] = useState('');

    const updateForm = (action, value, parent_action = '', formfield = '', stepindex = 0, formindex = -1, removeindex = -1) => {
        const obj = { ...data };

        if (removeindex >= 0) {
            obj[parent_action][stepindex][value].splice(removeindex, 1);
        } else {
            if (parent_action !== '' && parent_action != 'settings') {
                let s = ['product', 'pre_selected', 'form', 'subscription', 'content', 'discounts'];
                if (s.includes(action)) {
                    if (formindex == -1) {
                        obj[parent_action][stepindex][formfield] = value;
                    } else {
                        obj[parent_action][stepindex]['field'][formindex][formfield] = value;
                    }
                } else {
                    console.log('Eleeeeeeeeeeeeeee');
                    console.log(value);
                    obj[parent_action][action] = value;
                }
            } else {
                if (parent_action == 'settings') {
                    obj[parent_action][action][formfield] = value;
                } else {
                    obj[action] = value;
                }
            }
            setData(obj);
        }

    };

    useEffect(() => {

        if (id > 0) {
            getData('/bundle/' + id + '/edit');
        } else {
            if (type == 'simple') {
                getSettings('/customizations');
            }
        }
    }, []);

    function handleTabChange(selectedTabIndex) {
        let r = validate('bundle');
        if (!r) {
            setSelected(selectedTabIndex);
        }
        console.log(errors);
    }

    const validate = (type, stepindex = -1, formindex = -1) => {
        let e = {};
        if (type == 'bundle' && data.title == '') {
            e['title'] = ['Required'];
        }

        let s = ['product', 'pre_selected', 'form', 'subscription', 'content'];
        if (s.includes(type)) {
            e['steps'] = [];
            e['steps'][stepindex] = [];

            e['steps'][stepindex]['title'] = (data.steps[stepindex].title == '') ? ['required'] : [];
            e['steps'][stepindex]['display_order'] = (data.steps[stepindex].display_order == '') ? ['required'] : [];

            if (type == 'product') {
                e['steps'][stepindex]['minimum_selection'] = (data.steps[stepindex].minimum_selection == '') ? ['required'] : [];
                e['steps'][stepindex]['maximum_selection'] = (data.steps[stepindex].maximum_selection == '') ? ['required'] : [];
            }

            if (type == 'pre_selected') {
                e['steps'][stepindex]['button_text'] = (data.steps[stepindex].button_text == '') ? ['required'] : [];
            }

            if (type == 'form') {
                if (formindex >= 0) {
                    e['steps'][stepindex]['field'] = [];
                    e['steps'][stepindex]['field'][formindex] = [];

                    e['steps'][stepindex]['field'][formindex]['field_title'] = (data.steps[stepindex]['field'][formindex]['field_title'] == '') ? ['required'] : [];
                    e['steps'][stepindex]['field'][formindex]['display_order'] = (data.steps[stepindex]['field'][formindex]['display_order'] == '') ? ['required'] : [];
                } else {
                    e['steps'][stepindex]['field'] = (data.steps[stepindex].field.length == 0) ? ['Please add at least 1 field.'] : [];
                }
            }
        }

        if (type == 'discounts') {
            e['discounts'] = [];
            e['discounts'][stepindex] = [];

            e['discounts'][stepindex]['title'] = (data.discounts[stepindex].title == '') ? ['required'] : [];
            e['discounts'][stepindex]['amount'] = (data.discounts[stepindex].amount == '') ? ['required'] : [];
            e['discounts'][stepindex]['no_of_items_required'] = (data.discounts[stepindex].no_of_items_required == '') ? ['required'] : [];
            e['discounts'][stepindex]['order'] = (data.discounts[stepindex].order == '') ? ['required'] : [];
        }
        setErrors(e);
        if (Object.keys(e).length > 0) {
            clearEmpties(e);
        }
        return (Object.keys(e).length > 0);
    }

    function clearEmpties(o) {
        for (var k in o) {
            if (!o[k] || typeof o[k] !== "object") {
                continue // If null or not an object, skip to the next iteration
            }

            // The property is an object
            clearEmpties(o[k]); // <-- Make a recursive call on the nested object
            if (Object.keys(o[k]).length === 0 || Object.keys(o[k]) === null || Object.keys(o[k]) === '' || Object.keys(o[k]) === undefined) {
                delete o[k]; // The object had no properties, so delete that property
            }
        }
    }

    async function getData(url) {
        instance({
            url: url,
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        })
            .then(res => {
                res.data.data.builder.deletedSteps = [];
                res.data.data.builder.deletedDiscounts = [];
                setData(res.data.data.builder);
                console.log(data);
            })
            .catch(err => {
                console.log(err);
            })
    }

    async function getSettings(url) {

        instance({
            url: url,
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        })
            .then(res => {
                console.log(res);
                if (res.data.isSuccess == true) {
                    let display_settings = res.data.data.display_settings;
                    let style_settings = res.data.data.style_settings;
                    let theme = res.data.data.theme;

                    setData({
                        id: '',
                        title: '',
                        type: 'simple',
                        is_active: 1,
                        is_charge_tax: 1,
                        on_complete_action: '/cart',
                        is_use_fixed_price: 0,
                        start_price: '',
                        settings: {
                            product_settings: {
                                is_use_on_new_orders: 0,
                                is_keep_builder_on_order: 0,
                            },
                            display_settings: display_settings,
                            style_settings: style_settings,
                            theme: theme,
                            seo_settings: {
                                title: '',
                                keywords: '',
                                description: ''
                            },
                            shipping_fulfillment_settings: {
                                fulfillment_service: '',
                                is_shipping_required: 0
                            },
                            customize_urlpath_settings: {
                                url: ''
                            },
                        },
                        steps: [],
                        discounts: [],
                        deletedSteps: [],
                        deletedDiscounts: [],
                    });
                }
            })
            .catch(err => {
                console.log(err);
            })
    }

    const storeSetting = async () => {

        setGlobalError('');
        setIsDisable(true);
        let formData = new FormData();
        data.steps.map((step, s_i) => {
            if (step.type == 'form') {
                step.field.map((f, f_i) => {
                    if (f.field_type == "image") {
                        f.images.map((img, i) => {
                            formData.append(img.uniqueId, img.file);
                        })
                    }
                })
            }
        })

        formData.append('data', JSON.stringify(data));

        await instance.post("/bundle", formData, {
            headers: { "Content-Type": "multipart/form-data;" },
        })
            .then((res) => {
                console.log(res.data);
                if (res.data.isSuccess) {
                    setGlobalError('');
                    toastMessage('Builder saved succesfully.')
                    toastIsError(false)
                    toastShow();
                    selected == 2 ? handleTabChange(3) : ''
                } else {
                    console.log('ERROR:: ', res.data);
                }
            })
            .catch((err) => {
                console.log(err);
                if (err.response.status == 422) {
                    console.log(err.response.data);
                    console.log(err.response.data.data.url);
                    setGlobalError(err.response.data.data.url);
                    toastMessage('Some errors occured.')
                    toastIsError(true)
                    toastShow();
                }
            })
            .finally(() => {
                setIsDisable(false);
            })

        // await axios('/bundle', {
        //     method: 'POST',
        //     headers: {
        //         "Content-Type": "multipart/form-data;",
        //     },
        //     body: formData,
        // }).then(res => {
        //     if (res.isSuccess) {
        //         handleTabChange(3);
        //         setIsDisable(false);
        //         // history.push('/token-index')
        //     } else {
        //         console.log('ERROR:: ' + res.data);
        //         // setErrors(res);
        //     }
        // }).catch(err => {
        //     // setErrors(err);
        //     console.log(errors);
        // });

        // const response = await fetch('/bundle', {
        //     method: 'POST',
        //     headers: {
        //         "Content-Type": "multipart/form-data;",
        //     },
        //     body: formData,
        // }).then(response => response.json()).then(res => {
        //     if (res.isSuccess) {
        //         handleTabChange(3);
        //         setIsDisable(false);
        //         // history.push('/token-index')
        //     } else {
        //         console.log('ERROR:: ' + res.data);
        //         // setErrors(res);
        //     }
        // }).catch(err => {
        //     // setErrors(err);
        //     console.log(errors);
        // });
    }

    return (
        <Page
            title="Design Your Builder"
            subtitle="Complete each tab below to create a product builder. You'll be given instructions at the end."
            size="large"
        >
            {/* {(data.id) ?
                // <Tabs tabs={tabs} selected={selected} onSelect={handleTabChange}></Tabs>
                : */}
            <div className='tab-radius buid_tab'>
                <Tabs tabs={tabs} selected={selected} onSelect={(e) => { e < selected ? handleTabChange(e) : () => { } }}></Tabs>
            </div>
            {/* } */}

            {
                globalError.length > 0
                    ?
                    <Banner
                        title="Errors"
                        status="critical"
                        onDismiss={() => { setGlobalError('') }}
                    >
                        <p>
                            {globalError}
                        </p>
                    </Banner>
                    :
                    ''
            }

            <div className="create-builder-content mt-20">
                {/* <Card sectioned> */}

                <div>
                    {selected === 0 ?
                        <BundleSettings form={data} type={params.type} updateForm={updateForm} handleTabChange={handleTabChange} storeSetting={storeSetting} errors={errors} isDisable={isDisable} />
                        : selected === 1 ?
                            <Steps form={data} updateForm={updateForm} handleTabChange={handleTabChange} setData={setData} validate={validate} errors={errors} />
                            : selected === 2 ?
                                <Discounts form={data} updateForm={updateForm} handleTabChange={handleTabChange} storeSetting={storeSetting} errors={errors} validate={validate} isDisable={isDisable} />
                                : selected === 3 ?
                                    <Summary form={data} />
                                    : "Neither"}
                </div>
                {/*</Card>*/}
            </div>
        </Page>
    );
}

export default CreateBundle;
