import { Card, Heading, Page, Button } from '@shopify/polaris';
import React, { useEffect, useState } from 'react'
import DisplaySettings from '../Bundle/SubSteps/Settings/SubSettings/DisplaySettings';
import StyleSettings from '../Bundle/SubSteps/Settings/SubSettings/StyleSettings';
import ThemeSettings from '../Bundle/SubSteps/Settings/SubSettings/ThemeSettings';

import caretArrow from '../../snippets/caretArrow.svg'
import axios from 'axios';
import { Provider } from '@shopify/app-bridge-react';
// import { Toast } from '@shopify/app-bridge/actions';
import instance from '../../shopify/instance';
import { toastMessage, toastShow } from '../../AppbridgeActions/ToastActions';

function Customizations() {

    const [activeDisplay, setActiveDisplay] = useState(true);
    const [activeTheme, setActiveTheme] = useState(false);
    const [toast, setToast] = useState(false);

    const [data, setData] = useState({
        theme: {
            builder: "full",
            is_show_image_in_footer: true
        },
        style_settings: {
            total_text_color: "#CCCCCC",
            progress_bar_color: "#D9E1E6",
            lightbox_text_color: "#FFFFFF",
            primary_button_color: "#463B36",
            product_border_color: "#D9E1E6",
            product_select_color: "#81A252",
            secondary_button_color: "#E06C6C",
            total_price_text_color: "#000000",
            progress_bar_text_color: "#3A3A3A",
            lightbox_background_color: "#000000",
            total_box_background_color: "#FFFFFF",
            adding_to_cart_popup_text_color: "#000000",
            adding_to_cart_popup_title_color: "#000000",
            order_box_details_product_text_color: "#555555",
            adding_to_cart_popup_background_color: "#FFFFFF"
        },
        display_settings: {
            gif: "1.gif",
            add_button_text: "Buy",
            is_remember_selection: 1,
            is_show_step_progress: true,
            is_show_image_carousels: 1,
            is_show_price_in_button: 1,
            is_enable_image_lightbox: 1,
            is_hide_soldout_products: false,
            is_show_msg_on_addtocart: true,
            is_show_formatted_description: true
        }
    });

    function getSettings(url) {
        instance({
            url: url,
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        })
            .then(res => {
                console.log(res);
                if (res.data.isSuccess == true) {
                    setData(res.data.data);
                }
            })
            .catch(err => {
                console.log(err);
            })
    }

    const updateForm = (action, value, parent_action = '', formfield = '', stepindex = 0, formindex = -1, removeindex = -1) => {
        const obj = { ...data };

        if (parent_action == 'settings') {
            obj[action][formfield] = value;
        }

        setData(obj);
    }

    let sendingData = { form: { settings: data }, updateForm: updateForm }

    useEffect(() => {
        getSettings('/customizations');
    }, []);

    const saveCustomizations = () => {
        instance({
            url: '/customizations/edit',
            data: {
                data: data
            },
            method: 'POST',
        })
            .then((res) => {
                console.log(res);
                toastMessage('Customizations Saved')
                toastShow();
            })
            .catch((err) => {
                console.log(err);
            })
    }

    const resetCustomizations = () => {
        getSettings('/customizations');
    }


    return (
        <Page
            title="Customization Settings"
            subtitle="These settings will be applied to every new simple builder created."
            size="large"
        >
            <div className="create-builder-content mt-30">
                {/* <Card sectioned> */}
                <div>
                    <Card sectioned>

                        <div className='builder_settings_accrodian'>
                            <ul className='list-unstyled'>

                                <li className={`builder_settings_accrodian_list ${activeDisplay ? 'accrodian_active' : ''}`} >
                                    <div className='builder_settings_accrodian_title'>
                                        <Heading>Display Settings</Heading>
                                        <div className='accrodian_caret' onClick={() => { setActiveDisplay(!activeDisplay) }}>
                                            <img src={caretArrow} alt="Caret icon" />
                                        </div>
                                    </div>
                                    <div className='builder_settings_accrodian_content'>
                                        <DisplaySettings form={sendingData} />
                                    </div>
                                </li>

                                {/* StyleSettings */}
                                <StyleSettings form={sendingData} caretArrow={caretArrow} />


                                <li className={`builder_settings_accrodian_list ${activeTheme ? 'accrodian_active' : ''}`} >
                                    <div className='builder_settings_accrodian_title'>
                                        <Heading>Select which theme the builder will use</Heading>
                                        <div className='accrodian_caret' onClick={() => { setActiveTheme(!activeTheme) }}>
                                            <img src={caretArrow} alt="Caret icon" />
                                        </div>
                                    </div>
                                    <div className='builder_settings_accrodian_content'>
                                        <ThemeSettings form={sendingData} />
                                    </div>
                                </li>

                            </ul>
                        </div>

                        {/*footer buttons*/}
                        <div className='center-buttons'>
                            <Button onClick={() => resetCustomizations()}>Discard</Button>
                            <Button primary onClick={() => saveCustomizations()}>Save</Button>
                        </div>

                        {/* {
                            toast
                                ?
                                <Provider config={window.shopify_app_bridge_config}>
                                    <Toast content="Data Saved Succesfully." />
                                </Provider>
                                :
                                ''
                        } */}

                    </Card>
                </div>
            </div>

        </Page>
    )
};



export default Customizations