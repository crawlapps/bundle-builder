import RoutePath from "../routes/index";
import { Link } from "react-router-dom";

import { Card, Tabs, Button, Page } from '@shopify/polaris';
import Dashboard from "../pages/Dashboard";
import React, { useCallback, useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";

function Menu() {
    const navigate = useNavigate();
    const [selected, setSelected] = useState(0);

    const tabs = [
        {
            id: 'dashboard',
            content: 'My Builders',
            accessibilityLabel: 'All customers',
            panelID: 'dashboard',
            path: '/'
        },
        {
            id: 'customizationsEdit',
            content: 'Customizations',
            accessibilityLabel: 'Customizations',
            panelID: 'customizations',
            path: '/customizations/edit'
        },
        {
            id: 'support',
            content: 'Support',
            accessibilityLabel: 'support',
            panelID: 'support',
            path: '/support'
        },
    ];

    const handleTabChange = useCallback(
        (selectedTabIndex) => {
            setSelected(selectedTabIndex)
            navigate(tabs[selectedTabIndex].path)
        },
        [],
    );
    return (

        <Tabs tabs={tabs} selected={selected} onSelect={handleTabChange}></Tabs>

        // <Card sectioned>
        //     {(tabs.length > 0) ?
        //         <div className="navigation">
        //             {tabs.map((tab, index) => {
        //                 return (
        //                     <Link to={tab.path} key={index} className="navigation-link mr-15">
        //                         <Button type="button">
        //                             {tab.content}
        //                         </Button>
        //                     </Link>
        //                 )
        //             })}
        //         </div>
        //         :
        //         <Page />
        //     }

        // </Card>
    );
}

export default Menu;
