import React from "react";
import {
    Routes, Route, BrowserRouter
} from "react-router-dom";

import Menu from "../layouts/menu";
import Dashboard from "../pages/Dashboard";
import Bundle from "../pages/Bundle/CreateBundle";
import Customizations from "../pages/Customization";
import Support from "../pages/Support";

export default function RoutePath(){
    return(
        <BrowserRouter>
            <Menu />
            <Routes>
                <Route exact path='/' element={<Dashboard />}/>
                <Route path='/bundle/:id/:type' element={<Bundle />}/>
                <Route path='/customizations/edit' element={<Customizations />}/>
                <Route path='/support' element={<Support />}/>
            </Routes>
        </BrowserRouter>
    )
}

