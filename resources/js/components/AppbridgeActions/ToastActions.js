import { Toast } from '@shopify/app-bridge/actions';

const toastOptions = {
    message: 'Data saved',
    duration: 5000,
};
const toastNotice = Toast.create(window.shopify_app_bridge, toastOptions);

function toastShow() {
    toastNotice.dispatch(Toast.Action.SHOW);
}

function toastMessage(msg) {
    toastNotice.message = msg
}

function toastIsError(val = false){
    toastNotice.isError = val;
}


export { toastShow, toastMessage, toastIsError }
