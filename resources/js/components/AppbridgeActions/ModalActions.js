import { Modal, Button } from '@shopify/app-bridge/actions';

let modalOptions = {
    title: 'My Modal',
    message: 'Hello world!',
};

// const myModal = Modal.create(window.shopify_app_bridge, modalOptions);

function showModal(title, message, primaryLable = 'OK', primaryAction = ()=>{}, isPrimaryDanger = false) {

    modalOptions.title = title;
    modalOptions.message = message;

    const okButton = Button.create(window.shopify_app_bridge, { label: primaryLable });
    isPrimaryDanger && (okButton.style = Button.Style.Danger);

    modalOptions.footer = {
        buttons: {
            primary: okButton,
        },
    }

    const myModal = Modal.create(window.shopify_app_bridge, modalOptions);

    okButton.subscribe(Button.Action.CLICK, () => {
        primaryAction();
        hideModal();
    });
    myModal.dispatch(Modal.Action.OPEN);

    function hideModal() {
        myModal.dispatch(Modal.Action.CLOSE);
    }
}


export { showModal };

