import React, { useEffect, useState } from 'react'
import Filters from '../../layoutFull/partials/filters';
import Summary from '../../layoutFull/partials/Summary';


function BasicProduct({ options, removeFromModal, data, setOptions, addProduct, removeProduct, element, maxItems, setFormData, showSelectVariantError, filters, setFilters, getFilteredVariants, getTags }) {

    // Check item count from item
    function itemCountMethod(s_id) {

        let itemarray = options.items;

        const searchItem = (what) => itemarray.find(element => (element.id === what && element.step == options.step));

        if (itemarray.length > 0) {
            let searchedArray = searchItem(s_id);
            let result = 0;
            if (typeof searchedArray != 'undefined') {
                result = searchedArray.itemCount;
            }
            return (
                result
            );
        } else {
            return (
                0
            );
        }
    }

    // Modal actions.
    const imageAction = (id, action) => {
        if (action == 'open') {
            document.getElementsByClassName(`img-modal-${id}`)[0].classList.add('is-open');
        }
        if (action == 'close') {
            document.getElementsByClassName(`img-modal-${id}`)[0].classList.remove('is-open');
        }
    }

    // Select items from dropdown of variants :
    // Parameters : v_index => Variant index, ref_id => Product id for refrence , productObj : Selected product, imgSrc => Image source
    function selectFromDropdown(v_index, ref_id, productObj, imgsrc) {

        // Removing selections
        productObj.variants.map(function (variant) {
            let itemsCount = itemCountMethod(variant.id);
            for (let i = 0; i <= itemsCount; i++) {
                removeProduct(variant.id, variant.displayName, parseFloat(variant.price))
            }
        })

        // Selecting variant
        if (v_index != 'none') {
            let itemObj = productObj.variants[v_index];
            console.log(itemObj);
            addProduct(itemObj.id, itemObj.displayName, parseFloat(itemObj.price), imgsrc, false, ref_id)
        }
    }

    // Check item count from item in variant drop down layout : p_id => Product Id
    function itemCountMethodDropdown(p_id) {
        let itemarray = options.items;
        const searchItem = (what) => itemarray.find(element => (element.ref_id === what && element.step == options.step));

        if (itemarray.length > 0) {
            let searchedArray = searchItem(p_id);
            let result = {
                count: 0,
                variant: null
            }
            if (typeof searchedArray != 'undefined') {
                result.count = searchedArray.itemCount;
                result.variant = searchedArray;
                result.variant_price = (parseFloat(searchedArray.price).toFixed(2) / (searchedArray.itemCount)).toFixed(2);
            }
            return result;
        } else {
            let dropdown_select = document.getElementById(`dropdown_${p_id}`);
            if (dropdown_select != null) {
                dropdown_select.value = 'none';
            }
            return (
                {
                    count: 0,
                    variant: null
                }
            );
        }
    }

    return (

        <div id="bb-bldr-middle-wizard" className="bb-bldr-wizard-branch bb-bldr-wizard-wrapper"
        >

            <div className="bb-bldr-step bb-bldr-current bb-bldr-wizard-step" data-index="0"
                data-steptype="product">
                <div className="bb-bldr-question_title">
                    <h3 className="bb-bldr-wizard-header">{element.title}
                    </h3>
                    <p>{element.description}</p>
                </div>

                <Summary element={element} data={data} options={options} setOptions={setOptions} setFormData={setFormData} theme="right" removeFromModal={removeFromModal}/>

                <div className="bb-bldr-row">
                    <Filters data={data} filters={filters} setFilters={setFilters} element={element} getTags={getTags}/>
                </div>

                <div className="bb-step-content" >


                    {
                        element.show_variant_as == 0
                            ?

                            <>
                                {
                                    getFilteredVariants(element.selected_tab == 1 ? element.selected_products : element.collections, element.selected_tab, element.show_variant_as).map((varient) => {
                                        return <div className={"bb-bldr-item bb-filt-sort "} style={{ marginBottom: '1.5rem' }} key={varient.id} >

                                            <div className={"model_box " + (itemCountMethod(varient.id) > 0 && "active-border")}>
                                                <label className={itemCountMethod(varient.id) > 0 ? 'added-icon' : ''} style={{ display: 'flex', padding: '25px', paddingBottom: '0px' }}>

                                                    <div className="bb-qty-num" style={{ display: itemCountMethod(varient.id) > 0 ? 'block' : 'none' }}>{itemCountMethod(varient.id)}</div>

                                                    <figure >
                                                        <div className="bb-owl-carousel bb-owl-theme bb-owl-loaded bb-owl-drag" bis_skin_checked="1">
                                                            <div className="bb-owl-stage-outer" bis_skin_checked="1"><div className="bb-owl-stage" style={{ transform: 'translate3d(0px, 0px, 0px)', transition: 'all 0s ease 0s', width: '268px' }} bis_skin_checked="1"><div className="bb-owl-item active" bis_skin_checked="1" style={{ width: '258px', marginRight: '10px' }}><div className="bb-owl-item" bis_skin_checked="1">

                                                                {
                                                                    data.builderSettings.settings.display_settings.is_enable_image_lightbox
                                                                        ?
                                                                        <div className="bb-overlay-icon" onClick={() => imageAction(varient.id, 'open')}><span className="bb-icon"></span></div>
                                                                        :
                                                                        ''
                                                                }
                                                                <a image="//cdn.shopify.com/s/files/1/0549/7998/5653/products/1_800x.jpg?v=1649226505" title-strip="asdfgh" title-desc="asdfgh">
                                                                    <img src={varient.product_image} alt="Second Prodcut" className="bb-bldr-img-fluid owl-lazy" style={{ opacity: '1', width: '160px', height: '160px' }} />
                                                                </a>
                                                            </div></div></div></div><div className="bb-owl-nav disabled" bis_skin_checked="1"><button type="button" role="presentation" className="bb-owl-prev"><span aria-label="Previous">‹</span></button><button type="button" role="presentation" className="bb-owl-next"><span aria-label="Next">›</span></button></div><div className="bb-owl-dots disabled" bis_skin_checked="1"><button role="button" className="bb-owl-dot active"><span></span></button></div></div>
                                                    </figure>

                                                    <div>

                                                        {
                                                            element.is_allow_more_than_one ?
                                                                <>
                                                                    {
                                                                        itemCountMethod(varient.id) > 0
                                                                            ?
                                                                            <span className="bb-bldr-qty-down bb-bldr-qty-round" style={{ display: 'inline-block' }} onClick={() => removeProduct(varient.id, varient.displayName, parseFloat(varient.price))} >-</span>
                                                                            : ''
                                                                    }

                                                                    <span className="bb-bldr-item_price bb-bldr-item_price_round" onClick={() => { varient.inventoryQuantity > 0 ? addProduct(varient.id, varient.displayName, parseFloat(varient.price), varient.product_image) : () => { } }}>
                                                                        {varient.inventoryQuantity > 0 ? <span className="bb-bldr-add-text" >{data.builderSettings.settings.display_settings.add_button_text.length > 0 ? data.builderSettings.settings.display_settings.add_button_text : 'Add:'} {window.store_currency}{data.builderSettings.settings.display_settings.is_show_price_in_button ? `${varient.price}` : ''} </span> : ' Sold out'}
                                                                    </span>

                                                                    {
                                                                        itemCountMethod(varient.id) > 0
                                                                            ?
                                                                            <span className="bb-bldr-qty-up bb-bldr-qty-round" style={{ display: 'inline-block' }} onClick={() => { varient.inventoryQuantity > 0 ? addProduct(varient.id, varient.displayName, parseFloat(varient.price), varient.product_image) : () => { } }}>+</span>
                                                                            : ''
                                                                    }
                                                                </>
                                                                :
                                                                <>
                                                                    {
                                                                        itemCountMethod(varient.id) < 1
                                                                            ?
                                                                            <span className="bb-bldr-item_price bb-bldr-item_price_round" onClick={() => { varient.inventoryQuantity > 0 ? addProduct(varient.id, varient.displayName, parseFloat(varient.price), varient.product_image) : () => { } }}>
                                                                                {varient.inventoryQuantity > 0 ? <span className="bb-bldr-add-text" >{data.builderSettings.settings.display_settings.add_button_text.length > 0 ? data.builderSettings.settings.display_settings.add_button_text : 'Add:'} {window.store_currency}{data.builderSettings.settings.display_settings.is_show_price_in_button ? `${varient.price}` : ''} </span> : ' Sold out'}
                                                                            </span>
                                                                            : ''
                                                                    }
                                                                    {
                                                                        itemCountMethod(varient.id) > 0
                                                                            ?
                                                                            <span className="bb-bldr-item_price bb-bldr-item_price_round" onClick={() => removeProduct(varient.id, varient.displayName, parseFloat(varient.price))}><span
                                                                                className="bb-bldr-add-text">Remove:</span> {window.store_currency}{data.builderSettings.settings.display_settings.is_show_price_in_button ? `${varient.price}` : ''}</span>
                                                                            : ''
                                                                    }

                                                                </>
                                                        }

                                                        <strong style={{ height: 'fit-content' }}>{varient.displayName}</strong>

                                                    </div>
                                                </label>
                                            </div>

                                            <div className={"bb-modal micromodal-slide " + "img-modal-" + varient.id} id="bb-prod-info" aria-hidden="false">
                                                <div className="bb-modal__overlay" tabIndex="-1" data-micromodal-close="" bis_skin_checked="1">
                                                    <div className="bb-modal__container" role="dialog" aria-modal="true" aria-labelledby="bb-prod-info-title" bis_skin_checked="1" style={{backgroundColor : data.builderSettings.settings.style_settings.lightbox_background_color}}>
                                                        <header className="bb-modal__header">
                                                            <button type='button' className="bb-modal__close" style={{color: data.builderSettings.settings.style_settings.lightbox_text_color}} onClick={() => imageAction(varient.id, 'close')} aria-label="Close modal"></button>
                                                        </header>
                                                        <main className="bb-modal__content" id="bb-modal__content">
                                                            <div className="bb-bldr-row" bis_skin_checked="1">
                                                                <div className="bb-bldr-col-md-6" bis_skin_checked="1">
                                                                    <div className="bb-modal-img-container" id="bb-modal-img-container" bis_skin_checked="1">
                                                                        <div className="bb-owl-carousel bb-owl-theme bb-owl-loaded bb-owl-drag" bis_skin_checked="1">
                                                                            <div className="bb-owl-stage-outer" bis_skin_checked="1">
                                                                                <div className="bb-owl-stage" style={{ transform: 'translate3d(0px, 0px, 0px)', transition: 'all 0s ease 0s', width: '415px' }}>
                                                                                    <div className="bb-owl-item active" bis_skin_checked="1" style={{ width: '405px', marginRight: '10px' }}>
                                                                                        <div className="bb-owl-item" bis_skin_checked="1">
                                                                                            <img className="bb-bldr-img-fluid owl-lazy bb-full-width" src={varient.product_image} style={{ opacity: '1' }} />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div className="bb-owl-nav disabled" bis_skin_checked="1">
                                                                                <button type="button" role="presentation" className="bb-owl-prev">
                                                                                    <span aria-label="Previous"></span>
                                                                                </button>
                                                                                <button type="button" role="presentation" className="bb-owl-next">
                                                                                    <span aria-label="Next"></span>
                                                                                </button>
                                                                            </div>
                                                                            <div className="bb-owl-dots disabled" bis_skin_checked="1">
                                                                                <button role="button" className="bb-owl-dot active">
                                                                                    <span>
                                                                                    </span>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="bb-bldr-col-md-6" bis_skin_checked="1">
                                                                    <h2 className="bb-modal__title" style={{color: data.builderSettings.settings.style_settings.lightbox_text_color}} id="bb-prod-info-title">Product with image</h2>
                                                                    <p id="bb-prod-info-content" style={{color: data.builderSettings.settings.style_settings.lightbox_text_color}}>sdfghjkl</p>
                                                                    <div id="bb-modal-select" bis_skin_checked="1"></div>
                                                                    {
                                                                        itemCountMethod(varient.id) < 1
                                                                            ?
                                                                            <button type="button" id="bb-modal-add" onClick={() => addProduct(varient.id, varient.displayName, parseFloat(varient.price), varient.product_image)} className="bb-bldr-add">Add</button>

                                                                            :

                                                                            <button type="button" id="bb-modal-add" onClick={() => removeFromModal(varient.id)} className="bb-bldr-add">Remove</button>
                                                                    }
                                                                </div>
                                                            </div>
                                                        </main>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    })
                                }
                            </>

                            :

                            ''
                    }

                    {
                        element.show_variant_as == 1
                            ?

                            <>
                                {
                                    getFilteredVariants(element.selected_tab == 1 ? element.selected_products : element.collections, element.selected_tab, element.show_variant_as).map((product, i) => {
                                        let drropDownItem = itemCountMethodDropdown(product.product_id);

                                        return <div className={"bb-bldr-item bb-filt-sort "} style={{ marginBottom: '1.5rem' }} key={i} >

                                            <div className={"model_box " + (drropDownItem.count > 0 && "active-border")}>
                                                <label className={drropDownItem.count > 0 ? 'added-icon' : ''} style={{ display: 'flex', padding: '25px', paddingBottom: '0px' }}>

                                                    <div className="bb-qty-num" style={{ display: drropDownItem.count > 0 ? 'block' : 'none' }}>{drropDownItem.count}</div>

                                                    <figure >
                                                        <div className="bb-owl-carousel bb-owl-theme bb-owl-loaded bb-owl-drag" bis_skin_checked="1">
                                                            <div className="bb-owl-stage-outer" bis_skin_checked="1">
                                                                <div className="bb-owl-stage" style={{ transform: 'translate3d(0px, 0px, 0px)', transition: 'all 0s ease 0s', width: '268px' }} bis_skin_checked="1">
                                                                    <div className="bb-owl-item active" bis_skin_checked="1" style={{ width: '258px', marginRight: '10px' }}>
                                                                        <div className="bb-owl-item" bis_skin_checked="1">
                                                                            <a onClick={() => imageAction(product.product_id, 'open')} image="//cdn.shopify.com/s/files/1/0549/7998/5653/products/1_800x.jpg?v=1649226505" title-strip="asdfgh" title-desc="asdfgh">
                                                                                <img src={product.product_image} alt="Second Prodcut" className="bb-bldr-img-fluid owl-lazy" style={{ opacity: '1', width: '160px', height: '160px' }} />
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="bb-owl-nav disabled" bis_skin_checked="1">
                                                                <button type="button" role="presentation" className="bb-owl-prev">
                                                                    <span aria-label="Previous">‹</span>
                                                                </button>
                                                                <button type="button" role="presentation" className="bb-owl-next">
                                                                    <span aria-label="Next">›</span>
                                                                </button>
                                                            </div>
                                                            <div className="bb-owl-dots disabled" bis_skin_checked="1">
                                                                <button role="button" className="bb-owl-dot active">
                                                                    <span></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </figure>

                                                    <div>

                                                        {
                                                            element.is_allow_more_than_one ?
                                                                <>
                                                                    {
                                                                        drropDownItem.count > 0
                                                                            ?
                                                                            <span className="bb-bldr-qty-down bb-bldr-qty-round" style={{ display: 'inline-block' }} onClick={() => removeProduct(drropDownItem.variant.id, drropDownItem.variant.displayName, parseFloat(drropDownItem.variant_price))} >-</span>
                                                                            : ''
                                                                    }

                                                                    <span className="bb-bldr-item_price bb-bldr-item_price_round" onClick={() => drropDownItem.count > 0 ? addProduct(drropDownItem.variant.id, drropDownItem.variant.displayName, parseFloat(drropDownItem.variant_price), product.product_image, false, product.product_id) : showSelectVariantError()}>
                                                                        <span className="bb-bldr-add-text" >{data.builderSettings.settings.display_settings.add_button_text.length > 0 ? data.builderSettings.settings.display_settings.add_button_text : 'Add:'}</span> ${drropDownItem.count > 0 ? (drropDownItem.variant_price) : '--'}
                                                                    </span>

                                                                    {
                                                                        drropDownItem.count > 0
                                                                            ?
                                                                            <span className="bb-bldr-qty-up bb-bldr-qty-round" style={{ display: 'inline-block' }} onClick={() => drropDownItem.count > 0 ? addProduct(drropDownItem.variant.id, drropDownItem.variant.displayName, parseFloat(drropDownItem.variant_price), product.product_image, false, product.product_id) : showSelectVariantError()}>+</span>
                                                                            : ''
                                                                    }
                                                                </>
                                                                :
                                                                <>
                                                                    {
                                                                        drropDownItem.count < 1
                                                                            ?
                                                                            <span className="bb-bldr-item_price_round" onClick={() => drropDownItem.count > 0 ? addProduct(drropDownItem.variant.id, drropDownItem.variant.displayName, parseFloat(drropDownItem.variant_price), product.product_image, false, product.product_id) : showSelectVariantError()}>
                                                                                <span className="bb-bldr-add-text" >{data.builderSettings.settings.display_settings.add_button_text.length > 0 ? data.builderSettings.settings.display_settings.add_button_text : 'Add:'}</span> ${drropDownItem.count > 0 ? (drropDownItem.variant_price) : '--'}
                                                                            </span>
                                                                            : ''
                                                                    }
                                                                    {
                                                                        drropDownItem.count > 0
                                                                            ?
                                                                            <span className="bb-bldr-item_price_round" onClick={() => removeProduct(drropDownItem.variant.id, drropDownItem.variant.displayName, parseFloat(drropDownItem.variant_price))}><span
                                                                                className="bb-bldr-add-text">Remove:</span> {window.store_currency}{data.builderSettings.settings.display_settings.is_show_price_in_button ? `${varient.price}` : ''}</span>
                                                                            : ''
                                                                    }

                                                                </>
                                                        }

                                                        <strong style={{ height: 'fit-content' }}>{product.product_title}</strong>

                                                        <select className="bb-bldr-form-control bb-mt-10" id={`dropdown_${product.product_id}`} defaultValue={drropDownItem.count < 1 ? 'none' : drropDownItem.variant.id} onChange={(val) => { selectFromDropdown(val.target.value, product.product_id, product, product.product_image) }}>
                                                            <option value="none" >Choose a Hair Length...</option>
                                                            {
                                                                product.variants.map((variant, v_index) => {
                                                                    return <option key={v_index} price={variant.price} id={variant.id} value={v_index}>{variant.displayName}</option>
                                                                })
                                                            }

                                                        </select>

                                                    </div>
                                                </label>
                                            </div>

                                            <div className={"bb-modal micromodal-slide " + "img-modal-" + product.product_id} id="bb-prod-info" aria-hidden="false">
                                                <div className="bb-modal__overlay" tabIndex="-1" data-micromodal-close="" bis_skin_checked="1">
                                                    <div className="bb-modal__container" role="dialog" aria-modal="true" aria-labelledby="bb-prod-info-title" bis_skin_checked="1" style={{backgroundColor : data.builderSettings.settings.style_settings.lightbox_background_color}}>
                                                        <header className="bb-modal__header">
                                                            <button type='button' className="bb-modal__close" style={{color: data.builderSettings.settings.style_settings.lightbox_text_color}} onClick={() => imageAction(product.product_id, 'close')} aria-label="Close modal"></button>
                                                        </header>
                                                        <main className="bb-modal__content" id="bb-modal__content">
                                                            <div className="bb-bldr-row" bis_skin_checked="1">
                                                                <div className="bb-bldr-col-md-6" bis_skin_checked="1">
                                                                    <div className="bb-modal-img-container" id="bb-modal-img-container" bis_skin_checked="1">
                                                                        <div className="bb-owl-carousel bb-owl-theme bb-owl-loaded bb-owl-drag" bis_skin_checked="1">
                                                                            <div className="bb-owl-stage-outer" bis_skin_checked="1">
                                                                                <div className="bb-owl-stage" style={{ transform: 'translate3d(0px, 0px, 0px)', transition: 'all 0s ease 0s', width: '415px' }}>
                                                                                    <div className="bb-owl-item active" bis_skin_checked="1" style={{ width: '405px', marginRight: '10px' }}>
                                                                                        <div className="bb-owl-item" bis_skin_checked="1">
                                                                                            <img className="bb-bldr-img-fluid owl-lazy bb-full-width" src={product.product_image} style={{ opacity: '1' }} />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div className="bb-owl-nav disabled" bis_skin_checked="1">
                                                                                <button type="button" role="presentation" className="bb-owl-prev">
                                                                                    <span aria-label="Previous"></span>
                                                                                </button>
                                                                                <button type="button" role="presentation" className="bb-owl-next">
                                                                                    <span aria-label="Next"></span>
                                                                                </button>
                                                                            </div>
                                                                            <div className="bb-owl-dots disabled" bis_skin_checked="1">
                                                                                <button role="button" className="bb-owl-dot active">
                                                                                    <span>
                                                                                    </span>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="bb-bldr-col-md-6" bis_skin_checked="1">
                                                                    <h2 className="bb-modal__title" style={{color: data.builderSettings.settings.style_settings.lightbox_text_color}} id="bb-prod-info-title">Product with image</h2>
                                                                    <p id="bb-prod-info-content" style={{color: data.builderSettings.settings.style_settings.lightbox_text_color}}>sdfghjkl</p>
                                                                    <div id="bb-modal-select" bis_skin_checked="1"></div>
                                                                    <select className="bb-bldr-form-control bb-mt-10" id={`dropdown_${product.product_id}`} defaultValue={drropDownItem.count < 1 ? 'none' : drropDownItem.variant.id} onChange={(val) => { selectFromDropdown(val.target.value, product.product_id, product, product.product_image), imageAction(product.product_id, 'close') }}>
                                                                        <option value="none" >Choose a Hair Length...</option>
                                                                        {
                                                                            product.variants.map((variant, v_index) => {
                                                                                return <option key={v_index} price={variant.price} id={variant.id} value={v_index}>{variant.displayName}</option>
                                                                            })
                                                                        }
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </main>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    })
                                }
                            </>

                            :

                            ''
                    }


                </div>

            </div>

            <div id="bb-paginate-spinner" style={{ display: 'none' }}>
                <img src="https://builder-front.boxup.io/img/bxp-loader.gif" />
            </div>
            <div id="bb-toast" className="max-items" style={{ bottom: '76px' }}>You can only select {maxItems} product(s)</div>

        </div>

    )
}

export default BasicProduct