import React, { useEffect, useState } from 'react'
import Summary from '../../layoutFull/partials/Summary';


function PreSelectedProducts({ options, removeFromModal, data, setOptions, addProduct, removeProduct, element, maxItems, setFormData }) {

    function preselectProducts() {
        let search = options.items.find(ele => { return ele.step === options.step });
        if (search) {

        } else {
            data.builderSteps.forEach((ele, i) => {
                if (ele.type == "pre_selected") {
                    ele.selected_products.forEach((pro) => {
                        pro.variants.forEach((varient) => {

                            for (let index = 0; index < varient.quantity; index++) {
                                addProduct(varient.id, varient.displayName, parseFloat(varient.price), pro.image, true,null ,false);
                            }

                        })
                    })
                }
            })
        }
    }
    useEffect(() => {
        preselectProducts()
    }, [])

    // Check item count from item
    function itemCountMethod(s_id) {

        let itemarray = options.items;

        const searchItem = (what) => itemarray.find(element => (element.id === what && element.step == options.step));

        if (itemarray.length > 0) {
            let searchedArray = searchItem(s_id);
            let result = 0;
            if (typeof searchedArray != 'undefined') {
                result = searchedArray.itemCount;
            }
            return (
                result
            );
        } else {
            return (
                0
            );
        }
    }

    // Modal actions.
    const imageAction = (id, action) => {
        if (action == 'open') {
            document.getElementsByClassName(`img-modal-${id}`)[0].classList.add('is-open');
        }
        if (action == 'close') {
            document.getElementsByClassName(`img-modal-${id}`)[0].classList.remove('is-open');
        }
    }
    // preselectProducts();

    return (

        <div id="bb-bldr-middle-wizard" className="bb-bldr-wizard-branch bb-bldr-wizard-wrapper">

            <div className="bb-bldr-step bb-bldr-current bb-bldr-wizard-step" data-index="0"
                data-steptype="product">
                <div className="bb-bldr-question_title">
                    <h3 className="bb-bldr-wizard-header">{element.title}
                    </h3>
                    <p>{element.description}</p>
                </div>


                <Summary element={element} data={data} options={options} setOptions={setOptions} setFormData={setFormData} theme="right" removeFromModal={removeFromModal}/>


                <div className="bb-bldr-row bb-bldr-justify-content-center">

                </div>


                <div className="bb-step-content" >
                    {
                        element.selected_tab == 1 ?
                            <>
                                {
                                    element.selected_products.map((product) => {
                                        return <React.Fragment key={product.id}>

                                            {
                                                product.variants.map((varient) => {
                                                    return <div className={"bb-bldr-item bb-filt-sort "} style={{ marginBottom: '1.5rem' }} key={varient.id} >

                                                        <div className={"model_box " + (itemCountMethod(varient.id) > 0 && "active-border")}>
                                                            <label className={itemCountMethod(varient.id) > 0 ? 'added-icon' : ''} style={{ display: 'flex', padding: '25px', paddingBottom: '0px' }}>

                                                                <div className="bb-qty-num" style={{ display: itemCountMethod(varient.id) > 0 ? 'block' : 'none' }}>{itemCountMethod(varient.id)}</div>

                                                                <figure >
                                                                    <div className="bb-owl-carousel bb-owl-theme bb-owl-loaded bb-owl-drag" bis_skin_checked="1">
                                                                        <div className="bb-owl-stage-outer" bis_skin_checked="1">
                                                                            <div className="bb-owl-stage" style={{ transform: 'translate3d(0px, 0px, 0px)', transition: 'all 0s ease 0s', width: '268px' }}>
                                                                                <div className="bb-owl-item active" bis_skin_checked="1" style={{ width: '258px', marginRight: '10px' }}>
                                                                                    <div className="bb-owl-item" bis_skin_checked="1">
                                                                                        {
                                                                                            data.builderSettings.settings.display_settings.is_enable_image_lightbox
                                                                                                ?
                                                                                                <div className="bb-overlay-icon" onClick={() => imageAction(varient.id, 'open')}><span className="bb-icon"></span></div>
                                                                                                :
                                                                                                ''
                                                                                        }

                                                                                        <a image="//cdn.shopify.com/s/files/1/0549/7998/5653/products/1_800x.jpg?v=1649226505" title-strip="asdfgh" title-desc="asdfgh">
                                                                                            <img src={product.image} alt="Second Prodcut" className="bb-bldr-img-fluid owl-lazy" style={{ opacity: '1', width: '160px', height: '160px' }} />
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="bb-owl-nav disabled" bis_skin_checked="1"><button type="button" role="presentation" className="bb-owl-prev"><span aria-label="Previous">‹</span></button><button type="button" role="presentation" className="bb-owl-next"><span aria-label="Next">›</span></button></div><div className="bb-owl-dots disabled" bis_skin_checked="1"><button role="button" className="bb-owl-dot active"><span></span></button></div></div>
                                                                </figure>

                                                                <div>
                                                                    <span className="bb-bldr-item_price bb-bldr-item_price_round">
                                                                        <span className="bb-bldr-add-text">{element.button_text}</span>
                                                                    </span>



                                                                    <strong style={{ height: 'fit-content' }}>{varient.displayName}</strong>

                                                                </div>
                                                            </label>
                                                        </div>

                                                        <div className={"bb-modal micromodal-slide " + "img-modal-" + varient.id} id="bb-prod-info" aria-hidden="false">
                                                            <div className="bb-modal__overlay" tabIndex="-1" data-micromodal-close="" bis_skin_checked="1">
                                                                <div className="bb-modal__container" role="dialog" aria-modal="true" aria-labelledby="bb-prod-info-title" bis_skin_checked="1" style={{backgroundColor : data.builderSettings.settings.style_settings.lightbox_background_color}}>
                                                                    <header className="bb-modal__header">
                                                                        <button type='button' className="bb-modal__close" style={{color: data.builderSettings.settings.style_settings.lightbox_text_color}} onClick={() => imageAction(varient.id, 'close')} aria-label="Close modal"></button>
                                                                    </header>
                                                                    <main className="bb-modal__content" id="bb-modal__content">
                                                                        <div className="bb-bldr-row" bis_skin_checked="1">
                                                                            <div className="bb-bldr-col-md-6" bis_skin_checked="1">
                                                                                <div className="bb-modal-img-container" id="bb-modal-img-container" bis_skin_checked="1">
                                                                                    <div className="bb-owl-carousel bb-owl-theme bb-owl-loaded bb-owl-drag" bis_skin_checked="1">
                                                                                        <div className="bb-owl-stage-outer" bis_skin_checked="1">
                                                                                            <div className="bb-owl-stage" style={{ transform: 'translate3d(0px, 0px, 0px)', transition: 'all 0s ease 0s', width: '415px' }}>
                                                                                                <div className="bb-owl-item active" bis_skin_checked="1" style={{ width: '405px', marginRight: '10px' }}>
                                                                                                    <div className="bb-owl-item" bis_skin_checked="1">
                                                                                                        <img className="bb-bldr-img-fluid owl-lazy bb-full-width" src={product.image} style={{ opacity: '1' }} />
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div className="bb-owl-nav disabled" bis_skin_checked="1">
                                                                                            <button type="button" role="presentation" className="bb-owl-prev">
                                                                                                <span aria-label="Previous"></span>
                                                                                            </button>
                                                                                            <button type="button" role="presentation" className="bb-owl-next">
                                                                                                <span aria-label="Next"></span>
                                                                                            </button>
                                                                                        </div>
                                                                                        <div className="bb-owl-dots disabled" bis_skin_checked="1">
                                                                                            <button role="button" className="bb-owl-dot active">
                                                                                                <span>
                                                                                                </span>
                                                                                            </button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div className="bb-bldr-col-md-6" bis_skin_checked="1">
                                                                                <h2 className="bb-modal__title" style={{color: data.builderSettings.settings.style_settings.lightbox_text_color}} id="bb-prod-info-title">Product with image</h2>
                                                                                <p id="bb-prod-info-content" style={{color: data.builderSettings.settings.style_settings.lightbox_text_color}}>sdfghjkl</p>
                                                                                <div id="bb-modal-select" bis_skin_checked="1"></div>
                                                                                {/* {
                                                                                    itemCountMethod(varient.id) < 1
                                                                                        ?
                                                                                        <button type="button" id="bb-modal-add" onClick={() => addProduct(varient.id, varient.displayName, parseFloat(varient.price))} className="bb-bldr-add">Add</button>

                                                                                        :

                                                                                        <button type="button" id="bb-modal-add" onClick={() => removeFromModal(varient.id)} className="bb-bldr-add">Remove</button>
                                                                                } */}
                                                                            </div>
                                                                        </div>
                                                                    </main>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                })
                                            }
                                        </React.Fragment>
                                    })
                                }
                            </>
                            : ''
                    }

                    {
                        element.selected_tab == 0 ?
                            <>
                                {
                                    element.collections.map((collection) => {
                                        return <React.Fragment key={collection.id}>
                                            {
                                                collection.products.map((product) => {
                                                    return <React.Fragment key={product.id}>

                                                        {
                                                            product.variants.map((varient) => {

                                                                return <div className={"bb-bldr-item bb-filt-sort "} style={{ marginBottom: '1.5rem' }} key={varient.id} >

                                                                    <div className={"model_box " + (itemCountMethod(varient.id) > 0 && "active-border")}>
                                                                        <label className={itemCountMethod(varient.id) > 0 ? 'added-icon' : ''} style={{ display: 'flex', padding: '25px', paddingBottom: '0px' }}>

                                                                            <div className="bb-qty-num" style={{ display: itemCountMethod(varient.id) > 0 ? 'block' : 'none' }}>{itemCountMethod(varient.id)}</div>

                                                                            <figure >
                                                                                <div className="bb-owl-carousel bb-owl-theme bb-owl-loaded bb-owl-drag" bis_skin_checked="1">
                                                                                    <div className="bb-owl-stage-outer" bis_skin_checked="1"><div className="bb-owl-stage" style={{ transform: 'translate3d(0px, 0px, 0px)', transition: 'all 0s ease 0s', width: '268px' }} bis_skin_checked="1"><div className="bb-owl-item active" bis_skin_checked="1" style={{ width: '258px', marginRight: '10px' }}><div className="bb-owl-item" bis_skin_checked="1">

                                                                                        {
                                                                                            data.builderSettings.settings.display_settings.is_enable_image_lightbox
                                                                                                ?
                                                                                                <div className="bb-overlay-icon" onClick={() => imageAction(varient.id, 'open')}><span className="bb-icon"></span></div>
                                                                                                :
                                                                                                ''
                                                                                        }

                                                                                        <a image="//cdn.shopify.com/s/files/1/0549/7998/5653/products/1_800x.jpg?v=1649226505" title-strip="asdfgh" title-desc="asdfgh">
                                                                                            <img src={collection.image.originalSrc} alt="Second Prodcut" className="bb-bldr-img-fluid owl-lazy" style={{ opacity: '1', width: '160px', height: '160px' }} />
                                                                                        </a>

                                                                                    </div></div></div></div><div className="bb-owl-nav disabled" bis_skin_checked="1"><button type="button" role="presentation" className="bb-owl-prev"><span aria-label="Previous">‹</span></button><button type="button" role="presentation" className="bb-owl-next"><span aria-label="Next">›</span></button></div><div className="bb-owl-dots disabled" bis_skin_checked="1"><button role="button" className="bb-owl-dot active"><span></span></button></div></div>
                                                                            </figure>

                                                                            <div>

                                                                                <span className="bb-bldr-item_price bb-bldr-item_price_round">
                                                                                    <span className="bb-bldr-add-text">{element.button_text}</span>
                                                                                </span>

                                                                                <strong style={{ height: 'fit-content' }}>{varient.displayName}</strong>

                                                                            </div>
                                                                        </label>
                                                                    </div>

                                                                    <div className={"bb-modal micromodal-slide " + "img-modal-" + varient.id} id="bb-prod-info" aria-hidden="false">
                                                                        <div className="bb-modal__overlay" tabIndex="-1" data-micromodal-close="" bis_skin_checked="1">
                                                                            <div className="bb-modal__container" role="dialog" aria-modal="true" aria-labelledby="bb-prod-info-title" bis_skin_checked="1" style={{backgroundColor : data.builderSettings.settings.style_settings.lightbox_background_color}}>
                                                                                <header className="bb-modal__header">
                                                                                    <button type='button' className="bb-modal__close" style={{color: data.builderSettings.settings.style_settings.lightbox_text_color}} onClick={() => imageAction(varient.id, 'close')} aria-label="Close modal"></button>
                                                                                </header>
                                                                                <main className="bb-modal__content" id="bb-modal__content">
                                                                                    <div className="bb-bldr-row" bis_skin_checked="1">
                                                                                        <div className="bb-bldr-col-md-6" bis_skin_checked="1">
                                                                                            <div className="bb-modal-img-container" id="bb-modal-img-container" bis_skin_checked="1">
                                                                                                <div className="bb-owl-carousel bb-owl-theme bb-owl-loaded bb-owl-drag" bis_skin_checked="1">
                                                                                                    <div className="bb-owl-stage-outer" bis_skin_checked="1">
                                                                                                        <div className="bb-owl-stage" style={{ transform: 'translate3d(0px, 0px, 0px)', transition: 'all 0s ease 0s', width: '415px' }}>
                                                                                                            <div className="bb-owl-item active" bis_skin_checked="1" style={{ width: '405px', marginRight: '10px' }}>
                                                                                                                <div className="bb-owl-item" bis_skin_checked="1">
                                                                                                                    <img className="bb-bldr-img-fluid owl-lazy bb-full-width" src={product.image} style={{ opacity: '1' }} />
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div className="bb-owl-nav disabled" bis_skin_checked="1">
                                                                                                        <button type="button" role="presentation" className="bb-owl-prev">
                                                                                                            <span aria-label="Previous"></span>
                                                                                                        </button>
                                                                                                        <button type="button" role="presentation" className="bb-owl-next">
                                                                                                            <span aria-label="Next"></span>
                                                                                                        </button>
                                                                                                    </div>
                                                                                                    <div className="bb-owl-dots disabled" bis_skin_checked="1">
                                                                                                        <button role="button" className="bb-owl-dot active">
                                                                                                            <span>
                                                                                                            </span>
                                                                                                        </button>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div className="bb-bldr-col-md-6" bis_skin_checked="1">
                                                                                            <h2 className="bb-modal__title" style={{color: data.builderSettings.settings.style_settings.lightbox_text_color}} id="bb-prod-info-title">Product with image</h2>
                                                                                            <p id="bb-prod-info-content" style={{color: data.builderSettings.settings.style_settings.lightbox_text_color}}>sdfghjkl</p>
                                                                                            <div id="bb-modal-select" bis_skin_checked="1"></div>
                                                                                            {/* {
                                                                                                itemCountMethod(varient.id) < 1
                                                                                                    ?
                                                                                                    <button type="button" id="bb-modal-add" onClick={() => addProduct(varient.id, varient.displayName, parseFloat(varient.price))} className="bb-bldr-add">Add</button>

                                                                                                    :

                                                                                                    <button type="button" id="bb-modal-add" onClick={() => removeFromModal(varient.id)} className="bb-bldr-add">Remove</button>
                                                                                            } */}
                                                                                        </div>
                                                                                    </div>
                                                                                </main>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                            })
                                                        }
                                                    </React.Fragment>
                                                })
                                            }
                                        </React.Fragment>
                                    })
                                }

                            </>
                            : ''
                    }

                </div>

            </div>

            <div id="bb-paginate-spinner" style={{ display: 'none' }}>
                <img src="https://builder-front.boxup.io/img/bxp-loader.gif" />
            </div>
            <div id="bb-toast" className="max-items" style={{ bottom: '76px' }}>You can only select {maxItems} product(s)</div>

        </div>

    )
}

export default PreSelectedProducts;