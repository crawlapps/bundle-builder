import React from 'react'
import Content from '../layoutFull/partials/Content';
import FormStep from '../layoutFull/partials/formStep';
import ProgressBar from '../layoutFull/partials/progressBar';
import BasicProduct from './partials/basicProduct';
import PreSelectedProducts from './partials/preSelectedProducts';

function MainWizard(
    {
        options,
        data,
        setOptions,
        minItems,
        incrementStep,
        decrementStep,
        complatePurchase,
        formData,
        setFormData,
        showSelectVariantError,
        filters,
        setFilters,
        getFilteredVariants,
        maxItems,
        addProduct,
        removeProduct,
        removeFromModal,
        getTags
    }) {

    return (
        <main className="bb_main bxp-theme-right" id="bb_main">
            <div className="bb_container" >

                {
                    data.builderSettings.settings.display_settings.is_show_step_progress
                        ?
                        <ProgressBar data={data} options={options} />
                        :
                        ''
                }

                <div id="bb-bldr-wizard_container" className="wizard" >
                    <div className='bb-bldr-row'>
                        <div className='bb-bldr-col-lg-8'>
                            {
                                data.builderSteps.map((element, index) => {

                                    if (options.step == element.display_order) {

                                        if (element.type == 'product') {
                                            return <React.Fragment key={index}>
                                                <BasicProduct
                                                    key={index}
                                                    options={options}
                                                    setFormData={setFormData}
                                                    data={data}
                                                    removeFromModal={removeFromModal}
                                                    setOptions={setOptions}
                                                    addProduct={addProduct}
                                                    removeProduct={removeProduct}
                                                    element={element}
                                                    maxItems={maxItems}
                                                    showSelectVariantError={showSelectVariantError}
                                                    filters={filters}
                                                    setFilters={setFilters}
                                                    getFilteredVariants={getFilteredVariants}
                                                    getTags={getTags}
                                                />
                                            </React.Fragment>

                                        } else if (element.type == 'pre_selected') {
                                            return <React.Fragment key={index}>
                                                <PreSelectedProducts
                                                    key={index}
                                                    options={options}
                                                    setFormData={setFormData}
                                                    data={data}
                                                    removeFromModal={removeFromModal}
                                                    setOptions={setOptions}
                                                    addProduct={addProduct}
                                                    removeProduct={removeProduct}
                                                    element={element}
                                                    maxItems={maxItems}
                                                />
                                            </React.Fragment>
                                        } else if (element.type == 'form') {
                                            return <FormStep
                                                key={index}
                                                options={options}
                                                data={data}
                                                setOptions={setOptions}
                                                element={element}
                                                maxItems={maxItems}
                                                formData={formData}
                                                setFormData={setFormData}
                                            />
                                        } else if (element.type == 'content') {
                                            return <Content
                                                key={index}
                                                options={options}
                                                data={data}
                                                element={element}
                                            />
                                        }

                                    }

                                })
                            }

                        </div>
                        <div className="bxp-bldr-col-lg-4" id="bxp-bldr-sidebar" style={{ position: 'relative', overflow: 'visible', boxSizing: 'border-box', minHeight: '1px' }} >


                            <div className="theiaStickySidebar" style={{ paddingTop: '1px', paddingBottom: '1px', position: 'sticky', transform: 'none', top: '0px', left: '1156.5px' }} >
                                <div id="border_box" style={{ backgroundColor: data.builderSettings.settings.style_settings.total_box_background_color }}>
                                    <div id="bxp-order_summary" >
                                        <div id="" style={{ display: 'block !important', marginBottom: '25px', color: data.builderSettings.settings.style_settings.order_box_details_product_text_color }}>
                                            <h4>Order details</h4>
                                            Base Price:
                                            <span className="bxp-details-price">${data.builderSettings.start_price}</span>
                                            <br />
                                            {
                                                options.items.map((element, i) => {
                                                    return <div key={i} style={{ display: 'block', marginBottom: '-15px', marginTop: '5px' }}>
                                                        {element.itemCount}x &nbsp;
                                                        {element.title}
                                                        <span className="bxp-details-price">: {window.store_currency} {element.price}</span>
                                                        <br />
                                                        <br />
                                                    </div>;
                                                })
                                            }
                                            {
                                                options.discounts.map((discount, i) => {
                                                    return (
                                                        <React.Fragment key={i}>
                                                            <span className="bxp-discount" >{window.store_currency} {discount.value} {discount.title} {discount.type == 'percent' ? discount.amount + ' : %' : ''}</span>
                                                            <br />
                                                        </React.Fragment>
                                                    );
                                                })
                                            }
                                        </div>
                                        <div id="bxp-bldr-price_total" className='d-flex justify-content-between my-10' style={{ float: 'inherit' }} >
                                            <h3 style={{ color: data.builderSettings.settings.style_settings.total_text_color }}>Total: </h3>
                                            <span id="bxp-bldr-total_value" style={{ color: data.builderSettings.settings.style_settings.total_price_text_color }}> {window.store_currency} {options.mainAmount}</span>
                                        </div>
                                    </div>
                                    <div className="bxp-bldr-clearfix bxp-bldr-buttons" style={{ textAlign: 'right' }} >

                                        {
                                            // Back button
                                            options.step < 2 ? '' : <button type="button" name="backward" className="bxp-bldr-backward" onClick={decrementStep}>Back </button>
                                        }
                                        {
                                            // Next button
                                            (options.step > 0 && options.totalSteps != options.step) ? <button type="button" name="forward" className="bxp-bldr-forward" onClick={incrementStep}>Next</button> : ''
                                        }
                                        {
                                            // Complate Btn
                                            options.totalSteps == options.step ? <button type="button" name="process" id="bxp-complete" className="bb-bldr-submit" onClick={() => complatePurchase(1)}>Complete</button> : ''
                                        }
                                        <button type="button" name="loading" id="bxp-loading" className="bb-bldr-submit">
                                            <img src="https://builder-front.boxup.io/img/loader.svg" style={{ height: '0.9375rem' }} />
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div id="bb-toast" bis_skin_checked="1" className="min-toast nextError" style={{ bottom: '76px' }}>Please select at least {minItems} product(s)</div>

                        </div>
                    </div>
                </div>
            </div>
        </main>
    )
}

export default MainWizard