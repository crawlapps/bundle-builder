import React, { useRef, useState } from 'react'
import ScrollContainer from "react-indiana-drag-scroll";

function WizardFooter({ options, setOptions, data, minItems, incrementStep, decrementStep, complatePurchase }) {



    return (
        <>
            <div id="bxp-bldr-bottom-wizard" className="d-flex_custom" bis_skin_checked="1" style={{ backgroundColor: data.builderSettings.settings.style_settings.total_box_background_color }}>
                <div>


                    <div id="bxp-bldr-price_total" bis_skin_checked="1">
                        <h3 style={{ color: data.builderSettings.settings.style_settings.total_text_color }}>Total: </h3><span id="bxp-bldr-total_value" style={{ color: data.builderSettings.settings.style_settings.total_price_text_color }}> {window.store_currency} {options.mainAmount}</span>

                    </div>
                    <a id="bxp-bldr-details_bt" href="#" className="show">
                        <div id="bxp-bldr-order_details" bis_skin_checked="1" style={{ color: data.builderSettings.settings.style_settings.order_box_details_product_text_color, backgroundColor: data.builderSettings.settings.style_settings.total_box_background_color }}>

                            <h4>Order details</h4>
                            Base Price:
                            <span className="bxp-details-price">{window.store_currency} {data.builderSettings.start_price}</span>
                            <br />
                            {
                                options.items.map((element, i) => {
                                    return <div key={i} style={{ display: 'block', marginBottom: '-15px', marginTop: '5px' }}>
                                        {element.itemCount}x &nbsp;
                                        {element.title}
                                        <span className="bxp-details-price">: {window.store_currency} {element.price}</span>
                                        <br />
                                        <br />
                                    </div>;
                                })
                            }
                            {
                                options.discounts.map((discount, i) => {
                                    return (
                                        <React.Fragment key={i}>
                                            <span className="bxp-discount" >{window.store_currency} {discount.value} {discount.title} {discount.type == 'percent' ? discount.amount + ' : %' : ''}</span>
                                            <br />
                                        </React.Fragment>
                                    );
                                })
                            }

                        </div>
                    </a>
                </div>

                <div id="bb-toast" bis_skin_checked="1" className="min-toast nextError" style={{ bottom: '76px' }}>Please select at least {minItems} product(s)</div>

                {
                    data.builderSettings.settings.theme.is_show_image_in_footer
                        ?
                        <div className="bxp-sel-images-container">
                            <ScrollContainer className="mx-auto bxp-sel-images" id="bxp-sel-images" hideScrollbars={true}>
                                {
                                    options.items.map((item, i) => {
                                        return (
                                            <div key={i} className="bxp-thumbnail-container" title="Product with image" ind="1">
                                                <img className="bxp-thumbnail" src={item.img} />
                                                <span className="bxp-thumbnail-qty">{item.itemCount}</span>
                                            </div>
                                        );
                                    })
                                }
                            </ScrollContainer>
                        </div>
                        :
                        ''
                }

                <div className="bxp-bldr-float-right clearfix d-flex" bis_skin_checked="1">

                    {
                        // Back button
                        options.step < 2 ? '' : <button type="button" name="backward" className="bxp-bldr-backward" onClick={decrementStep} style={{ backgroundColor: data.builderSettings.settings.style_settings.secondary_button_color }}>Back </button>
                    }
                    {
                        // Next button
                        (options.step > 0 && options.totalSteps != options.step) ? <button type="button" name="forward" className="bxp-bldr-forward" onClick={incrementStep} style={{ backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }}>Next</button> : ''
                    }
                    {
                        // Complate Btn
                        options.totalSteps == options.step ? <button type="button" name="process" id="bxp-complete" className="bb-bldr-submit" onClick={() => complatePurchase(1)} style={{ backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }}>Complete</button> : ''
                    }


                    <button type="button" name="loading" id="bxp-loading" className="bb-bldr-submit">
                        <img src="https://builder-front.boxup.io/img/loader.svg" style={{ height: '0.9375rem' }} />
                    </button>
                </div>

            </div>
        </>
    )

}

export default WizardFooter