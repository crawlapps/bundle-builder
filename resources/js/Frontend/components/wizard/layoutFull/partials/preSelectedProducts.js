import React, { useEffect, useState } from 'react'
import Summary from './Summary';


function PreSelectedProducts({ options, removeFromModal, data, setOptions, addProduct, removeProduct, element, maxItems, setFormData }) {

    function preselectProducts() {

        let search = options.items.find(ele => { return ele.step === options.step });
        if (search) {

        } else {
            data.builderSteps.forEach((ele, i) => {
                if (ele.type == "pre_selected") {
                    ele.selected_products.forEach((pro) => {
                        pro.variants.forEach((varient) => {

                            for (let index = 0; index < varient.quantity; index++) {
                                addProduct(varient.id, varient.displayName, parseFloat(varient.price), pro.image, true, null,false);
                            }

                        })
                    })
                }
            })
        }
    }
    useEffect(() => {
        preselectProducts()
    }, [])

    // Check item count from item
    function itemCountMethod(s_id) {

        let itemarray = options.items;

        const searchItem = (what) => itemarray.find(element => (element.id === what && element.step == options.step));

        if (itemarray.length > 0) {
            let searchedArray = searchItem(s_id);
            let result = 0;
            if (typeof searchedArray != 'undefined') {
                result = searchedArray.itemCount;
            }
            return (
                result
            );
        } else {
            return (
                0
            );
        }
    }

    // Modal actions.
    const imageAction = (id, action) => {
        if (action == 'open') {
            document.getElementsByClassName(`img-modal-${id}`)[0].classList.add('is-open');
        }
        if (action == 'close') {
            document.getElementsByClassName(`img-modal-${id}`)[0].classList.remove('is-open');
        }
    }
    // preselectProducts();

    return (

        <div id="bb-bldr-middle-wizard" className="bb-bldr-wizard-branch bb-bldr-wizard-wrapper">

            <div className="bb-bldr-step bb-bldr-current bb-bldr-wizard-step" data-index="0"
                data-steptype="product">
                <div className="bb-bldr-question_title">
                    <h3 className="bb-bldr-wizard-header">{element.title}
                    </h3>
                    <p>{element.description}</p>
                </div>

                <Summary element={element} data={data} options={options} setOptions={setOptions} setFormData={setFormData} removeFromModal={removeFromModal}/>

                <div className="bb-bldr-row bb-bldr-justify-content-center">

                </div>


                <div className="bb-bldr-row bb-bldr-justify-content-center bb-step-content" style={{ display: 'flex', marginBottom: '100px' }}>
                    {
                        element.selected_tab == 1 ?
                            <>
                                {
                                    element.selected_products.map((product) => {
                                        return <React.Fragment key={product.id}>

                                            {
                                                product.variants.map((varient) => {
                                                    return <div className={"bb-bldr-col-md-4 bb-filt-sort "} style={{ marginBottom: '1.5rem' }} key={varient.id} >

                                                        <div className={"bb-bldr-item "} style={{ border: (itemCountMethod(varient.id) > 0 ? `1px solid ${data.builderSettings.settings.style_settings.product_select_color}` : ""), borderRadius: '5px' }}>

                                                            <label
                                                                className={itemCountMethod(varient.id) > 0 ? 'added-icon' : ''}
                                                            >
                                                                {
                                                                    itemCountMethod(varient.id) > 0 &&
                                                                    <div className="tick" style={{ background: data.builderSettings.settings.style_settings.product_select_color }}>
                                                                        <svg width="700pt" height="700pt" version="1.1" viewBox="0 0 700 700" xmlns="http://www.w3.org/2000/svg">
                                                                            <path d="m607.43 132.12-320.6 320.43c-3.2773 3.2578-7.7148 5.0898-12.336 5.0898-4.625 0-9.0586-1.832-12.34-5.0898l-169.57-169.4c-3.2578-3.2773-5.0898-7.7109-5.0898-12.336s1.832-9.0586 5.0898-12.336c3.2852-3.3164 7.7578-5.1797 12.426-5.1797s9.1406 1.8633 12.426 5.1797l157.5 157.5 307.65-308.53c4.4102-4.4375 10.852-6.1875 16.898-4.5898s10.785 6.3008 12.426 12.336c1.6406 6.0391-0.066407 12.492-4.4727 16.93z"></path>
                                                                        </svg>
                                                                    </div>
                                                                }
                                                                <div className="bb-qty-num" style={{ display: itemCountMethod(varient.id) > 0 ? 'block' : 'none', backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }}>{itemCountMethod(varient.id)}</div>
                                                                <div className="bb-owl-carousel bb-owl-theme bb-owl-loaded bb-owl-drag">

                                                                    <div className="bb-owl-stage-outer"><div className="bb-owl-stage" style={{ transform: 'translate3d(0px, 0px, 0px)', transition: 'all 0s ease 0s', width: '268px' }}><div className="bb-owl-item active" style={{ width: '258px', marginRight: '10px' }}><div className="bb-owl-item">
                                                                        {
                                                                            data.builderSettings.settings.display_settings.is_enable_image_lightbox
                                                                                ?
                                                                                <div className="bb-overlay-icon" onClick={() => imageAction(varient.id, 'open')}><span className="bb-icon"></span></div>
                                                                                :
                                                                                ''
                                                                        }
                                                                        <a href="#" image="https://builder-front.boxup.io/img/placeholder.png" >
                                                                            <img alt="Test Bulky1" className="bb-bldr-img-fluid owl-lazy ls-is-cached lazyloaded" src={product.image} style={{ opacity: '1' }} />
                                                                        </a>

                                                                    </div></div></div></div><div className="bb-owl-nav disabled"><button type="button" role="presentation" className="bb-owl-prev"><span aria-label="Previous">‹</span></button><button type="button" role="presentation" className="bb-owl-next"><span aria-label="Next">›</span></button></div><div className="bb-owl-dots disabled"><button role="button" className="bb-owl-dot active"><span></span></button></div></div>

                                                                <span className="bb-bldr-item_price  mt-15" style={{ backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }}>
                                                                    <span className="bb-bldr-add-text" >{element.button_text}</span>
                                                                </span>

                                                                <strong>{varient.displayName}</strong>
                                                            </label>
                                                        </div>

                                                        <div className={"bb-modal micromodal-slide " + "img-modal-" + varient.id} id="bb-prod-info" aria-hidden="false">
                                                            <div className="bb-modal__overlay" tabIndex="-1" data-micromodal-close="" bis_skin_checked="1">
                                                                <div className="bb-modal__container" role="dialog" aria-modal="true" aria-labelledby="bb-prod-info-title" bis_skin_checked="1" style={{ backgroundColor: data.builderSettings.settings.style_settings.lightbox_background_color }}>
                                                                    <header className="bb-modal__header">
                                                                        <button type='button' className="bb-modal__close" style={{ color: data.builderSettings.settings.style_settings.lightbox_text_color }} onClick={() => imageAction(varient.id, 'close')} aria-label="Close modal"></button>
                                                                    </header>
                                                                    <main className="bb-modal__content" id="bb-modal__content">
                                                                        <div className="bb-bldr-row" bis_skin_checked="1">
                                                                            <div className="bb-bldr-col-md-6" bis_skin_checked="1">
                                                                                <div className="bb-modal-img-container" id="bb-modal-img-container" bis_skin_checked="1">
                                                                                    <div className="bb-owl-carousel bb-owl-theme bb-owl-loaded bb-owl-drag" bis_skin_checked="1">
                                                                                        <div className="bb-owl-stage-outer" bis_skin_checked="1">
                                                                                            <div className="bb-owl-stage" style={{ transform: 'translate3d(0px, 0px, 0px)', transition: 'all 0s ease 0s', width: '415px' }}>
                                                                                                <div className="bb-owl-item active" bis_skin_checked="1" style={{ width: '405px', marginRight: '10px' }}>
                                                                                                    <div className="bb-owl-item" bis_skin_checked="1">
                                                                                                        <img className="bb-bldr-img-fluid owl-lazy bb-full-width" src={product.image} style={{ opacity: '1' }} />
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div className="bb-owl-nav disabled" bis_skin_checked="1">
                                                                                            <button type="button" role="presentation" className="bb-owl-prev">
                                                                                                <span aria-label="Previous"></span>
                                                                                            </button>
                                                                                            <button type="button" role="presentation" className="bb-owl-next">
                                                                                                <span aria-label="Next"></span>
                                                                                            </button>
                                                                                        </div>
                                                                                        <div className="bb-owl-dots disabled" bis_skin_checked="1">
                                                                                            <button role="button" className="bb-owl-dot active">
                                                                                                <span>
                                                                                                </span>
                                                                                            </button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div className="bb-bldr-col-md-6" bis_skin_checked="1">
                                                                                <h2 className="bb-modal__title" style={{ color: data.builderSettings.settings.style_settings.lightbox_text_color }} id="bb-prod-info-title">{product.title}</h2>
                                                                                <p id="bb-prod-info-content" style={{ color: data.builderSettings.settings.style_settings.lightbox_text_color }}>{varient.displayName}</p>
                                                                                <div id="bb-modal-select" bis_skin_checked="1"></div>
                                                                                {/* {
                                                                                    itemCountMethod(varient.id) < 1
                                                                                        ?
                                                                                        <button type="button" id="bb-modal-add" onClick={() => addProduct(varient.id, varient.displayName, parseFloat(varient.price))} className="bb-bldr-add">Add</button>

                                                                                        :

                                                                                        <button type="button" id="bb-modal-add" onClick={() => removeFromModal(varient.id)} className="bb-bldr-add">Remove</button>
                                                                                } */}
                                                                            </div>
                                                                        </div>
                                                                    </main>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                })
                                            }
                                        </React.Fragment>
                                    })
                                }
                            </>
                            : ''
                    }

                    {
                        element.selected_tab == 0 ?
                            <>
                                {
                                    element.collections.map((collection) => {
                                        return <React.Fragment key={collection.id}>
                                            {
                                                collection.products.map((product) => {
                                                    return <React.Fragment key={product.id}>

                                                        {
                                                            product.variants.map((varient) => {

                                                                return <div className={"bb-bldr-col-md-4 bb-filt-sort "} style={{ marginBottom: '1.5rem' }} key={varient.id} >

                                                                    <div className={"bb-bldr-item "} style={{ border: (itemCountMethod(varient.id) > 0 ? `1px solid ${data.builderSettings.settings.style_settings.product_select_color}` : ""), borderRadius: '5px' }}>

                                                                        <label
                                                                            className={itemCountMethod(varient.id) > 0 ? 'added-icon' : ''}
                                                                        >
                                                                            {
                                                                                itemCountMethod(varient.id) > 0 &&
                                                                                <div className="tick" style={{ background: data.builderSettings.settings.style_settings.product_select_color }}>
                                                                                    <svg width="700pt" height="700pt" version="1.1" viewBox="0 0 700 700" xmlns="http://www.w3.org/2000/svg">
                                                                                        <path d="m607.43 132.12-320.6 320.43c-3.2773 3.2578-7.7148 5.0898-12.336 5.0898-4.625 0-9.0586-1.832-12.34-5.0898l-169.57-169.4c-3.2578-3.2773-5.0898-7.7109-5.0898-12.336s1.832-9.0586 5.0898-12.336c3.2852-3.3164 7.7578-5.1797 12.426-5.1797s9.1406 1.8633 12.426 5.1797l157.5 157.5 307.65-308.53c4.4102-4.4375 10.852-6.1875 16.898-4.5898s10.785 6.3008 12.426 12.336c1.6406 6.0391-0.066407 12.492-4.4727 16.93z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                            }
                                                                            <div className="bb-qty-num" style={{ display: itemCountMethod(varient.id) > 0 ? 'block' : 'none' }}>{itemCountMethod(varient.id)}</div>
                                                                            <div className="bb-owl-carousel bb-owl-theme bb-owl-loaded bb-owl-drag">

                                                                                <div className="bb-owl-stage-outer"><div className="bb-owl-stage" style={{ transform: 'translate3d(0px, 0px, 0px)', transition: 'all 0s ease 0s', width: '268px' }}><div className="bb-owl-item active" style={{ width: '258px', marginRight: '10px' }}><div className="bb-owl-item">

                                                                                    <div className="bb-overlay-icon" onClick={() => imageAction(varient.id, 'open')}><span className="bb-icon"></span></div>
                                                                                    <a href="#" image="https://builder-front.boxup.io/img/placeholder.png" >
                                                                                        <img alt="Test Bulky1" className="bb-bldr-img-fluid owl-lazy ls-is-cached lazyloaded" src={collection.image.originalSrc} style={{ opacity: '1' }} />
                                                                                    </a>

                                                                                </div></div></div></div><div className="bb-owl-nav disabled"><button type="button" role="presentation" className="bb-owl-prev"><span aria-label="Previous">‹</span></button><button type="button" role="presentation" className="bb-owl-next"><span aria-label="Next">›</span></button></div><div className="bb-owl-dots disabled"><button role="button" className="bb-owl-dot active"><span></span></button></div></div>

                                                                            <span className="bb-bldr-item_price  mt-15">
                                                                                <span className="bb-bldr-add-text" >{element.button_text}</span>
                                                                            </span>

                                                                            <strong>{varient.displayName}</strong>
                                                                        </label>
                                                                    </div>

                                                                    <div className={"bb-modal micromodal-slide " + "img-modal-" + varient.id} id="bb-prod-info" aria-hidden="false">
                                                                        <div className="bb-modal__overlay" tabIndex="-1" data-micromodal-close="" bis_skin_checked="1">
                                                                            <div className="bb-modal__container" role="dialog" aria-modal="true" aria-labelledby="bb-prod-info-title" bis_skin_checked="1" style={{ backgroundColor: data.builderSettings.settings.style_settings.lightbox_background_color }}>
                                                                                <header className="bb-modal__header">
                                                                                    <button type='button' className="bb-modal__close" style={{ color: data.builderSettings.settings.style_settings.lightbox_text_color }} onClick={() => imageAction(varient.id, 'close')} aria-label="Close modal"></button>
                                                                                </header>
                                                                                <main className="bb-modal__content" id="bb-modal__content">
                                                                                    <div className="bb-bldr-row" bis_skin_checked="1">
                                                                                        <div className="bb-bldr-col-md-6" bis_skin_checked="1">
                                                                                            <div className="bb-modal-img-container" id="bb-modal-img-container" bis_skin_checked="1">
                                                                                                <div className="bb-owl-carousel bb-owl-theme bb-owl-loaded bb-owl-drag" bis_skin_checked="1">
                                                                                                    <div className="bb-owl-stage-outer" bis_skin_checked="1">
                                                                                                        <div className="bb-owl-stage" style={{ transform: 'translate3d(0px, 0px, 0px)', transition: 'all 0s ease 0s', width: '415px' }}>
                                                                                                            <div className="bb-owl-item active" bis_skin_checked="1" style={{ width: '405px', marginRight: '10px' }}>
                                                                                                                <div className="bb-owl-item" bis_skin_checked="1">
                                                                                                                    <img className="bb-bldr-img-fluid owl-lazy bb-full-width" src={product.image} style={{ opacity: '1' }} />
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div className="bb-owl-nav disabled" bis_skin_checked="1">
                                                                                                        <button type="button" role="presentation" className="bb-owl-prev">
                                                                                                            <span aria-label="Previous"></span>
                                                                                                        </button>
                                                                                                        <button type="button" role="presentation" className="bb-owl-next">
                                                                                                            <span aria-label="Next"></span>
                                                                                                        </button>
                                                                                                    </div>
                                                                                                    <div className="bb-owl-dots disabled" bis_skin_checked="1">
                                                                                                        <button role="button" className="bb-owl-dot active">
                                                                                                            <span>
                                                                                                            </span>
                                                                                                        </button>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div className="bb-bldr-col-md-6" bis_skin_checked="1">
                                                                                            <h2 className="bb-modal__title" style={{ color: data.builderSettings.settings.style_settings.lightbox_text_color }} id="bb-prod-info-title">{product.title}</h2>
                                                                                            <p id="bb-prod-info-content" style={{ color: data.builderSettings.settings.style_settings.lightbox_text_color }}>{varient.displayName}</p>
                                                                                            <div id="bb-modal-select" bis_skin_checked="1"></div>
                                                                                            {/* {
                                                                                                itemCountMethod(varient.id) < 1
                                                                                                    ?
                                                                                                    <button type="button" id="bb-modal-add" onClick={() => addProduct(varient.id, varient.displayName, parseFloat(varient.price))} className="bb-bldr-add">Add</button>

                                                                                                    :

                                                                                                    <button type="button" id="bb-modal-add" onClick={() => removeFromModal(varient.id)} className="bb-bldr-add">Remove</button>
                                                                                            } */}
                                                                                        </div>
                                                                                    </div>
                                                                                </main>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            })
                                                        }
                                                    </React.Fragment>
                                                })
                                            }
                                        </React.Fragment>
                                    })
                                }

                            </>
                            : ''
                    }

                </div>

            </div>

            <div className="bb-bldr-step bb-bldr-submit bb-bldr-wizard-step" data-index="1"
                data-steptype="product" style={{ display: 'none' }} disabled="disabled">
                <div className="bb-bldr-question_title">
                    <h3>My second step product</h3>
                    <p>My second step description</p>
                </div>


                <div className="bb-bldr-row bb-bldr-justify-content-center">

                </div>
                <div className="bb-bldr-row bb-bldr-justify-content-center bb-step-content"
                >

                </div>
            </div>

            <div id="bb-paginate-spinner" style={{ display: 'none' }}>
                <img src="https://builder-front.boxup.io/img/bxp-loader.gif" />
            </div>
            <div id="bb-toast" className="max-items" style={{ bottom: '76px' }}>You can only select {maxItems} product(s)</div>

        </div>

    )
}

export default PreSelectedProducts;