import React, { createRef, Fragment, useEffect, useState } from 'react'


function Content({ options, data, element }) {


    return (

        <div id="bb-bldr-middle-wizard" className="bb-bldr-wizard-branch bb-bldr-wizard-wrapper"
        >

            <div className="bb-bldr-step bb-bldr-current bb-bldr-wizard-step"
                data-steptype="product">
                <div className="bb-bldr-question_title">
                    <h3 className="bb-bldr-wizard-header">{element.title}
                    </h3>
                    <p>{element.description}</p>
                </div>

                <div className="bb-bldr-row bb-bldr-justify-content-center">

                </div>

            </div>

            <div
                className='bb-bldr-row bb-bldr-justify-content-center bb-step-content'
                dangerouslySetInnerHTML={{ __html: (element.content_data) }}
                style={{display : 'block',marginBottom: '100px'}}
            >
            </div>

            <div id="bb-paginate-spinner" style={{ display: 'none' }}>
                <img src="https://builder-front.boxup.io/img/bxp-loader.gif" />
            </div>

        </div>

    )
}

export default Content