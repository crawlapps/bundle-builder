import React, { useEffect, useState } from 'react'
import Summary from './Summary';


function BasicProduct({ options, removeFromModal, data, setOptions, addProduct, removeProduct, element, maxItems, setFormData, showSelectVariantError, filters, setFilters, getFilteredProducts }) {

    // Check item count from item
    function itemCountMethod(s_id) {

        let itemarray = options.items;

        const searchItem = (what) => itemarray.find(element => (element.id === what && element.step == options.step));

        if (itemarray.length > 0) {
            let searchedArray = searchItem(s_id);
            let result = 0;
            if (typeof searchedArray != 'undefined') {
                result = searchedArray.itemCount;
            }
            return (
                result
            );
        } else {
            return (
                0
            );
        }
    }

    // Modal actions.
    const imageAction = (id, action) => {
        if (action == 'open') {
            document.getElementsByClassName(`img-modal-${id}`)[0].classList.add('is-open');
        }
        if (action == 'close') {
            document.getElementsByClassName(`img-modal-${id}`)[0].classList.remove('is-open');
        }
    }

    function selectFromDropdown(val, ref_id, index, img) {
        let productObj = element.selected_products[index];

        productObj.variants.map(function (variant) {
            let itemsCount = itemCountMethod(variant.id);
            for (let i = 0; i <= itemsCount; i++) {
                removeProduct(variant.id, variant.displayName, parseFloat(variant.price))
            }
        })

        if (val != 'none') {
            let itemObj = element.selected_products[index].variants[val];
            addProduct(itemObj.id, itemObj.displayName, parseFloat(itemObj.price), img.image, false, ref_id)
        }
    }

    // Check item count from item in variant drop down layout
    function itemCountMethodDropdown(p_id) {
        let itemarray = options.items;
        const searchItem = (what) => itemarray.find(element => (element.ref_id === what && element.step == options.step));

        if (itemarray.length > 0) {
            let searchedArray = searchItem(p_id);
            // console.log(searchedArray);
            let result = {
                count: 0,
                variant: null
            }
            if (typeof searchedArray != 'undefined') {
                result.count = searchedArray.itemCount;
                result.variant = searchedArray;
                result.variant_price = (parseFloat(searchedArray.price).toFixed(2) / (searchedArray.itemCount)).toFixed(2);
            }
            return result;
        } else {
            let dropdown_select = document.getElementById(`dropdown_${p_id}`);
            if (dropdown_select != null) {
                dropdown_select.value = 'none';
            }
            return (
                {
                    count: 0,
                    variant: null
                }
            );
        }
    }

    getFilteredProducts(element.selected_products)

    return (

        <div id="bb-bldr-middle-wizard" className="bb-bldr-wizard-branch bb-bldr-wizard-wrapper"
        >

            <div className="bb-bldr-step bb-bldr-current bxp-bldr-wizard-step"
                data-steptype="product">
                <div className="bb-bldr-question_title">
                    <h3 className="bb-bldr-wizard-header">{element.title}
                    </h3>
                    <p>{element.description}</p>
                </div>

                <div className="bxp-bldr-row bxp-bldr-justify-content-center">
                    <div className="bxp-bldr-col-md-4 bxp-bldr-col-lg-3 bxp-mt-15">
                        <div className="bxp-bldr-form-group no-mb">
                            <div className="bxp-bldr-styled-select">
                                <div className="bxp-styled-select">
                                    <select className="bxp-bldr-form-control bxp-sort-by" value={filters.sort_by} onChange={(event) => { event.persist(), setFilters((prev) => ({ ...prev, sort_by: event.target.value })) }}>
                                        <option value="default">Sort by:</option>
                                        <option value="asc">Product Title (asc)</option>
                                        <option value="desc">Product Title (desc)</option>
                                        <option value="l2h">Price (low to high)</option>
                                        <option value="h2l">Price (high to low)</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="bxp-bldr-col-md-4 bxp-bldr-col-lg-3 bxp-mt-15">
                        <div className="bxp-bldr-form-group no-mb">
                            <div className="bxp-bldr-styled-select">
                                <div className="bxp-styled-select">
                                    <select className="bxp-bldr-form-control bxp-filter-collection" value={filters.filterByCollection} onChange={(event) => { event.persist(), setFilters((prev) => ({ ...prev, filterByCollection: event.target.value })) }}>
                                        <option value="default" >Filter by collection:</option>
                                        <option value="261850726645">Home page</option>
                                        <option value="391130841333">Test Collection</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="bxp-bldr-col-md-4 bxp-bldr-col-lg-3 bxp-mt-15">
                        <div className="bxp-bldr-form-group no-mb">
                            <div className="bxp-bldr-styled-select">
                                <input type="text" className="bxp-bldr-form-control bxp-filter-title" placeholder="Search..." value={filters.search} onChange={(event) => { event.persist(), setFilters((prev) => ({ ...prev, search: event.target?.value })) }} />
                            </div>
                        </div>
                    </div>
                </div>

                <Summary element={element} data={data} options={options} setOptions={setOptions} setFormData={setFormData} />

                {
                    // Saperate variants 
                    element.show_variant_as == 0
                        ?
                        <div className="bxp-bldr-row bxp-bldr-justify-content-center bxp-step-content" style={{ display: 'flex', marginBottom: '100px' }}>
                            {
                                element.selected_tab == 1 ?
                                    <>
                                        {
                                            element.selected_products.map((product) => {
                                                return <React.Fragment key={product.id}>

                                                    {
                                                        product.variants.map((varient) => {
                                                            return <div className={"bxp-bldr-col-md-4 bxp-filt-sort "} style={{ marginBottom: '1.5rem' }} key={varient.id} >

                                                                <div className={"bxp-bldr-item "} style={{ border: (itemCountMethod(varient.id) > 0 ? `1px solid ${data.builderSettings.settings.style_settings.product_select_color}` : ""), borderRadius: '5px' }}>

                                                                    <label
                                                                        className={itemCountMethod(varient.id) > 0 ? 'added-icon' : ''}
                                                                    >
                                                                        {
                                                                            itemCountMethod(varient.id) > 0 &&
                                                                            <div className="tick" style={{ background: data.builderSettings.settings.style_settings.product_select_color }}>
                                                                                <svg width="700pt" height="700pt" version="1.1" viewBox="0 0 700 700" xmlns="http://www.w3.org/2000/svg">
                                                                                    <path d="m607.43 132.12-320.6 320.43c-3.2773 3.2578-7.7148 5.0898-12.336 5.0898-4.625 0-9.0586-1.832-12.34-5.0898l-169.57-169.4c-3.2578-3.2773-5.0898-7.7109-5.0898-12.336s1.832-9.0586 5.0898-12.336c3.2852-3.3164 7.7578-5.1797 12.426-5.1797s9.1406 1.8633 12.426 5.1797l157.5 157.5 307.65-308.53c4.4102-4.4375 10.852-6.1875 16.898-4.5898s10.785 6.3008 12.426 12.336c1.6406 6.0391-0.066407 12.492-4.4727 16.93z"></path>
                                                                                </svg>
                                                                            </div>
                                                                        }

                                                                        <div className="bxp-qty-num" style={{ display: itemCountMethod(varient.id) > 0 ? 'block' : 'none', backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }}>{itemCountMethod(varient.id)}</div>
                                                                        <div className="bxp-owl-carousel bxp-owl-theme bxp-owl-loaded bxp-owl-drag">

                                                                            <div className="bxp-owl-stage-outer mb-15"><div className="bxp-owl-stage" style={{ transform: 'translate3d(0px, 0px, 0px)', transition: 'all 0s ease 0s', width: '268px' }}><div className="bxp-owl-item active" style={{ width: '258px', marginRight: '10px' }}><div className="bxp-owl-item">

                                                                                <div className="bxp-overlay-icon" onClick={() => imageAction(varient.id, 'open')}><span className="bxp-icon"></span></div>
                                                                                <a href="#" image="https://builder-front.boxup.io/img/placeholder.png" >
                                                                                    <img alt="Test Bulky1" className="bxp-bldr-img-fluid owl-lazy ls-is-cached lazyloaded" src={product.image} style={{ opacity: '1' }} />
                                                                                </a>

                                                                            </div></div></div></div><div className="bxp-owl-nav disabled"><button type="button" role="presentation" className="bxp-owl-prev"><span aria-label="Previous">‹</span></button><button type="button" role="presentation" className="bb-owl-next"><span aria-label="Next">›</span></button></div><div className="bxp-owl-dots disabled"><button role="button" className="bxp-owl-dot active"><span></span></button></div></div>

                                                                        {
                                                                            element.is_allow_more_than_one ?
                                                                                <>
                                                                                    {
                                                                                        itemCountMethod(varient.id) > 0
                                                                                            ?
                                                                                            <span className="bb-bldr-qty-down" style={{ display: 'inline-block', backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }} onClick={() => removeProduct(varient.id, varient.displayName, parseFloat(varient.price))} >-</span>
                                                                                            : ''
                                                                                    }

                                                                                    <span className="bxp-bldr-item_price" style={{ backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }} onClick={() => addProduct(varient.id, varient.displayName, parseFloat(varient.price), product.image)}><span
                                                                                        className="bxp-bldr-add-text">Add:</span> ${varient.price}</span>
                                                                                    {
                                                                                        itemCountMethod(varient.id) > 0
                                                                                            ?
                                                                                            <span className="bxp-bldr-qty-up" style={{ display: 'inline-block', backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }} onClick={() => addProduct(varient.id, varient.displayName, parseFloat(varient.price), product.image)}>+</span>
                                                                                            : ''
                                                                                    }
                                                                                </>
                                                                                :
                                                                                <>
                                                                                    {
                                                                                        itemCountMethod(varient.id) < 1
                                                                                            ?
                                                                                            <span className="bxp-bldr-item_price" style={{ backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }} onClick={() => addProduct(varient.id, varient.displayName, parseFloat(varient.price), product.image)}><span
                                                                                                className="bxp-bldr-add-text" >Add:</span> ${varient.price}</span>
                                                                                            : ''
                                                                                    }
                                                                                    {
                                                                                        itemCountMethod(varient.id) > 0
                                                                                            ?
                                                                                            <span className="bxp-bldr-item_price" style={{ backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }} onClick={() => removeProduct(varient.id, varient.displayName, parseFloat(varient.price))}><span
                                                                                                className="bxp-bldr-add-text" >Remove:</span> ${varient.price}</span>
                                                                                            : ''
                                                                                    }

                                                                                </>
                                                                        }

                                                                        <strong>{varient.displayName}</strong>
                                                                    </label>
                                                                </div>

                                                                <div className={"bb-modal micromodal-slide " + "img-modal-" + varient.id} id="bxp-prod-info" aria-hidden="false">
                                                                    <div className="bb-modal__overlay" tabIndex="-1" data-micromodal-close="" bis_skin_checked="1">
                                                                        <div className="bb-modal__container" role="dialog" aria-modal="true" aria-labelledby="bxp-prod-info-title" bis_skin_checked="1">
                                                                            <header className="bb-modal__header">
                                                                                <button type='button' className="bb-modal__close" onClick={() => imageAction(varient.id, 'close')} aria-label="Close modal"></button>
                                                                            </header>
                                                                            <main className="bb-modal__content" id="bb-modal__content">
                                                                                <div className="bxp-bldr-row" bis_skin_checked="1">
                                                                                    <div className="bxp-bldr-col-md-6" bis_skin_checked="1">
                                                                                        <div className="bb-modal-img-container" id="bb-modal-img-container" bis_skin_checked="1">
                                                                                            <div className="bxp-owl-carousel bxp-owl-theme bxp-owl-loaded bxp-owl-drag" bis_skin_checked="1">
                                                                                                <div className="bxp-owl-stage-outer" bis_skin_checked="1">
                                                                                                    <div className="bxp-owl-stage" style={{ transform: 'translate3d(0px, 0px, 0px)', transition: 'all 0s ease 0s', width: '415px' }}>
                                                                                                        <div className="bxp-owl-item active" bis_skin_checked="1" style={{ width: '405px', marginRight: '10px' }}>
                                                                                                            <div className="bxp-owl-item" bis_skin_checked="1">
                                                                                                                <img className="bxp-bldr-img-fluid owl-lazy bxp-full-width" src={product.image} style={{ opacity: '1' }} />
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div className="bxp-owl-nav disabled" bis_skin_checked="1">
                                                                                                    <button type="button" role="presentation" className="bxp-owl-prev">
                                                                                                        <span aria-label="Previous"></span>
                                                                                                    </button>
                                                                                                    <button type="button" role="presentation" className="bb-owl-next">
                                                                                                        <span aria-label="Next"></span>
                                                                                                    </button>
                                                                                                </div>
                                                                                                <div className="bxp-owl-dots disabled" bis_skin_checked="1">
                                                                                                    <button role="button" className="bxp-owl-dot active">
                                                                                                        <span>
                                                                                                        </span>
                                                                                                    </button>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="bxp-bldr-col-md-6" bis_skin_checked="1">
                                                                                        <h2 className="bb-modal__title" id="bxp-prod-info-title">{varient.title}</h2>
                                                                                        <p id="bxp-prod-info-content">{varient.displayName}</p>
                                                                                        <div id="bb-modal-select" bis_skin_checked="1"></div>
                                                                                        {
                                                                                            itemCountMethod(varient.id) < 1
                                                                                                ?
                                                                                                <button type="button" id="bb-modal-add" style={{ backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }} onClick={() => addProduct(varient.id, varient.displayName, parseFloat(varient.price), product.image)} className="bxp-bldr-add">Add</button>

                                                                                                :

                                                                                                <button type="button" id="bb-modal-add" style={{ backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }} onClick={() => removeFromModal(varient.id)} className="bxp-bldr-add">Remove</button>
                                                                                        }
                                                                                    </div>
                                                                                </div>
                                                                            </main>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        })
                                                    }
                                                </React.Fragment>
                                            })
                                        }
                                    </>
                                    : ''
                            }

                            {
                                element.selected_tab == 0 ?
                                    <>
                                        {
                                            element.collections.map((collection) => {
                                                return <React.Fragment key={collection.id}>
                                                    {
                                                        collection.products.map((product) => {
                                                            return <React.Fragment key={product.id}>

                                                                {
                                                                    product.variants.map((varient) => {

                                                                        return <div className={"bxp-bldr-col-md-4 bxp-filt-sort "} style={{ marginBottom: '1.5rem' }} key={varient.id} >

                                                                            <div className={"bxp-bldr-item "} style={{ border: (itemCountMethod(varient.id) > 0 ? `1px solid ${data.builderSettings.settings.style_settings.product_select_color}` : ""), borderRadius: '5px' }}>

                                                                                <label
                                                                                    className={itemCountMethod(varient.id) > 0 ? 'added-icon' : ''}
                                                                                >
                                                                                    {
                                                                                        itemCountMethod(varient.id) > 0 &&
                                                                                        <div className="tick" style={{ background: data.builderSettings.settings.style_settings.product_select_color }}>
                                                                                            <svg width="700pt" height="700pt" version="1.1" viewBox="0 0 700 700" xmlns="http://www.w3.org/2000/svg">
                                                                                                <path d="m607.43 132.12-320.6 320.43c-3.2773 3.2578-7.7148 5.0898-12.336 5.0898-4.625 0-9.0586-1.832-12.34-5.0898l-169.57-169.4c-3.2578-3.2773-5.0898-7.7109-5.0898-12.336s1.832-9.0586 5.0898-12.336c3.2852-3.3164 7.7578-5.1797 12.426-5.1797s9.1406 1.8633 12.426 5.1797l157.5 157.5 307.65-308.53c4.4102-4.4375 10.852-6.1875 16.898-4.5898s10.785 6.3008 12.426 12.336c1.6406 6.0391-0.066407 12.492-4.4727 16.93z"></path>
                                                                                            </svg>
                                                                                        </div>
                                                                                    }

                                                                                    <div className="bxp-qty-num" style={{ display: itemCountMethod(varient.id) > 0 ? 'block' : 'none', backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }}>{itemCountMethod(varient.id)}</div>
                                                                                    <div className="bxp-owl-carousel bxp-owl-theme bxp-owl-loaded bxp-owl-drag">

                                                                                        <div className="bxp-owl-stage-outer mb-15"><div className="bxp-owl-stage" style={{ transform: 'translate3d(0px, 0px, 0px)', transition: 'all 0s ease 0s', width: '268px' }}><div className="bxp-owl-item active" style={{ width: '258px', marginRight: '10px' }}><div className="bxp-owl-item">

                                                                                            <div className="bxp-overlay-icon" onClick={() => imageAction(varient.id, 'open')}><span className="bxp-icon"></span></div>
                                                                                            <a href="#" image="https://builder-front.boxup.io/img/placeholder.png" >
                                                                                                <img alt="Test Bulky1" className="bxp-bldr-img-fluid owl-lazy ls-is-cached lazyloaded" src={collection.image.originalSrc} style={{ opacity: '1' }} />
                                                                                            </a>

                                                                                        </div></div></div></div><div className="bxp-owl-nav disabled"><button type="button" role="presentation" className="bxp-owl-prev"><span aria-label="Previous">‹</span></button><button type="button" role="presentation" className="bb-owl-next"><span aria-label="Next">›</span></button></div><div className="bxp-owl-dots disabled"><button role="button" className="bxp-owl-dot active"><span></span></button></div></div>

                                                                                    {
                                                                                        element.is_allow_more_than_one ?
                                                                                            <>
                                                                                                {
                                                                                                    itemCountMethod(varient.id) > 0
                                                                                                        ?
                                                                                                        <span className="bb-bldr-qty-down" style={{ display: 'inline-block', backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }} onClick={() => removeProduct(varient.id, varient.displayName, parseFloat(varient.price))} >-</span>
                                                                                                        : ''
                                                                                                }

                                                                                                <span className="bxp-bldr-item_price" style={{ backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }} onClick={() => addProduct(varient.id, varient.displayName, parseFloat(varient.price), collection.image.originalSrc)}><span
                                                                                                    className="bxp-bldr-add-text">Add:</span> ${varient.price}</span>
                                                                                                {
                                                                                                    itemCountMethod(varient.id) > 0
                                                                                                        ?
                                                                                                        <span className="bxp-bldr-qty-up" style={{ display: 'inline-block', backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }} onClick={() => addProduct(varient.id, varient.displayName, parseFloat(varient.price), collection.image.originalSrc)}>+</span>
                                                                                                        : ''
                                                                                                }
                                                                                            </>
                                                                                            :
                                                                                            <>
                                                                                                {
                                                                                                    itemCountMethod(varient.id) < 1
                                                                                                        ?
                                                                                                        <span className="bxp-bldr-item_price" style={{ backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }} onClick={() => addProduct(varient.id, varient.displayName, parseFloat(varient.price), collection.image.originalSrc)}><span
                                                                                                            className="bxp-bldr-add-text">Add:</span> ${varient.price}</span>
                                                                                                        : ''
                                                                                                }
                                                                                                {
                                                                                                    itemCountMethod(varient.id) > 0
                                                                                                        ?
                                                                                                        <span className="bxp-bldr-item_price" style={{ backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }} onClick={() => removeProduct(varient.id, varient.displayName, parseFloat(varient.price))}><span
                                                                                                            className="bxp-bldr-add-text">Remove:</span> ${varient.price}</span>
                                                                                                        : ''
                                                                                                }

                                                                                            </>
                                                                                    }

                                                                                    <strong>{varient.displayName}</strong>
                                                                                </label>
                                                                            </div>

                                                                            <div className={"bb-modal micromodal-slide " + "img-modal-" + varient.id} id="bxp-prod-info" aria-hidden="false">
                                                                                <div className="bb-modal__overlay" tabIndex="-1" data-micromodal-close="" bis_skin_checked="1">
                                                                                    <div className="bb-modal__container" role="dialog" aria-modal="true" aria-labelledby="bxp-prod-info-title" bis_skin_checked="1">
                                                                                        <header className="bb-modal__header">
                                                                                            <button type='button' className="bb-modal__close" onClick={() => imageAction(varient.id, 'close')} aria-label="Close modal"></button>
                                                                                        </header>
                                                                                        <main className="bb-modal__content" id="bb-modal__content">
                                                                                            <div className="bxp-bldr-row" bis_skin_checked="1">
                                                                                                <div className="bxp-bldr-col-md-6" bis_skin_checked="1">
                                                                                                    <div className="bb-modal-img-container" id="bb-modal-img-container" bis_skin_checked="1">
                                                                                                        <div className="bxp-owl-carousel bxp-owl-theme bxp-owl-loaded bxp-owl-drag" bis_skin_checked="1">
                                                                                                            <div className="bxp-owl-stage-outer" bis_skin_checked="1">
                                                                                                                <div className="bxp-owl-stage" style={{ transform: 'translate3d(0px, 0px, 0px)', transition: 'all 0s ease 0s', width: '415px' }}>
                                                                                                                    <div className="bxp-owl-item active" bis_skin_checked="1" style={{ width: '405px', marginRight: '10px' }}>
                                                                                                                        <div className="bxp-owl-item" bis_skin_checked="1">
                                                                                                                            <img className="bxp-bldr-img-fluid owl-lazy bxp-full-width" src={product.image} style={{ opacity: '1' }} />
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div className="bxp-owl-nav disabled" bis_skin_checked="1">
                                                                                                                <button type="button" role="presentation" className="bxp-owl-prev">
                                                                                                                    <span aria-label="Previous"></span>
                                                                                                                </button>
                                                                                                                <button type="button" role="presentation" className="bb-owl-next">
                                                                                                                    <span aria-label="Next"></span>
                                                                                                                </button>
                                                                                                            </div>
                                                                                                            <div className="bxp-owl-dots disabled" bis_skin_checked="1">
                                                                                                                <button role="button" className="bxp-owl-dot active">
                                                                                                                    <span>
                                                                                                                    </span>
                                                                                                                </button>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div className="bxp-bldr-col-md-6" bis_skin_checked="1">
                                                                                                    <h2 className="bb-modal__title" id="bxp-prod-info-title">{varient.title}</h2>
                                                                                                    <p id="bxp-prod-info-content">{varient.displayName}</p>
                                                                                                    <div id="bb-modal-select" bis_skin_checked="1"></div>
                                                                                                    {
                                                                                                        itemCountMethod(varient.id) < 1
                                                                                                            ?
                                                                                                            <button type="button" id="bb-modal-add" style={{ backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }} onClick={() => addProduct(varient.id, varient.displayName, parseFloat(varient.price))} className="bxp-bldr-add">Add</button>

                                                                                                            :

                                                                                                            <button type="button" id="bb-modal-add" style={{ backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }} onClick={() => removeFromModal(varient.id)} className="bxp-bldr-add">Remove</button>
                                                                                                    }
                                                                                                </div>
                                                                                            </div>
                                                                                        </main>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    })
                                                                }
                                                            </React.Fragment>
                                                        })
                                                    }
                                                </React.Fragment>
                                            })
                                        }

                                    </>
                                    : ''
                            }

                        </div>
                        :
                        ''
                }

                {
                    // drop down variant box
                    element.show_variant_as == 1
                        ?
                        <div className="bxp-bldr-row bxp-bldr-justify-content-center bxp-step-content" style={{ display: 'flex', marginBottom: '100px' }}>
                            {
                                element.selected_tab == 1 ?
                                    <>
                                        {
                                            getFilteredProducts(element.selected_products).map((product, i) => {
                                                let drropDownItem = itemCountMethodDropdown(product.id);

                                                return <React.Fragment key={product.id}>

                                                    <div className={"bxp-bldr-col-md-4 bxp-filt-sort "} style={{ marginBottom: '1.5rem' }} >

                                                        <div className={"bxp-bldr-item "} style={{ border: (drropDownItem.count > 0 ? `1px solid ${data.builderSettings.settings.style_settings.product_select_color}` : ""), borderRadius: '5px' }}>

                                                            <label className={drropDownItem.count > 0 ? 'added-icon' : ''} >
                                                                {
                                                                    drropDownItem.count > 0 &&
                                                                    <div className="tick" style={{ background: data.builderSettings.settings.style_settings.product_select_color }}>
                                                                        <svg width="700pt" height="700pt" version="1.1" viewBox="0 0 700 700" xmlns="http://www.w3.org/2000/svg">
                                                                            <path d="m607.43 132.12-320.6 320.43c-3.2773 3.2578-7.7148 5.0898-12.336 5.0898-4.625 0-9.0586-1.832-12.34-5.0898l-169.57-169.4c-3.2578-3.2773-5.0898-7.7109-5.0898-12.336s1.832-9.0586 5.0898-12.336c3.2852-3.3164 7.7578-5.1797 12.426-5.1797s9.1406 1.8633 12.426 5.1797l157.5 157.5 307.65-308.53c4.4102-4.4375 10.852-6.1875 16.898-4.5898s10.785 6.3008 12.426 12.336c1.6406 6.0391-0.066407 12.492-4.4727 16.93z"></path>
                                                                        </svg>
                                                                    </div>
                                                                }

                                                                <div className="bxp-qty-num" style={{ display: drropDownItem.count > 0 ? 'block' : 'none', backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }}>{drropDownItem.count}</div>
                                                                <div className="bxp-owl-carousel bxp-owl-theme bxp-owl-loaded bxp-owl-drag">

                                                                    <div className="bxp-owl-stage-outer mb-15">
                                                                        <div className="bxp-owl-stage" style={{ transform: 'translate3d(0px, 0px, 0px)', transition: 'all 0s ease 0s', width: '268px' }}>
                                                                            <div className="bxp-owl-item active" style={{ width: '258px', marginRight: '10px' }}>
                                                                                <div className="bxp-owl-item">
                                                                                    {
                                                                                        drropDownItem.count < 1
                                                                                            ?
                                                                                            <div className="bxp-overlay-icon" onClick={() => imageAction(product.id, 'open')}><span className="bxp-icon"></span></div>
                                                                                            :
                                                                                            ''
                                                                                    }
                                                                                    <a image="https://builder-front.boxup.io/img/placeholder.png" >
                                                                                        <img alt="Test Bulky1" className="bxp-bldr-img-fluid owl-lazy ls-is-cached lazyloaded" src={product.image} style={{ opacity: '1' }} />
                                                                                    </a>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="bxp-owl-nav disabled">
                                                                        <button type="button" role="presentation" className="bxp-owl-prev">
                                                                            <span aria-label="Previous">‹</span>
                                                                        </button>
                                                                        <button type="button" role="presentation" className="bb-owl-next">
                                                                            <span aria-label="Next">›</span>
                                                                        </button>
                                                                    </div>
                                                                    <div className="bxp-owl-dots disabled">
                                                                        <button role="button" className="bxp-owl-dot active"><span></span></button>
                                                                    </div>
                                                                </div>

                                                                {
                                                                    element.is_allow_more_than_one ?
                                                                        <>
                                                                            {
                                                                                drropDownItem.count > 0
                                                                                    ?
                                                                                    <span className="bb-bldr-qty-down" style={{ display: 'inline-block', backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }} onClick={() => removeProduct(drropDownItem.variant.id, drropDownItem.variant.displayName, parseFloat(drropDownItem.variant_price))} >-</span>
                                                                                    : ''
                                                                            }

                                                                            <span className="bxp-bldr-item_price" style={{ backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }} onClick={() => drropDownItem.count > 0 ? addProduct(drropDownItem.variant.id, drropDownItem.variant.displayName, parseFloat(drropDownItem.variant_price), product.image) : showSelectVariantError()}><span
                                                                                className="bxp-bldr-add-text">Add:</span> ${drropDownItem.count > 0 ? drropDownItem.variant.price : '--'}</span>
                                                                            {
                                                                                drropDownItem.count > 0
                                                                                    ?
                                                                                    <span className="bxp-bldr-qty-up" style={{ display: 'inline-block', backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }} onClick={() => addProduct(drropDownItem.variant.id, drropDownItem.variant.displayName, parseFloat(drropDownItem.variant_price), product.image)}>+</span>
                                                                                    : ''
                                                                            }
                                                                        </>
                                                                        :
                                                                        <>
                                                                            {
                                                                                drropDownItem.count < 1
                                                                                    ?
                                                                                    <span className="bxp-bldr-item_price" style={{ backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }} onClick={() => addProduct(drropDownItem.variant.id, drropDownItem.variant.displayName, parseFloat(drropDownItem.variant_price), product.image)}><span
                                                                                        className="bxp-bldr-add-text" >Add:</span> ${drropDownItem.count > 0 ? drropDownItem.variant.price : '--'}</span>
                                                                                    : ''
                                                                            }
                                                                            {
                                                                                drropDownItem.count > 0
                                                                                    ?
                                                                                    <span className="bxp-bldr-item_price" style={{ backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }} onClick={() => removeProduct(drropDownItem.variant.id, drropDownItem.variant.displayName, parseFloat(drropDownItem.variant_price))}><span
                                                                                        className="bxp-bldr-add-text" >Remove:</span> ${drropDownItem.count > 0 ? drropDownItem.variant.price : '--'}</span>
                                                                                    : ''
                                                                            }

                                                                        </>
                                                                }

                                                                <strong>{product.handle}</strong>

                                                                <select className="bxp-bldr-form-control bxp-mt-10" id={`dropdown_${product.id}`} defaultValue={drropDownItem.count < 1 ? 'none' : drropDownItem.variant.id} onChange={(val) => { selectFromDropdown(val.target.value, product.id, i, product, product.image) }}>
                                                                    <option value="none" >Choose a Hair Length...</option>
                                                                    {
                                                                        product.variants.map((variant, v_index) => {
                                                                            return <option key={v_index} price={variant.price} id={variant.id} value={v_index}>{variant.displayName}</option>
                                                                        })
                                                                    }

                                                                </select>

                                                            </label>
                                                        </div>

                                                        <div className={"bb-modal micromodal-slide " + "img-modal-" + product.id} id="bxp-prod-info" aria-hidden="false">
                                                            <div className="bb-modal__overlay" tabIndex="-1" data-micromodal-close="" bis_skin_checked="1">
                                                                <div className="bb-modal__container" role="dialog" aria-modal="true" aria-labelledby="bxp-prod-info-title" bis_skin_checked="1">
                                                                    <header className="bb-modal__header">
                                                                        <button type='button' className="bb-modal__close" onClick={() => imageAction(product.id, 'close')} aria-label="Close modal"></button>
                                                                    </header>
                                                                    <main className="bb-modal__content" id="bb-modal__content">
                                                                        <div className="bxp-bldr-row" bis_skin_checked="1">
                                                                            <div className="bxp-bldr-col-md-6" bis_skin_checked="1">
                                                                                <div className="bb-modal-img-container" id="bb-modal-img-container" bis_skin_checked="1">
                                                                                    <div className="bxp-owl-carousel bxp-owl-theme bxp-owl-loaded bxp-owl-drag" bis_skin_checked="1">
                                                                                        <div className="bxp-owl-stage-outer" bis_skin_checked="1">
                                                                                            <div className="bxp-owl-stage" style={{ transform: 'translate3d(0px, 0px, 0px)', transition: 'all 0s ease 0s', width: '415px' }}>
                                                                                                <div className="bxp-owl-item active" bis_skin_checked="1" style={{ width: '405px', marginRight: '10px' }}>
                                                                                                    <div className="bxp-owl-item" bis_skin_checked="1">
                                                                                                        <img className="bxp-bldr-img-fluid owl-lazy bxp-full-width" src={product.image} style={{ opacity: '1' }} />
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div className="bxp-owl-nav disabled" bis_skin_checked="1">
                                                                                            <button type="button" role="presentation" className="bxp-owl-prev">
                                                                                                <span aria-label="Previous"></span>
                                                                                            </button>
                                                                                            <button type="button" role="presentation" className="bb-owl-next">
                                                                                                <span aria-label="Next"></span>
                                                                                            </button>
                                                                                        </div>
                                                                                        <div className="bxp-owl-dots disabled" bis_skin_checked="1">
                                                                                            <button role="button" className="bxp-owl-dot active">
                                                                                                <span>
                                                                                                </span>
                                                                                            </button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div className="bxp-bldr-col-md-6" bis_skin_checked="1">
                                                                                <h2 className="bb-modal__title" id="bxp-prod-info-title">{product.handle}</h2>
                                                                                {/* <p id="bxp-prod-info-content">{varient.displayName}</p> */}
                                                                                <div id="bb-modal-select" bis_skin_checked="1"></div>
                                                                                <select className="bxp-bldr-form-control bxp-mt-10" id={`dropdown_${product.id}`} defaultValue={drropDownItem.count < 1 ? 'none' : drropDownItem.variant.id} onChange={(val) => { selectFromDropdown(val.target.value, product.id, i, product, product.image), imageAction(product.id, 'close') }}>
                                                                                    <option value="none" >Choose a Hair Length...</option>
                                                                                    {
                                                                                        product.variants.map((variant, v_index) => {
                                                                                            return <option key={v_index} price={variant.price} id={variant.id} value={v_index}>{variant.displayName}</option>
                                                                                        })
                                                                                    }

                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </main>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </React.Fragment>
                                            })
                                        }
                                    </>
                                    : ''
                            }
                        </div>
                        :
                        ''
                }

                {/* <div className="bxp-bldr-col-md-4 bxp-filt-sort" data-hide="" data-tags="tag_testing" data-type="" data-title="9A Brazilian Deep Wave" data-collections="" data-price="25.00">
                    <div className="bxp-bldr-item">
                        <input data-weight="0.00" data-qty="1" data-price="0.00" data-label="9A Brazilian Deep Wave" input="input_7680343081205" product-id="7680343081205" type="radio" name="" value="Product with image " className="bxp-bldr-required valid" data-display="product" data-total="0" />
                        <label style={{ marginTop: '-25px' }}>
                            <div className="bxp-qty-num"></div>
                            <div id="bxp-select_7680343081205_owl">
                                <div className="bxp-owl-carousel bxp-owl-theme bxp-owl-loaded bxp-owl-drag">

                                    <div className="bxp-owl-stage-outer"><div className="bxp-owl-stage" style={{ transform: 'translate3d(0px, 0px, 0px)', transition: 'all 0s ease 0s', width: '536px' }}><div className="bxp-owl-item active" style={{ width: '258px', marginRight: '10px' }}><div className="bxp-owl-item">
                                        <div className="bxp-overlay-icon"><span className="bxp-icon"></span></div>
                                        <a href="#" image="//cdn.shopify.com/s/files/1/0549/7998/5653/products/brazilian_virgin_pineapple-wave_irfycckaxghveexu_800x.jpg?v=1652159294" >
                                            <img src="//cdn.shopify.com/s/files/1/0549/7998/5653/products/brazilian_virgin_pineapple-wave_irfycckaxghveexu_300x.jpg?v=1652159294" alt="9A Brazilian Deep Wave" className="bxp-bldr-img-fluid owl-lazy lazyloaded" />
                                        </a>
                                    </div></div><div className="bxp-owl-item" style={{ width: '258px', marginRight: '10px' }}><div className="bxp-owl-item">
                                        <div className="bxp-overlay-icon"><span className="bxp-icon"></span></div>
                                        <a href="#" image="//cdn.shopify.com/s/files/1/0549/7998/5653/products/brazilian_virgin_pineapple-wave_irfycckaxghveexu_b1b76b2d-a7d8-40f7-bf52-223dae76c2a1_800x.jpg?v=1652159294" >
                                            <img src="//cdn.shopify.com/s/files/1/0549/7998/5653/products/brazilian_virgin_pineapple-wave_irfycckaxghveexu_b1b76b2d-a7d8-40f7-bf52-223dae76c2a1_300x.jpg?v=1652159294" alt="9A Brazilian Deep Wave" className="bxp-bldr-img-fluid owl-lazy lazyloaded" />
                                        </a>
                                    </div></div></div></div><div className="bxp-owl-nav disabled"><button type="button" role="presentation" className="bxp-owl-prev"><span aria-label="Previous">‹</span></button><button type="button" role="presentation" className="bb-owl-next"><span aria-label="Next">›</span></button></div><div className="bxp-owl-dots"><button role="button" className="bxp-owl-dot active"><span></span></button><button role="button" className="bxp-owl-dot"><span></span></button></div></div>
                            </div>
                            <div id="bxp-select_7680343081205_fig" className="bxp-sel-figure">
                                <img id="bxp-select_7680343081205_img" src="//cdn.shopify.com/s/files/1/0549/7998/5653/products/brazilian_virgin_pineapple-wave_irfycckaxghveexu_300x.jpg?v=1652159294" alt="9A Brazilian Deep Wave" className="bxp-bldr-img-fluid" />
                            </div><span className="bxp-bldr-item_price"><span className="bxp-bldr-add-text">Add:</span> $25.00</span><strong>9A Brazilian Deep Wave</strong>
                            <select className="bxp-bldr-form-control bxp-mt-10" img="//cdn.shopify.com/s/files/1/0549/7998/5653/products/brazilian_virgin_pineapple-wave_irfycckaxghveexu_300x.jpg?v=1652159294" name="bxp-select_7680343081205" id="bxp-select_7680343081205"><option value="" price="0.00">Choose a Hair Length...</option>


                                <option data-label="9A Brazilian Deep Wave - 10&quot;" weight="0.0 lb" inventory="14" img="//cdn.shopify.com/s/files/1/0549/7998/5653/products/brazilian_virgin_pineapple-wave_irfycckaxghveexu_300x.jpg?v=1652159294" price="25.00" value="42741121089781">10" - $25.00</option>


                                <option data-label="9A Brazilian Deep Wave - 12&quot;" weight="0.0 lb" inventory="12" img="//cdn.shopify.com/s/files/1/0549/7998/5653/products/brazilian_virgin_pineapple-wave_irfycckaxghveexu_300x.jpg?v=1652159294" price="31.00" value="42741121122549">12" - $31.00</option>


                                <option data-label="9A Brazilian Deep Wave - 14&quot;" weight="0.0 lb" inventory="12" img="//cdn.shopify.com/s/files/1/0549/7998/5653/products/brazilian_virgin_pineapple-wave_irfycckaxghveexu_300x.jpg?v=1652159294" price="38.00" value="42741121155317">14" - $38.00</option>


                                <option data-label="9A Brazilian Deep Wave - 16&quot;" weight="0.0 lb" inventory="12" img="//cdn.shopify.com/s/files/1/0549/7998/5653/products/brazilian_virgin_pineapple-wave_irfycckaxghveexu_300x.jpg?v=1652159294" price="50.00" value="42741121188085">16" - $50.00</option>


                                <option data-label="9A Brazilian Deep Wave - 18&quot;" weight="0.0 lb" inventory="14" img="//cdn.shopify.com/s/files/1/0549/7998/5653/products/brazilian_virgin_pineapple-wave_irfycckaxghveexu_300x.jpg?v=1652159294" price="60.00" value="42741121220853">18" - $60.00</option>


                                <option data-label="9A Brazilian Deep Wave - 20&quot;" weight="0.0 lb" inventory="14" img="//cdn.shopify.com/s/files/1/0549/7998/5653/products/brazilian_virgin_pineapple-wave_irfycckaxghveexu_300x.jpg?v=1652159294" price="70.00" value="42741121253621">20" - $70.00</option>


                                <option data-label="9A Brazilian Deep Wave - 22&quot;" weight="0.0 lb" inventory="14" img="//cdn.shopify.com/s/files/1/0549/7998/5653/products/brazilian_virgin_pineapple-wave_irfycckaxghveexu_300x.jpg?v=1652159294" price="85.00" value="42741121286389">22" - $85.00</option>


                                <option data-label="9A Brazilian Deep Wave - 24&quot;" weight="0.0 lb" inventory="14" img="//cdn.shopify.com/s/files/1/0549/7998/5653/products/brazilian_virgin_pineapple-wave_irfycckaxghveexu_300x.jpg?v=1652159294" price="90.00" value="42741121319157">24" - $90.00</option>


                                <option data-label="9A Brazilian Deep Wave - 26&quot;" weight="0.0 lb" inventory="14" img="//cdn.shopify.com/s/files/1/0549/7998/5653/products/brazilian_virgin_pineapple-wave_irfycckaxghveexu_300x.jpg?v=1652159294" price="95.00" value="42741121351925">26" - $95.00</option>
                            </select>
                            <button className="bxp-deselect">X</button>
                        </label>
                    </div>
                </div> */}


            </div>

            <div className="bb-bldr-step bxp-bldr-submit bxp-bldr-wizard-step" data-index="1"
                data-steptype="product" style={{ display: 'none' }} disabled="disabled">
                <div className="bb-bldr-question_title">
                    <h3>My second step product</h3>
                    <p>My second step description</p>
                </div>


                <div className="bxp-bldr-row bxp-bldr-justify-content-center">

                </div>
                <div className="bxp-bldr-row bxp-bldr-justify-content-center bxp-step-content"
                >

                </div>
            </div>

            <div id="bxp-paginate-spinner" style={{ display: 'none' }}>
                <img src="https://builder-front.boxup.io/img/bxp-loader.gif" />
            </div>
            <div id="bxp-toast" className="max-items" style={{ bottom: '76px' }}>You can only select {maxItems} product(s)</div>


        </div>

    )
}

export default BasicProduct