import React from 'react'

function Filters({ data, filters, setFilters, element, getTags }) {

    // const getCollections = function (datas) {
    //     let finalCollections = []

    //     datas.map(function (collection) {
    //         collection.products.map(function (product) {
    //             if (product.tags.length > 0) {
    //                 product.tags.map(function (tag) {
    //                     finalCollections.push(tag);
    //                 })
    //             }
    //         });
    //     });

    //     finalCollections = [...new Set(finalCollections)]
    //     console.log(finalCollections)

    //     return finalCollections;
    // }

    // getCollections()

    return (
        <div className="bb-bldr-row bb-bldr-justify-content-center">
            {
                element.is_show_sortby_dropdown
                    ?
                    <div className={`${data.builderSettings.settings.theme.builder == "right" ? "bxp-mt-15" : "bb-bldr-col-md-4 bxp-bldr-col-lg-3 bxp-mt-15"}`}>
                        <div className="bb-bldr-form-group no-mb">
                            <div className="bb-bldr-styled-select">
                                <div className="bxp-styled-select">
                                    <select className="bb-bldr-form-control bxp-sort-by" style={data.builderSettings.settings.theme.builder == 'right' ? { paddingRight: '25px' } : {}} value={filters.sort_by} onChange={(event) => { event.persist(), setFilters((prev) => ({ ...prev, sort_by: event.target.value })) }}>
                                        <option value="default">Sort by:</option>
                                        <option value="asc">Product Title (asc)</option>
                                        <option value="desc">Product Title (desc)</option>
                                        <option value="l2h">Price (low to high)</option>
                                        <option value="h2l">Price (high to low)</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    :
                    ''
            }

            {
                element.is_show_collection_filter
                    ?
                    <div className={`${data.builderSettings.settings.theme.builder == "right" ? "bxp-mt-15" : "bb-bldr-col-md-4 bxp-bldr-col-lg-3 bxp-mt-15"}`}>
                        <div className="bb-bldr-form-group no-mb">
                            <div className="bb-bldr-styled-select">
                                <div className="bxp-styled-select">
                                    <select className="bb-bldr-form-control bxp-filter-collection" style={data.builderSettings.settings.theme.builder == 'right' ? { paddingRight: '25px' } : {}} value={filters.filterByCollection} onChange={(event) => { event.persist(), setFilters((prev) => ({ ...prev, filterByCollection: event.target.value })) }}>
                                        <option value="default" >Filter by collection:</option>
                                        {
                                            element.collections_filter.map((col, i) => {
                                                return <option value={col.id} key={i}>{col.title}</option>;
                                            })
                                        }
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    :
                    ''
            }

            {
                element.is_show_tags_filter
                    ?
                    <div className={`${data.builderSettings.settings.theme.builder == "right" ? "bxp-mt-15" : "bb-bldr-col-md-4 bxp-bldr-col-lg-3 bxp-mt-15"}`}>
                        <div className="bb-bldr-form-group no-mb">
                            <div className="bb-bldr-styled-select">
                                <div className="bxp-styled-select">
                                    <select className="bb-bldr-form-control bxp-filter-collection" style={data.builderSettings.settings.theme.builder == 'right' ? { paddingRight: '25px' } : {}} value={filters.filterByTags} onChange={(event) => { event.persist(), setFilters((prev) => ({ ...prev, filterByTags: event.target.value })) }}>
                                        <option value="default" >Filter by tags:</option>
                                        {
                                            getTags(element.selected_tab == 1 ? element.selected_products : element.collections, element.selected_tab).map((tag, i) => {
                                                return (
                                                    <option value={tag} key={i}>{tag}</option>
                                                );
                                            })
                                        }
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    :
                    ''
            }

            {
                element.is_show_title_filter
                    ?
                    <div className={`${data.builderSettings.settings.theme.builder == "right" ? "bxp-mt-15" : "bb-bldr-col-md-4 bxp-bldr-col-lg-3 bxp-mt-15"}`}>
                        <div className="bb-bldr-form-group no-mb">
                            <div className="bb-bldr-styled-select">
                                <input type="text" className="bb-bldr-form-control bxp-filter-title" placeholder="Search..." value={filters.search} onChange={(event) => { event.persist(), setFilters((prev) => ({ ...prev, search: event.target?.value })) }} />
                            </div>
                        </div>
                    </div>
                    :
                    ''
            }

        </div>
    )
}

export default Filters