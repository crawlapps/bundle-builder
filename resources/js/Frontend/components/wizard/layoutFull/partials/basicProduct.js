import React, { useEffect, useState } from 'react'
import Filters from './filters';
import Summary from './Summary';


function BasicProduct({ options, removeFromModal, data, setOptions, addProduct, removeProduct, element, maxItems, setFormData, showSelectVariantError, filters, setFilters, getFilteredVariants, getTags }) {

    // Check item count from item
    function itemCountMethod(s_id) {

        let itemarray = options.items;

        const searchItem = (what) => itemarray.find(element => (element.id === what && element.step == options.step));

        if (itemarray.length > 0) {
            let searchedArray = searchItem(s_id);
            let result = 0;
            if (typeof searchedArray != 'undefined') {
                result = searchedArray.itemCount;
            }
            return (
                result
            );
        } else {
            return (
                0
            );
        }
    }

    // Modal actions.
    const imageAction = (id, action) => {
        if (action == 'open') {
            document.getElementsByClassName(`img-modal-${id}`)[0].classList.add('is-open');
        }
        if (action == 'close') {
            document.getElementsByClassName(`img-modal-${id}`)[0].classList.remove('is-open');
        }
    }

    // Select items from dropdown of variants :
    // Parameters : v_index => Variant index, ref_id => Product id for refrence , productObj : Selected product, imgSrc => Image source
    function selectFromDropdown(v_index, ref_id, productObj, imgsrc) {

        // Removing selections
        productObj.variants.map(function (variant) {
            let itemsCount = itemCountMethod(variant.id);
            for (let i = 0; i <= itemsCount; i++) {
                removeProduct(variant.id, variant.displayName, parseFloat(variant.price))
            }
        })

        // Selecting variant
        if (v_index != 'none') {
            let itemObj = productObj.variants[v_index];
            console.log(itemObj);
            addProduct(itemObj.id, itemObj.displayName, parseFloat(itemObj.price), imgsrc, false, ref_id)
        }
    }

    // Check item count from item in variant drop down layout : p_id => Product Id
    function itemCountMethodDropdown(p_id) {
        let itemarray = options.items;
        const searchItem = (what) => itemarray.find(element => (element.ref_id === what && element.step == options.step));

        if (itemarray.length > 0) {
            let searchedArray = searchItem(p_id);
            let result = {
                count: 0,
                variant: null
            }
            if (typeof searchedArray != 'undefined') {
                result.count = searchedArray.itemCount;
                result.variant = searchedArray;
                result.variant_price = (parseFloat(searchedArray.price).toFixed(2) / (searchedArray.itemCount)).toFixed(2);
            }
            return result;
        } else {
            let dropdown_select = document.getElementById(`dropdown_${p_id}`);
            if (dropdown_select != null) {
                dropdown_select.value = 'none';
            }
            return (
                {
                    count: 0,
                    variant: null
                }
            );
        }
    }

    return (

        <div
            id="bb-bldr-middle-wizard"
            className="bb-bldr-wizard-branch bb-bldr-wizard-wrapper"
        >

            <div className="bb-bldr-step bb-bldr-current bb-bldr-wizard-step"
                data-steptype="product">
                <div className="bb-bldr-question_title">
                    <h3 className="bb-bldr-wizard-header">{element.title}
                    </h3>
                    <p>{element.description}</p>
                </div>

                <Summary element={element} data={data} options={options} setOptions={setOptions} setFormData={setFormData} removeFromModal={removeFromModal} />

                <Filters filters={filters} data={data} setFilters={setFilters} element={element} getTags={getTags} />

                {
                    // Saperate variants 
                    element.show_variant_as == 0
                        ?
                        <div className="bb-bldr-row bb-bldr-justify-content-center bb-step-content" style={{ display: 'flex', marginBottom: '100px' }}>

                            {
                                getFilteredVariants(element.selected_tab == 1 ? element.selected_products : element.collections, element.selected_tab, element.show_variant_as).map((varient) => {
                                    return (
                                        <div className={"bb-bldr-col-md-4 bb-filt-sort "} style={{ marginBottom: '1.5rem' }} key={varient.id} >

                                            <div className={"bb-bldr-item "} style={{ border: (itemCountMethod(varient.id) > 0 ? `1px solid ${data.builderSettings.settings.style_settings.product_select_color}` : ""), borderRadius: '5px' }}>

                                                <label className={itemCountMethod(varient.id) > 0 ? 'added-icon' : ''} >
                                                    {
                                                        itemCountMethod(varient.id) > 0 &&
                                                        <div className="tick" style={{ background: data.builderSettings.settings.style_settings.product_select_color }}>
                                                            <svg width="700pt" height="700pt" version="1.1" viewBox="0 0 700 700" xmlns="http://www.w3.org/2000/svg">
                                                                <path d="m607.43 132.12-320.6 320.43c-3.2773 3.2578-7.7148 5.0898-12.336 5.0898-4.625 0-9.0586-1.832-12.34-5.0898l-169.57-169.4c-3.2578-3.2773-5.0898-7.7109-5.0898-12.336s1.832-9.0586 5.0898-12.336c3.2852-3.3164 7.7578-5.1797 12.426-5.1797s9.1406 1.8633 12.426 5.1797l157.5 157.5 307.65-308.53c4.4102-4.4375 10.852-6.1875 16.898-4.5898s10.785 6.3008 12.426 12.336c1.6406 6.0391-0.066407 12.492-4.4727 16.93z"></path>
                                                            </svg>
                                                        </div>
                                                    }

                                                    <div className="bb-qty-num" style={{ display: itemCountMethod(varient.id) > 0 ? 'block' : 'none', backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }}>{itemCountMethod(varient.id)}</div>
                                                    <div className="bb-owl-carousel bb-owl-theme bb-owl-loaded bb-owl-drag">

                                                        <div className="bb-owl-stage-outer mb-15"><div className="bb-owl-stage" style={{ transform: 'translate3d(0px, 0px, 0px)', transition: 'all 0s ease 0s', width: '268px' }}><div className="bb-owl-item active" style={{ width: '258px', marginRight: '10px' }}><div className="bb-owl-item">
                                                            {
                                                                data.builderSettings.settings.display_settings.is_enable_image_lightbox
                                                                    ?
                                                                    <div className="bb-overlay-icon" onClick={() => imageAction(varient.id, 'open')}><span className="bb-icon"></span></div>
                                                                    :
                                                                    ''
                                                            }

                                                            <a href="#">
                                                                <img alt="Test Bulky1" className="bb-bldr-img-fluid owl-lazy ls-is-cached lazyloaded" src={varient.image == 'null' ? varient.product_image : JSON.parse(varient.image)} style={{ opacity: '1' }} />
                                                            </a>

                                                        </div></div></div></div><div className="bb-owl-nav disabled"><button type="button" role="presentation" className="bb-owl-prev"><span aria-label="Previous">‹</span></button><button type="button" role="presentation" className="bb-owl-next"><span aria-label="Next">›</span></button></div><div className="bb-owl-dots disabled"><button role="button" className="bb-owl-dot active"><span></span></button></div></div>

                                                    {
                                                        element.is_allow_more_than_one ?
                                                            <>
                                                                {
                                                                    itemCountMethod(varient.id) > 0
                                                                        ?
                                                                        <span className="bb-bldr-qty-down" style={{ display: 'inline-block', backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }} onClick={() => removeProduct(varient.id, varient.displayName, parseFloat(varient.price))} >-</span>
                                                                        : ''
                                                                }

                                                                <span className="bb-bldr-item_price" style={{ backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }} onClick={() => { varient.inventoryQuantity > 0 ? addProduct(varient.id, varient.displayName, parseFloat(varient.price), varient.image == 'null' ? varient.product_image : JSON.parse(varient.image)) : () => { } }}>
                                                                    {varient.inventoryQuantity > 0 ? <span className="bb-bldr-add-text" > {data.builderSettings.settings.display_settings.add_button_text.length > 0 ? data.builderSettings.settings.display_settings.add_button_text : 'Add:'} {window.store_currency}  {data.builderSettings.settings.display_settings.is_show_price_in_button ? ` ${varient.price}` : ''} </span> : ' Sold out'}
                                                                </span>
                                                                {
                                                                    itemCountMethod(varient.id) > 0
                                                                        ?
                                                                        <span className="bb-bldr-qty-up" style={{ display: 'inline-block', backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }} onClick={() => addProduct(varient.id, varient.displayName, parseFloat(varient.price), varient.image == 'null' ? varient.product_image : JSON.parse(varient.image))}>+</span>
                                                                        : ''
                                                                }
                                                            </>
                                                            :
                                                            <>
                                                                {
                                                                    itemCountMethod(varient.id) < 1
                                                                        ?
                                                                        <span className="bb-bldr-item_price" style={{ backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }} onClick={() => { varient.inventoryQuantity > 0 ? addProduct(varient.id, varient.displayName, parseFloat(varient.price), varient.image == 'null' ? varient.product_image : JSON.parse(varient.image)) : () => { } }}>
                                                                            {varient.inventoryQuantity > 0 ? <span className="bb-bldr-add-text" >{data.builderSettings.settings.display_settings.add_button_text.length > 0 ? data.builderSettings.settings.display_settings.add_button_text : 'Add:'}  {data.builderSettings.settings.display_settings.is_show_price_in_button ? window.store_currency + ` ${varient.price}` : ''} </span> : ' Sold out'}
                                                                        </span>
                                                                        : ''
                                                                }
                                                                {
                                                                    itemCountMethod(varient.id) > 0
                                                                        ?
                                                                        <span className="bb-bldr-item_price" style={{ backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }} onClick={() => removeProduct(varient.id, varient.displayName, parseFloat(varient.price))}><span
                                                                            className="bb-bldr-add-text" >Remove:</span>  {data.builderSettings.settings.display_settings.is_show_price_in_button ? window.store_currency + ` ${varient.price}` : ''}</span>
                                                                        : ''
                                                                }

                                                            </>
                                                    }

                                                    <strong>{varient.displayName}</strong>
                                                </label>
                                            </div>

                                            <div className={"bb-modal micromodal-slide " + "img-modal-" + varient.id} id="bb-prod-info" aria-hidden="false">
                                                <div className="bb-modal__overlay" tabIndex="-1" data-micromodal-close="" bis_skin_checked="1">
                                                    <div className="bb-modal__container" role="dialog" aria-modal="true" aria-labelledby="bb-prod-info-title" bis_skin_checked="1" style={{ backgroundColor: data.builderSettings.settings.style_settings.lightbox_background_color }}>
                                                        <header className="bb-modal__header">
                                                            <button type='button' className="bb-modal__close" style={{ color: data.builderSettings.settings.style_settings.lightbox_text_color }} onClick={() => imageAction(varient.id, 'close')} aria-label="Close modal"></button>
                                                        </header>
                                                        <main className="bb-modal__content" id="bb-modal__content">
                                                            <div className="bb-bldr-row" bis_skin_checked="1">
                                                                <div className="bb-bldr-col-md-6" bis_skin_checked="1">
                                                                    <div className="bb-modal-img-container" id="bb-modal-img-container" bis_skin_checked="1">
                                                                        <div className="bb-owl-carousel bb-owl-theme bb-owl-loaded bb-owl-drag" bis_skin_checked="1">
                                                                            <div className="bb-owl-stage-outer" bis_skin_checked="1">
                                                                                <div className="bb-owl-stage" style={{ transform: 'translate3d(0px, 0px, 0px)', transition: 'all 0s ease 0s', width: '415px' }}>
                                                                                    <div className="bb-owl-item active" bis_skin_checked="1" style={{ width: '405px', marginRight: '10px' }}>
                                                                                        <div className="bb-owl-item" bis_skin_checked="1">
                                                                                            <img className="bb-bldr-img-fluid owl-lazy bb-full-width" src={varient.image == 'null' ? varient.product_image : JSON.parse(varient.image)} style={{ opacity: '1' }} />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div className="bb-owl-nav disabled" bis_skin_checked="1">
                                                                                <button type="button" role="presentation" className="bb-owl-prev">
                                                                                    <span aria-label="Previous"></span>
                                                                                </button>
                                                                                <button type="button" role="presentation" className="bb-owl-next">
                                                                                    <span aria-label="Next"></span>
                                                                                </button>
                                                                            </div>
                                                                            <div className="bb-owl-dots disabled" bis_skin_checked="1">
                                                                                <button role="button" className="bb-owl-dot active">
                                                                                    <span>
                                                                                    </span>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="bb-bldr-col-md-6" bis_skin_checked="1">
                                                                    <h2 className="bb-modal__title" style={{ color: data.builderSettings.settings.style_settings.lightbox_text_color }} id="bb-prod-info-title">{varient.title}</h2>
                                                                    <p id="bb-prod-info-content" style={{ color: data.builderSettings.settings.style_settings.lightbox_text_color }}>{varient.displayName}</p>

                                                                    <div dangerouslySetInnerHTML={{ __html: varient.description }} />
                                                                    
                                                                    <div id="bb-modal-select" bis_skin_checked="1"></div>
                                                                    {
                                                                        itemCountMethod(varient.id) < 1
                                                                            ?
                                                                            <button type="button" id="bb-modal-add" style={{ backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }} onClick={() => addProduct(varient.id, varient.displayName, parseFloat(varient.price), varient.image == 'null' ? varient.product_image : JSON.parse(varient.image))} className="bb-bldr-add">Add</button>

                                                                            :

                                                                            <button type="button" id="bb-modal-add" style={{ backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }} onClick={() => removeFromModal(varient.id)} className="bb-bldr-add">Remove</button>
                                                                    }
                                                                </div>
                                                            </div>
                                                        </main>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    );
                                })
                            }

                        </div>
                        :
                        ''
                }

                {
                    // Drop down variant box
                    element.show_variant_as == 1
                        ?
                        <div className="bb-bldr-row bb-bldr-justify-content-center bb-step-content" style={{ display: 'flex', marginBottom: '100px' }}>
                            {
                                getFilteredVariants(element.selected_tab == 1 ? element.selected_products : element.collections, element.selected_tab, element.show_variant_as).map((product, i) => {
                                    let drropDownItem = itemCountMethodDropdown(product.product_id);

                                    return (
                                        <div key={i} className={"bb-bldr-col-md-4 bb-filt-sort "} style={{ marginBottom: '1.5rem' }} >

                                            <div className={"bb-bldr-item "} style={{ border: (drropDownItem.count > 0 ? `1px solid ${data.builderSettings.settings.style_settings.product_select_color}` : ""), borderRadius: '5px' }}>

                                                <label className={drropDownItem.count > 0 ? 'added-icon' : ''} >
                                                    {
                                                        drropDownItem.count > 0 &&
                                                        <div className="tick" style={{ background: data.builderSettings.settings.style_settings.product_select_color }}>
                                                            <svg width="700pt" height="700pt" version="1.1" viewBox="0 0 700 700" xmlns="http://www.w3.org/2000/svg">
                                                                <path d="m607.43 132.12-320.6 320.43c-3.2773 3.2578-7.7148 5.0898-12.336 5.0898-4.625 0-9.0586-1.832-12.34-5.0898l-169.57-169.4c-3.2578-3.2773-5.0898-7.7109-5.0898-12.336s1.832-9.0586 5.0898-12.336c3.2852-3.3164 7.7578-5.1797 12.426-5.1797s9.1406 1.8633 12.426 5.1797l157.5 157.5 307.65-308.53c4.4102-4.4375 10.852-6.1875 16.898-4.5898s10.785 6.3008 12.426 12.336c1.6406 6.0391-0.066407 12.492-4.4727 16.93z"></path>
                                                            </svg>
                                                        </div>
                                                    }

                                                    <div className="bb-qty-num" style={{ display: drropDownItem.count > 0 ? 'block' : 'none', backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }}>{drropDownItem.count}</div>
                                                    <div className="bb-owl-carousel bb-owl-theme bb-owl-loaded bb-owl-drag">

                                                        <div className="bb-owl-stage-outer mb-15">
                                                            <div className="bb-owl-stage" style={{ transform: 'translate3d(0px, 0px, 0px)', transition: 'all 0s ease 0s', width: '268px' }}>
                                                                <div className="bb-owl-item active" style={{ width: '258px', marginRight: '10px' }}>
                                                                    <div className="bb-owl-item">
                                                                        {
                                                                            drropDownItem.count < 1
                                                                                ?
                                                                                <div className="bb-overlay-icon" onClick={() => imageAction(product.product_id, 'open')}><span className="bb-icon"></span></div>
                                                                                :
                                                                                ''
                                                                        }
                                                                        <a>
                                                                            <img alt="Test Bulky1" className="bb-bldr-img-fluid owl-lazy ls-is-cached lazyloaded" src={product.product_image} style={{ opacity: '1' }} />
                                                                        </a>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="bb-owl-nav disabled">
                                                            <button type="button" role="presentation" className="bb-owl-prev">
                                                                <span aria-label="Previous">‹</span>
                                                            </button>
                                                            <button type="button" role="presentation" className="bb-owl-next">
                                                                <span aria-label="Next">›</span>
                                                            </button>
                                                        </div>
                                                        <div className="bb-owl-dots disabled">
                                                            <button role="button" className="bb-owl-dot active"><span></span></button>
                                                        </div>
                                                    </div>


                                                    {
                                                        element.is_allow_more_than_one ?
                                                            <>
                                                                {
                                                                    drropDownItem.count > 0
                                                                        ?
                                                                        <span className="bb-bldr-qty-down" style={{ display: 'inline-block', backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }} onClick={() => removeProduct(drropDownItem.variant.id, drropDownItem.variant.displayName, parseFloat(drropDownItem.variant_price))} >-</span>
                                                                        : ''
                                                                }

                                                                <span className="bb-bldr-item_price" style={{ backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }} onClick={() => drropDownItem.count > 0 ? addProduct(drropDownItem.variant.id, drropDownItem.variant.displayName, parseFloat(drropDownItem.variant_price), product.product_image, false, product.product_id) : showSelectVariantError()}>
                                                                    <span className="bb-bldr-add-text" >{data.builderSettings.settings.display_settings.add_button_text.length > 0 ? data.builderSettings.settings.display_settings.add_button_text : 'Add:'}</span> {window.store_currency} {drropDownItem.count > 0 ? (drropDownItem.variant_price) : '--'}
                                                                </span>
                                                                {
                                                                    drropDownItem.count > 0
                                                                        ?
                                                                        <span className="bb-bldr-qty-up" style={{ display: 'inline-block', backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }} onClick={() => addProduct(drropDownItem.variant.id, drropDownItem.variant.displayName, parseFloat(drropDownItem.variant_price), product.product_image, false, product.product_id)}>+</span>
                                                                        : ''
                                                                }
                                                            </>
                                                            :
                                                            <>
                                                                {
                                                                    drropDownItem.count < 1
                                                                        ?
                                                                        <span className="bb-bldr-item_price" style={{ backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }} onClick={() => addProduct(drropDownItem.variant.id, drropDownItem.variant.displayName, parseFloat(drropDownItem.variant_price), product.product_image, false, product.product_id)}>
                                                                            <span className="bb-bldr-add-text" >{data.builderSettings.settings.display_settings.add_button_text.length > 0 ? data.builderSettings.settings.display_settings.add_button_text : 'Add:'}</span> {window.store_currency} {drropDownItem.count > 0 ? drropDownItem.variant_price : '--'}
                                                                        </span>
                                                                        : ''
                                                                }
                                                                {
                                                                    drropDownItem.count > 0
                                                                        ?
                                                                        <span className="bb-bldr-item_price" style={{ backgroundColor: data.builderSettings.settings.style_settings.primary_button_color }} onClick={() => removeProduct(drropDownItem.variant.id, drropDownItem.variant.displayName, parseFloat(drropDownItem.variant_price))}><span
                                                                            className="bb-bldr-add-text" >Remove:</span> {window.store_currency} {drropDownItem.count > 0 ? drropDownItem.variant_price : '--'}</span>
                                                                        : ''
                                                                }

                                                            </>
                                                    }

                                                    <strong>{product.product_title}</strong>

                                                    <select className="bb-bldr-form-control bb-mt-10" id={`dropdown_${product.product_id}`} defaultValue={drropDownItem.count < 1 ? 'none' : drropDownItem.variant.id} onChange={(val) => { selectFromDropdown(val.target.value, product.product_id, product, product.product_image) }}>
                                                        <option value="none" >Choose a Hair Length...</option>
                                                        {
                                                            product.variants.map((variant, v_index) => {
                                                                return <option key={v_index} price={variant.price} id={variant.id} value={v_index}>{variant.displayName}</option>
                                                            })
                                                        }

                                                    </select>

                                                </label>
                                            </div>

                                            <div className={"bb-modal micromodal-slide " + "img-modal-" + product.product_id} id="bb-prod-info" aria-hidden="false">
                                                <div className="bb-modal__overlay" tabIndex="-1" data-micromodal-close="" bis_skin_checked="1">
                                                    <div className="bb-modal__container" role="dialog" aria-modal="true" aria-labelledby="bb-prod-info-title" bis_skin_checked="1" style={{ backgroundColor: data.builderSettings.settings.style_settings.lightbox_background_color }}>
                                                        <header className="bb-modal__header">
                                                            <button type='button' className="bb-modal__close" style={{ color: data.builderSettings.settings.style_settings.lightbox_text_color }} onClick={() => imageAction(product.product_id, 'close')} aria-label="Close modal"></button>
                                                        </header>
                                                        <main className="bb-modal__content" id="bb-modal__content">
                                                            <div className="bb-bldr-row" bis_skin_checked="1">
                                                                <div className="bb-bldr-col-md-6" bis_skin_checked="1">
                                                                    <div className="bb-modal-img-container" id="bb-modal-img-container" bis_skin_checked="1">
                                                                        <div className="bb-owl-carousel bb-owl-theme bb-owl-loaded bb-owl-drag" bis_skin_checked="1">
                                                                            <div className="bb-owl-stage-outer" bis_skin_checked="1">
                                                                                <div className="bb-owl-stage" style={{ transform: 'translate3d(0px, 0px, 0px)', transition: 'all 0s ease 0s', width: '415px' }}>
                                                                                    <div className="bb-owl-item active" bis_skin_checked="1" style={{ width: '405px', marginRight: '10px' }}>
                                                                                        <div className="bb-owl-item" bis_skin_checked="1">
                                                                                            <img className="bb-bldr-img-fluid owl-lazy bb-full-width" src={product.product_image} style={{ opacity: '1' }} />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div className="bb-owl-nav disabled" bis_skin_checked="1">
                                                                                <button type="button" role="presentation" className="bb-owl-prev">
                                                                                    <span aria-label="Previous"></span>
                                                                                </button>
                                                                                <button type="button" role="presentation" className="bb-owl-next">
                                                                                    <span aria-label="Next"></span>
                                                                                </button>
                                                                            </div>
                                                                            <div className="bb-owl-dots disabled" bis_skin_checked="1">
                                                                                <button role="button" className="bb-owl-dot active">
                                                                                    <span>
                                                                                    </span>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="bb-bldr-col-md-6" bis_skin_checked="1">
                                                                    <h2 className="bb-modal__title" style={{ color: data.builderSettings.settings.style_settings.lightbox_text_color }} id="bb-prod-info-title">{product.product_title}</h2>
                                                                    <div dangerouslySetInnerHTML={{ __html: product.description }} />
                                                                    {/* <p id="bb-prod-info-content" style={{color: data.builderSettings.settings.style_settings.lightbox_text_color}}>{varient.displayName}</p> */}
                                                                    <div id="bb-modal-select" bis_skin_checked="1"></div>
                                                                    <select className="bb-bldr-form-control bb-mt-10" id={`dropdown_${product.product_id}`} defaultValue={drropDownItem.count < 1 ? 'none' : drropDownItem.variant.id} onChange={(val) => { selectFromDropdown(val.target.value, product.product_id, product, product.product_image), imageAction(product.product_id, 'close') }}>
                                                                        <option value="none" >Choose a Hair Length...</option>
                                                                        {
                                                                            product.variants.map((variant, v_index) => {
                                                                                return <option key={v_index} price={variant.price} id={variant.id} value={v_index}>{variant.displayName}</option>
                                                                            })
                                                                        }

                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </main>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    );
                                })
                            }
                        </div>
                        :
                        ''
                }


            </div>

            <div className="bb-bldr-step bb-bldr-submit bb-bldr-wizard-step" data-index="1"
                data-steptype="product" style={{ display: 'none' }} disabled="disabled">
                <div className="bb-bldr-question_title">
                    <h3>My second step product</h3>
                    <p>My second step description</p>
                </div>


                <div className="bb-bldr-row bb-bldr-justify-content-center">

                </div>
                <div className="bb-bldr-row bb-bldr-justify-content-center bb-step-content"
                >

                </div>
            </div>

            <div id="bb-paginate-spinner" style={{ display: 'none' }}>
                <img src="https://builder-front.boxup.io/img/bxp-loader.gif" />
            </div>
            <div id="bb-toast" className="max-items" style={{ bottom: '76px' }}>You can only select {maxItems} product(s)</div>


        </div>

    )
}

export default BasicProduct