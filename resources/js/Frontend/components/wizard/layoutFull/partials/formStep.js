import React, { useEffect, useState } from 'react'
import { ChromePicker } from 'react-color';
import DatePicker from "react-datepicker";
import addDays from "date-fns/addDays";
import { registerLocale } from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Summary from './Summary';

function FormStep({ options, data, setOptions, element, maxItems, formData, setFormData }) {

    // const langs = require('date-fns/locale');
    const langs = (require(`date-fns/locale`));
    const [currentValue, setCurrentValue] = useState({});
    const [loading, setLoading] = useState(true);

    function loadElement() {

        let search = formData.values.find(ele => { return ele.id === element.id });

        if (search) {

        } else {

            let spareObj = {};
            spareObj.id = element.id;
            element.field.sort(function (a, b) { return parseInt(a.display_order) - parseInt(b.display_order) }).map(function (field, i) {
                console.log('Field ::', field)
                if (field.field_type == 'checkbox') {

                    spareObj[`${field.field_title}`] = {
                        value: false,
                        error: '',
                        required: field.is_required_field,
                        type: field.field_type,
                    };
                } else if (field.field_type == 'color-picker') {
                    spareObj[`${field.field_title}`] = {
                        value: {
                            hex: "#2200cb",
                            hsl: { h: 250.11499999999995, s: 1, l: 0.39895, a: 1 },
                            hsv: { h: 250.11499999999995, s: 1, v: 0.7979, a: 1 },
                            oldHue: 249.99999999999994,
                            rgb: { r: 34, g: 0, b: 203, a: 1 },
                            source: "hsv",
                        },
                        error: '',
                        required: field.is_required_field,
                        type: field.field_type,
                    };
                    // } else if (field.field_type == 'image') {
                    //     spareObj[`${field.field_title}`] = {
                    //         value: '',
                    //         error: '',
                    //         required: field.is_required_field,
                    //         type: field.field_type,
                    //         images: field.images,
                    //         condition: field.condition
                    //     };
                } else {
                    spareObj[`${field.field_title}`] = {
                        value: '',
                        error: '',
                        required: field.is_required_field,
                        type: field.field_type,
                    };

                }
            })

            let obj = JSON.parse(JSON.stringify(formData));
            obj.values.push(spareObj);
            setFormData(obj);
        }


    }

    function getCurrentValue() {
        let vals = [];
        if (formData.values.find(ele => { return ele.id === element.id })) {
            vals = formData.values.find(ele => { return ele.id === element.id });
        }
        return vals;
    }

    useEffect(function () {
        loadElement();
    }, [])

    function getDisabledDates(f_i, f) {
        let dates = element.field.sort((a, b) => { return parseInt(a.display_order) - parseInt(b.display_order) })[f_i].block_specific_dates
        let spareArr = [];
        dates.map((d) => {
            spareArr.push(new Date(d.start));
        })
        for (let i = 1; i <= f.lead_time; i++) {
            spareArr.push(addDays(new Date(), i));
        }
        return spareArr;
    }

    function updateForm(attribute, value) {
        let spareObj = Object.assign({}, formData);
        spareObj.values.find(ele => { return ele.id === element.id })[attribute].value = value;
        setFormData(spareObj);
    }

    function getDisabledDays(f) {
        let days = [];
        f.days_to_block.map((b_day, i) => {
            if (b_day.value == true) {
                days.push(i + 1);
                i < 6 ? days.push(i + 1) : days.push(0)
            }
        })
        return days;
    }

    // console.log("Form Data :: ", formData)

    return (

        <div id="bb-bldr-middle-wizard" className="bb-bldr-wizard-branch bb-bldr-wizard-wrapper">

            <div className="bb-bldr-step bb-bldr-current bb-bldr-wizard-step" data-index="0"
                data-steptype="product">
                <div className="bb-bldr-question_title">
                    <h3 className="bb-bldr-wizard-header">{element.title}
                    </h3>
                    <p>{element.description}</p>
                </div>

                <Summary element={element} data={data} options={options} setOptions={setOptions} setFormData={setFormData} />


                <div className="bb-bldr-row bb-bldr-justify-content-center">

                </div>


                <div className="bb-bldr-row bb-bldr-justify-content-center bb-step-content">
                    <div className='bb-bldr-col-lg-8'>
                        <div className='bb-bldr-box_general'>
                            {
                                formData.values.length > 0
                                    ?
                                    element.field.sort((a, b) => { return parseInt(a.display_order) - parseInt(b.display_order) }).map((f, f_i) => {
                                        if (f.field_type == 'text' || f.field_type == 'number') {
                                            return (
                                                <div className="bb-bldr-form-group" key={f_i}>
                                                    <label>{f.field_title}</label>
                                                    <input value={getCurrentValue()[f.field_title].value} onChange={(e) => { updateForm(f.field_title, e.target.value) }} type={f.field_type} attribute={f.field_title} name={`attr_${f.field_title.replaceAll(' ', '-').toLowerCase()}`} className={"bb-bldr-form-control mx-hieght-30 " + (f.is_required_field && 'required')} />
                                                    <span className="bb-error" >{getCurrentValue()[f.field_title].error}</span>
                                                </div>
                                            );
                                        } else if (f.field_type == 'textarea') {
                                            return (
                                                <div className="bb-bldr-form-group" key={f_i}>
                                                    <label>{f.field_title}</label>
                                                    <textarea value={getCurrentValue()[f.field_title].value} onChange={(e) => { updateForm(f.field_title, e.target.value) }} rows={5} type={f.field_type} attribute={f.field_title} name={`attr_${f.field_title.replaceAll(' ', '-').toLowerCase()}`} className={"bb-bldr-form-control " + (f.is_required_field && 'required')}>
                                                    </textarea>
                                                    <span className="bb-error" >{getCurrentValue()[f.field_title].error}</span>
                                                </div>
                                            );
                                        } else if (f.field_type == 'select') {
                                            return (
                                                <div className="bb-bldr-form-group add_bottom_30" key={f_i}>
                                                    <div className="bb-bldr-styled-select">
                                                        <label>{f.field_title}</label>
                                                        <select defaultValue={getCurrentValue()[f.field_title].value} onChange={(e) => { updateForm(f.field_title, e.target.value) }} className={"bb-bldr-form-control " + (f.is_required_field && 'required')} name={`attr_${f.field_title.replaceAll(' ', '-').toLowerCase()}`} attribute={f.field_title}>
                                                            <option defaultValue="">Please select...</option>
                                                            {
                                                                f.dropdown_options.split(',').map((option, o_i) => {
                                                                    return <option value={option} key={o_i}>{option}</option>
                                                                })
                                                            }
                                                        </select>
                                                    </div>
                                                    <span className="bb-error" >{getCurrentValue()[f.field_title].error}</span>
                                                </div>
                                            );
                                        } else if (f.field_type == 'date') {
                                            return (
                                                <div className="bb-bldr-form-group" key={f_i}>
                                                    <label>{f.field_title}</label>
                                                    <input value={getCurrentValue()[f.field_title].value} onChange={(e) => { updateForm(f.field_title, e.target.value) }} type={f.field_type} attribute={f.field_title} name={`attr_${f.field_title.replaceAll(' ', '-').toLowerCase()}`} className={"bb-bldr-form-control mx-hieght-30 " + (f.is_required_field && 'required')} />
                                                    <span className="bb-error" >{getCurrentValue()[f.field_title].error}</span>
                                                </div>
                                            );
                                        } else if (f.field_type == 'color') {
                                            return (
                                                <div className="bb-bldr-form-group" key={f_i}>
                                                    <label>{f.field_title}</label>
                                                    <div className="bb-table">
                                                        {
                                                            f.selected_colors.map((color, c_i) => {
                                                                return (
                                                                    <div key={c_i} className="bb-table-cell">
                                                                        <div
                                                                            className={"bb-swatch-color " + (color.color.hex == getCurrentValue()[f.field_title].value && 'bb-swatch-selected')}
                                                                            onClick={() => { updateForm(f.field_title, color.color.hex) }}
                                                                            style={{ backgroundColor: color.color.hex }}
                                                                        ></div>
                                                                        <span>{color.color.hex}</span>
                                                                    </div>
                                                                );
                                                            })
                                                        }
                                                    </div>
                                                    <span className="bb-error" >{getCurrentValue()[f.field_title].error}</span>
                                                </div>
                                            );
                                        } else if (f.field_type == 'custom-date') {
                                            // registerLocale(f.language,hi);
                                            return (
                                                <div className="bb-bldr-form-group" key={f_i}>
                                                    <label>{f.field_title}</label>
                                                    <DatePicker
                                                        selected={getCurrentValue()[f.field_title].value}
                                                        onChange={(date) => updateForm(f.field_title, date)}
                                                        excludeDates={getDisabledDates(f_i, f)}
                                                        placeholderText="Select a date other than today or yesterday"
                                                        locale={langs[f.language]}
                                                        filterDate={(date) => {
                                                            const day = date.getDay();
                                                            let days = getDisabledDays(f);
                                                            return !days.includes(day);
                                                        }}
                                                        className='bb-bldr-form-control mx-hieght-30'
                                                        dateFormat={f.date_formate.replaceAll('mm', 'MM')}
                                                    />
                                                    <span className="bb-error" >{getCurrentValue()[f.field_title].error}</span>
                                                </div>
                                            );

                                        } else if (f.field_type == 'image') {
                                            return (
                                                <div className="bb-bldr-form-group add_bottom_30" key={f_i}>
                                                    <label>{f.field_title}</label>
                                                    {
                                                        f.images.map((img, i_i) => {
                                                            return (
                                                                <div
                                                                    className={"bxp-image-select " + (img.imageName == getCurrentValue()[f.field_title]?.value ? "bxp-image-selected" : '')}
                                                                    key={i_i}
                                                                    onClick={() => { updateForm(f.field_title, img.imageName) }}
                                                                >
                                                                    <img src={img.src} />
                                                                </div>
                                                            );
                                                        })
                                                    }
                                                    <span className="bb-error" >{getCurrentValue()[f.field_title]?.error}</span>
                                                </div>
                                            );
                                        } else if (f.field_type == 'checkbox') {
                                            return (
                                                <div className="bb-bldr-form-group" key={f_i} >
                                                    <label>{f.field_title}</label>
                                                    <input
                                                        type={f.field_type}
                                                        attribute={f.field_title}
                                                        name={`attr_${f.field_title.replaceAll(' ', '-').toLowerCase()}`}
                                                        className={"bb-bldr-form-control " + (f.is_required_field && 'required')}
                                                        value={''}
                                                        style={{ width: 'unset' }}
                                                        checked={getCurrentValue()[f.field_title].value}
                                                        onChange={(e) => { updateForm(f.field_title, !getCurrentValue()[f.field_title].value) }}
                                                    />
                                                    <span className="bb-error" >{getCurrentValue()[f.field_title].error}</span>
                                                </div>
                                            );
                                        } else if (f.field_type == 'color-picker') {
                                            return (
                                                <div className="bb-bldr-form-group" key={f_i}>
                                                    <label>{f.field_title}</label>
                                                    <ChromePicker
                                                        value={getCurrentValue()[f.field_title].value}
                                                        color={getCurrentValue()[f.field_title].value}
                                                        onChange={(e) => { updateForm(f.field_title, e) }}
                                                        disableAlpha
                                                    />
                                                    <span className="bb-error" >{getCurrentValue()[f.field_title].error}</span>
                                                </div>
                                            );
                                        } else if (f.field_type == 'file') {
                                            return (
                                                <div className="bb-bldr-form-group" key={f_i}>
                                                    <label>{f.field_title}</label>
                                                    <input
                                                        onChange={(e) => { updateForm(f.field_title, e.target.files) }}
                                                        multiple={false} type={f.field_type}
                                                        attribute={f.field_title}
                                                        name={`attr_${f.field_title.replaceAll(' ', '-').toLowerCase()}`}
                                                        className={"bb-bldr-form-control mx-hieght-30 " + (f.is_required_field && 'required')}
                                                        accept={f.field_type == 'Only Images' ? "image/png, image/gif, image/jpeg" : ""}
                                                    />
                                                    <span className="bb-error" >{getCurrentValue()[f.field_title].error}</span>
                                                </div>
                                            );
                                        }
                                    })
                                    :
                                    ''
                            }


                        </div>
                    </div>
                </div>

            </div>

            <div className="bb-bldr-step bb-bldr-submit bb-bldr-wizard-step" data-index="1"
                data-steptype="product" style={{ display: 'none' }} disabled="disabled">
                <div className="bb-bldr-question_title">
                    <h3>My second step product</h3>
                    <p>My second step description</p>
                </div>


                <div className="bb-bldr-row bb-bldr-justify-content-center">

                </div>
                <div className="bb-bldr-row bb-bldr-justify-content-center bb-step-content"
                >

                </div>
            </div>

            <div id="bb-paginate-spinner" style={{ display: 'none' }}>
                <img src="https://builder-front.boxup.io/img/bxp-loader.gif" />
            </div>
            <div id="bb-toast" className="max-items" style={{ bottom: '76px' }}>You can only select {maxItems} product(s)</div>

        </div>

    )
}

export default FormStep;