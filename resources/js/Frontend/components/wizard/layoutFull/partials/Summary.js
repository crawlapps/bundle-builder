import React, { useEffect, useState } from 'react'


function Summary({ element, data, options, setOptions, setFormData, theme = 'full', removeFromModal }) {

    function reset() {
        setOptions({
            totalSteps: data.builderSteps.length,
            step: 1,
            totalAmount: data.builderSettings.start_price,
            items: [],
            errors: [],
            discounts: [],
            mainAmount: data.builderSettings.start_price
        });

        setFormData({
            values: []
        });
    }

    function handleRemoveFromSummary(id, title, price, step) {
        removeFromModal(id, title, price);
        setOptions({ ...options, step: step });
    }

    return (
        <>
            {
                element.is_show_box_content ?

                    <div className="bb-bldr-row bb-step-box bxp-flex">
                        <button type="button" className="bxp-reset" onClick={() => { reset() }}>Reset</button>
                        <div className="bb-bldr-col-lg-8">
                            <div className="bb-step-box-imgs">
                                {
                                    options.items.map((img, i) => {
                                        return (
                                            <div className="bxp-summary-container" title={img.title} key={i} >
                                                <img className="bxp-summary-img" src={img.img} />
                                                <span className="bxp-thumbnail-qty">{img.itemCount}</span>
                                                {
                                                    img.removable
                                                        ?
                                                        <span className="bxp-thumbnail-remove" onClick={() => handleRemoveFromSummary(img.id, img.title, img.price, img.step)}>X</span>
                                                        :
                                                        ''
                                                }
                                            </div>
                                        );
                                    })
                                }

                            </div>
                        </div>
                        {
                            theme == 'full'
                                ?
                                <div className="bxp-bldr-col-lg-4">
                                    <div className="bb-step-box-list">
                                        <h5>Order details</h5>
                                        <ul className="bxp-box-list">
                                            {
                                                options.items.map((item, i) => {
                                                    return (
                                                        <li key={i}>{item.title}<span className="bxp-right">x{item.itemCount}</span></li>
                                                    );
                                                })
                                            }
                                        </ul>
                                    </div>
                                </div>
                                :
                                ''
                        }

                    </div>

                    :

                    ''
            }
        </>
    )
}

export default Summary