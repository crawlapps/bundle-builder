import React from 'react'

function ProgressBar({ data, options }) {
    return (
        <div id="bb_progress" style={{ display: 'block' }}>
            <div className="bb_progress-bar" style={{ borderColor: data.builderSettings.settings.style_settings.progress_bar_color }}></div>
            <div id="bb-steps" className="bb-steps">
                {
                    data.builderSteps.sort((nstep, lstep) => { return nstep.display_order - lstep.display_order }).map((step, index) => {
                        if (!step.is_hide_from_step_progress) {

                            return (
                                <div
                                    className={`bb-step ${step.display_order == options.step ? 'bb-step-current' : ''} `}
                                    key={index}
                                    style={{ color: data.builderSettings.settings.style_settings.progress_bar_text_color }}
                                >
                                    <div className="bb-circle"></div>
                                    <div className="bb_progress-number">Step {index + 1}</div>
                                    <div className="bb_progress-title">{step.short_title}</div>
                                </div>
                            );
                        }
                    })
                }
            </div>
        </div>
    )
}

export default ProgressBar