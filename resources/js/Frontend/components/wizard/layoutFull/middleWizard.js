import React from 'react'

import BasicProduct from './partials/basicProduct.js';
import Content from './partials/Content.js';
import FormStep from './partials/formStep.js';
import PreSelectedProducts from './partials/preSelectedProducts.js';
import ProgressBar from './partials/progressBar.js';
import WizardFooter from './wizardFooter.js';

function MiddleWizard(
    {
        options,
        data,
        setOptions,
        minItems,
        incrementStep,
        decrementStep,
        complatePurchase,
        formData,
        setFormData,
        showSelectVariantError,
        filters,
        setFilters,
        getFilteredVariants,
        maxItems,
        addProduct,
        removeProduct,
        removeFromModal,
        getTags
    }) {

    return (
        <>
            <main className="bb_main bxp-theme-full" id="bb_main">
                <div className="bb_container" bis_skin_checked="1">

                    {
                        data.builderSettings.settings.display_settings.is_show_step_progress
                            ?
                            <ProgressBar data={data} options={options} />
                            :
                            ''
                    }

                    <div id="bb-bldr-wizard_container" className="wizard" bis_skin_checked="1">


                        {
                            data.builderSteps.map((element, index) => {

                                if (options.step == element.display_order) {

                                    if (element.type == 'product') {

                                        return (
                                            <BasicProduct
                                                key={index}
                                                options={options}
                                                setFormData={setFormData}
                                                removeFromModal={removeFromModal}
                                                data={data}
                                                setOptions={setOptions}
                                                addProduct={addProduct}
                                                removeProduct={removeProduct}
                                                element={element}
                                                showSelectVariantError={showSelectVariantError}
                                                filters={filters}
                                                setFilters={setFilters}
                                                getFilteredVariants={getFilteredVariants}
                                                getTags={getTags}
                                            />
                                        )

                                    } else if (element.type == 'pre_selected') {
                                        return (
                                            <PreSelectedProducts
                                                key={index}
                                                options={options}
                                                setFormData={setFormData}
                                                removeFromModal={removeFromModal}
                                                data={data}
                                                setOptions={setOptions}
                                                addProduct={addProduct}
                                                removeProduct={removeProduct}
                                                element={element}
                                                maxItems={maxItems}
                                            />
                                        )
                                    }
                                    else if (element.type == 'form') {
                                        return (
                                            <FormStep
                                                key={index}
                                                options={options}
                                                data={data}
                                                setOptions={setOptions}
                                                element={element}
                                                maxItems={maxItems}
                                                formData={formData}
                                                setFormData={setFormData}
                                            />
                                        )
                                    }
                                    else if (element.type == 'content') {
                                        return (
                                            <Content key={index}
                                                options={options}
                                                data={data}
                                                element={element}
                                            />
                                        )
                                    }

                                }

                            })
                        }
                        <WizardFooter options={options} data={data} setOptions={setOptions} minItems={minItems} incrementStep={incrementStep} decrementStep={decrementStep} complatePurchase={complatePurchase} />

                    </div>
                </div>
            </main>

        </>
    )
}

export default MiddleWizard