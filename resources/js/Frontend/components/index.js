
import axios from 'axios';
import React, { Component, useEffect, useState, createContext } from 'react';
import Alerts from './Alerts.js';
import MiddleWizard from './wizard/layoutFull/middleWizard.js';
import MainWizard from './wizard/layoutRight/mainWizard.js';

export default function Index(props) {

    const [data, setData] = useState(props.data);
    const [loading, setLoading] = useState(true);
    const [options, setOptions] = useState({});
    const [formData, setFormData] = useState({
        values: []
    });
    const [filters, setFilters] = useState({
        sort_by: 'default',
        filterByCollection: 'default',
        filterByTags: 'default',
        search: ''
    });

    const getFilteredVariants = (products, selected_tab, show_variant_as) => {

        let sortedVariants = JSON.parse(JSON.stringify(products));

        if (show_variant_as == 0) { // Variants display as saperate
            let variants = getVariants(products, selected_tab);

            // --------------> Sort by <--------------
            if (filters.sort_by == 'default' || filters.sort_by == ('asc')) {
                // Ascending
                sortedVariants = variants.sort((a, b) => a.displayName.localeCompare(b.displayName))
            } else if (filters.sort_by == ('desc')) {
                // Descending
                sortedVariants = variants.sort((a, b) => b.displayName.localeCompare(a.displayName))
            } else if (filters.sort_by == ('l2h')) {
                // Price : Low to high
                sortedVariants = variants.sort((a, b) => parseFloat(a.price) - parseFloat(b.price))
            } else if (filters.sort_by == ('h2l')) {
                // Price : High to low
                sortedVariants = variants.sort((a, b) => parseFloat(b.price) - parseFloat(a.price))
            }
            // --------------> Sort by <--------------

            // --------------> Search <--------------
            if (filters.search.length > 0) {
                const search = what => sortedVariants.filter(element => element.displayName.toLowerCase().includes(what.toLowerCase()));
                sortedVariants = search(filters.search);
            }
            // --------------> Search <--------------

        } else if (show_variant_as == 1) { // Variants in dropdown
            let variants = getDropdownProducts(products, selected_tab);
            sortedVariants = variants;

            // // --------------> Sort by <--------------
            if (filters.sort_by == 'default' || filters.sort_by == ('asc')) {
                // Ascending
                sortedVariants = variants.sort((a, b) => a.product_title.localeCompare(b.product_title))
            } else if (filters.sort_by == ('desc')) {
                // Descending
                sortedVariants = variants.sort((a, b) => b.product_title.localeCompare(a.product_title))
            } else if (filters.sort_by == ('l2h')) {
                // Price : Low to high
                sortedVariants = variants.sort((a, b) => parseFloat(a.variants[0].price) - parseFloat(b.variants[0].price))
            } else if (filters.sort_by == ('h2l')) {
                // Price : High to Low
                sortedVariants = variants.sort((a, b) => parseFloat(b.variants[0].price) - parseFloat(a.variants[0].price))
            }
            // // --------------> Sort by <--------------

            // --------------> Search <--------------
            if (filters.search.length > 0) {
                const search = what => sortedVariants.filter(element => element.product_title.toLowerCase().includes(what.toLowerCase()));
                sortedVariants = search(filters.search);
            }
            // --------------> Search <--------------
        }

        // // --------------> Tags Filter <--------------
        if (filters.filterByTags != 'default') {
            console.log('Tag :: ', filters.filterByTags);
            let spareVariants = JSON.parse(JSON.stringify(sortedVariants))
            sortedVariants = [];
            spareVariants.map(function (variant) {
                if (variant.product_tags.includes(filters.filterByTags)) {
                    // console.log(variant.product_tags);
                    sortedVariants.push(variant);
                }
            });
            // console.log(sortedVariants);
        }
        // // --------------> Tags Filter <--------------

        // // --------------> Collections Filter <--------------
        if (filters.filterByCollection != 'default') {
            // console.log(filters.filterByCollection)
            let spareVariants = JSON.parse(JSON.stringify(sortedVariants))
            sortedVariants = [];
            let stepArray = JSON.parse(JSON.stringify(data.builderSteps));
            const searchStepArray = what => stepArray.find(element => element.display_order === what);
            const searchCollectionFilter = what => collections_filter.find(element => element.id == what);

            let collections_filter = searchStepArray(options.step).collections_filter;
            let selected_collection = searchCollectionFilter(filters.filterByCollection);
            // console.log('selected_collection',selected_collection);

            if (Array.isArray(selected_collection?.products)) {
                spareVariants.map(function (variant) {
                    if (selected_collection.products.includes(variant.product_id)) {
                        sortedVariants.push(variant);
                    }
                });
            }

        }
        console.log(sortedVariants)
        // // --------------> Collections Filter <--------------

        return sortedVariants;
    }

    const getVariants = function (datas, selected_tab) {
        let variants = [];
        if (selected_tab == 1) {
            datas.map(function (product) {
                product.variants.map(function (varient) {
                    if (data.builderSettings.settings.display_settings.is_hide_soldout_products) {
                        if (varient.inventoryQuantity > 0) {
                            variants.push({
                                displayName: varient.displayName,
                                id: varient.id,
                                inventoryQuantity: varient.inventoryQuantity,
                                price: varient.price,
                                sku: varient.sku,
                                title: varient.title,
                                image: varient.image,
                                product_id: product.id,
                                product_image: product.image,
                                product_title: product.title,
                                product_tags: product.tags,
                                description: product.description,
                            });
                        }
                    } else {
                        variants.push({
                            displayName: varient.displayName,
                            id: varient.id,
                            inventoryQuantity: varient.inventoryQuantity,
                            price: varient.price,
                            sku: varient.sku,
                            title: varient.title,
                            image: varient.image,
                            product_id: product.id,
                            product_image: product.image,
                            product_title: product.title,
                            product_tags: product.tags,
                            description: product.description,
                        });
                    }
                });
            })
        } else {
            datas.map(function (collection) {
                collection.products.map(function (product) {
                    product.variants.map(function (varient) {
                        if (data.builderSettings.settings.display_settings.is_hide_soldout_products) {
                            if (varient.inventoryQuantity > 0) {
                                variants.push({
                                    displayName: varient.displayName,
                                    id: varient.id,
                                    inventoryQuantity: varient.inventoryQuantity,
                                    price: varient.price,
                                    sku: varient.sku,
                                    title: varient.title,
                                    image: varient.image,
                                    product_id: product.id,
                                    product_image: product.image,
                                    product_title: product.title,
                                    product_tags: product.tags,
                                    description: product.description,
                                });
                            }
                        } else {
                            variants.push({
                                displayName: varient.displayName,
                                id: varient.id,
                                inventoryQuantity: varient.inventoryQuantity,
                                price: varient.price,
                                sku: varient.sku,
                                title: varient.title,
                                image: varient.image,
                                product_id: product.id,
                                product_image: product.image,
                                product_title: product.title,
                                product_tags: product.tags,
                                description: product.description,
                            });
                        }
                    });
                });
            })
        }

        return variants;

    }

    const getDropdownProducts = function (datas, selected_tab) {
        let spareProducts = [];
        if (selected_tab == 1) {

            datas.map(function (product) {
                let spareVariants = [];
                product.variants.map(function (varient) {
                    if (data.builderSettings.settings.display_settings.is_hide_soldout_products) {
                        if (varient.inventoryQuantity > 0) {
                            spareVariants.push({
                                displayName: varient.displayName,
                                id: varient.id,
                                inventoryQuantity: varient.inventoryQuantity,
                                price: varient.price,
                                sku: varient.sku,
                                title: varient.title,
                            });
                        }
                    } else {
                        spareVariants.push({
                            displayName: varient.displayName,
                            id: varient.id,
                            inventoryQuantity: varient.inventoryQuantity,
                            price: varient.price,
                            sku: varient.sku,
                            title: varient.title,
                        });
                    }


                });
                spareProducts.push({
                    product_id: product.id,
                    product_image: product.image,
                    product_title: product.title,
                    product_tags: product.tags,
                    description: product.description,
                    variants: spareVariants
                });
            })

        } else {

            datas.map(function (collection) {
                collection.products.map(function (product) {
                    let spareVariants = [];
                    product.variants.sort((a, b) => parseFloat(a.price) - parseFloat(b.price)).map(function (varient) {

                        if (data.builderSettings.settings.display_settings.is_hide_soldout_products) {
                            if (varient.inventoryQuantity > 0) {
                                spareVariants.push({
                                    displayName: varient.displayName,
                                    id: varient.id,
                                    inventoryQuantity: varient.inventoryQuantity,
                                    price: varient.price,
                                    sku: varient.sku,
                                    title: varient.title,
                                });
                            }
                        } else {
                            spareVariants.push({
                                displayName: varient.displayName,
                                id: varient.id,
                                inventoryQuantity: varient.inventoryQuantity,
                                price: varient.price,
                                sku: varient.sku,
                                title: varient.title,
                            });
                        }

                    });
                    spareProducts.push({
                        product_id: product.id,
                        product_image: collection.image.originalSrc,
                        product_title: product.title,
                        product_tags: product.tags,
                        variants: spareVariants
                    });
                });
            });

        }
        return spareProducts;
    }

    const getTags = function (datas, selected_tab) {
        let finalTags = []
        let stepArray = JSON.parse(JSON.stringify(data.builderSteps));
        const searchStepArray = what => stepArray.find(element => element.display_order === what);
        let step = searchStepArray(options.step);
        if (step.dropdown_tags != "") {
            finalTags = step.dropdown_tags.split(',');
        } else {
            if (selected_tab == 1) {
                datas.map(function (product) {
                    if (product.tags.length > 0) {
                        product.tags.map(function (tag) {
                            finalTags.push(tag);
                        })
                    }
                })
            } else {
                datas.map(function (collection) {
                    collection.products.map(function (product) {
                        if (product.tags.length > 0) {
                            product.tags.map(function (tag) {
                                finalTags.push(tag);
                            })
                        }
                    });
                });
            }
        }
        finalTags = [...new Set(finalTags)]
        return finalTags;
    }

    const fetchdata = () => {
        console.log('Api called')
        if(typeof window.bundle_steps == 'undefined'){
            console.log('Undefined window............');
            axios({
                url: data.url + '/api/builder/fetch-builder/' + data.id,
                method: 'GET'
            })
                .then(function (res) {
                    console.log('res');
                    console.log(res);
                    setData(res.data);
    
                    // setOptions({
                    //     totalSteps: res.data.builderSteps.length,
                    //     step: 1,
                    //     totalAmount: res.data.builderSettings.start_price,
                    //     items: [
                    //     ],
                    //     errors: [
                    //     ],
                    //     discounts: [
                    //     ],
                    //     mainAmount: res.data.builderSettings.start_price
                    // });
                    // setLoading(false);
                    getSelection(res.data);
                })
                .catch((err) => {
                    console.log(err);
                })
        }else{
            let spareData = Object.assign({},data);
            spareData.builderSteps = window.bundle_steps;
            setData(spareData);
            console.log(spareData)

            setOptions({
                totalSteps: spareData.builderSteps.length,
                step: 1,
                totalAmount: spareData.builderSettings.start_price,
                items: [
                ],
                errors: [
                ],
                discounts: [
                ],
                mainAmount: spareData.builderSettings.start_price
            });
    
            // configureSEO(fetchedData);
            setLoading(false);
        }
    }

    useEffect(() => {
        fetchdata();
    }, [])

    const [minItems, setMinItems] = useState(1); // Count of muinimum items to be included

    // Increase step
    const incrementStep = () => {

        let stepArray = JSON.parse(JSON.stringify(data.builderSteps));
        const search = what => stepArray.find(element => element.display_order === what);
        let currentEle = search(options.step);

        if (currentEle.type == 'form') {

            let spareObbj = Object.assign({}, formData);
            let foundData = formData.values.find(ele => { return ele.id === currentEle.id });
            let errorsCount = 0;

            const index = spareObbj.values.map(i => i.id).indexOf(foundData.id)
            spareObbj.values.splice(index, 1)
            for (let [key, value] of Object.entries(foundData)) {
                if (key != 'id') {
                    if (value.required) {
                        if (value.value == '' || value.value == null || value.value == false) {
                            value.error = 'Required'
                            foundData[key] = value;
                            errorsCount++;
                        } else {
                            value.error = ''
                            foundData[key] = value;
                        }
                        // console.log(key, ' : ', value);
                    }
                }
            }
            spareObbj.values.push(foundData);
            setFormData(spareObbj);

            if (errorsCount < 1) {
                console.log('Increase Step');
                setOptions({ ...options, step: ++options.step });
            }


        } else if (currentEle.type == 'content') {
            setOptions({ ...options, step: ++options.step });
        } else {
            let minItem = search(options.step)

            let itemarray = JSON.parse(JSON.stringify(options.items));
            const searchItem = (what) => {
                let spArr = [];
                for (const i in itemarray) {
                    if (itemarray[i].step == what) {
                        // console.log(itemarray[i]);
                        spArr.push(itemarray[i]);
                    }
                }
                return spArr;
            };

            setMinItems(minItem.minimum_selection);

            let searchedItem = searchItem(minItem.display_order)

            if (searchedItem.length > 0 || minItem.minimum_selection == 0) {
                let iCount = 0;
                searchedItem.forEach(element => {
                    iCount = iCount + element.itemCount;
                });

                if (iCount >= minItem.minimum_selection) {
                    setOptions({ ...options, step: ++options.step });
                } else {
                    showMinItemToast()
                }
            } else {
                showMinItemToast()
            }
        }


        rememberSelection();
    }

    // Decrease step
    const decrementStep = () => {
        setOptions({ ...options, step: --options.step });
        rememberSelection();
    }

    // Complete Purchase
    const complatePurchase = () => {
        let stepArray = JSON.parse(JSON.stringify(data.builderSteps));
        const search = what => stepArray.find(element => element.display_order === what);
        let currentEle = search(options.step);

        if (currentEle.type == 'form') {

            let spareObbj = Object.assign({}, formData);
            let foundData = formData.values.find(ele => { return ele.id === currentEle.id });
            let errorsCount = 0;

            const index = spareObbj.values.map(i => i.id).indexOf(foundData.id)
            spareObbj.values.splice(index, 1)
            for (let [key, value] of Object.entries(foundData)) {
                if (key != 'id') {
                    if (value.required) {
                        if (value.value == '' || value.value == null || value.value == false) {
                            value.error = 'Required'
                            foundData[key] = value;
                            errorsCount++;
                        } else {
                            value.error = ''
                            foundData[key] = value;
                        }
                        // console.log(key, ' : ', value);
                    }
                }
            }
            spareObbj.values.push(foundData);
            setFormData(spareObbj);

            if (errorsCount < 1) {
                addToCart();
                // setOptions({ ...options, step: ++options.step });

            }


        } else if (currentEle.type == 'content') {
            addToCart();
        } else {
            let minItem = search(options.step)
            let itemarray = JSON.parse(JSON.stringify(options.items));
            const searchItem = (what) => {
                let spArr = [];
                for (const i in itemarray) {
                    if (itemarray[i].step == what) {
                        // console.log(itemarray[i]);
                        spArr.push(itemarray[i]);
                    }
                }
                return spArr;
            };

            setMinItems(minItem.minimum_selection);

            let searchedItem = searchItem(minItem.display_order)

            if (searchedItem.length > 0 || minItem.minimum_selection == 0) {
                let iCount = 0;
                searchedItem.forEach(element => {
                    iCount = iCount + element.itemCount;
                });

                if (iCount >= minItem.minimum_selection) {
                    addToCart();
                } else {
                    showMinItemToast()
                }
            } else {
                showMinItemToast()
            }
        }


    }

    const addToCart = () => {
        showCartAnimation();
        let base_url = window.location.origin;
        let cartData = new FormData();
        let itemCount = 0;

        // Adding every product in properties=
        options.items.map(function (val) {
            itemCount = itemCount + val.itemCount;
            cartData.append(`properties[${val.title}]`, val.itemCount)
        })

        addFormData();
        // Adding All form data in properties
        function addFormData() {
            formData.values.map(function (field, f_i) {
                // console.log(field);
                for (let [key, val] of Object.entries(field)) {
                    if (key != 'id') { // Skipping for id key value
                        if (val.value != '' && val.value != null) { // If value is not null then add it to cart properties.
                            // Checking and performing diffrent processes according to data types
                            if (val.type == 'color-picker') {
                                cartData.append(`properties[${key}]`, val.value.hex)
                            } else if (val.type == 'custom-date') {
                                let date = new Date(val.value);
                                cartData.append(`properties[${key}]`, ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '/' + ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '/' + date.getFullYear())
                            } else if (val.type == 'file') {
                                cartData.append(`properties[${key}]`, val.value[0])
                            } else {
                                cartData.append(`properties[${key}]`, val.value)
                            }
                        }
                    }
                }
                if (f_i + 1 == formData.values.length) {
                    makeVariant();
                }
            })
            if (formData.values.length == 0) {
                makeVariant();
            }
        }

        // Separate method for varient add cart operations To Get "Asyncronouse data from above files api"
        function makeVariant() {
            console.log('Make Variant Called With Price :: ', options.totalAmount);
            axios({
                method: 'POST',
                url: props.data.url + '/api/make-variant',
                data: {
                    product_id: data.shopify_product_id,
                    variant_title: `${itemCount} item(s) at ${options.totalAmount}`,
                    price: options.totalAmount,
                    lineItems: options.items
                }
            })
                .then((res) => {
                    if (res.data.isSuccess == true) {
                        cartData.append('id', res.data.data.variant_id)
                        axios({
                            url: base_url + '/cart/add.js',
                            method: 'POST',
                            data: cartData
                        }).then((res) => {
                            console.log(res);
                            // window.location = base_url + '/' + data.builderSettings.on_complete_action;
                        }).catch((err) => {
                            console.log(err);
                        }).finally(() => {
                            hideCartAnimation()
                        })
                    }
                }).catch((err) => {
                    console.log(err);
                })
        }

    }

    const showCartAnimation = () => {
        if (data.builderSettings.settings.display_settings.is_show_msg_on_addtocart) {
            let cartAnimation = document.getElementById('cartAnimation');
            cartAnimation.style.display = 'block'
        }
    }

    const hideCartAnimation = () => {
        if (data.builderSettings.settings.display_settings.is_show_msg_on_addtocart) {
            let cartAnimation = document.getElementById('cartAnimation');
            cartAnimation.style.display = 'none'
        }
    }

    // Show minimum item toast : Method
    const showMinItemToast = () => {

        let bxp_toast = document.getElementsByClassName('nextError')[0];
        bxp_toast.classList.remove('bxp-show-error');
        bxp_toast.classList.add('bxp-show-error');
        setTimeout(() => {
            bxp_toast.classList.remove('bxp-show-error');
        }, 3000);

    }

    // Show select a variant from down toast
    const showSelectVariantError = () => {
        let select_items_toast = document.querySelector('.select_items');
        select_items_toast.classList.remove('bxp-show-error');
        select_items_toast.classList.add('bxp-show-error');
        setTimeout(() => {
            select_items_toast.classList.remove('bxp-show-error');
        }, 3000);
    }

    // Remember Selection
    const rememberSelection = () => {
        if (data.builderSettings.settings.display_settings.is_remember_selection) {
            console.log('Remembering Selection');
            let selected_items = [];
            options.items.map((item) => {
                selected_items.push({
                    item_id: item.id,
                    step_no: item.step,
                    itemCount: item.itemCount,
                    ref_id: item.ref_id,

                });
            })

            localStorage.setItem(`${data.shopify_product_id}_selected_items`, JSON.stringify(selected_items));
        }
        setFilters({
            sort_by: 'default',
            filterByCollection: 'default',
            filterByTags: 'default',
            search: ''
        });
    }

    // Get rememberd data
    const getSelection = (fetchedData) => {

        setOptions({
            totalSteps: fetchedData.builderSteps.length,
            step: 1,
            totalAmount: fetchedData.builderSettings.start_price,
            items: [
            ],
            errors: [
            ],
            discounts: [
            ],
            mainAmount: fetchedData.builderSettings.start_price
        });

        // configureSEO(fetchedData);
        setLoading(false);

        // if (fetchedData.builderSettings.settings.display_settings.is_remember_selection) {
        //     console.log('Getiing data from local');
        //     if (localStorage.getItem(`${fetchedData.shopify_product_id}_selected_items`) !== null) {
        //         // setOptions(JSON.parse(localStorage.getItem('options')))

        //         let addedItems = JSON.parse(localStorage.getItem(`${fetchedData.shopify_product_id}_selected_items`));
        //         console.log(addedItems);
        //         let stepArray = fetchedData.builderSteps;

        //         addedItems.map((item) => {
        //             let selectedStep = stepArray.find(element => element.display_order === item.step_no);
        //             let stepVariants = getFilteredVariants(selectedStep.selected_tab == 1 ? selectedStep.selected_products : selectedStep.collections, selectedStep.selected_tab, selectedStep.show_variant_as);

        //             console.log('Step Variants :: ', stepVariants);
        //             let variant = stepVariants.find(ele => ele.id === ele.id);
        //             console.log(variant);
        //             for (let i = 0; i < item.itemCount; i++) {
        //                 if (selectedStep.show_variant_as == 0) {
        //                     console.log('adding item');
        //                     console.log(options);
        //                     // addProduct(variant.id, variant.displayName, parseFloat(variant.price), variant.product_image)
        //                 }
        //             }

        //         });
        //     }
        // }
    }

    const configureSEO = (spareData) => {
        let seoDetails = spareData.builderSettings.settings.seo_settings;

        let description = document.createElement('meta');
        description.setAttribute('name', 'description');
        description.setAttribute('content', seoDetails.description);

        let keywords = document.createElement('meta');
        keywords.setAttribute('name', 'keywords');
        keywords.setAttribute('content', seoDetails.keywords);

        document.head.appendChild(description)
        document.head.appendChild(keywords)

    }

    // -------------------------------------------------- Lower level methods -------------------------------------
    const [maxItems, setMaxItems] = useState(1);

    const addProduct = (s_id, s_title, s_price, imgsrc = null, ignor_max = false, refrence_id = null, removable = true) => {

        let itemarray = JSON.parse(JSON.stringify(options.items));
        const search = (what) => itemarray.find(element => (element.id === what && element.step == options.step));

        let stepArray = JSON.parse(JSON.stringify(data.builderSteps));
        const searchStepArray = what => stepArray.find(element => element.display_order === what);

        let maxItem = searchStepArray(options.step)
        let maxItemCount = (maxItem.maximum_selection);

        const searchItem = (what) => {
            let spArr = [];
            for (const i in itemarray) {
                if (itemarray[i].step == what) {
                    // console.log(itemarray[i]);
                    spArr.push(itemarray[i]);
                }
            }
            return spArr;
        };
        let maxItems = 0;
        searchItem(options.step).forEach(ele => {
            maxItems = maxItems + ele.itemCount;
        });


        // console.log(searchItem(options.step));
        if (ignor_max == false) {
            if (maxItems < maxItemCount) { // Check

                if (search(s_id)) {

                    let searchedArray = search(s_id);
                    searchedArray.itemCount++;
                    searchedArray.price += s_price;
                    itemarray[itemarray.findIndex((el, i) => (el.id === s_id && el.step == options.step))] = searchedArray;

                    showAddItemToast()

                } else {
                    itemarray.push({
                        itemCount: 1,
                        id: s_id,
                        title: s_title,
                        price: s_price,
                        step: options.step,
                        img: imgsrc,
                        ref_id: refrence_id,
                        removable: removable
                    });
                    showAddItemToast()
                }
            } else {
                // console.log('kadfnhkadfn');
                setMaxItems(maxItemCount);
                showMaxItemToast()
            }
        } else {
            if (search(s_id)) {

                let searchedArray = search(s_id);
                searchedArray.itemCount++;
                searchedArray.price += s_price;
                itemarray[itemarray.findIndex((el, i) => (el.id === s_id && el.step == options.step))] = searchedArray;

                showAddItemToast()

            } else {
                itemarray.push({
                    itemCount: 1,
                    id: s_id,
                    title: s_title,
                    price: s_price,
                    step: options.step,
                    img: imgsrc,
                    ref_id: refrence_id,
                    removable: removable
                });
                showAddItemToast()
            }
        }


        let spareOptions = options;

        spareOptions.items = itemarray;

        // Editing state of options
        setOptions(spareOptions);

        calculateAmount()
    }

    const removeProduct = (s_id, s_title, s_price) => {

        let itemarray = JSON.parse(JSON.stringify(options.items));
        // const search = what => itemarray.find(element => element.id === what);
        const search = (what) => itemarray.find(element => (element.id === what && element.step == options.step));

        // console.log(search(s_id))

        if (search(s_id)) {
            let searchedArray = search(s_id);
            if (searchedArray.itemCount < 2) {
                itemarray.splice(itemarray.findIndex((el, i) => (el.id === s_id && el.step == options.step)), 1);
                // setSelectedItem(0)
            } else {
                searchedArray.itemCount--;
                searchedArray.price -= s_price;
                itemarray[itemarray.findIndex((el, i) => (el.id === s_id && el.step == options.step))] = searchedArray;
            }
        }

        let spareOptions = options;

        spareOptions.items = itemarray;

        // Editing state of options
        setOptions(spareOptions);
        calculateAmount()
    }

    // Calculating discounts
    const manageDiscounts = (amount) => {
        // console.log(amount);
        let discounts = data.builderDiscounts.sort((a, b) => { return a.order - b.order });
        let spareOptions = Object.assign({}, options);
        let discountsArray = [];
        let mainAmount = parseFloat(amount); // Main amount is 
        const searchDiscount = (what) => options.discounts.find(element => (element.id === what));
        // Count total items of added in cart
        let totalItems = 0;
        options.items.map((item) => {
            totalItems = totalItems + item.itemCount;
        })

        if (!data.builderSettings.settings.product_settings.is_use_on_new_orders) {
            countDiscount();
        } else if (data.builderSettings.settings.product_settings.is_use_on_new_orders && data.builderSettings.settings.product_settings.is_keep_builder_on_order) {
            countDiscount();
        }

        function countDiscount() {
            // Looping through all discounts
            discounts.map((discount) => {

                // Remove other discounts added before if enabled it is enabled
                if (discount.is_remove_other_discount) {
                    mainAmount = parseFloat(amount);
                    discountsArray = [];
                }

                // No items discount
                if (discount.apply_discount == "count" && discount.no_of_items_required <= totalItems) {
                    let discountAmount = parseInt(discount.amount);

                    if (discount.type == 'percent') {
                        discountAmount = ((parseFloat(amount) * parseInt(discount.amount)) / 100).toFixed(2);
                        mainAmount = (mainAmount - ((mainAmount * parseInt(discount.amount)) / 100)).toFixed(2);
                    } else {
                        mainAmount = (mainAmount - parseInt(discount.amount)).toFixed(2);
                    }

                    discountsArray.push({
                        id: discount.id,
                        title: discount.title,
                        value: discountAmount,
                        title: discount.title,
                        amount: parseInt(discount.amount).toFixed(2),
                        type: discount.type
                    });
                    if (!searchDiscount(discount.id)) {
                        showDiscountPopup('Discount Applied')
                    }
                } else if (discount.apply_discount == "price" && amount >= discount.price_required) { // Price reaches discount
                    let discountAmount = parseInt(discount.amount);

                    if (discount.type == 'percent') {
                        discountAmount = ((parseFloat(amount) * parseInt(discount.amount)) / 100).toFixed(2);
                        mainAmount = (mainAmount - ((mainAmount * parseInt(discount.amount)) / 100)).toFixed(2);
                    } else {
                        mainAmount = (mainAmount - parseInt(discount.amount)).toFixed(2);
                    }

                    discountsArray.push({
                        id: discount.id,
                        title: discount.title,
                        value: discountAmount,
                        title: discount.title,
                        amount: parseInt(discount.amount).toFixed(2),
                        type: discount.type
                    });
                    if (!searchDiscount(discount.id)) {
                        showDiscountPopup('Discount Applied')
                    }
                }

            })
        }

        spareOptions.discounts = discountsArray;
        if (typeof mainAmount == 'number') {
            spareOptions.mainAmount = mainAmount.toFixed(2);
        } else {
            spareOptions.mainAmount = mainAmount;
        }
        spareOptions.totalAmount = amount;
        setOptions(spareOptions);

        if (!data.builderSettings.settings.product_settings.is_use_on_new_orders) {
            encourageDiscount(discounts, spareOptions);
        } else if (data.builderSettings.settings.product_settings.is_use_on_new_orders && data.builderSettings.settings.product_settings.is_keep_builder_on_order) {
            encourageDiscount(discounts, spareOptions);
        }

    }

    const encourageDiscount = (discounts, options) => {
        discounts.map(function (discount) {
            console.log(discount);
            console.log(options);
            if (discount.is_encourage_discount) {

                let totalItems = 0;
                options.items.map((item) => {
                    totalItems = totalItems + item.itemCount;
                })
                // If discount is item based
                if (discount.apply_discount == "count" && totalItems >= discount.encourage_discount_price) {
                    // Calculating total items
                    let discountString = discount.type == 'percent' ? `${discount.amount}% off.` : `$${discount.amount} off.`

                    let amount = discount.no_of_items_required - totalItems;
                    amount > 0 ? showDiscountPopup(`Add ${amount} more item to receive ${discountString}`, false) : ''

                } else if (discount.apply_discount == "price" && options.totalAmount >= (discount.encourage_discount_price)) {

                    let amount = (discount.price_required - options.totalAmount).toFixed(2);

                    let discountString = discount.type == 'percent' ? `${discount.amount}% off.` : `$${discount.amount} off.`
                    amount > 0 ? showDiscountPopup(`Spend $${amount} more to receive ${discountString}`, false) : ''
                    console.log("Encourage popup ", amount);
                } else {
                    hideDiscountPopup();
                }
            }
        })
    }

    // Calculate total amount for product
    const calculateAmount = function () {

        let spareAmount = 0;
        spareAmount = spareAmount + parseInt(data.builderSettings.start_price);

        if (options.items.length > 0) {
            options.items.forEach(item => {
                spareAmount += (item.price);
            });
        }

        manageDiscounts(spareAmount.toFixed(2));


    }

    // Showing addd cart toast.
    const showAddItemToast = () => {
        let addedItemToast = document.getElementsByClassName('add-item')[0];
        addedItemToast.classList.remove('bxp-show');

        addedItemToast.classList.add('bxp-show');
        setTimeout(() => {
            addedItemToast.classList.remove('bxp-show');
        }, 2000);
    }

    // Show minimum item toast : Method
    const showMaxItemToast = () => {
        let bxp_toast = document.getElementsByClassName('max-items')[0];
        bxp_toast.classList.remove('bxp-show-error');
        bxp_toast.classList.add('bxp-show-error');
        setTimeout(() => {
            bxp_toast.classList.remove('bxp-show-error');
        }, 3000);
    }

    // Show minimum item toast : Method
    const showDiscountPopup = (content, hide = true) => {
        let bb_discount_toast = document.getElementById('bb-discount-toast');
        bb_discount_toast.classList.remove('bxp-show');
        bb_discount_toast.innerHTML = content
        bb_discount_toast.classList.add('bxp-show');
        if (hide) {
            setTimeout(() => {
                hideDiscountPopup()
            }, 3000);
        }
    }

    const hideDiscountPopup = () => {
        let bb_discount_toast = document.getElementById('bb-discount-toast');
        bb_discount_toast.classList.remove('bxp-show');
    }

    // Modal actions.
    const imageAction = (id, action) => {
        if (action == 'open') {
            document.getElementsByClassName(`img-modal-${id}`)[0]?.classList.add('is-open');
        }
        if (action == 'close') {
            document.getElementsByClassName(`img-modal-${id}`)[0]?.classList.remove('is-open');
        }
    }

    const removeFromModal = (s_id) => {

        let itemarray = JSON.parse(JSON.stringify(options.items));
        const search = what => itemarray.find(element => element.id === what);
        imageAction(s_id, 'close')

        console.log(search(s_id));

        if (search(s_id)) {
            let spareOptions = Object.assign({}, options)
            spareOptions.step = search(s_id).step;
            spareOptions.items.splice(spareOptions.items.findIndex((el, i) => el.id === s_id), 1);
            // Editing state of options
            setOptions(spareOptions);
        }

        calculateAmount()
    }

    return (
        <>

            {
                loading == true
                    ?
                    <div className='animationDiv'>
                        <img width="150px" height="150px" src="https://builder.boxup.io/assets/image/9.gif" />
                    </div>
                    :
                    <>
                        {
                            data.is_active
                                ?
                                <>
                                    {
                                        data.builderSettings.settings.theme.builder == 'full'
                                        &&
                                        <>
                                            <MiddleWizard
                                                options={options}
                                                data={data}
                                                setOptions={setOptions}
                                                minItems={minItems}
                                                incrementStep={incrementStep}
                                                decrementStep={decrementStep}
                                                complatePurchase={complatePurchase}
                                                formData={formData}
                                                setFormData={setFormData}
                                                showSelectVariantError={showSelectVariantError}
                                                filters={filters}
                                                setFilters={setFilters}
                                                getFilteredVariants={getFilteredVariants}
                                                rememberSelection={rememberSelection}
                                                // Lower level Methods
                                                maxItems={maxItems}
                                                addProduct={addProduct}
                                                removeProduct={removeProduct}
                                                removeFromModal={removeFromModal}
                                                getTags={getTags}
                                            />
                                            <Alerts data={data} />
                                        </>
                                    }
                                    {
                                        data.builderSettings.settings.theme.builder == 'right'
                                        &&
                                        <>
                                            <MainWizard
                                                options={options}
                                                data={data}
                                                setOptions={setOptions}
                                                minItems={minItems}
                                                incrementStep={incrementStep}
                                                decrementStep={decrementStep}
                                                complatePurchase={complatePurchase}
                                                formData={formData}
                                                setFormData={setFormData}
                                                showSelectVariantError={showSelectVariantError}
                                                filters={filters}
                                                setFilters={setFilters}
                                                getFilteredVariants={getFilteredVariants}
                                                rememberSelection={rememberSelection}
                                                // Lower Level Methods
                                                maxItems={maxItems}
                                                addProduct={addProduct}
                                                removeProduct={removeProduct}
                                                removeFromModal={removeFromModal}
                                                getTags={getTags}
                                            />
                                            <Alerts data={data} />
                                        </>
                                    }
                                </>
                                :
                                ''
                        }
                    </>
            }

        </>
    )
}
