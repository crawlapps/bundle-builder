

import React from 'react';
import ReactDOM from 'react-dom';
import Index from './components/index.js';

export default function App(props) {

    return (
        <>
            <Index data={JSON.parse(props.data)} />
        </>
    );
}

if (document.getElementById('builder')) {
    const element = document.getElementById('builder')
    const props = Object.assign({}, element.dataset)

    ReactDOM.render(
        <App {...props} />,
        document.getElementById("builder")
    );
}
