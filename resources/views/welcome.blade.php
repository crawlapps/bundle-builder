@extends('shopify-app::layouts.default')

@section('content')
    <!-- You are: (shop domain name) -->
    <p>You are: {{ $shopDomain ?? Auth::user()->name }}</p>
    <div id="app">
    </div>
@endsection

@section('scripts')
    @parent
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- style custom css -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script>
        console.log('qqqqqqqqqqqqqqqqqqqqqqqqqqqq');
        window.shopify_app_bridge = app;
        console.log(app);
        actions.TitleBar.create(app, { title: 'Welcome' });
    </script>
@endsection
