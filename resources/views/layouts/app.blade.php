<!DOCTYPE HTML>
<html>

<head class="subscription-app">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Bundle builder') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- style custom css -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Polaris css cdn -->
    <link rel="stylesheet" href="https://unpkg.com/@shopify/polaris@6.0.1/dist/styles.css" />

    {{-- font-awesome css cdn--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    {{-- @if(config('shopify-app.appbridge_enabled')) --}}
    <script src="https://unpkg.com/@shopify/app-bridge@3.0.0"></script>
    <script>
        var AppBridge = window['app-bridge'];
        // console.log(AppBridge);
        var createApp = AppBridge.default;

        window.shopify_app_bridge = createApp({
            apiKey: '{{ config('shopify-app.api_key') }}',
            shopOrigin: '{{ Auth::user()->name }}',
            forceRedirect: true,
            host: '{{ session('shopifyHost') }}'
        });
        
        window.shopify_app_bridge_config = {
            apiKey: '{{ config('shopify-app.api_key') }}',
            shopOrigin: '{{ Auth::user()->name }}',
            forceRedirect: true,
            host: '{{ session('shopifyHost') }}'
        }

        window.shopOrigin = '{{ Auth::user()->name }}'

    </script>
    {{-- @endif --}}
</head>

<body class="">
    <div id="app">
    </div>
</body>

</html>