<!DOCTYPE html>
<html>

<head>
    <title>{{$data['title']}}</title>
    <meta name="description" content="{{$seo['description']}}">
    <meta name="keywords" content="{{$seo['keywords']}}">
    <!-- style custom css -->
</head>

<body>



    <script>
        {% capture currencyString %}@{{ shop.money_format }}{% endcapture %}
        {% assign tempArray = currencyString | split: '{' %}
        {% assign currencySymbol = tempArray.first %}
        window.store_currency = '@{{currencySymbol}}';

        let spareSteps = [];

        @foreach ($data['builderSteps'] as $stepIndex => $step)
            @php
                $step->selected_products = json_decode($step->selected_products);
                $step->collections = json_decode($step->collections);
                $step->collections_filter = json_decode($step->collections_filter);
            @endphp
            spareSteps.push(@json($step));
            
            @if ( $step->type == "product" ||  $step->type == "pre_selected" )
        
                @if ( $step->type == "product" && $step->selected_tab == 1)
                    console.log(@json($step));
                    
                    @foreach ($step->selected_products as $i => $prod)
                        {% assign liquid_product = all_products['{{ $prod["handle"] }}'] %}
                        
                        // Images || Tags
                        spareSteps[{{$stepIndex}}].selected_products[{{$i}}].image = @{{ liquid_product.images || json }};
                        spareSteps[{{$stepIndex}}].selected_products[{{$i}}].tags = @{{ liquid_product.tags || json }};
                        spareSteps[{{$stepIndex}}].selected_products[{{$i}}].description  = `@{{ liquid_product.content }}`;
                        
                        @foreach ($prod['variants'] as $vi => $var)
                        
                            {% for variant in liquid_product.variants %}

                                // console.log('Variant :: ','@{{variant.title}}');
                                if({{ $var['id'] }} == @{{variant.id}}){
                                    spareSteps[{{$stepIndex}}].selected_products[{{$i}}].variants[{{$vi}}].price = @{{variant.price | money_without_currency }}
                                    spareSteps[{{$stepIndex}}].selected_products[{{$i}}].variants[{{$vi}}].inventoryQuantity = @{{variant.inventory_quantity}}
                                    spareSteps[{{$stepIndex}}].selected_products[{{$i}}].variants[{{$vi}}].image = '@{{variant.image || json}}'
                                }

                            {% endfor %}

                        @endforeach

                    @endforeach

                @endif

                @if ( $step->type == "product" && $step->selected_tab == 0)
                    console.log(@json($step));
                    
                    @foreach ($step->collections as $i => $collection)

                        {% assign liquid_collection = collections['{{ $collection["handle"] }}'] %}
                        
                        spareSteps[{{$stepIndex}}].collections[{{$i}}] = @{{ liquid_collection || json}};
                        spareSteps[{{$stepIndex}}].collections[{{$i}}].products = [];
                        
                        {% for liquid_product in liquid_collection.products %}
                        
                            var spareVars = [];
                            {% for liquid_variant in liquid_product.variants %}

                                var variant = @{{ liquid_variant || json }};
                                spareVars.push({
                                    id : `@{{ liquid_variant.id }}`,
                                    sku : `@{{ liquid_variant.sku }}`,
                                    title: `@{{ liquid_variant.title }}`,
                                    displayName : variant.name,
                                    price: `@{{ liquid_variant.price | money_without_currency  }}`,
                                    inventoryQuantity : '@{{ liquid_variant.inventory_quantity }}',
                                    image : '@{{ liquid_variant.image || json }}'
                                });

                            {% endfor %}

                            spareSteps[{{$stepIndex}}].collections[{{$i}}].products.push({
                                id : `@{{ liquid_product.id }}`,
                                tags : @{{ liquid_product.tags || json }},
                                description : `@{{ liquid_product.content }}`,
                                handle : `@{{ liquid_product.handle }}`,
                                title : `@{{ liquid_product.title }}`,
                                image : @{{ liquid_product.images || json }},
                                variants : spareVars,
                            });

                        {% endfor %}

                    @endforeach

                @endif

                @if($step->is_show_collection_filter)
                    @foreach ($step->collections_filter as $i => $collection) 
                    
                        {% assign liquid_collection = collections['{{ $collection->handle }}'] %}

                        var productIds = [];
                        {% for liquid_product in liquid_collection.products %}
                            productIds.push('@{{ liquid_product.id }}');
                        {% endfor %}
                        
                        spareSteps[{{$stepIndex}}].collections_filter[{{$i}}] = {
                            id: @{{ liquid_collection.id }},
                            image : @{{ liquid_product.images || json }},
                            title : `@{{ liquid_collection.title }}`,
                            handle : `@{{ liquid_collection.handle }}`,
                            products : productIds
                        };

                    @endforeach
                @endif

            @endif


        @endforeach

        console.log('Spare Steps :: ',spareSteps);
        window.bundle_steps = spareSteps;
        

    </script>

    <div id="builder" data-data="{{json_encode($data)}}">
    </div>

    <script src="{{ asset('js/portal-app.js') }}"></script>
</body>

</html>