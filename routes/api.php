<?php

use App\Http\Controllers\Api\AssetController;
use App\Http\Controllers\Api\VariantController;
use App\Http\Controllers\Proxy\BuilderController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'builder', 'namespace' => 'Proxy'], function () {
    // Route::get('/fetch-builder/{id}', [BuilderController::class, 'fetchData']);
    Route::get('/fetch-builder/{id}', [BuilderController::class, 'fetchPreLoadedData']);
});

Route::group(['proxy.auth'], function () {
// Route::group([], function () {
    Route::get('/fetch-assets', [AssetController::class, 'fetchAssets']);
    Route::post('/make-variant', [VariantController::class, 'makeVariant']);
});