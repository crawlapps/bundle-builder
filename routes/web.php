<?php

use App\Http\Controllers\Api\VariantController;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Bundle\BundleController;
use App\Http\Controllers\Customization\CustomizationController;
use App\Http\Controllers\Proxy\BuilderController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', [AuthController::class, 'index'])->name('login');

Route::group(['middleware' => ['verify.shopify']], function () {

    Route::get('/', function () {
        return view('layouts.app');
    })->middleware(['check.host'])->name('home');

    // Bundle Routes
    Route::resource('bundle', BundleController::class);
    Route::prefix('bundle')->group(function () {
        Route::get('search/{keyword}', [BundleController::class, 'search'])->name('bundle.search');
        Route::delete('delete/{id}', [BundleController::class, 'delete'])->name('bundle.delete');
        Route::get('copy/{id}', [BundleController::class, 'copyBuilder'])->name('bundle.copyBuilder');
        Route::get('export/orders/{id}', [BundleController::class, 'exportOrders'])->name('export.orders');
    });

    // Customization Routes
    Route::prefix('customizations')->group(function () {
        Route::get('/', [CustomizationController::class, 'index'])->name('customizations');
        Route::post('/edit', [CustomizationController::class, 'edit'])->name('customizations.edit');
    });

});

// proxy
Route::group(['prefix' => 'builder', 'namespace' => 'Proxy', 'middleware' => ['proxy.auth']], function () {
    Route::get('/{slug?}', [BuilderController::class, 'index']);
});


//flush session
Route::get('flush', function () {
    request()->session()->flush();
});

Route::get('/test/{slug?}', [BuilderController::class, 'index']);
Route::get('/test-route', [VariantController::class, 'test']);
