<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Global constants
    |--------------------------------------------------------------------------
    */

    'PRODUCT_CUSTOM_TYPE'  => 'Box Builder',

    'PRODUCT_VENDOR'  => 'Box Builder',

    'PRODUCT_THEME_TEMPLATE' => 'boxbuilder',

    'SHOPIFY_GQL_RETURN_MESSAGE' => 'success',
];
?>
